package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.DATE;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.PHOTO_ARRAY;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, String>> map_list;

    public GalleryAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        HashMap<String, String> map = map_list.get(i);

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Get time from date
        SimpleDateFormat timeFormatter = new SimpleDateFormat("dd MMM yyyy");
        String displayValue = timeFormatter.format(date);
        viewHolder.date_tv.setText(displayValue);
        viewHolder.title_tv.setText(String.valueOf(map.get(TITLE)));
        //Log.e("count", String.valueOf(((Integer[]) map.get(PHOTO_ARRAY)).length));
        get_photo_list(map.get(PHOTO_ARRAY), viewHolder.photo_rv);


    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView photo_rv;
        private TextView title_tv;
        private TextView date_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo_rv = itemView.findViewById(R.id.photo_rv);
            title_tv = itemView.findViewById(R.id.title_tv);
            date_tv = itemView.findViewById(R.id.date_tv);

        }
    }

    private void get_photo_list(String pic_array, RecyclerView photo_rv) {
        try {
            JSONArray picArr = new JSONArray(pic_array);
            ArrayList<HashMap<String, String>> map_list = new ArrayList<HashMap<String, String>>();
            for (int i = 0; i < picArr.length(); i++) {
                String image = picArr.getJSONObject(i).getString("image");
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(IMG, image);
                map_list.add(map);
            }
            PhotoGalleryAdapter adapter = new PhotoGalleryAdapter(activity, map_list);
            photo_rv.setAdapter(adapter);
            photo_rv.setFocusable(false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
