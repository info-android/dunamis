package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.EventListing.CommentsActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

public class EventsDetailsAdapter extends RecyclerView.Adapter<EventsDetailsAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public EventsDetailsAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.events_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        HashMap<String, Object> map = map_list.get(i);
        Log.e("length", ""+map_list.size());

        viewHolder.add_tv.setText(activity.getResources().getString(R.string.location_txt));
        if (i%2==0){
            viewHolder.profile_iv.setImageResource(R.drawable.nation_worships);
            viewHolder.title_tv.setText(activity.getResources().getString(R.string.nation_worships));
        }else {
            viewHolder.profile_iv.setImageResource(R.drawable.days_fasting);
            viewHolder.title_tv.setText(activity.getResources().getString(R.string.glory_fast));
        }

        if (i==(map_list.size()-1)){
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, MethodClass.convert_into_sp_unit(activity,30));
            viewHolder.main_layout.setLayoutParams(layoutParams);
        }
        viewHolder.cmmnts_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, CommentsActivity.class));
            }
        });

        //viewHolder.profile_iv.setImageResource((Integer) map.get(IMG));
        //viewHolder.add_tv.setText(String.valueOf(map.get(ADDRESS)));

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView profile_iv;
        private TextView name_tv,add_tv,title_tv;
        private LinearLayout main_layout,cmmnts_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile_iv = itemView.findViewById(R.id.profile_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            add_tv = itemView.findViewById(R.id.add_tv);
            title_tv = itemView.findViewById(R.id.title_tv);
            main_layout = itemView.findViewById(R.id.main_layout);
            cmmnts_layout = itemView.findViewById(R.id.cmmnts_layout);

        }
    }
}
