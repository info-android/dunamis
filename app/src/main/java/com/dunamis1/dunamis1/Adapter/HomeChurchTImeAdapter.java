package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.dunamis1.dunamis1.Helper.Constant.DAYS;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.FROM_TIME;
import static com.dunamis1.dunamis1.Helper.Constant.TO_TIME;

public class HomeChurchTImeAdapter extends RecyclerView.Adapter<HomeChurchTImeAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public HomeChurchTImeAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.time_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        HashMap<String, Object> map = map_list.get(i);

        SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm:ss");
        Date date = null;
        Date date2 = null;
        try {
            date = dateFormatter.parse((String) map.get(FROM_TIME));
            date2 = dateFormatter.parse((String) map.get(TO_TIME));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Get time from date
        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
        String displayValue = timeFormatter.format(date);
        String displayValue2 = timeFormatter.format(date2);
        viewHolder.timeTv.setText((String)map.get(DAYS)+": "+displayValue+" to "+displayValue2);
        //viewHolder.add_tv.setText(String.valueOf(map.get(ADDRESS)));

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView timeTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            timeTv = itemView.findViewById(R.id.timeTv);
        }
    }
}
