package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.MainChurchDetailsActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class ChurchNearAdapter extends RecyclerView.Adapter<ChurchNearAdapter.OrderVh> {
    public ArrayList<HashMap<String, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public ChurchNearAdapter(Activity activity, ArrayList<HashMap<String, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dunamis_near_me_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, Object> map = list.get(position);

        Picasso.get().load(BRANCH_IMG_URL+map.get(IMGG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);
        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));
        holder.prdct_location_tv2.setText(String.valueOf(map.get(NAME)));
        holder.prdct_name_tv.setText(activity.getString(R.string.member)+": "+String.valueOf(map.get(MEMBER)));
        holder.prdct_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent I= new Intent(activity,MainChurchDetailsActivity.class);
                I.putExtra("id",String.valueOf(map.get(SEED_ID)));
                activity.startActivity(I);
            }
        });


    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        CircleImageView prdct_iv;
        TextView prdct_title_tv;
        TextView prdct_location_tv2;
        TextView prdct_name_tv;
        TextView prdct_view;


        public OrderVh(View v) {
            super(v);
            prdct_iv = (CircleImageView) v.findViewById(R.id.prdct_iv);
            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            prdct_location_tv2 = (TextView) v.findViewById(R.id.prdct_location_tv2);
            prdct_name_tv = (TextView) v.findViewById(R.id.prdct_name_tv);
            prdct_view = (TextView) v.findViewById(R.id.prdct_view);
        }
    }
}
