package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.ReApplyMembershipActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.DATE;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class NotiAdapter extends RecyclerView.Adapter<NotiAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public NotiAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.noti_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd h:m:s");
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(DATE));
            // Get time from date

            SimpleDateFormat format = new SimpleDateFormat("d");
            String datessa = format.format(date);

            if(datessa.endsWith("1") && !datessa.endsWith("11"))
                format = new SimpleDateFormat("d'st' MMM, yyyy");
            else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                format = new SimpleDateFormat("d'nd' MMM, yyyy");
            else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                format = new SimpleDateFormat("d'rd' MMM, yyyy");
            else
                format = new SimpleDateFormat("d'th' MMM, yyyy");

            String yourDate = format.format(date);

            holder.prdct_view.setText(yourDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.prdct_mem_tv3.setText((String)map.get(TITLE));
        holder.pro_det_tv.setText((String)map.get(DESC));


        if(map.get(TITLE).equals("Profile Rejected")){
           holder.main_layout.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   MethodClass.go_to_next_activity(activity, ReApplyMembershipActivity.class);
               }
           });
        }

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        private TextView prdct_view,prdct_mem_tv3,pro_det_tv;
        private LinearLayout main_layout;
        public OrderVh(View v) {
            super(v);

            prdct_view = v.findViewById(R.id.prdct_view);
            prdct_mem_tv3 = v.findViewById(R.id.prdct_mem_tv3);
            pro_det_tv = v.findViewById(R.id.pro_det_tv);
            main_layout = v.findViewById(R.id.main_layout);
        }
    }
}
