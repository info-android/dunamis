package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.DunamisSchoolDetailsActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.DATE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.SCHOOL_BANNER_IMG;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_DESC;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_ID;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_TYPE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class DunamisSchoolAdapter extends RecyclerView.Adapter<DunamisSchoolAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public DunamisSchoolAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dunmais_school_list, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);

        Picasso.get().load(SCHOOL_BANNER_IMG+(String.valueOf(map.get(SCH_THUMB)))).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);

        holder.prdct_title_tv.setText(String.valueOf(map.get(SCH_TITLE)));
        holder.desc_tv.setText(String.valueOf(map.get(SCH_DESC)));
        holder.prdct_name_tv.setText(String.valueOf(map.get(SCH_TYPE)));

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(DATE));
            // Get time from date

            SimpleDateFormat format = new SimpleDateFormat("d");
            String datessa = format.format(date);

            if(datessa.endsWith("1") && !datessa.endsWith("11"))
                format = new SimpleDateFormat("d'st' MMM, yyyy");
            else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                format = new SimpleDateFormat("d'nd' MMM, yyyy");
            else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                format = new SimpleDateFormat("d'rd' MMM, yyyy");
            else
                format = new SimpleDateFormat("d'th' MMM, yyyy");

            String yourDate = format.format(date);

            holder.prdct_date.setText(yourDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.prdct_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = null; // missing 'http://' will cause crashed
                uri = Uri.parse(String.valueOf(map.get(SCH_CODE)));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);
            }
        });
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity,DunamisSchoolDetailsActivity.class);
                I.putExtra("school_id",String.valueOf(map.get(SCH_ID)));
                activity.startActivity(I);
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView prdct_iv;
        TextView prdct_title_tv;
        TextView prdct_date;
        TextView prdct_view;
        TextView prdct_name_tv;
        TextView desc_tv;
        LinearLayout container;

        public OrderVh(View v) {
            super(v);
            prdct_iv =  v.findViewById(R.id.prdct_iv);
            prdct_title_tv =  v.findViewById(R.id.prdct_title_tv);
            prdct_date =  v.findViewById(R.id.prdct_date);
            prdct_view =  v.findViewById(R.id.prdct_view);
            prdct_name_tv =  v.findViewById(R.id.prdct_name_tv);
            desc_tv =  v.findViewById(R.id.desc_tv);
            container =  v.findViewById(R.id.container);
        }
    }
}
