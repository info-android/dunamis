package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.ADDRESS;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class ChurchOneAdapter extends RecyclerView.Adapter<ChurchOneAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public ChurchOneAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.church_detail_one, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);

        holder.prdct_iv.setImageResource((int) map.get(IMAGE));
        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));
        holder.prdct_name_tv.setText(String.valueOf(map.get(ADDRESS)));


    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        CircleImageView prdct_iv;
        TextView prdct_title_tv;
        TextView prdct_name_tv;


        public OrderVh(View v) {
            super(v);
            prdct_iv = (CircleImageView) v.findViewById(R.id.prdct_iv);
            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            prdct_name_tv = (TextView) v.findViewById(R.id.prdct_name_tv);
        }
    }
}
