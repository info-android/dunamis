package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.HomeChurchDetailsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ManageHomeChurchMembersActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.UpdateChurchDetailsActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.AGE;
import static com.dunamis1.dunamis1.Helper.Constant.APPROVED;
import static com.dunamis1.dunamis1.Helper.Constant.COUNTRY_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.EMAIL;
import static com.dunamis1.dunamis1.Helper.Constant.FNAME;
import static com.dunamis1.dunamis1.Helper.Constant.GENDER;
import static com.dunamis1.dunamis1.Helper.Constant.ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.LNAME;
import static com.dunamis1.dunamis1.Helper.Constant.MARITAL;
import static com.dunamis1.dunamis1.Helper.Constant.PHONE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class ManageHomeChurchMemberAdapter extends RecyclerView.Adapter<ManageHomeChurchMemberAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;
    private String gender,marital;

    public ManageHomeChurchMemberAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_home_church_members_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final HashMap<String, Object> map = map_list.get(i);

        ViewTreeObserver vto = viewHolder.profile_iv.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                viewHolder.profile_iv.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = viewHolder.profile_iv.getMeasuredHeight();
                int finalWidth = viewHolder.profile_iv.getMeasuredWidth();
                ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) viewHolder.profile_iv.getLayoutParams();
                layoutParams.width = finalHeight;
                viewHolder.profile_iv.setLayoutParams(layoutParams);
                return true;
            }
        });
        Picasso.get().load(PROFILE_IMG_URL+String.valueOf(map.get(IMGG))).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(viewHolder.profile_iv);

        viewHolder.name_tv.setText(String.valueOf(map.get(TITLE))+" "+String.valueOf(map.get(FNAME))+" "+String.valueOf(map.get(LNAME)));
        if(String.valueOf(map.get(GENDER)).equals("M")){
            gender = "Male";
        }else if(String.valueOf(map.get(GENDER)).equals("F")){
            gender = "Female";
        }else {
            gender= "Other";
        }
        if(String.valueOf(map.get(MARITAL)).equals("M")){
            marital = "Married";
        }else if(String.valueOf(map.get(MARITAL)).equals("S")){
            marital = "Single";
        }
        viewHolder.about_tv.setText(gender+", "+String.valueOf(map.get(AGE))+" Years, "+marital);

        if(!String.valueOf(map.get(COUNTRY_CODE)).equals("null") && !String.valueOf(map.get(COUNTRY_CODE)).equals(null)){
            viewHolder.ph_no_tv.setText(String.valueOf(map.get(COUNTRY_CODE))+" "+String.valueOf(map.get(PHONE)));
        }else {
            viewHolder.ph_no_tv.setText(String.valueOf(map.get(PHONE)));
        }
        viewHolder.email_tv.setText(String.valueOf(map.get(EMAIL)));


        if (i==(map_list.size()-1)){
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, MethodClass.convert_into_sp_unit(activity,30));
            viewHolder.main_layout.setLayoutParams(layoutParams);
        }
        if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Member")){
           viewHolder.menu_iv.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Guest")){
            viewHolder.menu_iv.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Home")){
            viewHolder.menu_iv.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Department")){
            viewHolder.menu_iv.setVisibility(View.VISIBLE);
        }
        viewHolder.prdct_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity, MessageChatActivity.class);
                intent.putExtra("rec_id",(String)map.get(ID));
                activity.startActivity(intent);
            }
        });

        viewHolder.menu_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(activity, R.style.pop_up_menu_style);
                PopupMenu popupMenu = new PopupMenu(wrapper, viewHolder.menu_iv, Gravity.BOTTOM | Gravity.RIGHT);
                MenuInflater inflater = popupMenu.getMenuInflater();
                inflater.inflate(R.menu.popup_menu, popupMenu.getMenu());
                popupMenu.getMenu().add(0, 0, 0, "View");
                if(map.get(APPROVED).equals("I") || map.get(APPROVED).equals("N")){
                    popupMenu.getMenu().add(0, 1, 0, "Approve");
                    popupMenu.getMenu().add(0, 2, 0, "Reject");
                }

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch(menuItem.getItemId()) {
                            case 0:
                                return true;
                            case 1:
                                approveMember((String)map.get(SEED_ID));
                                return true;
                            case 2:
                                rejectMember((String)map.get(SEED_ID));
                                return true;
                            default:
                                return onMenuItemClick(menuItem);
                        }
                    }
                });
                popupMenu.show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView profile_iv,menu_iv;
        private TextView name_tv,about_tv;
        private TextView email_tv;
        private TextView ph_no_tv;
        private TextView prdct_view;
        private LinearLayout main_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile_iv = itemView.findViewById(R.id.profile_iv);
            menu_iv = itemView.findViewById(R.id.menu_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            about_tv = itemView.findViewById(R.id.about_tv);
            email_tv = itemView.findViewById(R.id.email_tv);
            ph_no_tv = itemView.findViewById(R.id.ph_no_tv);
            prdct_view = itemView.findViewById(R.id.prdct_view);
            main_layout = itemView.findViewById(R.id.main_layout);

        }
    }

    public void approveMember(String mem_id){
        MethodClass.showProgressDialog(activity);
        String server_url = "";
        HashMap<String, String> params = new HashMap<String, String>();
        if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Home")){
            server_url = activity.getString(R.string.SERVER_URL) + "home-church/member/update";

            params.put("language", PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
            params.put("member_id",mem_id);
            params.put("type","A");
        }else {
            server_url = activity.getString(R.string.SERVER_URL) + "church-department/member/update";
            params.put("language", PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
            params.put("member_id",mem_id);
            params.put("type","A");
        }



        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(activity);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                    if (result_Object != null) {

                        String message = result_Object.getString("message");
                        String meaning = result_Object.getString("meaning");
                        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message)
                                .setContentText(meaning)
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        ((ManageHomeChurchMembersActivity)activity).get_members_list();
                                    }
                                })
                                .show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(activity);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }

    public void rejectMember(String mem_id ){
        MethodClass.showProgressDialog(activity);
        String server_url = "";
        HashMap<String, String> params = new HashMap<String, String>();
        if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Home")){
            server_url = activity.getString(R.string.SERVER_URL) + "home-church/member/update";
            params.put("language", PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
            params.put("member_id",mem_id);
            params.put("type","R");
        }else {
            server_url = activity.getString(R.string.SERVER_URL) + "church-department/member/update";
            params.put("language", PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
            params.put("member_id",mem_id);
            params.put("type","R");
        }




        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(activity);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                    if (result_Object != null) {

                        String message = result_Object.getString("message");
                        String meaning = result_Object.getString("meaning");
                        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message)
                                .setContentText(meaning)
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        ((ManageHomeChurchMembersActivity)activity).get_members_list();
                                    }
                                })
                                .show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(activity);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(activity);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }
}
