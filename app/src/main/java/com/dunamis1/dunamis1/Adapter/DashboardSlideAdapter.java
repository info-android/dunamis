package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.app.Dialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;

public class DashboardSlideAdapter  extends RecyclerView.Adapter<DashboardSlideAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public DashboardSlideAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.slider_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);


        if(position == 1){
            holder.photo_iv.setVisibility(View.GONE);
            holder.textIv.setVisibility(View.VISIBLE);
            holder.textIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog =new Dialog(activity);
                    dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.setContentView(R.layout.photo_popup_view2);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    ImageView close_iv =(ImageView)dialog.findViewById(R.id.close_iv);
                    final ImageView photo_iv =(ImageView)dialog.findViewById(R.id.photo_iv);
                    final TextView text_iv =(TextView)dialog.findViewById(R.id.textIv);
                    final RelativeLayout photoLayout =(RelativeLayout)dialog.findViewById(R.id.photoLayout);
                    final YouTubePlayerView youtube_player_view =(YouTubePlayerView)dialog.findViewById(R.id.youtube_player_view);

                    photoLayout.setVisibility(View.GONE);
                    youtube_player_view.setVisibility(View.GONE);
                    text_iv.setVisibility(View.VISIBLE);
                    close_iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            youtube_player_view.release();
                        }
                    });
                    dialog.show();
                }
            });
        }else {
            holder.photo_iv.setVisibility(View.VISIBLE);
            Picasso.get().load((int) map.get(IMAGE)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,720).into(holder.photo_iv);
            holder.textIv.setVisibility(View.GONE);
            holder.photo_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog =new Dialog(activity);
                    dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.setContentView(R.layout.photo_popup_view2);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    ImageView close_iv =(ImageView)dialog.findViewById(R.id.close_iv);
                    final ImageView photo_iv =(ImageView)dialog.findViewById(R.id.photo_iv);
                    final TextView text_iv =(TextView)dialog.findViewById(R.id.textIv);
                    final RelativeLayout photoLayout =(RelativeLayout)dialog.findViewById(R.id.photoLayout);
                    final YouTubePlayerView youtube_player_view =(YouTubePlayerView)dialog.findViewById(R.id.youtube_player_view);

                    if(position == 0){
                        photoLayout.setVisibility(View.GONE);
                        text_iv.setVisibility(View.GONE);
                        youtube_player_view.setVisibility(View.VISIBLE);
                        youtube_player_view.initialize(new YouTubePlayerInitListener() {
                            @Override
                            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                    @Override
                                    public void onReady() {
                                        String videoId = "Lnm74SE9rlY";
                                        initializedYouTubePlayer.loadVideo(videoId, 0);
                                    }
                                });
                            }
                        }, true);
                    }else {
                        photo_iv.setImageResource((int) map.get(IMAGE));
                        photoLayout.setVisibility(View.VISIBLE);
                        youtube_player_view.setVisibility(View.GONE);
                        text_iv.setVisibility(View.GONE);
                    }
                    close_iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            youtube_player_view.release();
                        }
                    });
                    dialog.show();
                }
            });
        }

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView photo_iv;
        TextView textIv;
        public OrderVh(View v) {
            super(v);

            photo_iv = (ImageView) v.findViewById(R.id.photo_iv);
            textIv = (TextView) v.findViewById(R.id.textIv);

        }
    }
}
