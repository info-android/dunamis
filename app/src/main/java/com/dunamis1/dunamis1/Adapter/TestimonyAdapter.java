package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.DATE;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TYPE;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.TESTIMONY_USER_IMG;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.VID_IMG;

public class TestimonyAdapter extends RecyclerView.Adapter<TestimonyAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public TestimonyAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.testimony_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);
        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));
        holder.name_tv.setText(String.valueOf(map.get(NAME)));

        if(String.valueOf(map.get(MUSIC_TYPE)).equals("V")){
            holder.prdct_name_tv.setVisibility(View.GONE);
            holder.imglayout.setVisibility(View.VISIBLE);
        }else {
            holder.prdct_name_tv.setVisibility(View.VISIBLE);
            holder.imglayout.setVisibility(View.GONE);
        }
        Picasso.get().load(TESTIMONY_USER_IMG+(String)map.get(IMAGE)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);
        String url = "https://img.youtube.com/vi/"+(String) map.get(VID_IMG)+"/mqdefault.jpg";
        Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.image_tv);

        holder.prdct_name_tv.setText((String)map.get(DESC));
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(DATE));
            // Get time from date

            SimpleDateFormat format = new SimpleDateFormat("d");
            String datessa = format.format(date);

            if(datessa.endsWith("1") && !datessa.endsWith("11"))
                format = new SimpleDateFormat("d'st' MMM, yyyy");
            else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                format = new SimpleDateFormat("d'nd' MMM, yyyy");
            else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                format = new SimpleDateFormat("d'rd' MMM, yyyy");
            else
                format = new SimpleDateFormat("d'th' MMM, yyyy");

            String yourDate = format.format(date);

            holder.date_tv.setText(yourDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.imglayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imglayout.setVisibility(View.GONE);
                holder.youtube_player_view.setVisibility(View.VISIBLE);
                holder.youtube_player_view.initialize(new YouTubePlayerInitListener() {
                    @Override
                    public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                        initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {
                                String videoId = (String)map.get(VID_IMG);
                                initializedYouTubePlayer.loadVideo(videoId, 0);
                            }
                        });
                    }
                }, true);
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        CircleImageView prdct_iv;
        ImageView image_tv;
        TextView name_tv;
        TextView date_tv;
        TextView prdct_title_tv;
        TextView prdct_name_tv;
        LinearLayout container;
        RelativeLayout imglayout;

        private YouTubePlayerView youtube_player_view;
        public OrderVh(View v) {
            super(v);
            prdct_iv =  v.findViewById(R.id.prdct_iv);
            prdct_title_tv =  v.findViewById(R.id.prdct_title_tv);
            image_tv =  v.findViewById(R.id.image_tv);
            name_tv =  v.findViewById(R.id.name_tv);
            date_tv =  v.findViewById(R.id.date_tv);
            prdct_name_tv =  v.findViewById(R.id.prdct_name_tv);
            imglayout =  v.findViewById(R.id.imglayout);
            youtube_player_view =  v.findViewById(R.id.youtube_player_view);

        }
    }
}
