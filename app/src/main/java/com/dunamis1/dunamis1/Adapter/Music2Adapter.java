package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.EventListing.CommentsActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicDetailsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_DESC;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_ID;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TYPE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class Music2Adapter extends RecyclerView.Adapter<Music2Adapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public Music2Adapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.fav_music_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);

        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");
        if(map.get(MUSIC_TYPE).equals("F")){
            Picasso.get().load(MUSIC_IMG_URL+map.get(MUSIC_THUMB)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);
        }else{
            String url = "https://img.youtube.com/vi/"+map.get(MUSIC_CODE)+"/mqdefault.jpg";
            Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);
        }

        holder.prdct_title_tv.setText(String.valueOf(map.get(MUSIC_TITLE)));
        holder.prdct_mem_tv3.setText(String.valueOf(map.get(MUSIC_DESC)));
        holder.totLike.setText((String)map.get(MUSIC_LIKE)+" "+activity.getString(R.string.likes));
        holder.totComment.setText((String)map.get(MUSIC_COMMENT)+" "+activity.getString(R.string.comm));
        holder.totShare.setText((String)map.get(MUSIC_SHARE)+" "+activity.getString(R.string.shar));
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.StopAudio(activity);
                Intent I = new Intent(activity, MusicDetailsActivity.class);
                I.putExtra("title",String.valueOf(map.get(MUSIC_TITLE)));
                I.putExtra("id",String.valueOf(map.get(MUSIC_ID)));
                activity.startActivity(I);
            }
        });
        holder.totShareCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "http://phpwebdevelopmentservices.com/development/dunamis/api/share?content="+map.get(MUSIC_ID)+"&language="+PreferenceManager.getDefaultSharedPreferences(activity).getString("lang","en")+"&type=M");
                sendIntent.setType("text/plain");
                activity.startActivity(sendIntent);
            }
        });
        holder.totComCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, CommentsActivity.class);
                I.putExtra("from","music");
                I.putExtra("id",map.get(MUSIC_ID));
                activity.startActivity(I);
            }
        });
        holder.totLikeCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = activity.getString(R.string.SERVER_URL) + "music/like";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("music_id", map.get(MUSIC_ID));
                params.put("language",PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String total_likes = result_Object.getString("total_likes");
                                holder.totLike.setText(total_likes+" "+activity.getString(R.string.likes));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
            }
        });
        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = activity.getString(R.string.SERVER_URL) + "favourite";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", map.get(MUSIC_ID));
                params.put("type", "M");
                params.put("language",PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String favourite = result_Object.getString("favourite");
                                String message = result_Object.getString("message");
                                String meaning = result_Object.getString("meaning");
                                ((MyFavouriteActivity)activity).step3part();
                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(message)
                                        .setContentText(meaning)
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
            }
        });
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView prdct_iv;
        ImageView close;
        TextView prdct_mem_tv3;
        TextView prdct_title_tv;
        LinearLayout container;
        TextView totLike;
        TextView totComment;
        TextView totShare;
        LinearLayout totLikeCon;
        LinearLayout totComCon;
        LinearLayout totShareCon;
        public OrderVh(View v) {
            super(v);
            prdct_iv =  v.findViewById(R.id.prdct_iv);
            close =  v.findViewById(R.id.close);
            prdct_title_tv =  v.findViewById(R.id.prdct_title_tv);
            prdct_mem_tv3 =  v.findViewById(R.id.prdct_mem_tv3);
            container =  v.findViewById(R.id.container);
            totLike =  v.findViewById(R.id.totLike);
            totComment =  v.findViewById(R.id.totCom);
            totShare =  v.findViewById(R.id.totShare);
            totLikeCon =  v.findViewById(R.id.totLikeCon);
            totComCon =  v.findViewById(R.id.totComCon);
            totShareCon =  v.findViewById(R.id.totShareCon);
        }
    }
}
