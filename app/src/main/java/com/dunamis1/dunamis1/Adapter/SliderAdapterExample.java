package com.dunamis1.dunamis1.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dunamis1.dunamis1.Helper.SliderItem;
import com.dunamis1.dunamis1.R;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;

public class SliderAdapterExample extends
        SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {

    private Context context;
    private List<SliderItem> mSliderItems = new ArrayList<>();

    public SliderAdapterExample(Context context, List<SliderItem> sliderItems) {
        this.context = context;
        this.mSliderItems = sliderItems;
    }

    public void renewItems(List<SliderItem> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mSliderItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(SliderItem sliderItem) {
        this.mSliderItems.add(sliderItem);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_layout, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        final SliderItem sliderItem = mSliderItems.get(position);

        if(position == 1){
            viewHolder.photo_iv.setVisibility(View.GONE);
            viewHolder.textIv.setVisibility(View.VISIBLE);
            viewHolder.textIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog =new Dialog(context);
                    dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.setContentView(R.layout.photo_popup_view2);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    ImageView close_iv =(ImageView)dialog.findViewById(R.id.close_iv);
                    final ImageView photo_iv =(ImageView)dialog.findViewById(R.id.photo_iv);
                    final TextView text_iv =(TextView)dialog.findViewById(R.id.textIv);
                    final RelativeLayout photoLayout =(RelativeLayout)dialog.findViewById(R.id.photoLayout);
                    final YouTubePlayerView youtube_player_view =(YouTubePlayerView)dialog.findViewById(R.id.youtube_player_view);

                    photoLayout.setVisibility(View.GONE);
                    youtube_player_view.setVisibility(View.GONE);
                    text_iv.setVisibility(View.VISIBLE);
                    close_iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            youtube_player_view.release();
                        }
                    });
                    dialog.show();
                }
            });
        }else {
            viewHolder.photo_iv.setVisibility(View.VISIBLE);
            Picasso.get().load(sliderItem.getImageUrl()).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,720).into(viewHolder.photo_iv);
            viewHolder.textIv.setVisibility(View.GONE);
            viewHolder.photo_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog =new Dialog(context);
                    dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.setContentView(R.layout.photo_popup_view2);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    ImageView close_iv =(ImageView)dialog.findViewById(R.id.close_iv);
                    final ImageView photo_iv =(ImageView)dialog.findViewById(R.id.photo_iv);
                    final TextView text_iv =(TextView)dialog.findViewById(R.id.textIv);
                    final RelativeLayout photoLayout =(RelativeLayout)dialog.findViewById(R.id.photoLayout);
                    final YouTubePlayerView youtube_player_view =(YouTubePlayerView)dialog.findViewById(R.id.youtube_player_view);

                    if(position == 0){
                        photoLayout.setVisibility(View.GONE);
                        text_iv.setVisibility(View.GONE);
                        youtube_player_view.setVisibility(View.VISIBLE);
                        youtube_player_view.initialize(new YouTubePlayerInitListener() {
                            @Override
                            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                    @Override
                                    public void onReady() {
                                        String videoId = "Lnm74SE9rlY";
                                        initializedYouTubePlayer.loadVideo(videoId, 0);
                                    }
                                });
                            }
                        }, true);
                    }else {
                        Picasso.get().load(sliderItem.getImageUrl()).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,720).into(photo_iv);
                        photoLayout.setVisibility(View.VISIBLE);
                        youtube_player_view.setVisibility(View.GONE);
                        text_iv.setVisibility(View.GONE);
                    }
                    close_iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            youtube_player_view.release();
                        }
                    });
                    dialog.show();
                }
            });
        }
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return mSliderItems.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;

        ImageView photo_iv;
        TextView textIv;

        public SliderAdapterVH(View itemView) {
            super(itemView);

            photo_iv = (ImageView) itemView.findViewById(R.id.photo_iv);
            textIv = (TextView) itemView.findViewById(R.id.textIv);
            this.itemView = itemView;
        }
    }

}
