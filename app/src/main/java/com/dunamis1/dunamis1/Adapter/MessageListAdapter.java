package com.dunamis1.dunamis1.Adapter;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_USER;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_REC_ID;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ViewHolder> {
    AppCompatActivity activity;
    ArrayList<HashMap<String, String>> arrayList;

    public MessageListAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.message_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final HashMap<String, String> map = arrayList.get(i);
        viewHolder.userName.setText(map.get(COMM_USER));
        viewHolder.userMesg.setText(map.get(COMMENT));

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd h:m:s");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(COMM_DATE));
            // Get time from date

            SimpleDateFormat format = new SimpleDateFormat("d");
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            format.setTimeZone(tz);
            String datessa = format.format(date);

            if(datessa.endsWith("1") && !datessa.endsWith("11"))
                format = new SimpleDateFormat("d'st' MMM, yyyy");
            else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                format = new SimpleDateFormat("d'nd' MMM, yyyy");
            else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                format = new SimpleDateFormat("d'rd' MMM, yyyy");
            else
                format = new SimpleDateFormat("d'th' MMM, yyyy");

            String yourDate = format.format(date);


            viewHolder.userTime.setText(yourDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.get().load(PROFILE_IMG_URL+map.get(COMM_PIC)).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(viewHolder.userImage);
        viewHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(activity, MessageChatActivity.class);
                intent.putExtra("rec_id",(String)map.get(MSG_REC_ID));
                activity.startActivity(intent);
            }
        });


        if (arrayList.size() == i + 1) {
            LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(MethodClass.convert_into_sp_unit(activity,15), MethodClass.convert_into_sp_unit(activity,7), MethodClass.convert_into_sp_unit(activity,15), MethodClass.convert_into_sp_unit(activity,20));
            viewHolder.itemLayout.setLayoutParams(layoutParams);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout itemLayout;
        private CircleImageView  userImage;
        private TextView  userName;
        private TextView  userMesg;
        private TextView  userTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.itemLayout);
            userImage = itemView.findViewById(R.id.userImage);
            userName = itemView.findViewById(R.id.userName);
            userMesg = itemView.findViewById(R.id.userMesg);
            userTime = itemView.findViewById(R.id.userTime);
        }
    }
}
