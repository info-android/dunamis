package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_USER;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public CommentsAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.commnts_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        HashMap<String, Object> map = map_list.get(i);

        Picasso.get().load(PROFILE_IMG_URL_THUMB+map.get(COMM_PIC)).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(viewHolder.profile_iv);
        viewHolder.name_tv.setText(String.valueOf(map.get(COMM_USER)));
        viewHolder.date_tv.setText(String.valueOf(map.get(COMM_DATE)));
        viewHolder.comm.setText(String.valueOf(map.get(COMMENT)));

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView profile_iv;
        private TextView name_tv;
        private TextView date_tv;
        private TextView comm;
        private LinearLayout main_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile_iv = itemView.findViewById(R.id.profile_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            comm = itemView.findViewById(R.id.comm);
            main_layout = itemView.findViewById(R.id.main_layout);

        }
    }
}
