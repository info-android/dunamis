package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public NoticeAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.notica_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd h:m:s");
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(SEED_DATE));
            // Get time from date

            SimpleDateFormat format = new SimpleDateFormat("d");
            String datessa = format.format(date);

            if(datessa.endsWith("1") && !datessa.endsWith("11"))
                format = new SimpleDateFormat("d'st' MMM, yyyy");
            else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                format = new SimpleDateFormat("d'nd' MMM, yyyy");
            else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                format = new SimpleDateFormat("d'rd' MMM, yyyy");
            else
                format = new SimpleDateFormat("d'th' MMM, yyyy");

            String yourDate = format.format(date);

            holder.prdct_view.setText(yourDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.prdct_mem_tv3.setText((String) map.get(SEED_TITLE));
        holder.prdct_desc.setText((String) map.get(DESC));
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        TextView prdct_view;
        TextView prdct_mem_tv3;
        TextView prdct_desc;

        public OrderVh(View v) {
            super(v);
            prdct_view = v.findViewById(R.id.prdct_view);
            prdct_mem_tv3 = v.findViewById(R.id.prdct_mem_tv3);
            prdct_desc = v.findViewById(R.id.prdct_desc);
        }
    }
}
