package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.Bible.BibleDeuteronomyActivity;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.ABBR;
import static com.dunamis1.dunamis1.Helper.Constant.BIBLE_ABBR;
import static com.dunamis1.dunamis1.Helper.Constant.BIBLE_ID;
import static com.dunamis1.dunamis1.Helper.Constant.BIBLE_NAME;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class BibleAdapter extends RecyclerView.Adapter<BibleAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, String>> map_list;

    public BibleAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bible_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final HashMap<String, String> map = map_list.get(i);
        viewHolder.title_tv.setText(map.get(BIBLE_ABBR));
        viewHolder.desc_tv.setText(map.get(BIBLE_NAME));
        if (i == (map_list.size() - 1)) {
            viewHolder.dash_tv.setText("");
        }
        else {
            viewHolder.dash_tv.setText(activity.getResources().getString(R.string.dash_line));
        }
        viewHolder.main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ABBR = map.get(BIBLE_ABBR);
                Intent I = new Intent(activity, BibleDeuteronomyActivity.class);
                I.putExtra("id",map.get(BIBLE_ID));
                activity.startActivity(I);
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout main_layout;
        private TextView title_tv, desc_tv, dash_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            main_layout = itemView.findViewById(R.id.main_layout);
            title_tv = itemView.findViewById(R.id.title_tv);
            desc_tv = itemView.findViewById(R.id.desc_tv);
            dash_tv = itemView.findViewById(R.id.dash_tv);
        }
    }
}
