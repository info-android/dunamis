package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.DEPARTMENT_THUMB_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_CHURCH_THUMB_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;

public class ChurchPhotoGalleryAdapter extends RecyclerView.Adapter<ChurchPhotoGalleryAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public ChurchPhotoGalleryAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.church_photo_gallery_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final HashMap<String, Object> map = map_list.get(i);

        ViewTreeObserver vto = viewHolder.photo_iv.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                viewHolder.photo_iv.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = viewHolder.photo_iv.getMeasuredHeight();
                int finalWidth = viewHolder.photo_iv.getMeasuredWidth();
                ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) viewHolder.photo_iv.getLayoutParams();
                layoutParams.height = finalWidth;
                viewHolder.photo_iv.setLayoutParams(layoutParams);
                return true;
            }
        });


        if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Home")){
            Picasso.get().load(HOME_CHURCH_THUMB_IMG_URL+map.get(IMGG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(viewHolder.photo_iv);
        }else if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Department")){
            Picasso.get().load(DEPARTMENT_THUMB_IMG_URL+map.get(IMGG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(viewHolder.photo_iv);
        }

        viewHolder.close_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(activity.getString(R.string.remove))
                        .setContentText(activity.getString(R.string.romevemsg))
                        .setConfirmText(activity.getString(R.string.yes))
                        .setCancelText(activity.getString(R.string.no))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Home")){
                                    String server_url = activity.getString(R.string.SERVER_URL) + "gallery-photo/delete";
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", (String) map.get(SEED_ID));
                                    params.put("type", "H");
                                    params.put("language",PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
                                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.e("resp", response.toString());
                                            try{
                                                JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                                if (result_Object != null) {
                                                    String message = result_Object.getString("message");
                                                    String meaning = result_Object.getString("meaning");
                                                    ((ChurchPhotoGalleryActivity)activity).get_gallery_list();
                                                    new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText(message)
                                                            .setContentText(meaning)
                                                            .setConfirmText("Okay")
                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();
                                                                }
                                                            })
                                                            .show();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Log.e("error", e.getMessage());
                                            }


                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            Log.e("error", error.toString());
                                            if (error.toString().contains("AuthFailureError")) {
                                                Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }) {
                                        //* Passing some request headers*
                                        @Override
                                        public Map getHeaders() throws AuthFailureError {
                                            HashMap headers = new HashMap();
                                            headers.put("Content-Type", "application/json");
                                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                            Log.e("getHeaders: ", headers.toString());

                                            return headers;
                                        }
                                    };

                                    MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                                }else if(PreferenceManager.getDefaultSharedPreferences(activity).getString("role","Member").equals("Department")){
                                    String server_url = activity.getString(R.string.SERVER_URL) + "gallery-photo/delete";
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", (String) map.get(SEED_ID));
                                    params.put("type", "D");
                                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.e("resp", response.toString());
                                            try{
                                                JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                                if (result_Object != null) {
                                                    String message = result_Object.getString("message");
                                                    String meaning = result_Object.getString("meaning");
                                                    ((ChurchPhotoGalleryActivity)activity).get_gallery_list();
                                                    new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText(message)
                                                            .setContentText(meaning)
                                                            .setConfirmText("Okay")
                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();
                                                                }
                                                            })
                                                            .show();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Log.e("error", e.getMessage());
                                            }


                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            Log.e("error", error.toString());
                                            if (error.toString().contains("AuthFailureError")) {
                                                Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }) {
                                        //* Passing some request headers*
                                        @Override
                                        public Map getHeaders() throws AuthFailureError {
                                            HashMap headers = new HashMap();
                                            headers.put("Content-Type", "application/json");
                                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                            Log.e("getHeaders: ", headers.toString());

                                            return headers;
                                        }
                                    };

                                    MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                                }
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView photo_iv;
        private ImageView close_iv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo_iv = itemView.findViewById(R.id.photo_iv);
            close_iv = itemView.findViewById(R.id.close_iv);

        }
    }
}
