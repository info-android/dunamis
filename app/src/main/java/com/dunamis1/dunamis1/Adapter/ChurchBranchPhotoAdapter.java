package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.recyclerview.widget.RecyclerView;

import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_PHOTO_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;

public class ChurchBranchPhotoAdapter extends RecyclerView.Adapter<ChurchBranchPhotoAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public ChurchBranchPhotoAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.church_dep_photo, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);
        Picasso.get().load(BRANCH_PHOTO_IMG_URL+map.get(IMAGE)).placeholder(R.drawable.image60).error(R.drawable.image60).into(holder.prdct_iv);

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView prdct_iv;
        TextView prdct_title_tv;
        LinearLayout container;


        public OrderVh(View v) {
            super(v);
            prdct_iv = (ImageView) v.findViewById(R.id.prdct_iv);
        }
    }
}
