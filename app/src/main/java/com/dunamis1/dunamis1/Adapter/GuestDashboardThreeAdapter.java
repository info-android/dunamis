package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.HomeChurchDetailsActivity;
import com.dunamis1.dunamis1.Activity.GuestDashboardActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.VIEWS;

public class GuestDashboardThreeAdapter extends RecyclerView.Adapter<GuestDashboardThreeAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public GuestDashboardThreeAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dash3_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);

        Picasso.get().load(HOME_BANNER_IMG_URL+map.get(IMAGE)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);
        if(map.get(VIEWS).equals("join")){
            holder.prdct_view.setVisibility(View.VISIBLE);
            holder.prdct_view2.setVisibility(View.GONE);
        }else if(map.get(VIEWS).equals("Waiting")){
            holder.prdct_view.setVisibility(View.GONE);
            holder.prdct_view2.setText(activity.getString(R.string.waiting));
            holder.prdct_view2.setVisibility(View.VISIBLE);
        }else{
            holder.prdct_view.setVisibility(View.GONE);
            holder.prdct_view2.setVisibility(View.VISIBLE);
        }
        holder.prdct_mem_tv3.setText(activity.getString(R.string.member)+": "+(String) map.get(MEMBER));
        holder.prdct_location_tv2.setText((String) map.get(DESC));
        holder.prdct_name_tv.setText((String) map.get(NAME));
        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));

        holder.prdct_view2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, HomeChurchDetailsActivity.class);
                I.putExtra("home_church_id",(String) map.get(HOME_ID));
                activity.startActivity(I);
            }
        });
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, HomeChurchDetailsActivity.class);
                I.putExtra("home_church_id",(String) map.get(HOME_ID));
                activity.startActivity(I);
            }
        });
        holder.prdct_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.showProgressDialog(activity);
                String server_url = activity.getString(R.string.SERVER_URL) + "member/homechurch/join";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
                params.put("home_church_id", (String) map.get(HOME_ID));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            MethodClass.hideProgressDialog(activity);
                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String message = result_Object.getString("message");
                                ((GuestDashboardActivity)activity).getDash1();
                                final Dialog dialog = new Dialog(activity);
                                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.join_popup);
                                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(activity);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(activity);

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

            }
        });


    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        CircleImageView prdct_iv;
        TextView prdct_view;
        TextView prdct_view2;
        TextView prdct_mem_tv3;
        TextView prdct_location_tv2;
        TextView prdct_name_tv;
        TextView prdct_title_tv;
        LinearLayout container;


        public OrderVh(View v) {
            super(v);
            prdct_iv = (CircleImageView) v.findViewById(R.id.prdct_iv);
            prdct_view = (TextView) v.findViewById(R.id.prdct_view);
            prdct_view2 = (TextView) v.findViewById(R.id.prdct_view2);
            prdct_mem_tv3 = (TextView) v.findViewById(R.id.prdct_mem_tv3);
            prdct_location_tv2 = (TextView) v.findViewById(R.id.prdct_location_tv2);
            prdct_name_tv = (TextView) v.findViewById(R.id.prdct_name_tv);
            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            container = (LinearLayout) v.findViewById(R.id.container);
        }
    }
}
