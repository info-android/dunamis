package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.Bible.BibleChapterActivity;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.BIBLE_ID;
import static com.dunamis1.dunamis1.Helper.Constant.BIBLE_NAME;
import static com.dunamis1.dunamis1.Helper.Constant.BOOK;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class BibleDeuteronomyAdapter extends RecyclerView.Adapter<BibleDeuteronomyAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, String>> map_list;

    public BibleDeuteronomyAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bible_deuteronomy_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final HashMap<String, String> map = map_list.get(i);
        viewHolder.title_tv.setText(map.get(BIBLE_NAME));
        if (i == (map_list.size() - 1)) {
            viewHolder.dash_tv.setText("");
        }
        else {
            viewHolder.dash_tv.setText(activity.getResources().getString(R.string.dash_line));
        }
        viewHolder.main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BOOK  = map.get(BIBLE_NAME);
                Intent i = new Intent(activity, BibleChapterActivity.class);
                i.putExtra("book_id",map.get(BIBLE_ID));
                i.putExtra("id",map.get(SEED_ID));
                activity.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout main_layout;
        private TextView title_tv, dash_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            main_layout = itemView.findViewById(R.id.main_layout);
            title_tv = itemView.findViewById(R.id.title_tv);
            dash_tv = itemView.findViewById(R.id.dash_tv);
        }
    }
}
