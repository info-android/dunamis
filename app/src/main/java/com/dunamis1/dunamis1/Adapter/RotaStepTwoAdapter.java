package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.FNAME;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.LNAME;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.USER_SELECTED_MEMBER;

public class RotaStepTwoAdapter extends RecyclerView.Adapter<RotaStepTwoAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public RotaStepTwoAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.step_two_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final HashMap<String, Object> map = map_list.get(i);


        Picasso.get().load(PROFILE_IMG_URL+(String)map.get(IMGG)).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(viewHolder.profile_iv);
        viewHolder.name_tv.setText(String.valueOf(map.get(TITLE))+" "+String.valueOf(map.get(FNAME))+" "+String.valueOf(map.get(LNAME)));

        viewHolder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //do your toast programming here.
                if(b){
                    for(int i=0;i<USER_SELECTED_MEMBER.size();i++)
                    {
                        if(USER_SELECTED_MEMBER.get(i) == Integer.valueOf((String)map.get(SEED_ID)))
                        {
                            USER_SELECTED_MEMBER.remove(i);
                        }

                    }
                    USER_SELECTED_MEMBER.add(Integer.valueOf((String)map.get(SEED_ID)));
                }else{
                    for(int i=0;i<USER_SELECTED_MEMBER.size();i++)
                    {
                        if(USER_SELECTED_MEMBER.get(i) == Integer.valueOf((String)map.get(SEED_ID)))
                        {
                            USER_SELECTED_MEMBER.remove(i);
                        }
                    }
                }
            }
        });
        viewHolder.name_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder.chk.isChecked()){
                    viewHolder.chk.setChecked(false);
                    for(int i=0;i<USER_SELECTED_MEMBER.size();i++)
                    {
                        if(USER_SELECTED_MEMBER.get(i) == Integer.valueOf((String) map.get(SEED_ID)))
                        {
                            USER_SELECTED_MEMBER.remove(i);
                        }
                    }

                }else {
                    viewHolder.chk.setChecked(true);
                    for(int i=0;i<USER_SELECTED_MEMBER.size();i++)
                    {
                        if(USER_SELECTED_MEMBER.get(i) == Integer.valueOf((String)map.get(SEED_ID)))
                        {
                            USER_SELECTED_MEMBER.remove(i);
                        }

                    }
                    USER_SELECTED_MEMBER.add(Integer.valueOf((String)map.get(SEED_ID)));

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView profile_iv;
        private TextView name_tv;
        private CheckBox chk;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile_iv = itemView.findViewById(R.id.profile_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            chk = itemView.findViewById(R.id.chk);

        }
    }
}
