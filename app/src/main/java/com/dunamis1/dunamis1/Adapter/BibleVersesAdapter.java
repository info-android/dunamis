package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.Bible.BibleVersesDetailsActivity;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.BIBLE_ID;
import static com.dunamis1.dunamis1.Helper.Constant.BIBLE_NAME;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.VERSE;

public class BibleVersesAdapter extends RecyclerView.Adapter<BibleVersesAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, String>> map_list;

    public BibleVersesAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bible_verses_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final HashMap<String, String> map = map_list.get(i);
        viewHolder.chapter_no_tv.setText(map.get(BIBLE_NAME));
        ViewTreeObserver vto = viewHolder.chapter_no_tv.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                VERSE = map.get(BIBLE_NAME);
                viewHolder.chapter_no_tv.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = viewHolder.chapter_no_tv.getMeasuredHeight();
                int finalWidth = viewHolder.chapter_no_tv.getMeasuredWidth();
                ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) viewHolder.chapter_no_tv.getLayoutParams();
                layoutParams.height = finalWidth;
                viewHolder.chapter_no_tv.setLayoutParams(layoutParams);
                return true;
            }
        });
        viewHolder.main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.chapter_no_tv.setBackground(activity.getResources().getDrawable(R.drawable.small_rounded_yellow));
                viewHolder.chapter_no_tv.setTextColor(activity.getResources().getColor(R.color.white));
                VERSE = map.get(BIBLE_NAME);
                Intent I = new Intent(activity, BibleVersesDetailsActivity.class);
                I.putExtra("id",map.get(SEED_ID));
                I.putExtra("verse_id",map.get(BIBLE_ID));
                activity.startActivity(I);
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView main_layout;
        private TextView chapter_no_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            main_layout = itemView.findViewById(R.id.main_layout);
            chapter_no_tv = itemView.findViewById(R.id.chapter_no_tv);

        }
    }
}
