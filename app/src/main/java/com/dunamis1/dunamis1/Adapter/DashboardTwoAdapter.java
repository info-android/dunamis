package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SAVE;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class DashboardTwoAdapter extends RecyclerView.Adapter<DashboardTwoAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public DashboardTwoAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dash2_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);
        Picasso.get().load(EVENT_IMG_URL+map.get(IMAGE)).placeholder(R.drawable.daddy).error(R.drawable.daddy).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);

        holder.prdct_iv.setClipToOutline(true);

        // Get date from string
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(DESC));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Get time from date
        SimpleDateFormat timeFormatter = new SimpleDateFormat("dd MMM yyyy");
        String displayValue = timeFormatter.format(date);
        holder.discount.setText(displayValue);

        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, EventDetailsActivity.class);
                I.putExtra("id",(String) map.get(EVENT_ID));
                I.putExtra("save",(String)map.get(EVENT_SAVE));
                activity.startActivity(I);
            }
        });


    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView prdct_iv;
        TextView discount;
        TextView prdct_title_tv;
        LinearLayout container;

        public OrderVh(View v) {
            super(v);
            prdct_iv = (ImageView) v.findViewById(R.id.prdct_iv);
            discount = (TextView) v.findViewById(R.id.discount);
            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            container = (LinearLayout) v.findViewById(R.id.container);
        }
    }
}