package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.ChurchDetailActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.DEPARTMENT_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class DepartmentListPhotoGalleryAdapter extends RecyclerView.Adapter<DepartmentListPhotoGalleryAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public DepartmentListPhotoGalleryAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.department_list_photo_gallery_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final HashMap<String, Object> map = map_list.get(i);

        ViewTreeObserver vto = viewHolder.photo_iv.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                viewHolder.photo_iv.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = viewHolder.photo_iv.getMeasuredHeight();
                int finalWidth = viewHolder.photo_iv.getMeasuredWidth();
                ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) viewHolder.photo_iv.getLayoutParams();
                layoutParams.height = finalWidth;
                viewHolder.photo_iv.setLayoutParams(layoutParams);


                CardView.LayoutParams layoutParams2 = (CardView.LayoutParams) viewHolder.photo_iv.getLayoutParams();
                layoutParams.height = finalWidth;
                viewHolder.tv_container.setLayoutParams(layoutParams2);
                return true;
            }
        });
        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+(String)map.get(IMGG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(viewHolder.photo_iv);
        viewHolder.title_tv.setText(String.valueOf(map.get(TITLE)));

        viewHolder.main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity,ChurchDetailActivity.class);
                I.putExtra("id",(String)map.get(SEED_ID));
                activity.startActivity(I);
            }
        });
        /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,0,0,MethodClass.convert_into_sp_unit(activity, 15));
        viewHolder.title_tv.setLayoutParams(params);*/

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView photo_iv;
        private TextView title_tv;
        private LinearLayout tv_container;
        private CardView main_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo_iv = itemView.findViewById(R.id.photo_iv);
            tv_container = itemView.findViewById(R.id.tv_container);
            title_tv = itemView.findViewById(R.id.title_tv);
            main_layout = itemView.findViewById(R.id.main_layout);

        }
    }
}
