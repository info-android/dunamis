package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.IMG;

public class AboutPhotoAdapter extends RecyclerView.Adapter<AboutPhotoAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public AboutPhotoAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_photo_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        HashMap<String, Object> map = map_list.get(i);
        viewHolder.profile_iv.setImageResource((Integer) map.get(IMG));
        //viewHolder.add_tv.setText(String.valueOf(map.get(ADDRESS)));

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView profile_iv;
        private CardView container;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile_iv = itemView.findViewById(R.id.profile_iv);
            container = itemView.findViewById(R.id.container);

        }
    }
}
