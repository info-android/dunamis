package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.Question.QuestionAndAnswerActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.ANS_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.QUES_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class QuestionListAdapter extends RecyclerView.Adapter<QuestionListAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;

    public QuestionListAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.question_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        HashMap<String, Object> map = map_list.get(i);


        if (i==(map_list.size()-1)){
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, MethodClass.convert_into_sp_unit(activity,30));
            viewHolder.main_layout.setLayoutParams(layoutParams);
        }

        viewHolder.main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activity.startActivity(new Intent(activity, EventDetailsActivity.class));
                MethodClass.go_to_next_activity(activity, QuestionAndAnswerActivity.class);
            }
        });
       viewHolder.ques_date_tv.setText("Asked on : "+String.valueOf(map.get(QUES_DATE)));
       viewHolder.ans_date_tv.setText("Answered on : "+String.valueOf(map.get(ANS_DATE)));
       viewHolder.title_tv.setText(String.valueOf(map.get(TITLE)));

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title_tv,ques_date_tv,ans_date_tv;
        private LinearLayout main_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title_tv = itemView.findViewById(R.id.title_tv);
            ques_date_tv = itemView.findViewById(R.id.ques_date_tv);
            ans_date_tv = itemView.findViewById(R.id.ans_date_tv);
            main_layout = itemView.findViewById(R.id.main_layout);

        }
    }
}
