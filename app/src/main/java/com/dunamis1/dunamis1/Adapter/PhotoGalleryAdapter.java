package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.app.Dialog;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.dunamis1.dunamis1.R;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.EVENT_IMG_URL2;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;

public class PhotoGalleryAdapter extends RecyclerView.Adapter<PhotoGalleryAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, String>> map_list;
    private Integer j;
    public PhotoGalleryAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.
                photo_gallery_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        HashMap<String, String> map = map_list.get(i);

        ViewTreeObserver vto = viewHolder.photo_iv.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                viewHolder.photo_iv.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = viewHolder.photo_iv.getMeasuredHeight();
                int finalWidth = viewHolder.photo_iv.getMeasuredWidth();
                ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) viewHolder.photo_iv.getLayoutParams();
                layoutParams.height = finalWidth;
                viewHolder.photo_iv.setLayoutParams(layoutParams);
                return true;
            }
        });

        Picasso.get().load(EVENT_IMG_URL2+map.get(IMG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(viewHolder.photo_iv);

        viewHolder.photo_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                j = i;
                final Dialog dialog =new Dialog(activity);
                dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setContentView(R.layout.photo_view_popup);
                dialog.setCancelable(false);
                ImageView close_iv =(ImageView)dialog.findViewById(R.id.close_iv);
                ImageView right_left_iv =(ImageView)dialog.findViewById(R.id.right_left_iv);
                ImageView right_arrow_iv =(ImageView)dialog.findViewById(R.id.right_arrow_iv);
                final ImageView photo_iv =(ImageView)dialog.findViewById(R.id.photo_iv);
                Picasso.get().load(EVENT_IMG_URL2+map_list.get(j).get(IMG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(photo_iv);
                close_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                right_left_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(j>0){
                            Picasso.get().load(EVENT_IMG_URL2+map_list.get(j-1).get(IMG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(photo_iv);
                            j = j-1;
                        }
                    }
                });
                right_arrow_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("JJ", j.toString()+"       "+map_list.size());
                        if(j+1<map_list.size()){
                            Picasso.get().load(EVENT_IMG_URL2+map_list.get(j+1).get(IMG)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(photo_iv);
                            j = j+1;
                        }
                    }
                });

                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView photo_iv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo_iv = itemView.findViewById(R.id.photo_iv);

        }
    }
}
