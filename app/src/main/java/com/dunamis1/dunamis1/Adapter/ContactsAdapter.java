package com.dunamis1.dunamis1.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dunamis1.dunamis1.Helper.ContactObject;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ContactsAdapter  extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    public ArrayList<HashMap<String, String>> list;
    private List<ContactObject> mainDataList = null;
    private ArrayList<ContactObject> arraylist;
    private boolean flag;
    public ArrayList<ContactObject> Search = null;
    public ContactsAdapter(Context context, List<ContactObject> mainDataList, boolean flag) {
        super();
        mContext = context;
        this.mainDataList = mainDataList;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arraylist = new ArrayList<ContactObject>();
        this.arraylist.addAll(mainDataList);
        this.flag=flag;
    }
    //override function for adapter extend to baseadater
    @Override
    public int getCount() {
        return arraylist.size();
    }
    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    //create holder according to id of view page
    public  class  ViewHolder{
        TextView name;
        TextView phone;
        CheckBox check;
    }
    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    //get view function to set adapter with the all sparkle list
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.contact_item, null);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.phone = (TextView) view.findViewById(R.id.phone);
            holder.check = (CheckBox) view.findViewById(R.id.contact_chckbx);
            view.setTag(holder);
            view.setTag(R.id.name, holder.name);
            view.setTag(R.id.phone, holder.phone);
            view.setTag(R.id.contact_chckbx, holder.check);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.check.setTag(position);
        holder.name.setText(arraylist.get(position).getName());
        holder.phone.setText(arraylist.get(position).getNumber());

        holder.check.setChecked(arraylist.get(position).isSelected());
        holder.check
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton vw,
                                                 boolean isChecked) {
                        int getPosition = (Integer) vw.getTag();
                        if (arraylist.get(getPosition).isSelected()){
                            arraylist.get(getPosition).setSelected(false);
                        }
                        else {
                            arraylist.get(getPosition).setSelected(true);
                        }
                    }
                });
        return view;
    }
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        arraylist.clear();
        if (charText.length() == 0) {
            arraylist.addAll(mainDataList);
        }
        else
        {
            //Search.addAll(list);
            for (int i = 0; i<mainDataList.size();i++) {
                Log.e("Search",mainDataList.get(i).toString());
                if(mainDataList.get(i).getName().toLowerCase(Locale.getDefault()).contains(charText)){
                    //Search.add(list.get(i));
                    arraylist.add(mainDataList.get(i));
                    Log.e("MatchSearch",mainDataList.get(i).getName());
                }
            }
        }
        if(arraylist.size()>0){
            notifyDataSetChanged();
        }
    }
}
