package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class ChurchDepOneAdapter extends RecyclerView.Adapter<ChurchDepOneAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public ChurchDepOneAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.church_dep1, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);

        Picasso.get().load(PROFILE_IMG_URL+map.get(IMAGE)).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);
        holder.desc_tv.setText(String.valueOf(map.get(TITLE)));
        if (position == (list.size() - 1)) {
            holder.dash_tv.setText("");
        }
        else {
            holder.dash_tv.setText(activity.getResources().getString(R.string.dash_line));
        }
        holder.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MessageChatActivity.class);
                intent.putExtra("rec_id",(String)map.get(ID));
                activity.startActivity(intent);
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView prdct_iv;
        ImageView message;
        TextView desc_tv;
        TextView dash_tv;


        public OrderVh(View v) {
            super(v);
            prdct_iv = (ImageView) v.findViewById(R.id.prdct_iv);
            message = (ImageView) v.findViewById(R.id.message);
            desc_tv = (TextView) v.findViewById(R.id.desc_tv);
            dash_tv = (TextView) v.findViewById(R.id.dash_tv);
        }
    }
}
