package com.dunamis1.dunamis1.Adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.MESSAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MSG;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_DAT;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_REC_ID;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_SND_NAME;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_SND_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;

public class MessageChatAdapter extends RecyclerView.Adapter<MessageChatAdapter.ViewHolder> {
    AppCompatActivity activity;
    ArrayList<HashMap<String, String>> arrayList;
    private String user_id = "";
    public MessageChatAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.message_chate_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        user_id  = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");
        HashMap<String, String> map = arrayList.get(i);
        if (map.get(MSG_REC_ID).equals(user_id)){
            viewHolder.otherUserName.setText(map.get(MSG_SND_NAME));
            viewHolder.otherUserMesg.setText(map.get(MSG));
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd h:m:s");
            dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            try {
                date = dateFormatter.parse((String) map.get(MSG_DAT));
                // Get time from date

                SimpleDateFormat format = new SimpleDateFormat("d");
                Calendar cal = Calendar.getInstance();
                TimeZone tz = cal.getTimeZone();
                format.setTimeZone(tz);
                String datessa = format.format(date);

                if(datessa.endsWith("1") && !datessa.endsWith("11"))
                    format = new SimpleDateFormat("d'st' MMM, yyyy h:m a");
                else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                    format = new SimpleDateFormat("d'nd' MMM, yyyy h:m a");
                else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                    format = new SimpleDateFormat("d'rd' MMM, yyyy h:m a");
                else
                    format = new SimpleDateFormat("d'th' MMM, yyyy h:m a");

                String yourDate = format.format(date);

                viewHolder.otherUserTime.setText(yourDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.get().load(PROFILE_IMG_URL+map.get(MSG_SND_PIC)).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(viewHolder.otherUserImage);
            viewHolder.otherUserLay.setVisibility(View.VISIBLE);
            viewHolder.userLay.setVisibility(View.GONE);
        }else {
            viewHolder.userName.setText(map.get(MSG_SND_NAME));
            viewHolder.userMesg.setText(map.get(MSG));
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd h:m:s");
            dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            try {
                date = dateFormatter.parse((String) map.get(MSG_DAT));
                // Get time from date

                SimpleDateFormat format = new SimpleDateFormat("d");
                Calendar cal = Calendar.getInstance();
                TimeZone tz = cal.getTimeZone();
                format.setTimeZone(tz);
                String datessa = format.format(date);

                if(datessa.endsWith("1") && !datessa.endsWith("11"))
                    format = new SimpleDateFormat("d'st' MMM, yyyy h:m a");
                else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                    format = new SimpleDateFormat("d'nd' MMM, yyyy h:m a");
                else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                    format = new SimpleDateFormat("d'rd' MMM, yyyy h:m a");
                else
                    format = new SimpleDateFormat("d'th' MMM, yyyy h:m a");

                String yourDate = format.format(date);

                viewHolder.userTime.setText(yourDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.get().load(PROFILE_IMG_URL+map.get(MSG_SND_PIC)).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(viewHolder.userImage);
            viewHolder.userLay.setVisibility(View.VISIBLE);
            viewHolder.otherUserLay.setVisibility(View.GONE);
        }

        if (arrayList.size()==i+1){
            viewHolder.itemLayout.setPadding(0,0,0, MethodClass.convert_into_sp_unit(activity,15));
        }
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout itemLayout;
        private RelativeLayout otherUserLay,userLay;
        private CircleImageView otherUserImage,userImage;
        private TextView otherUserName,userName;
        private TextView otherUserMesg,userMesg;
        private TextView otherUserTime,userTime;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.itemLayout);
            otherUserLay = itemView.findViewById(R.id.otherUserLay);
            userLay = itemView.findViewById(R.id.userLay);
            otherUserImage = itemView.findViewById(R.id.otherUserImage);
            userImage = itemView.findViewById(R.id.userImage);
            otherUserName = itemView.findViewById(R.id.otherUserName);
            userName = itemView.findViewById(R.id.userName);
            otherUserMesg = itemView.findViewById(R.id.otherUserMesg);
            userMesg = itemView.findViewById(R.id.userMesg);
            otherUserTime = itemView.findViewById(R.id.otherUserTime);
            userTime = itemView.findViewById(R.id.userTime);
        }
    }
}
