package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.SOCIAL;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class DashboardOneAdapter extends RecyclerView.Adapter<DashboardOneAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public DashboardOneAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dash1_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);

        holder.prdct_iv.setImageResource((int) map.get(IMAGE));
        holder.discount.setImageResource((int) map.get(SOCIAL));
        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));
        holder.prdct_dtls_tv.setText(String.valueOf(map.get(DESC)));

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView prdct_iv;
        ImageView discount;
        TextView prdct_title_tv;
        TextView prdct_dtls_tv;
        public OrderVh(View v) {
            super(v);
            prdct_iv = (ImageView) v.findViewById(R.id.prdct_iv);
            discount = (ImageView) v.findViewById(R.id.discount);
            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = (TextView) v.findViewById(R.id.prdct_dtls_tv);
        }
    }
}
