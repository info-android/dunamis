package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.ChurchDetailActivity;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class ProfileOneAdapter extends RecyclerView.Adapter<ProfileOneAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public ProfileOneAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.profile_one_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);

        holder.prdct_iv.setImageResource((int) map.get(IMAGE));
        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, ChurchDetailActivity.class);
                activity.startActivity(I);
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        ImageView prdct_iv;
        TextView prdct_title_tv;
        LinearLayout container;


        public OrderVh(View v) {
            super(v);
            prdct_iv = (ImageView) v.findViewById(R.id.prdct_iv);
            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            container = (LinearLayout) v.findViewById(R.id.container);
        }
    }
}