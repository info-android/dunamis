package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.ChurchDepartment.HomeChurchDetailsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.UpdateChurchDetailsActivity;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class HomeDashboardThreeAdapter extends RecyclerView.Adapter<HomeDashboardThreeAdapter.OrderVh> {
    public ArrayList<HashMap<Object, Object>> list;
    Activity activity;
    String user_id;

    //innitialize listview adapter
    public HomeDashboardThreeAdapter(Activity activity, ArrayList<HashMap<Object, Object>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dash3_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<Object, Object> map = list.get(position);

        Picasso.get().load(HOME_BANNER_IMG_URL+map.get(IMAGE)).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(holder.prdct_iv);
        if(position == 0){
            holder.prdct_view.setText(activity.getString(R.string.viewMore));
            holder.prdct_view.setVisibility(View.VISIBLE);
        }

        holder.prdct_mem_tv3.setText(activity.getString(R.string.member)+": "+(String) map.get(MEMBER));
        holder.prdct_location_tv2.setText((String) map.get(DESC));
        holder.prdct_name_tv.setText((String) map.get(NAME));
        holder.prdct_title_tv.setText(String.valueOf(map.get(TITLE)));

        holder.prdct_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, HomeChurchDetailsActivity.class);
                I.putExtra("home_church_id",(String) map.get(HOME_ID));
                activity.startActivity(I);
            }
        });
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, HomeChurchDetailsActivity.class);
                I.putExtra("home_church_id",(String) map.get(HOME_ID));
                activity.startActivity(I);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, UpdateChurchDetailsActivity.class);
                activity.startActivity(I);
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        CircleImageView prdct_iv;
        TextView prdct_view;
        TextView prdct_mem_tv3;
        TextView prdct_location_tv2;
        TextView prdct_name_tv;
        TextView prdct_title_tv;
        ImageView edit;
        LinearLayout container;


        public OrderVh(View v) {
            super(v);
            prdct_iv = (CircleImageView) v.findViewById(R.id.prdct_iv);
            prdct_view = (TextView) v.findViewById(R.id.prdct_view);
            prdct_mem_tv3 = (TextView) v.findViewById(R.id.prdct_mem_tv3);
            prdct_location_tv2 = (TextView) v.findViewById(R.id.prdct_location_tv2);
            prdct_name_tv = (TextView) v.findViewById(R.id.prdct_name_tv);
            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            edit = (ImageView) v.findViewById(R.id.edit);
            container = (LinearLayout) v.findViewById(R.id.container);
        }
    }
}
