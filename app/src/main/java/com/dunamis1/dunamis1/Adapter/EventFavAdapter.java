package com.dunamis1.dunamis1.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.EventListing.CommentsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ADD;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SAVE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_TITLE;

public class EventFavAdapter extends RecyclerView.Adapter<EventFavAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, String>> map_list;

    public EventFavAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.events_item2, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final HashMap<String, String> map = map_list.get(i);

        Picasso.get().load(EVENT_IMG_URL+map.get(EVENT_THUMB)).placeholder(R.drawable.image60).error(R.drawable.image60).into(viewHolder.profile_iv);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormatter.parse((String) map.get(EVENT_DATE));
            // Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("d");
            String displayValue = timeFormatter.format(date);

            SimpleDateFormat format = new SimpleDateFormat("d");
            String datessa = format.format(date);

            if(datessa.endsWith("1") && !datessa.endsWith("11"))
                format = new SimpleDateFormat("d'st' MMM, yyyy");
            else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                format = new SimpleDateFormat("d'nd' MMM, yyyy");
            else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                format = new SimpleDateFormat("d'rd' MMM, yyyy");
            else
                format = new SimpleDateFormat("d'th' MMM, yyyy");

            String yourDate = format.format(date);

            viewHolder.date_tv.setText(yourDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        viewHolder.title_tv.setText((String) map.get(EVENT_TITLE));
        viewHolder.add_tv.setText((String) map.get(EVENT_ADD));
        viewHolder.msg_tv.setText((String) map.get(EVENT_COMMENT));
        viewHolder.share_tv.setText((String) map.get(EVENT_SHARE));
        viewHolder.like_tv.setText((String) map.get(EVENT_LIKE));
        if (i==(map_list.size()-1)){
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, MethodClass.convert_into_sp_unit(activity,30));
            viewHolder.main_layout.setLayoutParams(layoutParams);
        }

        viewHolder.main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, EventDetailsActivity.class);

                I.putExtra("id",map.get(EVENT_ID));
                I.putExtra("save",map.get(EVENT_SAVE));
                activity.startActivity(I);
            }
        });
        viewHolder.sharelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "http://phpwebdevelopmentservices.com/development/dunamis/api/share?content="+map.get(EVENT_ID)+"&language="+ PreferenceManager.getDefaultSharedPreferences(activity).getString("lang","en")+"&type=E");
                sendIntent.setType("text/plain");
                activity.startActivity(sendIntent);
            }
        });
        viewHolder.cmmnts_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, CommentsActivity.class);
                I.putExtra("from","event");
                I.putExtra("id",map.get(EVENT_ID));
                I.putExtra("save",map.get(EVENT_SAVE));
                activity.startActivity(I);
            }
        });
        viewHolder.likelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = activity.getString(R.string.SERVER_URL) + "event/like";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("event_id", map.get(EVENT_ID));
                params.put("language",PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String total_likes = result_Object.getString("total_likes");
                                viewHolder.like_tv.setText(total_likes);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
            }
        });

        viewHolder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = activity.getString(R.string.SERVER_URL) + "favourite";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", map.get(EVENT_ID));
                params.put("type", "E");
                params.put("language",PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String favourite = result_Object.getString("favourite");
                                String message = result_Object.getString("message");
                                String meaning = result_Object.getString("meaning");
                                ((MyFavouriteActivity)activity).step1part();
                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(message)
                                        .setContentText(meaning)
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
            }
        });

        Log.e("SAVE", map.get(EVENT_SAVE));
        if(map.get(EVENT_SAVE).equals("YES")){
            viewHolder.savelayout.setVisibility(View.VISIBLE);
        }else{
            viewHolder.savelayout.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView profile_iv;
        private TextView name_tv,add_tv,date_tv;
        private LinearLayout main_layout,cmmnts_layout;
        private TextView title_tv;
        private TextView share_tv;
        private TextView msg_tv;
        private TextView like_tv;
        private LinearLayout sharelayout;
        private LinearLayout savelayout;
        private ImageView close;

        private LinearLayout likelayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile_iv = itemView.findViewById(R.id.profile_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            add_tv = itemView.findViewById(R.id.add_tv);
            main_layout = itemView.findViewById(R.id.main_layout);
            cmmnts_layout = itemView.findViewById(R.id.cmmnts_layout);
            title_tv = itemView.findViewById(R.id.title_tv);
            share_tv = itemView.findViewById(R.id.share_tv);
            msg_tv = itemView.findViewById(R.id.msg_tv);
            like_tv = itemView.findViewById(R.id.like_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            sharelayout = itemView.findViewById(R.id.sharelayout);
            cmmnts_layout = itemView.findViewById(R.id.cmmnts_layout);
            likelayout = itemView.findViewById(R.id.likelayout);
            savelayout = itemView.findViewById(R.id.savelayout);
            close = itemView.findViewById(R.id.close);

        }
    }
}
