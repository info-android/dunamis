package com.dunamis1.dunamis1.Service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static com.dunamis1.dunamis1.Helper.Constant.SHARED_PREF;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    String resultDate;
    private NotificationManager mNotificationManager;
    String user_id ="";
    boolean isLogin = false;
    private SharedPreferences sp;
    @Override
    public void onNewToken(String s) {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.e("My Token",token);
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(SHARED_PREF, 0);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("regId", token);
                        editor.commit();
                    }
                });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e("remoteMessage", "remoteMessage: " + remoteMessage.getData());
        isLogin = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("user_id","");
        if (remoteMessage == null)
            return;
        for (int i=0; i<5; i++) {
            Log.i(TAG, "Working... " + (i + 1)
                    + "/5 @ " + SystemClock.elapsedRealtime());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
        JSONObject jsonObj = new JSONObject(remoteMessage.getData());
        String title= null;
        String message= "";
        String type= "";
        String from_id= "";
        String to_id= "";
        String message_id= "";
        try {
            title = jsonObj.getString("title");
            if(jsonObj.has("message")){
                message=jsonObj.getString("message");
            }
            if(jsonObj.has("type")){
                type = jsonObj.getString("type");
                if(type.equals("message")){
                    from_id = jsonObj.getString("from_id");
                    message_id = jsonObj.getString("message_id");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("EROOR", e.toString() );
        }


        String noti_type="";

        //final String date = jsonObj.getString("date");

        if(type.equals("message")){
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getApplicationContext(), "notify_001");
            Intent ii = null;
            String activity = getForegroundActivity();
            Log.e("activity", activity);
            if(isLogin){
                Log.e("id", from_id);
                if (activity.equals("com.dunamis1.dunamis1.Activity.Message.MessageChatActivity") ) {
                    Intent intent = new Intent("updateMessage");
                    intent.putExtra("id", from_id);
                    intent.putExtra("title", title);
                    intent.putExtra("message", message);
                    intent.putExtra("message_id", message_id);
                    Log.e("id", from_id);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                }else{
                    ii = new Intent(getApplicationContext(), MessageChatActivity.class);
                    ii.putExtra("rec_id",from_id);
                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);
                    Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    mBuilder.setContentIntent(pendingIntent);
                    mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                    mBuilder.setContentTitle(title);
                    mBuilder.setContentText(message);
                    mBuilder.setSound(soundUri);
                    mBuilder.setAutoCancel(true);
                    mBuilder.setPriority(Notification.PRIORITY_MAX);

                    mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        String channelId = "1003";
                        NotificationChannel channel = new NotificationChannel(channelId,
                                "DUNMAIS",
                                NotificationManager.IMPORTANCE_HIGH
                        );
                        mNotificationManager.createNotificationChannel(channel);
                        mBuilder.setChannelId(channelId);
                    }

                    mNotificationManager.notify(0, mBuilder.build());
                }

            }else{
                ii = new Intent(getApplicationContext(), SplashActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);
                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                mBuilder.setContentTitle(title);
                mBuilder.setContentText(message);
                mBuilder.setSound(soundUri);
                mBuilder.setAutoCancel(true);
                mBuilder.setPriority(Notification.PRIORITY_MAX);

                mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    String channelId = "1003";
                    NotificationChannel channel = new NotificationChannel(channelId,
                            "DUNMAIS",
                            NotificationManager.IMPORTANCE_HIGH
                    );
                    mNotificationManager.createNotificationChannel(channel);
                    mBuilder.setChannelId(channelId);
                }

                mNotificationManager.notify(0, mBuilder.build());
            }


        }else{
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getApplicationContext(), "notify_001");
            Intent ii = null;
            ii = new Intent(getApplicationContext(), SplashActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
            mBuilder.setContentTitle(title);
            mBuilder.setContentText(message);
            mBuilder.setSound(soundUri);
            mBuilder.setAutoCancel(true);
            mBuilder.setPriority(Notification.PRIORITY_MAX);

            mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = "1003";
                NotificationChannel channel = new NotificationChannel(channelId,
                        "DUNMAIS",
                        NotificationManager.IMPORTANCE_HIGH
                );
                mNotificationManager.createNotificationChannel(channel);
                mBuilder.setChannelId(channelId);
            }

            mNotificationManager.notify(0, mBuilder.build());
        }


    }
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    public String getForegroundActivity(){
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
        return taskInfo.get(0).topActivity.getClassName();
    }

}