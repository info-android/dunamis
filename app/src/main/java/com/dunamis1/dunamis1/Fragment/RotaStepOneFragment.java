package com.dunamis1.dunamis1.Fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;


public class RotaStepOneFragment extends Fragment {

    private RecyclerView order_hist_rv;
    private ArrayList<HashMap<String, String>> map_list;
    private Activity activity;
    private Spinner location_spinner,day_of_mnth_spinner;
    private Button cont;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public RotaStepOneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rota_step_one, container, false);
        activity = (Activity) container.getContext();

        location_spinner = view.findViewById(R.id.location_spinner);
        day_of_mnth_spinner = view.findViewById(R.id.day_of_mnth_spinner);
        cont = view.findViewById(R.id.cont);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.select_here, R.layout.spinner_item);
        ArrayAdapter adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.select, R.layout.spinner_item);
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment someFragment = new RotaStepTwoFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, someFragment ); // give your fragment container id in first parameter
                transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                transaction.commit();
            }
        });
        location_spinner.setAdapter(adapter);
        day_of_mnth_spinner.setAdapter(adapter2);

        return view;
    }


}
