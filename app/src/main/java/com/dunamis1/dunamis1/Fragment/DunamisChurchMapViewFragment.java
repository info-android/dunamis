package com.dunamis1.dunamis1.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.DashboardActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.MAP_LANG;
import static com.dunamis1.dunamis1.Helper.Constant.MAP_LAT;
import static com.dunamis1.dunamis1.Helper.Constant.MAP_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.RADIUS;


public class DunamisChurchMapViewFragment extends Fragment{

    private RecyclerView order_hist_rv;
    private ArrayList<HashMap<String, String>> map_list;
    private Activity activity;
    MapView mMapView;
    private GoogleMap googleMap;
    private LinearLayout getDirectionBtn;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public DunamisChurchMapViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dunamis_church_map_view_one, container, false);
        activity = (Activity) container.getContext();
        mMapView = (MapView) view.findViewById(R.id.mapView);
        getDirectionBtn = (LinearLayout) view.findViewById(R.id.getDirectionBtn);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
            }
        });
        getDirectionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mapss = "http://maps.google.com/maps?q=loc:" + MAP_LAT + "," + MAP_LANG + " (" + MAP_TITLE + ")";
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(mapss));
                startActivity(i);
            }
        });
        getDetails();
        return view;
    }
    public void getDetails(){
        MethodClass.showProgressDialog(getActivity());
        String server_url = getActivity().getString(R.string.SERVER_URL) + "branch/list";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG","en"));
        if(!RADIUS.equals("")){
            params.put("radius",RADIUS);
        }
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONArray dunamis_church_list = result_Object.getJSONArray("dunamis_church_list");

                        for (int i = 0; i <dunamis_church_list.length() ; i++) {

                            final String lat = dunamis_church_list.getJSONObject(i).getString("latitude");
                            final String longs = dunamis_church_list.getJSONObject(i).getString("longitude");
                            final String title = dunamis_church_list.getJSONObject(i).getJSONObject("churchdetails").getString("name");

                            createMarker(lat,longs,title);


                        }
                        String country_id = result_Object.getString("country_id");
                        if(country_id.equals("161")){
                            ((DunamisChurch)getActivity()).hideFilter();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }
    public void createMarker(String lat,String long_,String titl){

        if(googleMap !=null){
            Log.e("LAT", lat);
            LatLng sydney = new LatLng(Double.parseDouble(lat), Double.parseDouble(long_));
            googleMap.addMarker(new MarkerOptions().position(sydney).title(titl));
            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(5).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    MAP_LAT = String.valueOf(marker.getPosition().latitude);
                    MAP_LANG = String.valueOf(marker.getPosition().longitude);
                    MAP_TITLE = marker.getTitle();
                    return false;
                }
            });
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
