package com.dunamis1.dunamis1.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.GuestEditProfileStep1Activity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuestStep1Fragment  extends Fragment {
    private Spinner title_spinner;
    private Spinner gender_spinner;
    private Spinner mar_spinner;
    private Spinner age_spinner;
    private Button cont;
    private String user_id;
    private String gender="",martial="",age="",titles="";
    private EditText fname,lname,emailaddress,phone;
    Spinner code_spinner;
    private String[] codeArr = {"+376","+971","+93","+1268","+1264","+355","+374","+599","+244","+672","+54","+1684","+43","+61","+297","+994","+387","+1246","+880","+32","+226","+359","+973","+257","+229","+590","+1441","+673","+591","+55","+1242","+975","+267","+375","+501","+1","+61","+243","+236","+242","+41","+225","+682","+56","+237","+86","+57","+506","+53","+238","+61","+357","+420","+49","+253","+45","+1767","+1809","+213","+593","+372","+20","+291","+34","+251","+358","+679","+500","+691","+298","+33","+241","+44","+1473","+995","+233","+350","+299","+220","+224","+240","+30","+502","+1671","+245","+592","+852","+504","+385","+509","+36","+62","+353","+972","+44","+91","+964","+98","+354","+39","+1876","+962","+81","+254","+996","+855","+686","+269","+1869","+850","+82","+965","+1345","+7","+856","+961","+1758","+423","+94","+231","+266","+370","+352","+371","+218","+212","+377","+373","+382","+1599","+261","+692","+389","+223","+95","+976","+853","+1670","+222","+1664","+356","+230","+960","+265","+52","+60","+258","+264","+687","+227","+234","+505","+31","+47","+977","+674","+683","+64","+968","+507","+51","+689","+675","+63","+92","+48","+508","+870","+1","+351","+680","+595","+974","+40","+381","+7","+250","+966","+677","+248","+249","+46","+65","+290","+386","+421","+232","+378","+221","+252","+597","+239","+503","+963","+268","+1649","+235","+228","+66","+992","+690","+670","+993","+216","+676","+90","+1868","+688","+886","+255","+380","+256","+1","+598","+998","+39","+1784","+58","+1284","+1340","+84","+678","+681","+685","+381","+967","+262","+27","+260","+263"};
    private String code = "";
    private Integer selected_gen = 0,selected_mar = 0,selected_age = 0,selected_title = 0,sel_code = 0;

    public GuestStep1Fragment() {
        // MapFragment empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guest_edit_step1, container, false);
        //here Facebook callback Manager
        user_id = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("user_id","");

        title_spinner = view.findViewById(R.id.title_spinner);
        gender_spinner = view.findViewById(R.id.gender_spinner);
        mar_spinner = view.findViewById(R.id.mar_spinner);
        age_spinner = view.findViewById(R.id.age_spinner);
        code_spinner = view.findViewById(R.id.code_spinner);
        cont = view.findViewById(R.id.cont);
        fname = view.findViewById(R.id.fname);
        lname = view.findViewById(R.id.lname);
        emailaddress = view.findViewById(R.id.emailaddress);
        phone = view.findViewById(R.id.phone);

        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileOne();

            }
        });

        getDetails();
        return view;
    }
    public void updateProfileOne(){
        if(titles.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseTitle), Snackbar.LENGTH_LONG);
            snackbar.show();
            title_spinner.requestFocus();
            return;
        }if(fname.getText().toString().trim().length() == 0){
            fname.setError(getActivity().getString(R.string.fnameReq));
            fname.requestFocus();
            return;
        }
        if(lname.getText().toString().trim().length() == 0){
            lname.setError(getActivity().getString(R.string.lnameReq));
            lname.requestFocus();
            return;
        }
        if(age.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseAge), Snackbar.LENGTH_LONG);
            snackbar.show();
            age_spinner.requestFocus();
            return;
        }
        if(gender.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseGender), Snackbar.LENGTH_LONG);
            snackbar.show();
            gender_spinner.requestFocus();
            return;
        }
        if(martial.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseMartial), Snackbar.LENGTH_LONG);
            snackbar.show();
            mar_spinner.requestFocus();
            return;
        }

        MethodClass.showProgressDialog(getActivity());
        String server_url = getString(R.string.SERVER_URL) + "guest-update/step-one";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        params.put("fname", fname.getText().toString().trim());
        params.put("lname", lname.getText().toString().trim());
        params.put("title",titles);
        params.put("age",age);
        params.put("gender",gender);
        params.put("country_code", code);
        params.put("marital_status",martial);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONObject message = result_Object.getJSONObject("user");
                        String fname = message.getString("fname");
                        String lname = message.getString("lname");
                        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("user_name", fname+" "+lname).commit();
                        ((GuestEditProfileStep1Activity)getActivity()).step2part();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }
    public void getDetails(){
        MethodClass.showProgressDialog(getActivity());
        String server_url = getActivity().getString(R.string.SERVER_URL) + "guest-details/step-one";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONObject user_details = result_Object.getJSONObject("user");
                        List<StringWithTag> spinnerArraysCode = new ArrayList<StringWithTag>();
                        for (int j = 0; j <codeArr.length ; j++) {
                            spinnerArraysCode.add(new StringWithTag(codeArr[j], "M"));
                            if(user_details.getString("country_code").equals(codeArr[j])){
                                sel_code = j;
                            }
                        }
                        ArrayAdapter adapterCode = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArraysCode) {
                        };
                        code_spinner.setAdapter(adapterCode);
                        code_spinner.setSelection(sel_code);
                        code_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                code = s.string;
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });
                        List<StringWithTag> spinnerArraysGen = new ArrayList<StringWithTag>();
                        spinnerArraysGen.add(new StringWithTag(getString(R.string.selgender),""));
                        for (int j = 0; j <3 ; j++) {
                            if(j ==0){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.Male), "M"));
                            }
                            if(j ==1){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.Female), "F"));
                            }
                            if(j ==2){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.Other), "O"));
                            }
                            if(user_details.getString("gender").equals("M")){
                                selected_gen = 1;
                            }else if(user_details.getString("gender").equals("F")){
                                selected_gen = 2;
                            }else if(user_details.getString("gender").equals("O")){
                                selected_gen = 3;
                            }

                        }
                        ArrayAdapter adapterGen = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArraysGen) {
                        };
                        gender_spinner.setAdapter(adapterGen);
                        gender_spinner.setSelection(selected_gen);
                        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                gender = String.valueOf(s.tag);
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });
                        List<StringWithTag> spinnerArraysMart = new ArrayList<StringWithTag>();
                        spinnerArraysMart.add(new StringWithTag(getString(R.string.selMartial),""));
                        for (int K = 0; K <2 ; K++) {

                            if(K ==0){
                                spinnerArraysMart.add(new StringWithTag(getString(R.string.Married), "M"));
                            }
                            if(K ==1){
                                spinnerArraysMart.add(new StringWithTag(getString(R.string.Single), "S"));
                            }
                            if(user_details.getString("marital_status").equals("M")){
                                selected_mar = 1;
                            }else if(user_details.getString("marital_status").equals("S")){
                                selected_mar = 2;
                            }

                        }
                        ArrayAdapter adapter3 = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArraysMart) {
                        };
                        mar_spinner.setAdapter(adapter3);
                        mar_spinner.setSelection(selected_mar);
                        mar_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                martial = String.valueOf(s.tag);

                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });
                        List<StringWithTag> spinnerArraysTitle = new ArrayList<StringWithTag>();
                        spinnerArraysTitle.add(new StringWithTag(getString(R.string.selTitle),""));
                        for (int l = 0; l <11 ; l++) {

                            if(l ==0){
                                spinnerArraysTitle.add(new StringWithTag(getString(R.string.dr), "M"));
                            }
                            if(l ==1){
                                spinnerArraysTitle.add(new StringWithTag("Mr.", "S"));
                            }if(l ==2){
                                spinnerArraysTitle.add(new StringWithTag("Ms.", "S"));
                            }if(l ==3){
                                spinnerArraysTitle.add(new StringWithTag("Mrs.", "S"));
                            }if(l ==4){
                                spinnerArraysTitle.add(new StringWithTag("Eng.", "S"));
                            }if(l ==5){
                                spinnerArraysTitle.add(new StringWithTag("Prof.", "S"));
                            }if(l ==6){
                                spinnerArraysTitle.add(new StringWithTag("Rev.", "S"));
                            }if(l ==7){
                                spinnerArraysTitle.add(new StringWithTag("Rt. Hon.", "S"));
                            }if(l ==8){
                                spinnerArraysTitle.add(new StringWithTag("Sr.", "S"));
                            }if(l ==9){
                                spinnerArraysTitle.add(new StringWithTag("Esq.", "S"));
                            }if(l ==10){
                                spinnerArraysTitle.add(new StringWithTag("Hon.", "S"));
                            }if(user_details.getString("title").equals("Dr.")){
                                selected_title = 1;
                            }
                            else if(user_details.getString("title").equals("Mr.")){
                                selected_title = 2;
                            }else if(user_details.getString("title").equals("Ms.")){
                                selected_title = 3;
                            }else if(user_details.getString("title").equals("Eng.")){
                                selected_title = 5;
                            }else if(user_details.getString("title").equals("Prof.")){
                                selected_title = 6;
                            }else if(user_details.getString("title").equals("Rev.")){
                                selected_title = 7;
                            }else if(user_details.getString("title").equals("Rt. Hon.")){
                                selected_title = 8;
                            }else if(user_details.getString("title").equals("Sr.")){
                                selected_title = 9;
                            }else if(user_details.getString("title").equals("Esq.")){
                                selected_title = 10;
                            }else if(user_details.getString("title").equals("Hon.")){
                                selected_title = 11;
                            }
                            else if(user_details.getString("title").equals("Mrs.")){
                                selected_title = 4;
                            }

                        }
                        ArrayAdapter title_spinneradapter3 = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArraysTitle) {
                        };
                        title_spinner.setAdapter(title_spinneradapter3);
                        title_spinner.setSelection(selected_title);
                        title_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                titles = String.valueOf(s.string);

                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });
                        List<StringWithTag> spinnerArraysAge = new ArrayList<StringWithTag>();
                        spinnerArraysAge.add(new StringWithTag(getString(R.string.selAge),""));
                        for (int l = 0; l <10 ; l++) {

                            if(l ==0){
                                spinnerArraysAge.add(new StringWithTag("18-25", "M"));
                            }
                            if(l ==1){
                                spinnerArraysAge.add(new StringWithTag("26-35", "S"));
                            }if(l ==2){
                                spinnerArraysAge.add(new StringWithTag("36-45", "S"));
                            }if(l ==3){
                                spinnerArraysAge.add(new StringWithTag("46-55", "S"));
                            }if(l ==4){
                                spinnerArraysAge.add(new StringWithTag("56 above", "S"));
                            }

                            if(user_details.getString("age").equals("18-25")){
                                selected_age = 1;
                            }
                            else if(user_details.getString("age").equals("26-35")){
                                selected_age = 2;
                            }else if(user_details.getString("age").equals("36-45")){
                                selected_age = 3;
                            }else if(user_details.getString("age").equals("46-55")){
                                selected_age = 4;
                            }else if(user_details.getString("age").equals("56 above")){
                                selected_age = 5;
                            }

                        }
                        ArrayAdapter ageadapter3 = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArraysAge) {
                        };
                        age_spinner.setAdapter(ageadapter3);
                        age_spinner.setSelection(selected_age);
                        age_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                age = String.valueOf(s.string);

                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });

                        fname.setText(user_details.getString("fname"));
                        lname.setText(user_details.getString("lname"));
                        emailaddress.setText(user_details.getString("email"));
                        phone.setText(user_details.getString("phone_number"));


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }
    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

}
