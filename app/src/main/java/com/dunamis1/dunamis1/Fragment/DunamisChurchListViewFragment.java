package com.dunamis1.dunamis1.Fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Adapter.ChurchNearAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.RADIUS;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;


public class DunamisChurchListViewFragment extends Fragment {

    private String[] title_array = {"DIGC ABA CENTRAL", "DIGC UMUAHIA CENTRAL", "DIGC NUMAN", "DIGC YOLA CENTRAL", "DIGC KADUNA CENTRAL"};
    private String[] loc_array = {"158 ABA OWERRI ROAD ABA| 10 Miles", "2/4 FINBARRS ROAD, BY UMUOBASI, UMUAHIA | 12 Miles", "DUNAMIS ARENA 60 DOWAYA, YOLA ROAD, NUMAN | 10 Miles", "BACHURE ROAD, JIMETA, YOLA | 16 Miles", "DUNAMIS ARENA, BEHIND CICMA HOUSE  | 6 Miles"};
    int[] sampleImagesArr = {R.drawable.image60, R.drawable.dashchurchnear3, R.drawable.image60, R.drawable.dashcarousel, R.drawable.church4};
    private ArrayList<HashMap<String, Object>> map_list;
    private RecyclerView rv_Dash1;
    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public DunamisChurchListViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dunamis_church_list_view_one, container, false);
        activity = (Activity) container.getContext();
        rv_Dash1 = (RecyclerView) view.findViewById(R.id.rv_Dash1);
        getDep();
        return view;
    }
    public void getDep() {
        MethodClass.showProgressDialog(getActivity());
        String server_url = getActivity().getString(R.string.SERVER_URL) + "branch/list";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG","en"));
        if(!RADIUS.equals("")){
            params.put("radius",RADIUS);
        }

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONArray dunamis_church_list = result_Object.getJSONArray("dunamis_church_list");

                        map_list = new ArrayList<>();
                        for (int i = 0; i < dunamis_church_list.length(); i++) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put(TITLE, dunamis_church_list.getJSONObject(i).getJSONObject("churchdetails").getString("name"));
                            map.put(IMGG, dunamis_church_list.getJSONObject(i).getString("banner_img"));
                            map.put(NAME, dunamis_church_list.getJSONObject(i).getString("address"));
                            map.put(MEMBER, dunamis_church_list.getJSONObject(i).getString("total_members"));
                            map.put(SEED_ID, dunamis_church_list.getJSONObject(i).getString("id"));
                            map_list.add(map);
                        }
                        ChurchNearAdapter adapter = new ChurchNearAdapter(activity, map_list);
                        rv_Dash1.setAdapter(adapter);
                        rv_Dash1.setFocusable(false);

                        String country_id = result_Object.getString("country_id");
                        if(country_id.equals("161")){
                            ((DunamisChurch)getActivity()).hideFilter();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

    }


}
