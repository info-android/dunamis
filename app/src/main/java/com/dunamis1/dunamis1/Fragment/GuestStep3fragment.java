package com.dunamis1.dunamis1.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.Helper.RequestManager;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.PICK_FROM_GALLERY;
import static com.dunamis1.dunamis1.Helper.Constant.TAKE_PHOTO_CODE;

public class GuestStep3fragment extends Fragment {
    private RadioButton seeRad;
    private RadioButton metRad;
    private RadioButton prayRad;
    private RadioButton attendYes;
    private RadioButton attendNo;
    private RadioButton dunamemberYes;
    private RadioButton dunamemberNo;
    private RadioButton dunaDisipleYes;
    private RadioButton dunaDisipleNo;
    private RadioButton rededicateYes;
    private RadioButton rededicateNo;
    private Button cont;
    private RadioGroup dunamisAttend;
    private RadioGroup dunamisMember;
    private RadioGroup dunaDisiple;
    private RadioGroup rededicate;
    private EditText aboutme;

    private String dunaAttendId = "";
    private String dunamisMemberId = "";
    private String dunaDisipleId = "";
    private String rededicateId = "";
    private String urgAssisId = "";

    private LinearLayout uploac_img;
    private LinearLayout upload_img;
    private Uri outputFileUri;
    private String imageFilePath;
    private String product_image ="";
    View rootView;
    private ArrayList<String> ListImage = new ArrayList<String>();
    public GuestStep3fragment() {
        // MapFragment empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guest_edit_step3, container, false);
        //here Facebook callback Manager
        seeRad = view.findViewById(R.id.seeRad);
        metRad = view.findViewById(R.id.metRad);
        prayRad = view.findViewById(R.id.prayRad);
        dunamisAttend = view.findViewById(R.id.dunamisAttend);
        dunamisMember = view.findViewById(R.id.dunamisMember);
        dunaDisiple = view.findViewById(R.id.dunaDisiple);
        rededicate = view.findViewById(R.id.rededicate);
        aboutme = view.findViewById(R.id.aboutme);
        uploac_img = view.findViewById(R.id.uploac_img);
        upload_img = view.findViewById(R.id.upload_img);

        attendYes = view.findViewById(R.id.attendYes);
        attendNo = view.findViewById(R.id.attendNo);
        dunamemberYes = view.findViewById(R.id.dunamemberYes);
        dunamemberNo = view.findViewById(R.id.dunamemberNo);
        dunaDisipleYes = view.findViewById(R.id.dunaDisipleYes);
        dunaDisipleNo = view.findViewById(R.id.dunaDisipleNo);
        rededicateYes = view.findViewById(R.id.rededicateYes);
        rededicateNo = view.findViewById(R.id.rededicateNo);

        cont = view.findViewById(R.id.cont);
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateProfile();

            }
        });
        seeRad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                urgAssisId = "1";
                seeRad.setChecked(true);
                metRad.setChecked(false);
                prayRad.setChecked(false);
            }
        });

        metRad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                urgAssisId = "2";
                seeRad.setChecked(false);
                metRad.setChecked(true);
                prayRad.setChecked(false);
            }
        });

        prayRad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                urgAssisId = "3";
                prayRad.setChecked(true);
                seeRad.setChecked(false);
                metRad.setChecked(false);
            }
        });
        dunamisAttend.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.attendYes){
                    dunaAttendId = "Y";
                }
                if(checkedId == R.id.attendNo){
                    dunaAttendId = "N";
                }
            }
        });
        dunamisMember.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.dunamemberYes){
                    dunamisMemberId = "Y";
                }
                if(checkedId == R.id.dunamemberNo){
                    dunamisMemberId = "N";
                }
            }
        });
        dunaDisiple.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.dunaDisipleYes){
                    dunaDisipleId = "Y";
                }
                if(checkedId == R.id.dunaDisipleNo){
                    dunaDisipleId = "N";
                }
            }
        });
        rededicate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.rededicateYes){
                    rededicateId = "Y";
                }
                if(checkedId == R.id.rededicateNo){
                    rededicateId = "N";
                }
            }
        });
        uploac_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu= new PopupMenu(getActivity(),uploac_img);
                popupMenu.getMenuInflater().inflate(R.menu.camera_popup_menu,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id=item.getItemId();
                        if (id== R.id.camera){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                                }
                                else{
                                    CameraIntent();
                                }
                            }
                        }
                        else if (id==R.id.gallery){
                            try {
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                                } else {
                                    galleryIntent();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        getDetails();
        return view;
    }
    public void updateProfile(){
        if(dunaAttendId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDunaAt), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(dunamisMemberId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDunaMem), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(urgAssisId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseUrgAss), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(dunaDisipleId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDisiple), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(rededicateId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseRededicate), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(aboutme.getText().toString().trim().length() == 0){
            aboutme.setError(getActivity().getString(R.string.abtReq));
            aboutme.requestFocus();
            return;
        }

        if (MethodClass.isNetworkConnected(getActivity())) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = getString(R.string.SERVER_URL) + "guest-update/step-three";
            SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    MethodClass.hideProgressDialog(getActivity());
                    try {
                        JSONObject jsonObject = MethodClass.get_result_from_webservice(getActivity(), new JSONObject(response));
                        if (jsonObject != null) {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(jsonObject.getString("message"))
                                    .setContentText(jsonObject.getString("meaning"))
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            Intent I = new Intent(getActivity(),ProfileActivity.class);
                                            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                            getActivity().startActivity(I);
                                        }
                                    })
                                    .show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", error.toString());
                    MethodClass.hideProgressDialog(getActivity());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(getActivity());
                    } else {
                        MethodClass.error_alert(getActivity());
                    }
                }
            }){
                // Passing some request headers
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            RequestQueue requestQueue = RequestManager.getnstance(getActivity());

            Log.e("updateProfile", dunaAttendId+""+dunamisMemberId+""+urgAssisId+""+dunaDisipleId+""+rededicateId );

            simpleMultiPartRequest.addMultipartParam("about_me", "text", aboutme.getText().toString().trim());
            simpleMultiPartRequest.addMultipartParam("attend_foundation_class", "text", dunaAttendId);
            simpleMultiPartRequest.addMultipartParam("interested_membership", "text", dunamisMemberId);
            simpleMultiPartRequest.addMultipartParam("assitance_required", "text", urgAssisId);
            simpleMultiPartRequest.addMultipartParam("disciple_class", "text", dunaDisipleId);
            simpleMultiPartRequest.addMultipartParam("rededicated_life_today", "text", rededicateId);
            //simpleMultiPartRequest.addMultipartParam("email", "text", get_email);
            simpleMultiPartRequest.addMultipartParam("count","text", String.valueOf(ListImage.size()));
            if(ListImage.size()>0){
                for (int i = 0; i <ListImage.size() ; i++) {
                    simpleMultiPartRequest.addFile("profile_images"+(i+1), ListImage.get(i).toString());
                }
            }

            simpleMultiPartRequest.setFixedStreamingMode(true);
            int socketTimeout = 100000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
            simpleMultiPartRequest.setRetryPolicy(policy);
            requestQueue.getCache().clear();
            requestQueue.add(simpleMultiPartRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }

    public void getDetails(){
        MethodClass.showProgressDialog(getActivity());
        String server_url = getActivity().getString(R.string.SERVER_URL) + "guest-details/step-three";
        HashMap<String, String> params = new HashMap<String, String>();

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONObject user_details = result_Object.getJSONObject("user");
                        dunaAttendId  = user_details.getString("attend_foundation_class");
                        dunamisMemberId  = user_details.getString("interested_membership");
                        urgAssisId  = user_details.getString("assitance_required");
                        dunaDisipleId  = user_details.getString("disciple_class");
                        rededicateId  = user_details.getString("rededicated_life_today");
                        String about_me  = user_details.getString("about_me");

                        if(urgAssisId.equals("1")){
                            seeRad.setChecked(true);
                        }else if(urgAssisId.equals("2")){
                            metRad.setChecked(true);
                        }else {
                            prayRad.setChecked(true);
                        }

                        if(dunaAttendId.equals("Y")){
                            attendYes.setChecked(true);
                        }else if(dunaAttendId.equals("N")){
                            attendNo.setChecked(true);
                        }
                        if(dunamisMemberId.equals("Y")){
                            dunamemberYes.setChecked(true);
                        }else if(dunamisMemberId.equals("N")){
                            dunamemberNo.setChecked(true);
                        }
                        if(dunaDisipleId.equals("Y")){
                            dunaDisipleYes.setChecked(true);
                        }else if(dunaDisipleId.equals("N")){
                            dunaDisipleNo.setChecked(true);
                        }
                        if(rededicateId.equals("Y")){
                            rededicateYes.setChecked(true);
                        }else if(rededicateId.equals("N")){
                            rededicateNo.setChecked(true);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }

    //for image upload

    //for multipicture

    private void CameraIntent(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();

        }catch (Exception e){
            e.printStackTrace();
        }
        outputFileUri = FileProvider.getUriForFile(getActivity(), "${applicationId}.fileprovider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,outputFileUri);
        Log.e("Intentdata", intent.toString() );
        startActivityForResult(intent, TAKE_PHOTO_CODE);

    }

    private File createImageFile() throws IOException {
        File image = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // upload_map_list= new ArrayList<HashMap<String,String>>();
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY) {
                Uri selectedUri;
                if (null != data) { // checking empty selection
                    if (null != data.getClipData()) { // checking multiple selection or not
                        if(data.getClipData().getItemCount()+ListImage.size() > 1){
                            ListImage = new ArrayList<String>();
                        }
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            selectedUri = data.getClipData().getItemAt(i).getUri();
                            try {
                                product_image = getPath(getActivity(), selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        if(!data.getData().getAuthority().equals("com.android.providers.downloads.documents")){
                            selectedUri = data.getData();
                            try {
                                product_image = getPath(getActivity(), selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.e("SIZE", String.valueOf(ListImage.size()));
                            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),"Please choose image from other folder", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    }
                    Log.e("SIZE", String.valueOf(ListImage.size()));
                    showImage();
                }
            } else if (requestCode == TAKE_PHOTO_CODE) {

                product_image = imageFilePath;
                ListImage.add(product_image);
                showImage();
            }

        }
        else {
            upload_img.setVisibility(View.GONE);
        }
    }
    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        Log.e("LOGURI", uri.getScheme() );
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private void showImage(){
        MethodClass.hideProgressDialog(getActivity());
        upload_img.removeAllViews();
        if(ListImage.size()>0){
            for (int i = 0; i < ListImage.size(); i++) {

                View child = LayoutInflater.from(getContext()).inflate(R.layout.upload_img_gv, null);
                final ImageView image = (ImageView) child.findViewById(R.id.upload_imgs);
                final ImageButton dlt_btn = (ImageButton) child.findViewById(R.id.dlt_btn);
                File imgFile = new  File(ListImage.get(i));
                if(imgFile.exists()){
                    Bitmap b = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    image.setImageBitmap(Bitmap.createScaledBitmap(b, 120, 120, false));
                    //image.setImageBitmap(myBitmap);
                }
                final int finalI = i;
                upload_img.addView(child);
            }

        }
    }
}
