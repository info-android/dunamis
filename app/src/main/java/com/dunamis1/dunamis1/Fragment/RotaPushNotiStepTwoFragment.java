package com.dunamis1.dunamis1.Fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.AddNoticeActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ManageHomeChurchMembersActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Adapter.ManageHomeChurchMemberAdapter;
import com.dunamis1.dunamis1.Adapter.RotaStepTwoAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.AGE;
import static com.dunamis1.dunamis1.Helper.Constant.APPROVED;
import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_ID;
import static com.dunamis1.dunamis1.Helper.Constant.COUNTRY_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.EMAIL;
import static com.dunamis1.dunamis1.Helper.Constant.FNAME;
import static com.dunamis1.dunamis1.Helper.Constant.GENDER;
import static com.dunamis1.dunamis1.Helper.Constant.ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.LNAME;
import static com.dunamis1.dunamis1.Helper.Constant.MARITAL;
import static com.dunamis1.dunamis1.Helper.Constant.MESSAGE;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.NOTI_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.PHONE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.USER_SELECTED_MEMBER;

public class RotaPushNotiStepTwoFragment extends Fragment {

    private RecyclerView step_two_rv;
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;
    private String[] name_array = {"Anthony James", "Obi", "Paul Alfred", "Uju", "Lola Babs", "Emeka", "Olivia",
             "Lola"};
    private Integer[] pic_array = {R.drawable.man1, R.drawable.man2, R.drawable.man3, R.drawable.man4,
            R.drawable.man5, R.drawable.man6, R.drawable.man7,
            R.drawable.man2};
    private Button cont;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public RotaPushNotiStepTwoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rota_push_noti_step_two, container, false);
        activity = (Activity) container.getContext();
        step_two_rv=(RecyclerView)view.findViewById(R.id.step_two_rv);
        cont=(Button) view.findViewById(R.id.cont);
        get_list();
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNoti();
            }
        });
        return view;
    }
    private void get_list(){
        MethodClass.showProgressDialog(getActivity());
        String server_url = "";
        if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("role","Member").equals("Department")){
            server_url = getActivity().getString(R.string.SERVER_URL) + "church-department/members";
        }else {
            server_url = getActivity().getString(R.string.SERVER_URL) + "home-church/members";
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG","en"));
        params.put("type","M");
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONArray seedArray = result_Object.getJSONArray("members");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            HashMap<String,Object> map = new HashMap<>();

                            if(!seedArray.getJSONObject(i).getString("user").equals("null")){
                                JSONObject SeedObj = seedArray.getJSONObject(i).getJSONObject("user");
                                String title = SeedObj.getString("title");
                                String fname = SeedObj.getString("fname");
                                String lname = SeedObj.getString("lname");
                                String email = SeedObj.getString("email");
                                String country_code = SeedObj.getString("country_code");
                                String phone_number = SeedObj.getString("phone_number");
                                String profile_pic = SeedObj.getString("profile_pic");
                                String is_approved = SeedObj.getString("is_approved");
                                String age = SeedObj.getString("age");
                                String gender = SeedObj.getString("gender");
                                String marital_status = SeedObj.getString("marital_status");
                                String id = seedArray.getJSONObject(i).getString("id");

                                map.put(TITLE,title);
                                map.put(FNAME,fname);
                                map.put(LNAME,lname);
                                map.put(EMAIL,email);
                                map.put(PHONE,phone_number);
                                map.put(AGE,age);
                                map.put(GENDER,gender);
                                map.put(MARITAL,marital_status);
                                map.put(IMGG,profile_pic);
                                map.put(COUNTRY_CODE,country_code);

                                map.put(SEED_ID,id);
                            }else {
                                String id = seedArray.getJSONObject(i).getString("id");
                                map.put(TITLE,"");
                                map.put(FNAME,"");
                                map.put(LNAME,"");
                                map.put(EMAIL,"");
                                map.put(PHONE,"");
                                map.put(AGE,"");
                                map.put(GENDER,"");
                                map.put(MARITAL,"");
                                map.put(IMGG,"");
                                map.put(COUNTRY_CODE,"");
                                map.put(SEED_ID,id);
                            }
                            map_list.add(map);
                        }
                        RotaStepTwoAdapter adapter = new RotaStepTwoAdapter(activity, map_list);
                        step_two_rv.setAdapter(adapter);
                        step_two_rv.setFocusable(false);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }

    public void sendNoti(){
        if(NOTI_TITLE.equals("")){
            Toast.makeText(getActivity(),getActivity().getString(R.string.notiTitleReq),Toast.LENGTH_LONG).show();
            return;
        }
        if(MESSAGE.equals("")){
            Toast.makeText(getActivity(),getActivity().getString(R.string.notiMsgReq),Toast.LENGTH_LONG).show();
            return;
        }
        if(USER_SELECTED_MEMBER.size()>0) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = "";
            HashMap<String, String> params = new HashMap<String, String>();
            if (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("role", "Member").equals("Department")) {
                server_url = getActivity().getString(R.string.SERVER_URL) + "notification/add";
                params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG", "en"));
                params.put("title", NOTI_TITLE);
                params.put("message", MESSAGE);
                params.put("member_type", "M");
                params.put("members",TextUtils.join(",",USER_SELECTED_MEMBER));
                params.put("type", "D");
            } else {
                server_url = getActivity().getString(R.string.SERVER_URL) + "notification/add";
                params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG", "en"));
                params.put("title", NOTI_TITLE);
                params.put("message", MESSAGE);
                params.put("member_type", "M");
                params.put("members",TextUtils.join(",",USER_SELECTED_MEMBER));
                params.put("type", "H");
            }


            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("resp", response.toString());
                    try {
                        MethodClass.hideProgressDialog(getActivity());

                        JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                        if (result_Object != null) {
                            final String message = result_Object.getString("message");
                            final String meaning = result_Object.getString("meaning");
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(message)
                                    .setContentText(meaning)
                                    .setConfirmText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            getActivity().finish();
                                        }
                                    })
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(getActivity());
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(getActivity());

                    Log.e("error", error.toString());
                    if (error.toString().contains("AuthFailureError")) {
                        Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        }else{
            Toast.makeText(getActivity(),getActivity().getString(R.string.chooseMemNoti),Toast.LENGTH_LONG).show();
            return;
        }
    }

}
