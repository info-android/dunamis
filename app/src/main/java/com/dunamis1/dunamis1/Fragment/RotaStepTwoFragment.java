package com.dunamis1.dunamis1.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dunamis1.dunamis1.Activity.ChurchDepartment.RotaStep3Activity;
import com.dunamis1.dunamis1.Adapter.RotaStepTwoAdapter;
import com.dunamis1.dunamis1.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;

public class RotaStepTwoFragment extends Fragment {

    private RecyclerView step_two_rv;
    private Activity activity;
    private ArrayList<HashMap<String, Object>> map_list;
    private String[] name_array = {"Anthony James", "Obi", "Paul Alfred", "Uju", "Lola Babs", "Emeka", "Olivia",
             "Lola"};
    private Integer[] pic_array = {R.drawable.man1, R.drawable.man2, R.drawable.man3, R.drawable.man4,
            R.drawable.man5, R.drawable.man6, R.drawable.man7,
            R.drawable.man2};

    private Button cont;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public RotaStepTwoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rota_step_two, container, false);
        activity = (Activity) container.getContext();
        step_two_rv=(RecyclerView)view.findViewById(R.id.step_two_rv);
        cont=(Button) view.findViewById(R.id.cont);
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(getActivity(), RotaStep3Activity.class);
                getActivity().startActivity(I);
            }
        });
        get_list();

        return view;
    }
    private void get_list(){
        map_list = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < pic_array.length; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put(IMG, pic_array[i]);
            map.put(NAME, name_array[i]);
            map_list.add(map);
        }
        RotaStepTwoAdapter adapter = new RotaStepTwoAdapter(activity, map_list);
        step_two_rv.setAdapter(adapter);
        step_two_rv.setFocusable(false);
    }



}
