package com.dunamis1.dunamis1.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.Helper.RequestManager;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.PICK_FROM_GALLERY;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.TAKE_PHOTO_CODE;

public class MemberStep2Fragment extends Fragment {
    private Spinner country_spinner;
    private EditText chrchDep;

    private EditText founda;
    private EditText aboutme;
    private Button cont;
    private String user_id = "";
    private String branch_id = "";
    private String dept_id = "";
    private String cucrhcLoca = "";
    private LinearLayout uploac_img;
    private LinearLayout upload_img;
    private ArrayList<Integer> selDep;
    private Integer selctedChurh = 0;
    private Uri outputFileUri;
    private String imageFilePath;
    private String product_image ="";
    View rootView;
    private ArrayList<String> ListImage = new ArrayList<String>();
    private ArrayList<String> depName;

    public MemberStep2Fragment() {
        // MapFragment empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_member_step2, container, false);
        user_id = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("user_id","");
        //here Facebook callback Manager
        country_spinner = rootView.findViewById(R.id.country_spinner);
        chrchDep = rootView.findViewById(R.id.chrchDep);
        founda = rootView.findViewById(R.id.founda);
        cont = rootView.findViewById(R.id.cont);
        aboutme = rootView.findViewById(R.id.aboutme);
        uploac_img = rootView.findViewById(R.id.uploac_img);
        upload_img = rootView.findViewById(R.id.upload_img);

        founda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, (month));
                        calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                        String myFormat = "dd-MM-yyyy";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        founda.setText(sdf.format(calendar.getTime()));
                    }
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
                dialog.show();
            }
        });

        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });
        uploac_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu= new PopupMenu(getActivity(),uploac_img);
                popupMenu.getMenuInflater().inflate(R.menu.camera_popup_menu,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id=item.getItemId();
                        if (id== R.id.camera){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                                }
                                else{
                                    CameraIntent();
                                }
                            }
                        }
                        else if (id==R.id.gallery){
                            try {
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                                } else {
                                    galleryIntent();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        getDetails();
        return rootView;
    }
    public void updateProfile(){
        if(aboutme.getText().toString().trim().length() == 0){
            aboutme.setError(getActivity().getString(R.string.abtReq));
            aboutme.requestFocus();
            return;
        }
        if(branch_id.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseChurchBanch), Snackbar.LENGTH_LONG);
            snackbar.show();
            country_spinner.requestFocus();
            return;
        }
        if(dept_id.equals("")){
            chrchDep.setError(getString(R.string.chooseDept));
            chrchDep.requestFocus();
            return;
        }

        if (MethodClass.isNetworkConnected(getActivity())) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = getString(R.string.SERVER_URL) + "user-update/step-two";
            SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    MethodClass.hideProgressDialog(getActivity());
                    try {
                        JSONObject jsonObject = MethodClass.get_result_from_webservice(getActivity(), new JSONObject(response));
                        if (jsonObject != null) {

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(jsonObject.getString("message"))
                                    .setContentText(jsonObject.getString("meaning"))
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            Intent I = new Intent(getActivity(),ProfileActivity.class);
                                            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                            getActivity().startActivity(I);
                                        }
                                    })
                                    .show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", error.toString());
                    MethodClass.hideProgressDialog(getActivity());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(getActivity());
                    } else {
                        MethodClass.error_alert(getActivity());
                    }
                }
            }){
                // Passing some request headers
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            RequestQueue requestQueue = RequestManager.getnstance(getActivity());


            simpleMultiPartRequest.addMultipartParam("about_me", "text", aboutme.getText().toString().trim());
            simpleMultiPartRequest.addMultipartParam("church_location_id", "text", branch_id);
            simpleMultiPartRequest.addMultipartParam("church_department_id", "text", dept_id);
            simpleMultiPartRequest.addMultipartParam("completion_date", "text", founda.getText().toString().trim());
            //simpleMultiPartRequest.addMultipartParam("email", "text", get_email);
            simpleMultiPartRequest.addMultipartParam("count","text", String.valueOf(ListImage.size()));
            if(ListImage.size()>0){
                for (int i = 0; i <ListImage.size() ; i++) {
                    simpleMultiPartRequest.addFile("profile_images"+(i+1), ListImage.get(i).toString());
                }
            }

            simpleMultiPartRequest.setFixedStreamingMode(true);
            int socketTimeout = 100000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
            simpleMultiPartRequest.setRetryPolicy(policy);
            requestQueue.getCache().clear();
            requestQueue.add(simpleMultiPartRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

    }

    public void getDetails(){
        MethodClass.showProgressDialog(getActivity());
        String server_url = getActivity().getString(R.string.SERVER_URL) + "user-details/step-two";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONObject user_details = result_Object.getJSONObject("user");

                        if(!user_details.getString("about_me").equals(null) && !user_details.getString("about_me").equals("null")){
                            aboutme.setText(user_details.getString("about_me"));
                        }

                        founda.setText(user_details.getString("completion_date"));
                        cucrhcLoca  = user_details.getString("church_branch_id");
                        JSONArray department = user_details.getJSONArray("department");

                        selDep = new ArrayList<Integer>();
                        for (int i = 0; i < department.length(); i++) {
                            selDep.add(Integer.parseInt(department.getJSONObject(i).getString("department_id")));
                        }

                        String phot = user_details.getString("profile_image");
                        upload_img.removeAllViews();
                        try{
                            JSONArray photoArr = new JSONArray(phot);
                            for (int j = 0; j < photoArr.length(); j++) {
                                View child = LayoutInflater.from(getContext()).inflate(R.layout.upload_img_gv, null);
                                final ImageView image = (ImageView) child.findViewById(R.id.upload_imgs);
                                final ImageButton dlt_btn = (ImageButton) child.findViewById(R.id.dlt_btn);
                                Picasso.get().load(PROFILE_IMG_URL+photoArr.getJSONObject(j).getString("profile_image")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(image);

                                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("profile_pic",photoArr.getJSONObject(j).getString("profile_image")).commit();
                                upload_img.addView(child);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }



                        getChurchLoc();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }
    public void getChurchLoc(){
        if (MethodClass.isNetworkConnected(getActivity())) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = getString(R.string.SERVER_URL) + "church-location";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("language",PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG","en"));
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(getActivity());

                        JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                        if (result_Object != null) {

                            JSONArray locationArray = result_Object.getJSONArray("location");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selctChurchLoc),""));
                            for (int i = 0; i <locationArray.length() ; i++) {

                                String city_id = locationArray.getJSONObject(i).getString("id");
                                JSONObject churchdetails = locationArray.getJSONObject(i).getJSONObject("churchdetails");
                                String cityname = churchdetails.getString("name");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                                if(cucrhcLoca.equals(city_id)){
                                    selctedChurh = i+1;
                                }

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArrays2) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setSelection(selctedChurh);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    branch_id = String.valueOf(s.tag);
                                    getDepartment(branch_id);

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(getActivity());
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(getActivity());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(getActivity());
                    } else {
                        MethodClass.error_alert(getActivity());
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getDepartment(String branc_id){
        if (MethodClass.isNetworkConnected(getActivity())) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = getString(R.string.SERVER_URL) + "church-department";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("location_id",branc_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(getActivity());

                        JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                        if (result_Object != null) {

                            JSONArray districtArray = result_Object.getJSONArray("department");
                            depName = new ArrayList<String>();
                            ArrayList<MultiSelectModel> listOfCountries= new ArrayList<>();
                            for (int i = 0; i <districtArray.length() ; i++) {
                                JSONObject department = districtArray.getJSONObject(i).getJSONObject("department");
                                listOfCountries.add(new MultiSelectModel(Integer.parseInt(districtArray.getJSONObject(i).getString("department_name_id")),department.getString("department_name")));

                                if(selDep.contains(Integer.parseInt(districtArray.getJSONObject(i).getString("department_name_id")))){
                                    depName.add(department.getString("department_name"));
                                }

                            }
                            chrchDep.setText(TextUtils.join(",",depName));
                            dept_id = TextUtils.join(",",selDep);
                            final MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                                    .title(getResources().getString(R.string.chrchDep)) //setting title for dialog
                                    .titleSize(25)
                                    .positiveText("Done")
                                    .negativeText(getResources().getString(R.string.dialog_cancel))
                                    .setMinSelectionLimit(1)
                                    .setMaxSelectionLimit(2)
                                    .preSelectIDsList(selDep)
                                    .multiSelectList(listOfCountries) // the multi select model list with ids and name
                                    .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                        @Override
                                        public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                            //will return list of selected IDS
                                            for (int s = 0; s < selectedIds.size(); s++) {
                                                chrchDep.setText(dataString);
                                                dept_id = TextUtils.join(",",selectedIds);
                                            }
                                        }
                                        @Override
                                        public void onCancel() {

                                        }
                                    });
                            chrchDep.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    multiSelectDialog.show(getActivity().getSupportFragmentManager(), "multiSelectDialog");
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(getActivity());
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(getActivity());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(getActivity());
                    } else {
                        MethodClass.error_alert(getActivity());
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }


    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }



    //for multipicture

    private void CameraIntent(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();

        }catch (Exception e){
            e.printStackTrace();
        }
        outputFileUri = FileProvider.getUriForFile(getActivity(), "${applicationId}.fileprovider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,outputFileUri);
        Log.e("Intentdata", intent.toString() );
        startActivityForResult(intent, TAKE_PHOTO_CODE);

    }

    private File createImageFile() throws IOException {
        File image = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // upload_map_list= new ArrayList<HashMap<String,String>>();
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY) {
                Uri selectedUri;
                if (null != data) { // checking empty selection
                    if (null != data.getClipData()) { // checking multiple selection or not
                        if(data.getClipData().getItemCount()+ListImage.size() > 1){
                            ListImage = new ArrayList<String>();
                        }
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            selectedUri = data.getClipData().getItemAt(i).getUri();
                            try {
                                product_image = getPath(getActivity(), selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        if(!data.getData().getAuthority().equals("com.android.providers.downloads.documents")){
                            selectedUri = data.getData();
                            try {
                                product_image = getPath(getActivity(), selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.e("SIZE", String.valueOf(ListImage.size()));
                            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),"Please choose image from other folder", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    }
                    Log.e("SIZE", String.valueOf(ListImage.size()));
                    showImage();
                }
            } else if (requestCode == TAKE_PHOTO_CODE) {

                product_image = imageFilePath;
                ListImage.add(product_image);
                showImage();
            }

        }
        else {
            upload_img.setVisibility(View.GONE);
        }
    }
    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        Log.e("LOGURI", uri.getScheme() );
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private void showImage(){
        MethodClass.hideProgressDialog(getActivity());
        upload_img.removeAllViews();
        if(ListImage.size()>0){
            for (int i = 0; i < ListImage.size(); i++) {

                View child = LayoutInflater.from(getContext()).inflate(R.layout.upload_img_gv, null);
                final ImageView image = (ImageView) child.findViewById(R.id.upload_imgs);
                final ImageButton dlt_btn = (ImageButton) child.findViewById(R.id.dlt_btn);
                File imgFile = new  File(ListImage.get(i));
                if(imgFile.exists()){
                    Bitmap b = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    image.setImageBitmap(Bitmap.createScaledBitmap(b, 120, 120, false));
                    //image.setImageBitmap(myBitmap);
                }
                final int finalI = i;
                upload_img.addView(child);
            }

        }
    }
}
