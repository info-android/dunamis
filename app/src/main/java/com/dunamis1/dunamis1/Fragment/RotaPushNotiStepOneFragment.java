package com.dunamis1.dunamis1.Fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.Constant;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_ID;
import static com.dunamis1.dunamis1.Helper.Constant.ID;
import static com.dunamis1.dunamis1.Helper.Constant.MESSAGE;
import static com.dunamis1.dunamis1.Helper.Constant.USER_SELECTED_MEMBER;


public class RotaPushNotiStepOneFragment extends Fragment {

    private RecyclerView order_hist_rv;
    private ArrayList<HashMap<String, String>> map_list;
    private Activity activity;

    private Button cont;
    private RadioGroup  radgrp;
    private EditText message;
    private EditText title;
    String next="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public RotaPushNotiStepOneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rota_push_noti_step_one, container, false);
        activity = (Activity) container.getContext();

        cont = view.findViewById(R.id.cont);
        radgrp = view.findViewById(R.id.radgrp);
        message = view.findViewById(R.id.message);
        title = view.findViewById(R.id.title);
        radgrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)radgrp.findViewById(R.id.custom);
                RadioButton checkedRadioButton2 = (RadioButton)radgrp.findViewById(R.id.all);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    // Changes the textview's text to "Checked: example radiobutton text"
                    cont.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(title.getText().toString().trim().length() == 0){
                                title.setError(getActivity().getString(R.string.notiTitleReq));
                                title.requestFocus();
                                return;
                            }
                            if(message.getText().toString().trim().length() == 0){
                                message.setError(getActivity().getString(R.string.notiMsgReq));
                                message.requestFocus();
                                return;
                            }
                            Constant.MESSAGE = message.getText().toString().trim();
                            Constant.NOTI_TITLE = title.getText().toString().trim();
                            Fragment someFragment = new RotaPushNotiStepTwoFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_layout, someFragment ); // give your fragment container id in first parameter
                            transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                            transaction.commit();
                        }
                    });
                }
                boolean isCheckeds = checkedRadioButton2.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isCheckeds)
                {
                    // Changes the textview's text to "Checked: example radiobutton text"
                    cont.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitMessgae();
                        }
                    });
                }
            }
        });


        return view;
    }

    public void submitMessgae(){
        if(title.getText().toString().trim().length() == 0){
            title.setError(getActivity().getString(R.string.notiTitleReq));
            title.requestFocus();
            return;
        }
        if(message.getText().toString().trim().length() == 0){
            message.setError(getActivity().getString(R.string.notiMsgReq));
            message.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(getActivity());
        String server_url = "";
        HashMap<String, String> params = new HashMap<String, String>();
        if (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("role", "Member").equals("Department")) {
            server_url = getActivity().getString(R.string.SERVER_URL) + "notification/add";
            params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG", "en"));
            params.put("title", title.getText().toString().trim());
            params.put("message", message.getText().toString().trim());
            params.put("member_type", "A");
            params.put("type", "D");
        } else {
            server_url = getActivity().getString(R.string.SERVER_URL) + "notification/add";
            params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG", "en"));
            params.put("title",title.getText().toString().trim());
            params.put("message", message.getText().toString().trim());
            params.put("member_type", "A");
            params.put("type", "H");
        }


        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try {
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        final String message = result_Object.getString("message");
                        final String meaning = result_Object.getString("meaning");
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message)
                                .setContentText(meaning)
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        getActivity().finish();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }

}
