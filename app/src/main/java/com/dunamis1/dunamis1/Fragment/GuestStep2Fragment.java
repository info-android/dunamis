package com.dunamis1.dunamis1.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.GuestEditProfileStep1Activity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuestStep2Fragment  extends Fragment {
    private Spinner country_spinner;
    private Spinner dist_spinner;
    private Spinner city_spinner;
    private Spinner hear_spinner;
    private LinearLayout dstlay;
    private Button cont;
    private String country_id = "";
    private String cityId = "";
    private String distId = "";
    private String branch_id = "";
    private String hear_id = "";

    private Integer sel_coun = 0,sel_city = 0,sel_dist=0,sel_hear = 0;

    private RadioGroup visitOut,memanother,bornAgain,baptized;
    private String visitId = "";
    private String memanotherId = "";
    private String bornAgainId = "";
    private String baptizedId = "";
    private RadioButton churchRad,frndRad,flyRad,yes,no,memYes,memNo,bornYes,bornNo,bapYes,bapNo;
    public GuestStep2Fragment() {
        // MapFragment empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guest_edit_step2, container, false);
        //here Facebook callback Manager
        country_spinner = view.findViewById(R.id.country_spinner);
        dist_spinner = view.findViewById(R.id.dist_spinner);
        city_spinner = view.findViewById(R.id.city_spinner);
        hear_spinner = view.findViewById(R.id.hear_spinner);
        dstlay = view.findViewById(R.id.dstlay);
        cont = view.findViewById(R.id.cont);
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileOne();
            }
        });

        visitOut  = view.findViewById(R.id.visitOut);
        memanother  = view.findViewById(R.id.memanother);
        bornAgain  = view.findViewById(R.id.bornAgain);
        baptized  = view.findViewById(R.id.baptized);
        yes  = view.findViewById(R.id.yes);
        no  = view.findViewById(R.id.no);
        memYes  = view.findViewById(R.id.memYes);
        memNo  = view.findViewById(R.id.memNo);
        bornYes  = view.findViewById(R.id.bornYes);
        bornNo  = view.findViewById(R.id.bornNo);
        bapYes  = view.findViewById(R.id.bapYes);
        bapNo  = view.findViewById(R.id.bapNo);
        visitOut.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.yes){
                    visitId = "Y";
                }
                if(checkedId == R.id.no){
                    visitId = "N";
                }
            }
        });
        memanother.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.memYes){
                    memanotherId = "Y";
                }
                if(checkedId == R.id.memNo){
                    memanotherId = "N";
                }
            }
        });
        bornAgain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.bornYes){
                    bornAgainId = "Y";
                }
                if(checkedId == R.id.bornNo){
                    bornAgainId = "N";
                }
            }
        });
        baptized.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.bapYes){
                    baptizedId = "Y";
                }
                if(checkedId == R.id.bapNo){
                    baptizedId = "N";
                }
            }
        });

        getDetails();
        return view;
    }
    public void updateProfileOne(){

        if(country_id.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCount), Snackbar.LENGTH_LONG);
            snackbar.show();
            country_spinner.requestFocus();
            return;
        }if(cityId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCity), Snackbar.LENGTH_LONG);
            snackbar.show();
            city_spinner.requestFocus();
            return;
        }
        if(dstlay.getVisibility() == getView().VISIBLE){
            if(distId.equals("")){
                View parentLayout = getActivity().findViewById(android.R.id.content);
                Snackbar snackbar = Snackbar
                        .make(parentLayout, getString(R.string.chooseDist), Snackbar.LENGTH_LONG);
                snackbar.show();
                dist_spinner.requestFocus();
                return;
            }
        }

        if(hear_id.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseHear), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(visitId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseVisit), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(memanotherId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseMem), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(bornAgainId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseBorn), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }if(baptizedId.equals("")){
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseBap), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }

        MethodClass.showProgressDialog(getActivity());
        String server_url = getString(R.string.SERVER_URL) + "guest-update/step-two";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("country_id", country_id);
        params.put("city_id", cityId);
        if(dstlay.getVisibility() == getView().VISIBLE){
            params.put("district_id", distId);
        }

        params.put("hear_about_us",hear_id);
        params.put("out_of_town",visitId);
        params.put("another_church",memanotherId);
        params.put("born_again",bornAgainId);
        params.put("baptized_in_water",baptizedId);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        String message = result_Object.getString("message");
                        ((GuestEditProfileStep1Activity)getActivity()).step3part();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }
    public void getDetails(){
        MethodClass.showProgressDialog(getActivity());
        String server_url = getActivity().getString(R.string.SERVER_URL) + "guest-details/step-two";
        HashMap<String, String> params = new HashMap<String, String>();

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONObject user_details = result_Object.getJSONObject("user");
                        country_id  = user_details.getString("country_id");
                        cityId  = user_details.getString("city_id");
                        distId  = user_details.getString("district_id");
                        hear_id  = user_details.getString("hear_about_us");
                        visitId  = user_details.getString("out_of_town");
                        memanotherId  = user_details.getString("another_church");
                        bornAgainId  = user_details.getString("born_again");
                        baptizedId  = user_details.getString("baptized_in_water");

                        List<StringWithTag> spinnerArraysGen = new ArrayList<StringWithTag>();
                        spinnerArraysGen.add(new StringWithTag(getString(R.string.selHearAbt),""));
                        for (int j = 0; j <6 ; j++) {
                            if(j ==0){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.chruchMem), "1"));
                            }
                            if(j ==1){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.frnd), "2"));
                            }
                            if(j ==2){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.flyer), "3"));
                            }if(j ==3){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.evan), "4"));
                            }if(j ==4){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.medTv), "5"));
                            }if(j ==5){
                                spinnerArraysGen.add(new StringWithTag(getString(R.string.postr), "6"));
                            }
                            if(hear_id.equals("1")){
                                sel_hear = 1;
                            }else if(hear_id.equals("2")){
                                sel_hear = 2;
                            }else if(hear_id.equals("3")){
                                sel_hear = 3;
                            }else if(hear_id.equals("4")){
                                sel_hear = 4;
                            }else if(hear_id.equals("5")){
                                sel_hear = 5;
                            }else if(hear_id.equals("6")){
                                sel_hear = 6;
                            }

                        }
                        ArrayAdapter adapterGen = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArraysGen) {
                        };
                        hear_spinner.setAdapter(adapterGen);
                        hear_spinner.setSelection(sel_hear);
                        hear_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                hear_id = String.valueOf(s.tag);
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });
                        if(memanotherId.equals("Y")){
                            memYes.setChecked(true);
                        }else if(memanotherId.equals("N")){
                            memNo.setChecked(true);
                        }
                        if(visitId.equals("Y")){
                            yes.setChecked(true);
                        }else if(visitId.equals("N")){
                            no.setChecked(true);
                        }
                        if(bornAgainId.equals("Y")){
                            bornYes.setChecked(true);
                        }else if(bornAgainId.equals("N")){
                            bornNo.setChecked(true);
                        }
                        if(baptizedId.equals("Y")){
                            bapYes.setChecked(true);
                        }else if(visitId.equals("N")){
                            bapNo.setChecked(true);
                        }

                        getCountry();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }
    public void getCountry(){
        if (MethodClass.isNetworkConnected(getActivity())) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = getString(R.string.SERVER_URL) + "country";
            HashMap<String, String> params = new HashMap<String, String>();
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(getActivity());

                        JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                        if (result_Object != null) {

                            JSONArray countryArray = result_Object.getJSONArray("country");

                            List<StringWithTag> spinnerArraysCoun = new ArrayList<StringWithTag>();
                            spinnerArraysCoun.add(new StringWithTag(getString(R.string.selCountry),""));
                            for (int i = 0; i <countryArray.length() ; i++) {

                                String counid = countryArray.getJSONObject(i).getString("id");
                                String countryname = countryArray.getJSONObject(i).getString("countryname");
                                spinnerArraysCoun.add(new StringWithTag(countryname, counid));

                                if(country_id.equals(counid)){
                                    sel_coun = i+1;
                                }

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArraysCoun) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setSelection(sel_coun);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    country_id = String.valueOf(s.tag);
                                    if(country_id.equals("161")){
                                        dstlay.setVisibility(View.VISIBLE);
                                    }else {
                                        dstlay.setVisibility(View.GONE);
                                    }
                                    getCity(country_id);

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(getActivity());
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(getActivity());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(getActivity());
                    } else {
                        MethodClass.error_alert(getActivity());
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getCity(String cou_id){
        if (MethodClass.isNetworkConnected(getActivity())) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = getString(R.string.SERVER_URL) + "city";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("country_id", cou_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(getActivity());

                        JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                        if (result_Object != null) {

                            JSONArray cityArray = result_Object.getJSONArray("city");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selCity),""));
                            for (int i = 0; i <cityArray.length() ; i++) {

                                String city_id = cityArray.getJSONObject(i).getString("id");
                                String cityname = cityArray.getJSONObject(i).getString("city");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                                if(cityId.equals(city_id)){
                                    sel_city = i+1;
                                }
                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArrays2) {
                            };
                            city_spinner.setAdapter(adapter4);
                            city_spinner.setSelection(sel_city);
                            city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    cityId = String.valueOf(s.tag);
                                    if(country_id.equals("161")){
                                        getDistrict(cityId);
                                    }

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(getActivity());
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(getActivity());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(getActivity());
                    } else {
                        MethodClass.error_alert(getActivity());
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getDistrict(final String cit_id){
        if (MethodClass.isNetworkConnected(getActivity())) {
            MethodClass.showProgressDialog(getActivity());
            String server_url = getString(R.string.SERVER_URL) + "district";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("city_id", cit_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(getActivity());

                        JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                        if (result_Object != null) {

                            JSONArray districtArray = result_Object.getJSONArray("district");
                            List<StringWithTag> spinnerArrays3 = new ArrayList<StringWithTag>();
                            spinnerArrays3.add(new StringWithTag(getString(R.string.distHint),""));
                            for (int i = 0; i <districtArray.length() ; i++) {

                                String city_id = districtArray.getJSONObject(i).getString("id");
                                String cityname = districtArray.getJSONObject(i).getString("district");
                                spinnerArrays3.add(new StringWithTag(cityname, city_id));

                                if(distId.equals(city_id)){
                                    sel_dist  = i+1;
                                }
                            }
                            ArrayAdapter adapter5 = new ArrayAdapter(getActivity(), R.layout.spinner_item, spinnerArrays3) {
                            };
                            dist_spinner.setAdapter(adapter5);
                            dist_spinner.setSelection(sel_dist);
                            dist_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    distId = String.valueOf(s.tag);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(getActivity());
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(getActivity());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(getActivity());
                    } else {
                        MethodClass.error_alert(getActivity());
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

}
