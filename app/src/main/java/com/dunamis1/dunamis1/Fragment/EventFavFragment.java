package com.dunamis1.dunamis1.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Adapter.EventFavAdapter;
import com.dunamis1.dunamis1.Adapter.EventsAdapter;
import com.dunamis1.dunamis1.Adapter.Music2Adapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ADD;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SAVE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class EventFavFragment extends Fragment {

    private RecyclerView rv_Dash1;
    private ArrayList<HashMap<String, String>> map_list;
    private Activity activity;
    int[] sampleImagesArr = {R.drawable.image51, R.drawable.image52, R.drawable.image53, R.drawable.image54, R.drawable.image55, R.drawable.image56};
    private String[] title_array = {"AN INSTRUMENT OF WORSHIP", "I am all for you.mp3", "I HAVE COME BEFORE YOUR HOLY HILL", "I remain your baby", "LORD MY LIFE IN YOUR HANDS", "LORD IF NOT FOR YOU"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public EventFavFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_fav, container, false);
        activity = (Activity) container.getContext();

        rv_Dash1 = view.findViewById(R.id.rv_Dash1);
        getMusic();
        return view;
    }

    public void getMusic() {
        MethodClass.showProgressDialog(getActivity());
        String server_url = getActivity().getString(R.string.SERVER_URL) + "favourite/list";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("LANG","en"));
        params.put("type","E");
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(getActivity());

                    JSONObject result_Object = MethodClass.get_result_from_webservice(getActivity(), response);
                    if (result_Object != null) {
                        JSONArray seedArray = result_Object.getJSONArray("event_list");


                        if(seedArray.length()>0){
                            map_list = new ArrayList<>();

                            for (int i = 0; i <seedArray.length() ; i++) {
                                JSONObject SeedObj = seedArray.getJSONObject(i);
                                String title = SeedObj.getJSONObject("eventdetails").getString("title");
                                String google_address = SeedObj.getString("google_address");
                                String thumbnail = SeedObj.getString("thumbnail");
                                String date = SeedObj.getString("from_date");
                                String total_likes = SeedObj.getString("total_likes");
                                String total_comments = SeedObj.getString("total_comments");
                                String total_share = SeedObj.getString("total_share");
                                String id = SeedObj.getString("id");

                                String favourite = SeedObj.getString("favourite");


                                HashMap<String,String> map = new HashMap<>();
                                map.put(EVENT_TITLE,title);
                                map.put(EVENT_THUMB,thumbnail);
                                map.put(EVENT_DATE,date);
                                map.put(EVENT_ADD,google_address);
                                map.put(EVENT_LIKE,total_likes);
                                map.put(EVENT_COMMENT,total_comments);
                                map.put(EVENT_SHARE,total_share);
                                map.put(EVENT_ID,id);
                                Log.e("FAV", favourite);
                                if(!favourite.equals(null) && !favourite.equals("null"))
                                {
                                    map.put(EVENT_SAVE,"YES");
                                }else{
                                    map.put(EVENT_SAVE,"NO");
                                }

                                map_list.add(map);
                            }
                            EventFavAdapter adapter = new EventFavAdapter(getActivity(), map_list);
                            rv_Dash1.setAdapter(adapter);
                            rv_Dash1.setFocusable(false);
                        }



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(getActivity());
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(getActivity());

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(getActivity(), "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);


    }
}