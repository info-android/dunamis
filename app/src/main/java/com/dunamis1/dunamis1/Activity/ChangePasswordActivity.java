package com.dunamis1.dunamis1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordActivity extends AppCompatActivity {
    private EditText pass,cpass;
    private String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        pass = findViewById(R.id.pass);
        cpass = findViewById(R.id.cpass);

        phone = getIntent().getStringExtra("phone");
    }
    public void nextPage(View view){
        if(pass.getText().toString().trim().length() == 0){
            pass.setError(getString(R.string.passReq));
            pass.requestFocus();
            return;
        }
        if(cpass.getText().toString().trim().length() == 0){
            cpass.setError(getString(R.string.passReq));
            cpass.requestFocus();
            return;
        }
        if(!pass.getText().toString().trim().equals(cpass.getText().toString().trim())){
            cpass.setError(getString(R.string.confirmpassMismatch));
            cpass.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(ChangePasswordActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "set-password";


        HashMap<String, String> params = new HashMap<String, String>();

        params.put("phone", phone);
        params.put("password",pass.getText().toString().trim());
        params.put("otp",getIntent().getStringExtra("otp"));

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ChangePasswordActivity.this, response);
                    if (jsonObject != null) {
                        final String message = jsonObject.getString("message");
                        final String meaning = jsonObject.getString("meaning");
                        new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message)
                                .setContentText(meaning)
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent I =new Intent(ChangePasswordActivity.this,LoginActivity.class);
                                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(I);
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(ChangePasswordActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ChangePasswordActivity.this);
                } else {
                    MethodClass.error_alert(ChangePasswordActivity.this);
                }
            }
        });

        MySingleton.getInstance(ChangePasswordActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void back(View view){
        Intent I =new Intent(this,LoginActivity.class);
        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(I);
    }
}
