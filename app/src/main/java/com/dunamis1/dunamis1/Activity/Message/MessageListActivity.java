package com.dunamis1.dunamis1.Activity.Message;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Adapter.SeedsAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.MessageListAdapter;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_USER;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_REC_ID;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_TITLE;

public class MessageListActivity extends AppCompatActivity {

    private ArrayList<HashMap<String,String>> arrayList;
    private RecyclerView recyView;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    public int [] userPicArray = {R.drawable.man1, R.drawable.man2, R.drawable.man3, R.drawable.man4, R.drawable.man5, R.drawable.man6, R.drawable.man7, R.drawable.man1};
    private String[] nameArray= {"Anthony James", "Obi", "Uju","Lola Babs", "Emeka", "Olivia", "Paul Alfred","Lola"};
    private String[] timeArray={"10 min ago","12 min ago","21 min ago","30 min ago","45 min ago",
            "50 min ago","52 min ago","1 hours ago"};
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    private String user_id= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        recyView=findViewById(R.id.recyView);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        user_id = PreferenceManager.getDefaultSharedPreferences(this).getString("user_id","");
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        chatList();
        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    private void chatList(){

        MethodClass.showProgressDialog(MessageListActivity.this);
        String server_url = MessageListActivity.this.getString(R.string.SERVER_URL) + "message/user/list";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MessageListActivity.this).getString("LANG","en"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MessageListActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MessageListActivity.this, response);
                    if (result_Object != null) {
                        JSONArray msgArray = result_Object.getJSONArray("list");

                        arrayList = new ArrayList<>();

                        for (int i = 0; i <msgArray.length() ; i++) {
                            JSONObject SeedObj = msgArray.getJSONObject(i);
                            String message = SeedObj.getString("last_message");


                            String sender_title = "";
                            String sender_fname = "";
                            String sender_lname = "";
                            String sender_profile_pic = "";
                            String sender_id = "";

                            if(!SeedObj.getString("user_one").equals("null") && !SeedObj.getString("user_one").equals(null)){
                                sender_title = SeedObj.getJSONObject("user_one").getString("title");
                                sender_fname = SeedObj.getJSONObject("user_one").getString("fname");
                                sender_lname = SeedObj.getJSONObject("user_one").getString("lname");
                                sender_id = SeedObj.getJSONObject("user_one").getString("id");
                                sender_profile_pic = SeedObj.getJSONObject("user_one").getString("profile_pic");
                            }

                            String rec_title = "";
                            String rec_fname = "";
                            String rec_lname = "";
                            String rec_profile_pic = "";
                            String rec_id = "";

                            if(!SeedObj.getString("user_two").equals("null") && !SeedObj.getString("user_two").equals(null)){
                                rec_title = SeedObj.getJSONObject("user_two").getString("title");
                                rec_fname = SeedObj.getJSONObject("user_two").getString("fname");
                                rec_lname = SeedObj.getJSONObject("user_two").getString("lname");
                                rec_id = SeedObj.getJSONObject("user_two").getString("id");
                                rec_profile_pic = SeedObj.getJSONObject("user_two").getString("profile_pic");
                            }


                            String date = SeedObj.getString("updated_at");

                            String id = SeedObj.getString("id");
                            HashMap<String,String> map = new HashMap<>();
                            if(user_id.equals(sender_id)){
                                map.put(COMM_PIC,rec_profile_pic);
                                if(!rec_title.equals("null")){
                                    map.put(COMM_USER,rec_title+" "+rec_fname+" "+rec_lname);
                                }else {
                                    map.put(COMM_USER,rec_fname+" "+rec_lname);
                                }

                                map.put(MSG_REC_ID,rec_id);
                            }else{
                                map.put(COMM_PIC,sender_profile_pic);
                                if(!sender_title.equals("null")){
                                    map.put(COMM_USER,sender_title+" "+sender_fname+" "+sender_lname);
                                }else {
                                    map.put(COMM_USER,sender_fname+" "+sender_lname);
                                }

                                map.put(MSG_REC_ID,sender_id);
                            }

                            map.put(COMMENT,message);
                            map.put(COMM_DATE,date);
                            map.put(SEED_ID,id);

                            arrayList.add(map);
                        }
                        MessageListAdapter messageListAdapter=new MessageListAdapter(MessageListActivity.this,arrayList);
                        recyView.setAdapter(messageListAdapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MessageListActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MessageListActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MessageListActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MessageListActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MessageListActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MessageListActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void back(View view) {
        super.onBackPressed();
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
