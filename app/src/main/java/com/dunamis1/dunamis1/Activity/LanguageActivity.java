package com.dunamis1.dunamis1.Activity;

import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;

public class LanguageActivity extends AppCompatActivity {

    private RelativeLayout espLay,engLay,frLay;
    private ImageView espTick,frTick,engTick;
    private Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(LanguageActivity.this);
        setContentView(R.layout.activity_language);

        espLay = findViewById(R.id.espLay);
        engLay = findViewById(R.id.engLay);
        frLay = findViewById(R.id.frLay);
        espTick = findViewById(R.id.espTick);
        frTick = findViewById(R.id.frTick);
        engTick = findViewById(R.id.engTick);
        save = findViewById(R.id.save);

        espLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(LanguageActivity.this).edit().putString("LANG", "es").commit();
                espTick.setVisibility(View.VISIBLE);
                frTick.setVisibility(View.GONE);
                engTick.setVisibility(View.GONE);
            }
        });
        engLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(LanguageActivity.this).edit().putString("LANG", "en").commit();
                espTick.setVisibility(View.GONE);
                frTick.setVisibility(View.GONE);
                engTick.setVisibility(View.VISIBLE);
            }
        });
        frLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(LanguageActivity.this).edit().putString("LANG", "fr").commit();
                espTick.setVisibility(View.GONE);
                frTick.setVisibility(View.VISIBLE);
                engTick.setVisibility(View.GONE);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(engTick.getVisibility() == View.GONE && espTick.getVisibility() == View.GONE && frTick.getVisibility() == View.GONE){
                    Snackbar snackbar = Snackbar
                            .make(findViewById(android.R.id.content), getString(R.string.chooselang), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else{
                    MethodClass.go_to_next_activity(LanguageActivity.this,ChooseSignupActivity.class);
                }
            }
        });

    }

}
