package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPassword extends AppCompatActivity {

    private EditText emailaddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        emailaddress = findViewById(R.id.emailaddress);

    }
    public void nextPage(View view){
        if(!MethodClass.emailValidator(emailaddress.getText().toString().trim())){
            emailaddress.setError(getString(R.string.emailReq));
            emailaddress.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(ForgotPassword.this);
        String server_url = getString(R.string.SERVER_URL) + "forgot-password";


        HashMap<String, String> params = new HashMap<String, String>();

        params.put("email", emailaddress.getText().toString().trim());

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ForgotPassword.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ForgotPassword.this, response);
                    if (jsonObject != null) {
                        final String phone = jsonObject.getString("phone");
                        final String otp = jsonObject.getString("otp");
                        new SweetAlertDialog(ForgotPassword.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("OTP sent")
                                .setContentText("An OTP sent to your registered email. Please enter it in your next step")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent I =new Intent(ForgotPassword.this, ForgotVerificationActivity.class);
                                        I.putExtra("vcode",otp);
                                        I.putExtra("phone",phone);
                                        I.putExtra("email",emailaddress.getText().toString().trim());
                                        startActivity(I);
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(ForgotPassword.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ForgotPassword.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ForgotPassword.this);
                } else {
                    MethodClass.error_alert(ForgotPassword.this);
                }
            }
        });

        MySingleton.getInstance(ForgotPassword.this).addToRequestQueue(jsonObjectRequest);

    }
    public void back(View view){
        super.onBackPressed();
    }

}
