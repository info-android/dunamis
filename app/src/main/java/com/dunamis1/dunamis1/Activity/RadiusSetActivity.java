package com.dunamis1.dunamis1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.dunamis1.dunamis1.Activity.EventListing.EventFilterActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.R;

import static com.dunamis1.dunamis1.Helper.Constant.RADIUS;

public class RadiusSetActivity extends AppCompatActivity {
    private CrystalSeekbar rangeSeekbar1;
    private TextView accText;
    private Button applyBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radius_set);
        rangeSeekbar1 = findViewById(R.id.rangeSeekbar1);
        accText = findViewById(R.id.accText);
        applyBtn = findViewById(R.id.applyBtn);
        // get min and max text view
        if(!RADIUS.equals("")){
            rangeSeekbar1.setMinStartValue(Float.parseFloat(RADIUS));
            accText.setText("Change Radius: " + RADIUS + " km");
            rangeSeekbar1.apply();
        }

// set listener
        rangeSeekbar1.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue) {

                RADIUS = String.valueOf(minValue);

            }
        });

// set final value listener
        rangeSeekbar1.setOnSeekbarFinalValueListener(new OnSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number value) {
                Log.e("RADIUS", "valueChanged: " + String.valueOf(value));
                RADIUS = String.valueOf(value);
                accText.setText("Change Radius: " + RADIUS + " km");
            }
        });
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RadiusSetActivity.this, DunamisChurch.class);
                startActivity(intent);
                finish();
            }
        });
    }
    public void clear(View view){
        RADIUS="";
        Intent intent=new Intent(RadiusSetActivity.this,DunamisChurch.class);
        startActivity(intent);
        finish();
    }
    public void back(View view) {
        onBackPressed();
        finish();
    }
}
