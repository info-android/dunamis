package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuestSignupStep2Activity extends AppCompatActivity {

    Spinner country_spinner;
    Spinner city_spinner;
    Spinner dis_spinner;
    RelativeLayout ctlay, dstlay;
    private Spinner hear_spinner;
    private Spinner branch_spinner;
    private String country_id = "";
    private String cityId = "";
    private String distId = "";
    private String branch_id = "";
    private String hear_id = "";

    private RadioGroup visitOut,memanother,bornAgain,baptized;
    private String visitId = "";
    private String memanotherId = "";
    private String bornAgainId = "";
    private String baptizedId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_signup_step2);
        MethodClass.hide_keyboard(GuestSignupStep2Activity.this);
        country_spinner = findViewById(R.id.country_spinner);
        city_spinner = findViewById(R.id.city_spinner);
        dis_spinner = findViewById(R.id.dis_spinner);
        ctlay = findViewById(R.id.ctlay);
        dstlay = findViewById(R.id.dstlay);
        hear_spinner  = findViewById(R.id.hear_spinner);
        branch_spinner  = findViewById(R.id.branch_spinner);
        visitOut  = findViewById(R.id.visitOut);
        memanother  = findViewById(R.id.memanother);
        bornAgain  = findViewById(R.id.bornAgain);
        baptized  = findViewById(R.id.baptized);

        visitOut.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.yes){
                    visitId = "Y";
                }
                if(checkedId == R.id.no){
                    visitId = "N";
                }
            }
        });
        memanother.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.memYes){
                    memanotherId = "Y";
                }
                if(checkedId == R.id.memNo){
                    memanotherId = "N";
                }
            }
        });
        bornAgain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.bornYes){
                    bornAgainId = "Y";
                }
                if(checkedId == R.id.bornNo){
                    bornAgainId = "N";
                }
            }
        });
        baptized.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.bapYes){
                    baptizedId = "Y";
                }
                if(checkedId == R.id.bapNo){
                    baptizedId = "N";
                }
            }
        });

        getChurchBranch();

    }
    public void getChurchBranch(){
        if (MethodClass.isNetworkConnected(GuestSignupStep2Activity.this)) {
            MethodClass.showProgressDialog(GuestSignupStep2Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "church-location";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("language",PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("LANG","en"));
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(GuestSignupStep2Activity.this, response);
                        if (result_Object != null) {
                            getCouontry();
                            JSONArray locationArray = result_Object.getJSONArray("location");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selctChurchLoc),""));
                            for (int i = 0; i <locationArray.length() ; i++) {

                                String city_id = locationArray.getJSONObject(i).getString("id");
                                JSONObject churchdetails = locationArray.getJSONObject(i).getJSONObject("churchdetails");
                                String cityname = churchdetails.getString("name");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(GuestSignupStep2Activity.this, R.layout.spinner_item, spinnerArrays2) {
                            };
                            branch_spinner.setAdapter(adapter4);
                            branch_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    branch_id = String.valueOf(s.tag);

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(GuestSignupStep2Activity.this);
                    } else {
                        MethodClass.error_alert(GuestSignupStep2Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getCouontry(){
        if (MethodClass.isNetworkConnected(GuestSignupStep2Activity.this)) {
            MethodClass.showProgressDialog(GuestSignupStep2Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "country";
            HashMap<String, String> params = new HashMap<String, String>();
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(GuestSignupStep2Activity.this, response);
                        if (result_Object != null) {

                            JSONArray countryArray = result_Object.getJSONArray("country");

                            List<StringWithTag> spinnerArraysGen = new ArrayList<StringWithTag>();
                            spinnerArraysGen.add(new StringWithTag(getString(R.string.hearAbt),""));
                            for (int j = 0; j <6 ; j++) {
                                if(j ==0){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.chruchMem), "1"));
                                }
                                if(j ==1){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.frnd), "2"));
                                }
                                if(j ==2){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.flyer), "3"));
                                }if(j ==3){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.evan), "4"));
                                }if(j ==4){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.medTv), "5"));
                                }if(j ==5){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.postr), "6"));
                                }

                            }
                            ArrayAdapter adapterGen = new ArrayAdapter(GuestSignupStep2Activity.this, R.layout.spinner_item, spinnerArraysGen) {
                            };
                            hear_spinner.setAdapter(adapterGen);
                            hear_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    hear_id = String.valueOf(s.tag);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                            List<StringWithTag> spinnerArraysCoun = new ArrayList<StringWithTag>();
                            spinnerArraysCoun.add(new StringWithTag(getString(R.string.selCountry),""));
                            for (int i = 0; i <countryArray.length() ; i++) {

                                String counid = countryArray.getJSONObject(i).getString("id");
                                String countryname = countryArray.getJSONObject(i).getString("countryname");
                                spinnerArraysCoun.add(new StringWithTag(countryname, counid));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(GuestSignupStep2Activity.this, R.layout.spinner_item, spinnerArraysCoun) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    country_id = String.valueOf(s.tag);
                                    if(country_id.equals("161")){
                                        dstlay.setVisibility(View.VISIBLE);
                                    }else {
                                        dstlay.setVisibility(View.GONE);
                                    }
                                    getCity(country_id);

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(GuestSignupStep2Activity.this);
                    } else {
                        MethodClass.error_alert(GuestSignupStep2Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getCity(String cou_id){
        if (MethodClass.isNetworkConnected(GuestSignupStep2Activity.this)) {
            MethodClass.showProgressDialog(GuestSignupStep2Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "city";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("country_id", cou_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(GuestSignupStep2Activity.this, response);
                        if (result_Object != null) {

                            JSONArray cityArray = result_Object.getJSONArray("city");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selCity),""));
                            for (int i = 0; i <cityArray.length() ; i++) {

                                String city_id = cityArray.getJSONObject(i).getString("id");
                                String cityname = cityArray.getJSONObject(i).getString("city");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(GuestSignupStep2Activity.this, R.layout.spinner_item, spinnerArrays2) {
                            };
                            city_spinner.setAdapter(adapter4);
                            city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    cityId = String.valueOf(s.tag);
                                    if(country_id.equals("161")){
                                        getDistrict(cityId);
                                    }

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(GuestSignupStep2Activity.this);
                    } else {
                        MethodClass.error_alert(GuestSignupStep2Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getDistrict(String cit_id){
        if (MethodClass.isNetworkConnected(GuestSignupStep2Activity.this)) {
            MethodClass.showProgressDialog(GuestSignupStep2Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "district";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("city_id", cit_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(GuestSignupStep2Activity.this, response);
                        if (result_Object != null) {

                            JSONArray districtArray = result_Object.getJSONArray("district");
                            List<StringWithTag> spinnerArrays3 = new ArrayList<StringWithTag>();
                            spinnerArrays3.add(new StringWithTag(getString(R.string.Dist),""));
                            for (int i = 0; i <districtArray.length() ; i++) {

                                String city_id = districtArray.getJSONObject(i).getString("id");
                                String cityname = districtArray.getJSONObject(i).getString("district");
                                spinnerArrays3.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter5 = new ArrayAdapter(GuestSignupStep2Activity.this, R.layout.spinner_item, spinnerArrays3) {
                            };
                            dis_spinner.setAdapter(adapter5);
                            dis_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    distId = String.valueOf(s.tag);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(GuestSignupStep2Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(GuestSignupStep2Activity.this);
                    } else {
                        MethodClass.error_alert(GuestSignupStep2Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(GuestSignupStep2Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public void nextPage(View view){
        if(country_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCountry), Snackbar.LENGTH_LONG);
            snackbar.show();
            country_spinner.requestFocus();
            return;
        }
        if(cityId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCity), Snackbar.LENGTH_LONG);
            snackbar.show();
            city_spinner.requestFocus();
            return;
        }
        if(dstlay.getVisibility() == view.VISIBLE){
            if(distId.equals("")){
                View parentLayout = findViewById(android.R.id.content);
                Snackbar snackbar = Snackbar
                        .make(parentLayout, getString(R.string.chooseCity), Snackbar.LENGTH_LONG);
                snackbar.show();
                dis_spinner.requestFocus();
                return;
            }
        }
        if(branch_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseChurchBanch), Snackbar.LENGTH_LONG);
            snackbar.show();
            branch_spinner.requestFocus();
            return;
        }
        if(hear_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseHear), Snackbar.LENGTH_LONG);
            snackbar.show();
            hear_spinner.requestFocus();
            return;
        }
        if(visitId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseVisit), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(memanotherId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseMem), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(bornAgainId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseBorn), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(baptizedId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseBap), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }

        Intent I = new Intent(this,GuestSignupStep3Activity.class);
        I.putExtra("title",getIntent().getStringExtra("title"));
        I.putExtra("fname",getIntent().getStringExtra("fname"));
        I.putExtra("lname",getIntent().getStringExtra("lname"));
        I.putExtra("phone",getIntent().getStringExtra("phone"));
        I.putExtra("age",getIntent().getStringExtra("age"));
        I.putExtra("gender",getIntent().getStringExtra("gender"));
        I.putExtra("martial",getIntent().getStringExtra("martial"));
        I.putExtra("email",getIntent().getStringExtra("email"));

        I.putExtra("password",getIntent().getStringExtra("password"));
        I.putExtra("country",country_id);
        I.putExtra("city",cityId);
        I.putExtra("hear",hear_id);
        I.putExtra("visitout",visitId);
        I.putExtra("branchId",branch_id);
        I.putExtra("memberAnother",memanotherId);
        I.putExtra("bornagain",bornAgainId);
        I.putExtra("baptized",baptizedId);
        I.putExtra("password",getIntent().getStringExtra("password"));
        I.putExtra("code",getIntent().getStringExtra("code"));
        if(dstlay.getVisibility() == view.VISIBLE){
            I.putExtra("district",distId);
        }else{
            I.putExtra("district","");
        }

        startActivity(I);
    }
    public void Login(View view){
        Intent I = new Intent(this,LoginActivity.class);
        startActivity(I);
    }
    public void back(View view){
        super.onBackPressed();
    }

}
