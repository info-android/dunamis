package com.dunamis1.dunamis1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dunamis1.dunamis1.Adapter.SliderAdapterExample;
import com.dunamis1.dunamis1.Adapter.SliderAdpaterTwoExample;
import com.dunamis1.dunamis1.Helper.SliderItem;
import com.dunamis1.dunamis1.R;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import static com.dunamis1.dunamis1.Helper.Constant.SCH_CODE;

public class ForeignerDeskIntroActivity extends AppCompatActivity {
    private SliderView imageSlider;
    private SliderAdpaterTwoExample adapter;
    private TextView moreInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foreigner_desk_intro);
        imageSlider = findViewById(R.id.imageSlider);
        moreInfo = findViewById(R.id.moreInfo);
        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = null; // missing 'http://' will cause crashed
                uri = Uri.parse("https://dunamisforeigndesk.org/index.html");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        List<SliderItem> sliderItemList = new ArrayList<>();
        //dummy data
        for (int i = 0; i < 3; i++) {
            SliderItem sliderItem = new SliderItem();
            if(i == 0){
                sliderItem.setImageUrl(R.drawable.fd1);
            }else if(i == 1){
                sliderItem.setImageUrl(R.drawable.fd2);
            }else if(i == 2){
                sliderItem.setImageUrl(R.drawable.fd3);
            }
            sliderItemList.add(sliderItem);
        }
        adapter = new SliderAdpaterTwoExample(ForeignerDeskIntroActivity.this,sliderItemList);
        imageSlider.setSliderAdapter(adapter);
        imageSlider.setIndicatorAnimation(IndicatorAnimations.COLOR); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        imageSlider.setIndicatorSelectedColor(Color.WHITE);
        imageSlider.setIndicatorUnselectedColor(Color.GRAY);
        imageSlider.setScrollTimeInSec(3);
        imageSlider.setAutoCycle(true);
        imageSlider.startAutoCycle();

    }
    public void nextPage(View view){
        Intent I = new Intent(this,ForeignerDeskOneActivity.class);
        startActivity(I);
    }
}
