package com.dunamis1.dunamis1.Activity.Music;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.MusicAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_DESC;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_ID;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TYPE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class MusicListActivity extends AppCompatActivity {
    int[] sampleImagesArr = {R.drawable.image51,R.drawable.image52,R.drawable.image53,R.drawable.image54, R.drawable.image55, R.drawable.image56};
    private String[] title_array = {"AN INSTRUMENT OF WORSHIP", "I am all for you.mp3", "I HAVE COME BEFORE YOUR HOLY HILL","I remain your baby","LORD MY LIFE IN YOUR HANDS","LORD IF NOT FOR YOU"};
    private ArrayList<HashMap<String,String>> map_list;
    private RecyclerView rv_Dash1;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private LinearLayout playLayout;
    private ImageView prdct_iv;
    private TextView prdct_title_tv;
    private TextView prdct_mem_tv3;
    private ImageView play;
    private ImageView pause;
    MediaPlayer mediaPlayer;
    private boolean isStarted;
    boolean pauses = false;
    private int resumePosition;

    private TextView nav_name_tv;
    private CircleImageView imgPro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        prdct_iv = (ImageView) findViewById(R.id.prdct_iv);
        play = (ImageView) findViewById(R.id.play);
        pause = (ImageView) findViewById(R.id.pause);
        prdct_title_tv = (TextView) findViewById(R.id.prdct_title_tv);
        prdct_mem_tv3 = (TextView) findViewById(R.id.prdct_mem_tv3);
        playLayout = (LinearLayout) findViewById(R.id.playLayout);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        if(MethodClass.AudioPlaying()){
            playLayout.setVisibility(View.VISIBLE);
            prdct_title_tv.setText(PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("Mname",""));
            prdct_mem_tv3.setText(PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("Mdesc",""));
            Picasso.get().load(MUSIC_IMG_URL+PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("Mimg","")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(prdct_iv);
        }else {
            playLayout.setVisibility(View.GONE);
        }
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.PlayAudio(MusicListActivity.this,PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("file",""));
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.PauseAudio(MusicListActivity.this);
                pause.setVisibility(View.GONE);
                play.setVisibility(View.VISIBLE);
            }
        });

        getDep();
    }
    public void getDep(){
        MethodClass.showProgressDialog(MusicListActivity.this);
        String server_url = MusicListActivity.this.getString(R.string.SERVER_URL) + "music/list";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("LANG","en"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MusicListActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MusicListActivity.this, response);
                    if (result_Object != null) {
                        JSONArray seedArray = result_Object.getJSONArray("list");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            JSONObject SeedObj = seedArray.getJSONObject(i);
                            String title = SeedObj.getJSONObject("musicdetails").getString("title");
                            String description = SeedObj.getJSONObject("musicdetails").getString("description");
                            String code = SeedObj.getString("code");
                            String type = SeedObj.getString("type");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String thubmnail = SeedObj.getString("thumbnail");
                            String id = SeedObj.getString("id");


                            HashMap<String,String> map = new HashMap<>();
                            map.put(MUSIC_TITLE,title);
                            map.put(MUSIC_DESC,description);
                            map.put(MUSIC_TYPE,type);
                            map.put(MUSIC_CODE,code);
                            map.put(MUSIC_LIKE,total_likes);
                            map.put(MUSIC_COMMENT,total_comments);
                            map.put(MUSIC_SHARE,total_share);
                            map.put(MUSIC_ID,id);
                            map.put(MUSIC_THUMB,thubmnail);

                            map_list.add(map);
                        }
                        MusicAdapter bibleDeuteronomyAdapter= new MusicAdapter(MusicListActivity.this,map_list);
                        rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                        rv_Dash1.setFocusable(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MusicListActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MusicListActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MusicListActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MusicListActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MusicListActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void back(View view){
        super.onBackPressed();
        MethodClass.StopAudio(MusicListActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));

        if(MethodClass.AudioPlaying()){
            playLayout.setVisibility(View.VISIBLE);
            prdct_title_tv.setText(PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("Mname",""));
            prdct_mem_tv3.setText(PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("Mdesc",""));
            Picasso.get().load(MUSIC_IMG_URL+PreferenceManager.getDefaultSharedPreferences(MusicListActivity.this).getString("Mimg","")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(prdct_iv);
            pause.setVisibility(View.VISIBLE);
            play.setVisibility(View.GONE);
        }else {
            playLayout.setVisibility(View.GONE);
        }
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
