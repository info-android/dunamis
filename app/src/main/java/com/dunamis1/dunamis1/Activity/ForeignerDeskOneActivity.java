package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForeignerDeskOneActivity extends AppCompatActivity {

    Spinner gender_spinner,mar_spinner,country_spinner,how_spinner,title_spinner;
    private RadioGroup grpRad;
    private RelativeLayout dstlay;

    private EditText fname,lname,address;
    private RadioGroup dunaInter,traAlone,transreq;
    private Spinner hear_spinner;

    private RadioButton churchRad,frndRad,flyRad,dunaInterYes,dunaInterNo,traAloneYes,traAloneNo,transreqYes,transreqNo;


    private String gender = "",martial = "",country_id="",titles = "",traAlone_id="",transreq_id="",dunaInter_id="",hear_id = "",grp_id = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foreigner_desk_one);
        MethodClass.hide_keyboard(ForeignerDeskOneActivity.this);
        gender_spinner  = findViewById(R.id.gender_spinner);
        mar_spinner  = findViewById(R.id.mar_spinner);
        country_spinner  = findViewById(R.id.country_spinner);
        how_spinner  = findViewById(R.id.how_spinner);
        title_spinner  = findViewById(R.id.title_spinner);
        grpRad  = findViewById(R.id.grpRad);
        dstlay  = findViewById(R.id.dstlay);
        fname  = findViewById(R.id.fname);
        lname  = findViewById(R.id.lname);
        address  = findViewById(R.id.address);

        dunaInter  = findViewById(R.id.dunaInter);
        traAlone  = findViewById(R.id.traAlone);
        transreq  = findViewById(R.id.transreq);
        hear_spinner  = findViewById(R.id.hear_spinner);
        dunaInterYes  = findViewById(R.id.dunaInterYes);
        dunaInterNo  = findViewById(R.id.dunaInterNo);
        traAloneYes  = findViewById(R.id.traAloneYes);
        traAloneNo  = findViewById(R.id.traAloneNo);
        transreqYes  = findViewById(R.id.transreqYes);
        transreqNo  = findViewById(R.id.transreqNo);
        ArrayAdapter adapter4 = ArrayAdapter.createFromResource(this, R.array.howmany, R.layout.spinner_item);

        how_spinner.setAdapter(adapter4);
        grpRad.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.yes:
                        // TODO Something
                        grp_id = "Y";
                        dstlay.setVisibility(View.VISIBLE);
                        break;
                    case R.id.no:
                        // TODO Something
                        grp_id = "N";
                        dstlay.setVisibility(View.GONE);
                        break;
                }
            }
        });
        List<StringWithTag> spinnerArraysHear = new ArrayList<StringWithTag>();
        spinnerArraysHear.add(new StringWithTag(getString(R.string.hearAbt),""));
        for (int j = 0; j <6 ; j++) {
            if(j ==0){
                spinnerArraysHear.add(new StringWithTag(getString(R.string.chruchMem), "1"));
            }
            if(j ==1){
                spinnerArraysHear.add(new StringWithTag(getString(R.string.frnd), "2"));
            }
            if(j ==2){
                spinnerArraysHear.add(new StringWithTag(getString(R.string.flyer), "3"));
            }if(j ==3){
                spinnerArraysHear.add(new StringWithTag(getString(R.string.evan), "4"));
            }if(j ==4){
                spinnerArraysHear.add(new StringWithTag(getString(R.string.medTv), "5"));
            }if(j ==5){
                spinnerArraysHear.add(new StringWithTag(getString(R.string.postr), "6"));
            }

        }
        ArrayAdapter adapterGens = new ArrayAdapter(this, R.layout.spinner_item, spinnerArraysHear) {
        };
        hear_spinner.setAdapter(adapterGens);
        hear_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            //for user salutation select
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // An item was selected. You can retrieve the selected item using
                //StringWithTag s
                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                hear_id = String.valueOf(s.tag);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
                return;
            }
        });
        dunaInter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.dunaInterYes){
                    dunaInter_id = "Y";
                }
                if(checkedId == R.id.dunaInterNo){
                    dunaInter_id = "N";
                }
            }
        });
        traAlone.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.traAloneYes){
                    traAlone_id = "Y";
                }
                if(checkedId == R.id.traAloneNo){
                    traAlone_id = "N";
                }
            }
        });
        transreq.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.transreqYes){
                    transreq_id = "Y";
                }
                if(checkedId == R.id.transreqNo){
                    transreq_id = "N";
                }
            }
        });

        List<StringWithTag> spinnerArraysGen = new ArrayList<StringWithTag>();
        spinnerArraysGen.add(new StringWithTag(getString(R.string.selgender),""));
        for (int j = 0; j <3 ; j++) {
            if(j ==0){
                spinnerArraysGen.add(new StringWithTag(getString(R.string.Male), "M"));
            }
            if(j ==1){
                spinnerArraysGen.add(new StringWithTag(getString(R.string.Female), "F"));
            }
            if(j ==2){
                spinnerArraysGen.add(new StringWithTag(getString(R.string.Other), "O"));
            }
        }
        ArrayAdapter adapterGen = new ArrayAdapter(ForeignerDeskOneActivity.this, R.layout.spinner_item, spinnerArraysGen) {
        };
        gender_spinner.setAdapter(adapterGen);
        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            //for user salutation select
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // An item was selected. You can retrieve the selected item using
                //StringWithTag s
                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                gender = String.valueOf(s.tag);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
                return;
            }
        });
        List<StringWithTag> spinnerArraysMart = new ArrayList<StringWithTag>();
        spinnerArraysMart.add(new StringWithTag(getString(R.string.selMartial),""));
        for (int K = 0; K <2 ; K++) {

            if(K ==0){
                spinnerArraysMart.add(new StringWithTag(getString(R.string.Married), "M"));
            }
            if(K ==1){
                spinnerArraysMart.add(new StringWithTag(getString(R.string.Single), "S"));
            }

        }
        ArrayAdapter adapter3 = new ArrayAdapter(ForeignerDeskOneActivity.this, R.layout.spinner_item, spinnerArraysMart) {
        };
        mar_spinner.setAdapter(adapter3);
        mar_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            //for user salutation select
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // An item was selected. You can retrieve the selected item using
                //StringWithTag s
                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                martial = String.valueOf(s.tag);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
                return;
            }
        });
        List<StringWithTag> spinnerArraysTitle = new ArrayList<StringWithTag>();
        spinnerArraysTitle.add(new StringWithTag(getString(R.string.selTitle),""));
        for (int l = 0; l <11 ; l++) {

            if(l ==0){
                spinnerArraysTitle.add(new StringWithTag(getString(R.string.dr), "M"));
            }
            if(l ==1){
                spinnerArraysTitle.add(new StringWithTag("Mr.", "S"));
            }if(l ==2){
                spinnerArraysTitle.add(new StringWithTag("Ms.", "S"));
            }if(l ==3){
                spinnerArraysTitle.add(new StringWithTag("Mrs.", "S"));
            }if(l ==4){
                spinnerArraysTitle.add(new StringWithTag("Eng.", "S"));
            }if(l ==5){
                spinnerArraysTitle.add(new StringWithTag("Prof.", "S"));
            }if(l ==6){
                spinnerArraysTitle.add(new StringWithTag("Rev.", "S"));
            }if(l ==7){
                spinnerArraysTitle.add(new StringWithTag("Rt. Hon.", "S"));
            }if(l ==8){
                spinnerArraysTitle.add(new StringWithTag("Sr.", "S"));
            }if(l ==9){
                spinnerArraysTitle.add(new StringWithTag("Esq.", "S"));
            }if(l ==10){
                spinnerArraysTitle.add(new StringWithTag("Hon.", "S"));
            }

        }
        ArrayAdapter title_spinneradapter3 = new ArrayAdapter(ForeignerDeskOneActivity.this, R.layout.spinner_item, spinnerArraysTitle) {
        };
        title_spinner.setAdapter(title_spinneradapter3);
        title_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            //for user salutation select
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // An item was selected. You can retrieve the selected item using
                //StringWithTag s
                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                titles = String.valueOf(s.string);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
                return;
            }
        });
        getCountry();
    }
    public void getCountry(){
        if (MethodClass.isNetworkConnected(ForeignerDeskOneActivity.this)) {
            MethodClass.showProgressDialog(ForeignerDeskOneActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "country";
            HashMap<String, String> params = new HashMap<String, String>();
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(ForeignerDeskOneActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(ForeignerDeskOneActivity.this, response);
                        if (result_Object != null) {

                            JSONArray countryArray = result_Object.getJSONArray("country");

                            List<StringWithTag> spinnerArraysCoun = new ArrayList<StringWithTag>();
                            spinnerArraysCoun.add(new StringWithTag(getString(R.string.selCountry),""));
                            for (int i = 0; i <countryArray.length() ; i++) {

                                String counid = countryArray.getJSONObject(i).getString("id");
                                String countryname = countryArray.getJSONObject(i).getString("countryname");
                                spinnerArraysCoun.add(new StringWithTag(countryname, counid));
                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(ForeignerDeskOneActivity.this, R.layout.spinner_item, spinnerArraysCoun) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    country_id = String.valueOf(s.tag);

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(ForeignerDeskOneActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(ForeignerDeskOneActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(ForeignerDeskOneActivity.this);
                    } else {
                        MethodClass.error_alert(ForeignerDeskOneActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(ForeignerDeskOneActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ForeignerDeskOneActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(ForeignerDeskOneActivity.this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(ForeignerDeskOneActivity.this.findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }
    public void nextPage(View view){
        if(titles.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseTitle), Snackbar.LENGTH_LONG);
            snackbar.show();
            title_spinner.requestFocus();
            return;
        }
        if(fname.getText().toString().trim().length()==0){
            fname.setError(getString(R.string.fnameReq));
            fname.requestFocus();
            return;
        }
        if(lname.getText().toString().trim().length()==0){
            lname.setError(getString(R.string.lnameReq));
            lname.requestFocus();
            return;
        }
        if(gender.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseSpiner), Snackbar.LENGTH_LONG);
            snackbar.show();
            gender_spinner.requestFocus();
            return;
        }
        if(martial.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseMartial), Snackbar.LENGTH_LONG);
            snackbar.show();
            mar_spinner.requestFocus();
            return;
        }
        if(!MethodClass.emailValidator(address.getText().toString().trim())){
            address.setError(getString(R.string.emailReq));
            address.requestFocus();
            return;
        }
        if(country_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCountry), Snackbar.LENGTH_LONG);
            snackbar.show();
            country_spinner.requestFocus();
            return;
        }
        if(hear_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseHear), Snackbar.LENGTH_LONG);
            snackbar.show();
            hear_spinner.requestFocus();
            return;
        }
        if(dunaInter_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDunaInter), Snackbar.LENGTH_LONG);
            snackbar.show();
            dunaInter.requestFocus();
            return;
        }
        if(grp_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseGrp), Snackbar.LENGTH_LONG);
            snackbar.show();
            grpRad.requestFocus();
            return;
        }
        if(dstlay.getVisibility() == view.VISIBLE){
            if(how_spinner.getSelectedItem().toString().equals("How many in the group?")){
                View parentLayout = findViewById(android.R.id.content);
                Snackbar snackbar = Snackbar
                        .make(parentLayout, getString(R.string.chooseHowMany), Snackbar.LENGTH_LONG);
                snackbar.show();
                how_spinner.requestFocus();
                return;
            }
        }
        if(traAlone_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseTralone), Snackbar.LENGTH_LONG);
            snackbar.show();
            traAlone.requestFocus();
            return;
        }
        if(transreq_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseTransReq), Snackbar.LENGTH_LONG);
            snackbar.show();
            transreq.requestFocus();
            return;
        }
        Intent I = new Intent(this,FiregnersDeskStep2Activity.class);
        I.putExtra("title",titles);
        I.putExtra("fname",fname.getText().toString().trim());
        I.putExtra("lname",lname.getText().toString().trim());
        I.putExtra("gender",gender);
        I.putExtra("martial",martial);
        I.putExtra("email",address.getText().toString().trim());
        I.putExtra("country",country_id);
        I.putExtra("hear",hear_id);
        I.putExtra("dunaInter",dunaInter_id);
        I.putExtra("grp",grp_id);
        if(dstlay.getVisibility() == view.VISIBLE){
            I.putExtra("member",how_spinner.getSelectedItem().toString());
        }
        I.putExtra("traAlone",traAlone_id);
        I.putExtra("transReq",transreq_id);
        startActivity(I);
    }

    public void back(View view){
        super.onBackPressed();
    }
}
