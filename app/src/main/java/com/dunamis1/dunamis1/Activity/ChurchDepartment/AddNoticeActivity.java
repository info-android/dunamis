package com.dunamis1.dunamis1.Activity.ChurchDepartment;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChangePasswordActivity;
import com.dunamis1.dunamis1.Activity.LoginActivity;
import com.dunamis1.dunamis1.Activity.NoticeActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;

public class AddNoticeActivity extends AppCompatActivity {

    private Button cont;
    private EditText noticeDesc,noticeTitle;
    private String from = "";
    private String id = "";
    private String branch_id = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notice);
        cont = findViewById(R.id.cont);
        noticeDesc = findViewById(R.id.noticeDesc);
        noticeTitle = findViewById(R.id.noticeTitle);
        if(getIntent().getExtras() !=null){
            from = getIntent().getStringExtra("from");
            if(from.equals("H")){
                id = getIntent().getStringExtra("id");
            }else{
                id = getIntent().getStringExtra("id");
                branch_id = getIntent().getStringExtra("branch_id");
            }

        }
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(noticeTitle.getText().toString().trim().length()==0){
                    noticeTitle.setError(getString(R.string.notcTitleReq));
                    noticeTitle.requestFocus();
                    return;
                }
                if(noticeDesc.getText().toString().trim().length()==0){
                    noticeDesc.setError(getString(R.string.notcDescReq));
                    noticeDesc.requestFocus();
                    return;
                }
                MethodClass.showProgressDialog(AddNoticeActivity.this);
                HashMap<String, String> params = new HashMap<String, String>();
                String server_url = "";
                if(from.equals("H")){
                    server_url = getString(R.string.SERVER_URL) + "home-church/notice/add";
                    params.put("id", id);
                    params.put("heading",noticeTitle.getText().toString().trim());
                    params.put("description",noticeDesc.getText().toString().trim());

                }else{
                    server_url = getString(R.string.SERVER_URL) + "church-department/notice/add";
                    params.put("department_id", id);
                    params.put("branch_id", branch_id);
                    params.put("heading",noticeTitle.getText().toString().trim());
                    params.put("description",noticeDesc.getText().toString().trim());
                }

                JSONObject jsonObject = MethodClass.Json_rpc_format(params);

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MethodClass.hideProgressDialog(AddNoticeActivity.this);
                        Log.e("resp", response.toString());
                        try {

                            JSONObject jsonObject = MethodClass.get_result_from_webservice(AddNoticeActivity.this, response);
                            if (jsonObject != null) {
                                final String message = jsonObject.getString("message");
                                final String meaning = jsonObject.getString("meaning");
                                new SweetAlertDialog(AddNoticeActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(message)
                                        .setContentText(meaning)
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                finish();
                                            }
                                        })
                                        .show();
                            }


                        } catch (JSONException e) {
                            MethodClass.error_alert(AddNoticeActivity.this);
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(AddNoticeActivity.this);
                        if (error.toString().contains("ConnectException")) {
                            MethodClass.network_error_alert(AddNoticeActivity.this);
                        } else {
                            MethodClass.error_alert(AddNoticeActivity.this);
                        }
                    }
                }){
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddNoticeActivity.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(AddNoticeActivity.this).addToRequestQueue(jsonObjectRequest);
            }
        });
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }
}
