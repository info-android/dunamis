package com.dunamis1.dunamis1.Activity;

import android.app.Dialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.SHARED_PREF;

public class GuestSignupStep3Activity extends AppCompatActivity {
    RadioButton seeRad, metRad, prayRad;
    private CheckBox chk;
    private Boolean agreed = false;
    private RadioGroup dunamisAttend;
    private RadioGroup dunamisMember;
    private RadioGroup dunaDisiple;
    private RadioGroup rededicate;

    private String dunaAttendId = "";
    private String dunamisMemberId = "";
    private String dunaDisipleId = "";
    private String rededicateId = "";
    private String urgAssisId = "";
    private  String regId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_signup_step3);

        chk = findViewById(R.id.chk);
        seeRad = findViewById(R.id.seeRad);
        metRad = findViewById(R.id.metRad);
        prayRad = findViewById(R.id.prayRad);
        dunamisAttend = findViewById(R.id.dunamisAttend);
        dunamisMember = findViewById(R.id.dunamisMember);
        dunaDisiple = findViewById(R.id.dunaDisiple);
        rededicate = findViewById(R.id.rededicate);

        seeRad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                urgAssisId = "1";
                seeRad.setChecked(true);
                metRad.setChecked(false);
                prayRad.setChecked(false);
            }
        });

        metRad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                urgAssisId = "2";
                seeRad.setChecked(false);
                metRad.setChecked(true);
                prayRad.setChecked(false);
            }
        });

        prayRad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                urgAssisId = "3";
                prayRad.setChecked(true);
                seeRad.setChecked(false);
                metRad.setChecked(false);
            }
        });
        String text = "<font color=#ffffff>"+getString(R.string.byCre)+"</font><font color=#f3a600>"+" <u>"+getString(R.string.privacy)+"</u></font><font color=#ffffff>"+" "+getString(R.string.and)+"</font><font color=#f3a600>"+" <u>"+getString(R.string.terms)+"</u></font>";
        chk.setText(Html.fromHtml(text));

        chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(GuestSignupStep3Activity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.terms);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                Button conti = (Button) dialog.findViewById(R.id.conti);

                conti.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        agreed = true;
                        chk.setChecked(true);
                        signUp();

                    }
                });close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        dunamisAttend.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.attendYes){
                    dunaAttendId = "Y";
                }
                if(checkedId == R.id.attendNo){
                    dunaAttendId = "N";
                }
            }
        });
        dunamisMember.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.dunamemberYes){
                    dunamisMemberId = "Y";
                }
                if(checkedId == R.id.dunamemberNo){
                    dunamisMemberId = "N";
                }
            }
        });
        dunaDisiple.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.dunaDisipleYes){
                    dunaDisipleId = "Y";
                }
                if(checkedId == R.id.dunaDisipleNo){
                    dunaDisipleId = "N";
                }
            }
        });
        rededicate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.rededicateYes){
                    rededicateId = "Y";
                }
                if(checkedId == R.id.rededicateNo){
                    rededicateId = "N";
                }
            }
        });

    }

    public void signUp(){
        if(dunaAttendId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDunaAt), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(dunamisMemberId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDunaMem), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(urgAssisId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseUrgAss), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(dunaDisipleId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDisiple), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        if(rededicateId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseRededicate), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }

        if(agreed){

            if (MethodClass.isNetworkConnected(GuestSignupStep3Activity.this)) {
                MethodClass.showProgressDialog(GuestSignupStep3Activity.this);
                SharedPreferences pref = getSharedPreferences(SHARED_PREF, 0);
                regId = pref.getString("regId", null);
                String server_url = getString(R.string.SERVER_URL) + "guest-signup";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("title",getIntent().getStringExtra("title"));
                params.put("fname",getIntent().getStringExtra("fname"));
                params.put("lname",getIntent().getStringExtra("lname"));
                params.put("email",getIntent().getStringExtra("email"));
                params.put("phone",getIntent().getStringExtra("phone"));
                params.put("age",getIntent().getStringExtra("age"));
                params.put("gender",getIntent().getStringExtra("gender"));
                params.put("marital_status",getIntent().getStringExtra("martial"));
                params.put("country_id",getIntent().getStringExtra("country"));
                params.put("city_id",getIntent().getStringExtra("city"));
                params.put("district_id",getIntent().getStringExtra("district"));
                params.put("password",getIntent().getStringExtra("password"));
                params.put("church_location_id",getIntent().getStringExtra("branchId"));
                params.put("hear_about_us",getIntent().getStringExtra("hear"));
                params.put("out_of_town",getIntent().getStringExtra("visitout"));
                params.put("another_church",getIntent().getStringExtra("memberAnother"));
                params.put("born_again",getIntent().getStringExtra("bornagain"));
                params.put("baptized_in_water",getIntent().getStringExtra("baptized"));
                params.put("country_code",getIntent().getStringExtra("code"));
                params.put("attend_foundation_class",dunaAttendId);
                params.put("interested_membership",dunamisMemberId);
                params.put("assitance_required",urgAssisId);
                params.put("disciple_class",dunaDisipleId);
                params.put("rededicated_life_today",rededicateId);
                params.put("reg_id", regId);
                params.put("language",PreferenceManager.getDefaultSharedPreferences(GuestSignupStep3Activity.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("HomeRes", response.toString());

                        try {
                            MethodClass.hideProgressDialog(GuestSignupStep3Activity.this);

                            JSONObject result_Object = MethodClass.get_result_from_webservice(GuestSignupStep3Activity.this, response);
                            if (result_Object != null) {
                                final String vcode = result_Object.getString("otp");
                                new SweetAlertDialog(GuestSignupStep3Activity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("REGISTRATION SUCCESSFUL")
                                        .setContentText("An OTP has been sent to your registered phone number. Please enter that in the next page")
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                Intent I =new Intent(GuestSignupStep3Activity.this, VerificationActivity.class);
                                                I.putExtra("vcode",vcode);
                                                I.putExtra("phone",getIntent().getStringExtra("phone"));
                                                I.putExtra("email",getIntent().getStringExtra("email"));
                                                I.putExtra("password",getIntent().getStringExtra("password"));
                                                I.putExtra("from","G");
                                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(I);
                                            }
                                        })
                                        .show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(GuestSignupStep3Activity.this);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR", error.toString());
                        MethodClass.hideProgressDialog(GuestSignupStep3Activity.this);
                        if (error.toString().contains("ConnectException")) {
                            MethodClass.network_error_alert(GuestSignupStep3Activity.this);
                        } else {
                            MethodClass.error_alert(GuestSignupStep3Activity.this);
                        }

                    }
                }){
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        if(!PreferenceManager.getDefaultSharedPreferences(GuestSignupStep3Activity.this).getString("token", "").equals("")){
                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(GuestSignupStep3Activity.this).getString("token", ""));
                        }


                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Access the RequestQueue through your singleton class.
                MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
            } else {
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                snackbar.show();
                return;
            }
        }else{
            final Dialog dialog = new Dialog(GuestSignupStep3Activity.this);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.terms);
            ImageView close = (ImageView) dialog.findViewById(R.id.close);
            Button conti = (Button) dialog.findViewById(R.id.conti);

            conti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    agreed = true;
                    chk.setChecked(true);
                    signUp();
                }
            });close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    public void nextPage(View view){

        signUp();

    }
    public void Login(View view){
        Intent I = new Intent(this,LoginActivity.class);
        startActivity(I);
    }
    public void back(View view){
        super.onBackPressed();
    }
}
