package com.dunamis1.dunamis1.Activity.ChurchDepartment;

import android.Manifest;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.dunamis1.dunamis1.Activity.MemberSignupStep2Activity;
import com.dunamis1.dunamis1.Activity.VerificationActivity;
import com.dunamis1.dunamis1.Adapter.SeedsAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.Helper.RequestManager;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.HOME_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.PICK_FROM_GALLERY;
import static com.dunamis1.dunamis1.Helper.Constant.PICK_FROM_GALLERY_OREO;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.TAKE_PHOTO_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.TAKE_PHOTO_CODE2;

public class UpdateChurchDetailsActivity extends AppCompatActivity {
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView mon_frm_tv,tues_frm_tv,wed_frm_tv,thu_frm_tv,fri_frm_tv,sat_frm_tv,sun_frm_tv;
    private TextView mon_to_tv,tues_to_tv,wed_to_tv,thu_to_tv,fri_to_tv,sat_to_tv,sun_to_tv;
    private TextView rules_tv;
    private String mon_to_time="",tues_to_time="",wed_to_time="",thu_to_time="",fri_to_time="",sat_to_time="",sun_to_time="";
    private String mon_frm_time="",tues_frm_time="",wed_frm_time="",thu_frm_time="",fri_frm_time="",sat_frm_time="",sun_frm_time="";
    private StringBuilder day_str_bldr,frm_str_bldr,to_str_bldr;
    private ArrayList<String> day_list,frm_time_list,to_time_list;
    private String frm_time ="From time", to_time_ ="To time";
    private  String lasttime,final_frm_time,final_to_time;
    private Date date,date2,date3;
    private String user_to_timing_id,business_id,day_of_week="",from_time="",to_time="";
    private String day="",all_to_time="",all_frm_time="";

    private ImageView mfclose,mtclose,tufclose,tutclose,wfclose,wtclose,thfclose,thtclose,frfclose,frtclose,safclose,satclose,sufclose,sutclose;

    private Button next_btn;
    private LinearLayout uploac_img;
    private CircleImageView upload_imgs;
    private Uri outputFileUri;
    private String imageFilePath;
    private String product_image ="";
    private ArrayList<String> ListImage = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_church_details);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        mon_frm_tv=(TextView) findViewById(R.id.mon_frm_tv);
        tues_frm_tv=(TextView)findViewById(R.id.tues_frm_tv);
        wed_frm_tv=(TextView)findViewById(R.id.wed_frm_tv);
        thu_frm_tv=(TextView)findViewById(R.id.thu_frm_tv);
        fri_frm_tv=(TextView)findViewById(R.id.fri_frm_tv);
        sat_frm_tv=(TextView)findViewById(R.id.sat_frm_tv);
        sun_frm_tv=(TextView)findViewById(R.id.sun_frm_tv);
        upload_imgs = findViewById(R.id.upload_imgs);
        uploac_img = findViewById(R.id.uploac_img);
        mon_to_tv=(TextView)findViewById(R.id.mon_to_tv);
        tues_to_tv=(TextView)findViewById(R.id.tues_to_tv);
        wed_to_tv=(TextView)findViewById(R.id.wed_to_tv);
        thu_to_tv=(TextView)findViewById(R.id.thu_to_tv);
        fri_to_tv=(TextView)findViewById(R.id.fri_to_tv);
        sat_to_tv=(TextView)findViewById(R.id.sat_to_tv);
        sun_to_tv=(TextView)findViewById(R.id.sun_to_tv);

        mfclose=(ImageView) findViewById(R.id.mfclose);
        tufclose=(ImageView)findViewById(R.id.tufclose);
        wfclose=(ImageView)findViewById(R.id.wfclose);
        thfclose=(ImageView)findViewById(R.id.thfclose);
        frfclose=(ImageView)findViewById(R.id.frfclose);
        safclose=(ImageView)findViewById(R.id.safclose);
        sufclose=(ImageView)findViewById(R.id.sufclose);

        mtclose=(ImageView)findViewById(R.id.mtclose);
        tutclose=(ImageView)findViewById(R.id.tutclose);
        wtclose=(ImageView)findViewById(R.id.wtclose);
        thtclose=(ImageView)findViewById(R.id.thtclose);
        frtclose=(ImageView)findViewById(R.id.frtclose);
        satclose=(ImageView)findViewById(R.id.satclose);
        sutclose=(ImageView)findViewById(R.id.sutclose);

        rules_tv=(TextView)findViewById(R.id.rules_tv);
        next_btn=(Button)findViewById(R.id.next_btn);

        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }

        //set_all_spinner_entities();
        set_time_at_textview(mon_frm_tv);
        set_time_at_textview(tues_frm_tv);
        set_time_at_textview(wed_frm_tv);
        set_time_at_textview(thu_frm_tv);
        set_time_at_textview(fri_frm_tv);
        set_time_at_textview(sat_frm_tv);
        set_time_at_textview(sun_frm_tv);

        set_time_at_textview(mon_to_tv);
        set_time_at_textview(tues_to_tv);
        set_time_at_textview(wed_to_tv);
        set_time_at_textview(thu_to_tv);
        set_time_at_textview(fri_to_tv);
        set_time_at_textview(sat_to_tv);
        set_time_at_textview(sun_to_tv);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                day_list=new ArrayList<String>();
                frm_time_list=new ArrayList<String>();
                to_time_list=new ArrayList<String>();

                sun_frm_time=sun_frm_tv.getText().toString();
                mon_frm_time=mon_frm_tv.getText().toString();
                tues_frm_time=tues_frm_tv.getText().toString();
                wed_frm_time=wed_frm_tv.getText().toString();
                thu_frm_time=thu_frm_tv.getText().toString();
                fri_frm_time=fri_frm_tv.getText().toString();
                sat_frm_time=sat_frm_tv.getText().toString();

                sun_to_time=sun_to_tv.getText().toString();
                mon_to_time=mon_to_tv.getText().toString();
                tues_to_time=tues_to_tv.getText().toString();
                wed_to_time=wed_to_tv.getText().toString();
                thu_to_time=thu_to_tv.getText().toString();
                fri_to_time=fri_to_tv.getText().toString();
                sat_to_time=sat_to_tv.getText().toString();

                check_as_from_time();
                check_as_to_time();


                if (!mon_frm_time.equals("") && !mon_to_time.equals("")){
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dates;
                    Date dates2;
                    try {
                        dates = sdfs.parse(mon_frm_time);
                        dates2 = sdfs.parse(mon_to_time);
                        boolean isafter = dates2.after(dates);
                        if (!isafter) {
                            Snackbar.make(findViewById(android.R.id.content), "To time must be greater than from time for Monday", Snackbar.LENGTH_LONG).show();
                            return;
                        } else {
                            day_list.add("2");
                            frm_time_list.add(mon_frm_time);
                            to_time_list.add(mon_to_time);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (!tues_frm_time.equals("") && !tues_to_time.equals("")){
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dates;
                    Date dates2;
                    try {
                        dates = sdfs.parse(tues_frm_time);
                        dates2 = sdfs.parse(tues_to_time);
                        boolean isafter = dates2.after(dates);
                        if (!isafter) {
                            Snackbar.make(findViewById(android.R.id.content), "To time must be greater than from time for Tuesday", Snackbar.LENGTH_LONG).show();
                            return;
                        } else {
                            day_list.add("3");
                            frm_time_list.add(tues_frm_time);
                            to_time_list.add(tues_to_time);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (!wed_frm_time.equals("") && !wed_to_time.equals("")){
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dates;
                    Date dates2;
                    try {
                        dates = sdfs.parse(wed_frm_time);
                        dates2 = sdfs.parse(wed_to_time);
                        boolean isafter = dates2.after(dates);
                        if (!isafter) {
                            Snackbar.make(findViewById(android.R.id.content), "To time must be greater than from time for Wednesday", Snackbar.LENGTH_LONG).show();
                            return;
                        } else {
                            day_list.add("4");
                            frm_time_list.add(wed_frm_time);
                            to_time_list.add(wed_to_time);
                        }
                    }catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (!thu_frm_time.equals("") && !thu_to_time.equals("")){
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dates;
                    Date dates2;
                    try {
                        dates = sdfs.parse(thu_frm_time);
                        dates2 = sdfs.parse(thu_to_time);
                        boolean isafter = dates2.after(dates);
                        if (!isafter) {
                            Snackbar.make(findViewById(android.R.id.content), "To time must be greater than from time for Thursday", Snackbar.LENGTH_LONG).show();
                            return;
                        } else {
                            day_list.add("5");
                            frm_time_list.add(thu_frm_time);
                            to_time_list.add(thu_to_time);
                        }
                    }catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
                if (!fri_frm_time.equals("") && !fri_to_time.equals("")){
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dates;
                    Date dates2;
                    try {
                        dates = sdfs.parse(fri_frm_time);
                        dates2 = sdfs.parse(fri_to_time);
                        boolean isafter = dates2.after(dates);
                        if (!isafter) {
                            Snackbar.make(findViewById(android.R.id.content), "To time must be greater than from time for Friday", Snackbar.LENGTH_LONG).show();
                            return;
                        } else {
                            day_list.add("6");
                            frm_time_list.add(fri_frm_time);
                            to_time_list.add(fri_to_time);
                            Log.e("TO_TIME", to_time_list.toString() );
                        }
                    }catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (!sat_frm_time.equals("") && !sat_to_time.equals("")){
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dates;
                    Date dates2;
                    try {
                        dates = sdfs.parse(sat_frm_time);
                        dates2 = sdfs.parse(sat_to_time);
                        boolean isafter = dates2.after(dates);
                        if (!isafter) {
                            Snackbar.make(findViewById(android.R.id.content), "To time must be greater than from time for Saturday", Snackbar.LENGTH_LONG).show();
                            return;
                        } else {
                            day_list.add("7");
                            frm_time_list.add(sat_frm_time);
                            to_time_list.add(sat_to_time);
                        }
                    }catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (!sun_frm_time.equals("") && !sun_to_time.equals("")){
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dates;
                    Date dates2;
                    try {
                        dates = sdfs.parse(sun_frm_time);
                        dates2 = sdfs.parse(sun_to_time);
                        boolean isafter = dates2.after(dates);
                        if(!isafter){
                            Snackbar.make(findViewById(android.R.id.content), "To time must be greater than from time for Sunday", Snackbar.LENGTH_LONG).show();
                            return;
                        }else {
                            day_list.add("1");
                            frm_time_list.add(sun_frm_time);
                            to_time_list.add(sun_to_time);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

                day_str_bldr=new StringBuilder();
                frm_str_bldr=new StringBuilder();
                to_str_bldr=new StringBuilder();
                for (int i=0;i<day_list.size();i++){
                    day_str_bldr.append(day_list.get(i));
                    if (i < day_list.size() - 1) {
                        day_str_bldr.append(",");
                    }
                    frm_str_bldr.append(frm_time_list.get(i));
                    if (i < frm_time_list.size() - 1) {
                        frm_str_bldr.append(",");
                    }
                    Log.e("TO_TIME", to_time_list.get(i) );
                    to_str_bldr.append(to_time_list.get(i));
                    if (i < to_time_list.size() - 1) {
                        to_str_bldr.append(",");
                    }
                }

                day=day_str_bldr+"";
                all_frm_time=frm_str_bldr+"";
                all_to_time=to_str_bldr+"";
                Log.e("TOT", to_str_bldr+"" );
                if (all_frm_time.equals("")) {
                    Snackbar.make(findViewById(android.R.id.content), "Please enter at least one day's meeting time", Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (all_to_time.equals("")) {
                    Snackbar.make(findViewById(android.R.id.content), "Please enter at least one day's meeting time", Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (!mon_frm_time.equals("") ){
                    if (mon_to_time.equals("")) {
                        Snackbar.make(findViewById(android.R.id.content), "Please select to time for Monday", Snackbar.LENGTH_LONG).show();
                        return;
                    }

                }
                if (!tues_frm_time.equals("") ){
                    if (tues_to_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select to time for Tuesday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!wed_frm_time.equals("") ){
                    if ( wed_to_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select to time for Wednesday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!thu_frm_time.equals("") ){
                    if (thu_to_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select to time for Thursday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!fri_frm_time.equals("") ){
                    if (fri_to_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select to time for Friday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!sat_frm_time.equals("") ){
                    if (sat_to_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select to time for Saturday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!sun_frm_time.equals("") ){
                    if (sun_to_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select to time for Sunday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }



                if (!mon_to_time.equals("") ){
                    if (mon_frm_time.equals("")) {
                        Snackbar.make(findViewById(android.R.id.content), "Please select from time for Monday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!tues_to_time.equals("") ){
                    if (tues_frm_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select from time for Tuesday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!wed_to_time.equals("") ){
                    if ( wed_frm_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select from time  for Wednesday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!thu_to_time.equals("") ){
                    if (thu_frm_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select from time for Thursday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!fri_to_time.equals("") ){
                    if (fri_frm_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select from time  for Friday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!sat_to_time.equals("") ){
                    if (sat_frm_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select from time in Saturday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (!sun_to_time.equals("") ){
                    if (sun_frm_time.equals("")){
                        Snackbar.make(findViewById(android.R.id.content), "Please select from time in Sunday", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if(rules_tv.getText().toString().length() == 0){
                    rules_tv.setError(getString(R.string.ruleReq));
                    rules_tv.requestFocus();
                    return;
                }


                add_timings();


                Log.e( "day_str_bldr",day );
                //Toast.makeText(ProfileActivity2.this, ""+day, Toast.LENGTH_SHORT).show();

            }
        });

        mfclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mon_frm_tv.setText("");
            }
        });

        mtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mon_to_tv.setText("");
            }
        });
        tufclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tues_frm_tv.setText("");
            }
        });
        tutclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tues_to_tv.setText("");
            }
        });
        wfclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wed_frm_tv.setText("");
            }
        });
        wtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wed_to_tv.setText("");
            }
        });
        thfclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thu_frm_tv.setText("");
            }
        });
        thtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thu_to_tv.setText("");
            }
        });
        frfclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fri_frm_tv.setText("");
            }
        });
        frtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fri_to_tv.setText("");
            }
        });
        safclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sat_frm_tv.setText("");
            }
        });
        satclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sat_to_tv.setText("");
            }
        });
        sufclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sun_frm_tv.setText("");
            }
        });
        sutclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sun_to_tv.setText("");
            }
        });

        uploac_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu= new PopupMenu(UpdateChurchDetailsActivity.this,uploac_img);
                popupMenu.getMenuInflater().inflate(R.menu.camera_popup_menu,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id=item.getItemId();
                        if (id== R.id.camera){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (UpdateChurchDetailsActivity.this.checkSelfPermission(Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                                }
                                else{
                                    CameraIntent2();
                                }
                            }
                        }
                        else if (id==R.id.gallery){
                            try {
                                if (ActivityCompat.checkSelfPermission(UpdateChurchDetailsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(UpdateChurchDetailsActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                                } else {
                                    galleryIntent2();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        getTime();
    }
    private void CameraIntent2(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();

        }catch (Exception e){
            e.printStackTrace();
        }
        outputFileUri = FileProvider.getUriForFile(UpdateChurchDetailsActivity.this, "com.dunamis1.dunamis1.fileprovider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,outputFileUri);
        Log.e("Intentdata", intent.toString() );
        startActivityForResult(intent, TAKE_PHOTO_CODE2);

    }
    private void galleryIntent2()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_FROM_GALLERY_OREO);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == PICK_FROM_GALLERY_OREO) {
                Uri selectedUri;
                if (null != data) { // checking empty selection
                    if (null != data.getClipData()) { // checking multiple selection or not
                        if(data.getClipData().getItemCount()+ListImage.size() > 1){
                            ListImage = new ArrayList<String>();
                        }
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            selectedUri = data.getClipData().getItemAt(i).getUri();
                            try {
                                product_image = getPath(UpdateChurchDetailsActivity.this, selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        if(!data.getData().getAuthority().equals("com.android.providers.downloads.documents")){
                            selectedUri = data.getData();
                            try {
                                product_image = getPath(UpdateChurchDetailsActivity.this, selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.e("SIZE", String.valueOf(ListImage.size()));
                            Snackbar snackbar = Snackbar.make(UpdateChurchDetailsActivity.this.findViewById(android.R.id.content),"Please choose image from other folder", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    }
                    Log.e("SIZE", String.valueOf(ListImage.size()));
                    showImage2();
                }
            }  else if (requestCode == TAKE_PHOTO_CODE2) {

                product_image = imageFilePath;
                ListImage.add(product_image);
                showImage2();
            }

        }
    }
    private File createImageFile() throws IOException {
        File image = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = UpdateChurchDetailsActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }
    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        Log.e("LOGURI", uri.getScheme() );
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = UpdateChurchDetailsActivity.this.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    private void showImage2(){
        MethodClass.hideProgressDialog(UpdateChurchDetailsActivity.this);
        if(ListImage.size()>0){
            for (int i = 0; i < ListImage.size(); i++) {
                File imgFile = new  File(ListImage.get(i));
                if(imgFile.exists()){
                    Bitmap b = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    upload_imgs.setImageBitmap(Bitmap.createScaledBitmap(b, 120, 120, false));
                    //image.setImageBitmap(myBitmap);
                }
                final int finalI = i;
            }

        }
    }
    public void add_timings(){


        if (MethodClass.isNetworkConnected(UpdateChurchDetailsActivity.this)) {
            String server_url = UpdateChurchDetailsActivity.this.getString(R.string.SERVER_URL) + "home-church/opening-times/update";
            MethodClass.showProgressDialog(UpdateChurchDetailsActivity.this);
            SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    MethodClass.hideProgressDialog(UpdateChurchDetailsActivity.this);
                    try {
                        JSONObject jsonObject = MethodClass.get_result_from_webservice(UpdateChurchDetailsActivity.this, new JSONObject(response));
                        Log.e("RESULT", jsonObject.toString() );
                        if (jsonObject != null) {
                            JSONArray opening_times = jsonObject.getJSONArray("opening_times");
                            String message = jsonObject.getString("message");
                            String meaning = jsonObject.getString("meaning");
                            new SweetAlertDialog(UpdateChurchDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(message)
                                    .setContentText(meaning)
                                    .setConfirmText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            finish();
                                        }
                                    })
                                    .show();
                            if(opening_times.length()>0){
                                day_str_bldr=new StringBuilder();
                                frm_str_bldr=new StringBuilder();
                                to_str_bldr=new StringBuilder();
                                for (int i = 0; i <opening_times.length() ; i++) {
                                    JSONObject jsonObject2= opening_times.getJSONObject(i);
                                    day_of_week=jsonObject2.getString("day_no");
                                    from_time=jsonObject2.getString("from_time");
                                    to_time=jsonObject2.getString("to_time");


                                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                                    date2 = null;
                                    try {
                                        date2 = sdf.parse(from_time);
                                        SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm a");
                                        final_frm_time = sdf2.format(date2);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    SimpleDateFormat sdf3 = new SimpleDateFormat("HH:mm");
                                    date3 = null;
                                    try {
                                        date3 = sdf3.parse(to_time);
                                        SimpleDateFormat sdf4 = new SimpleDateFormat("hh:mm a");
                                        final_to_time = sdf4.format(date3);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    day_str_bldr.append(day_of_week);
                                    if (i < opening_times.length() - 1) {
                                        day_str_bldr.append(",");
                                    }

                                    frm_str_bldr.append(final_frm_time);
                                    if (i < opening_times.length() - 1) {
                                        frm_str_bldr.append(",");
                                    }

                                    to_str_bldr.append(final_to_time);
                                    if (i < opening_times.length() - 1) {
                                        to_str_bldr.append(",");
                                    }

                                    if (day_of_week.equals("1")){
                                        sun_frm_tv.setText(final_frm_time);
                                        sun_frm_time=final_frm_time;
                                        sun_to_tv.setText(final_to_time);
                                        sun_to_time=final_to_time;
                                    }
                                    if (day_of_week.equals("2")){
                                        mon_frm_tv.setText(final_frm_time);
                                        mon_to_time=final_frm_time;
                                        mon_to_tv.setText(final_to_time);
                                        mon_to_time=final_to_time;
                                    }
                                    if (day_of_week.equals("3")){
                                        tues_frm_tv.setText(final_frm_time);
                                        tues_frm_time=final_frm_time;
                                        tues_to_tv.setText(final_to_time);
                                        tues_to_time=final_to_time;
                                    }
                                    if (day_of_week.equals("4")){
                                        wed_frm_tv.setText(final_frm_time);
                                        wed_frm_time=final_frm_time;
                                        wed_to_tv.setText(final_to_time);
                                        wed_to_time=final_to_time;
                                    }
                                    if (day_of_week.equals("5")){
                                        thu_frm_tv.setText(final_frm_time);
                                        thu_frm_time=final_frm_time;
                                        thu_to_tv.setText(final_to_time);
                                        thu_to_time=final_to_time;
                                    }
                                    if (day_of_week.equals("6")){
                                        fri_frm_tv.setText(final_frm_time);
                                        fri_frm_time=final_frm_time;
                                        fri_to_tv.setText(final_to_time);
                                        fri_to_time=final_to_time;
                                    }
                                    if (day_of_week.equals("7")){
                                        sat_frm_tv.setText(final_frm_time);
                                        sat_frm_time=final_frm_time;
                                        sat_to_tv.setText(final_to_time);
                                        sat_to_time=final_to_time;
                                    }

                                }
                                day=day_str_bldr+"";
                                Log.e("day_str_bldr", day_str_bldr.toString() );
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    MethodClass.hideProgressDialog(UpdateChurchDetailsActivity.this);
                    Log.e("error", error.toString());
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(UpdateChurchDetailsActivity.this);
                    } else {
                        MethodClass.error_alert(UpdateChurchDetailsActivity.this);
                    }
                }
            }){
                // Passing some request headers
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(UpdateChurchDetailsActivity.this).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            RequestQueue requestQueue = RequestManager.getnstance(UpdateChurchDetailsActivity.this);


            simpleMultiPartRequest.addMultipartParam("language", "text", PreferenceManager.getDefaultSharedPreferences(this).getString("lang","en"));
            simpleMultiPartRequest.addMultipartParam("home_church_id", "text", getIntent().getStringExtra("home_church_id"));
            simpleMultiPartRequest.addMultipartParam("day", "text", day);
            simpleMultiPartRequest.addMultipartParam("from_time", "text", all_frm_time);
            Log.e("FROM", all_frm_time );
            simpleMultiPartRequest.addMultipartParam("to_time", "text", all_to_time);
            Log.e("FROM", to_time );
            simpleMultiPartRequest.addMultipartParam("rules", "text", rules_tv.getText().toString().trim());
            Log.e("FROM", rules_tv.getText().toString() );
            simpleMultiPartRequest.addMultipartParam("banner_count","text", String.valueOf(ListImage.size()));
            Log.e("FROM", String.valueOf(ListImage.size()) );
            if(ListImage.size()>0){
                for (int i = 0; i <ListImage.size() ; i++) {
                    simpleMultiPartRequest.addFile("banner_image"+(i+1), ListImage.get(i).toString());
                    Log.e("FROM", ListImage.get(i).toString() );
                }
            }

            simpleMultiPartRequest.setFixedStreamingMode(true);
            int socketTimeout = 100000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
            simpleMultiPartRequest.setRetryPolicy(policy);
            requestQueue.getCache().clear();
            requestQueue.add(simpleMultiPartRequest);

        } else {
            Snackbar snackbar = Snackbar.make(UpdateChurchDetailsActivity.this.findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }

    public void getTime(){
        MethodClass.showProgressDialog(UpdateChurchDetailsActivity.this);
        String server_url = UpdateChurchDetailsActivity.this.getString(R.string.SERVER_URL) + "home-church/opening-times";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(UpdateChurchDetailsActivity.this).getString("LANG","en"));
        params.put("home_church_id",getIntent().getStringExtra("home_church_id"));


        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(UpdateChurchDetailsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(UpdateChurchDetailsActivity.this, response);
                    if (result_Object != null) {
                        JSONArray opening_times = result_Object.getJSONArray("opening_times");
                        String rules = result_Object.getJSONObject("rules").getJSONObject("details").getString("rules");
                        String banner_img = result_Object.getJSONObject("rules").getString("banner_img");

                        Picasso.get().load(HOME_BANNER_IMG_URL+banner_img).placeholder(R.drawable.image60).error(R.drawable.image60).into(upload_imgs);
                        rules_tv.setText(rules);

                        day_str_bldr=new StringBuilder();
                        frm_str_bldr=new StringBuilder();
                        to_str_bldr=new StringBuilder();
                        for (int i = 0; i <opening_times.length() ; i++) {
                            JSONObject jsonObject= opening_times.getJSONObject(i);
                            day_of_week=jsonObject.getString("day_no");
                            from_time=jsonObject.getString("from_time");
                            to_time=jsonObject.getString("to_time");


                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                            date2 = null;
                            try {
                                date2 = sdf.parse(from_time);
                                SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm a");
                                final_frm_time = sdf2.format(date2);
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            SimpleDateFormat sdf3 = new SimpleDateFormat("HH:mm");
                            date3 = null;
                            try {
                                date3 = sdf3.parse(to_time);
                                SimpleDateFormat sdf4 = new SimpleDateFormat("hh:mm a");
                                final_to_time = sdf4.format(date3);
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            day_str_bldr.append(day_of_week);
                            if (i < opening_times.length() - 1) {
                                day_str_bldr.append(",");
                            }

                            frm_str_bldr.append(final_frm_time);
                            if (i < opening_times.length() - 1) {
                                frm_str_bldr.append(",");
                            }

                            to_str_bldr.append(final_to_time);
                            if (i < opening_times.length() - 1) {
                                to_str_bldr.append(",");
                            }

                            if (day_of_week.equals("1")){
                                sun_frm_tv.setText(final_frm_time);
                                sun_frm_time=final_frm_time;
                                sun_to_tv.setText(final_to_time);
                                sun_to_time=final_to_time;
                            }
                            if (day_of_week.equals("2")){
                                mon_frm_tv.setText(final_frm_time);
                                mon_to_time=final_frm_time;
                                mon_to_tv.setText(final_to_time);
                                mon_to_time=final_to_time;
                            }
                            if (day_of_week.equals("3")){
                                tues_frm_tv.setText(final_frm_time);
                                tues_frm_time=final_frm_time;
                                tues_to_tv.setText(final_to_time);
                                tues_to_time=final_to_time;
                            }
                            if (day_of_week.equals("4")){
                                wed_frm_tv.setText(final_frm_time);
                                wed_frm_time=final_frm_time;
                                wed_to_tv.setText(final_to_time);
                                wed_to_time=final_to_time;
                            }
                            if (day_of_week.equals("5")){
                                thu_frm_tv.setText(final_frm_time);
                                thu_frm_time=final_frm_time;
                                thu_to_tv.setText(final_to_time);
                                thu_to_time=final_to_time;
                            }
                            if (day_of_week.equals("6")){
                                fri_frm_tv.setText(final_frm_time);
                                fri_frm_time=final_frm_time;
                                fri_to_tv.setText(final_to_time);
                                fri_to_time=final_to_time;
                            }
                            if (day_of_week.equals("7")){
                                sat_frm_tv.setText(final_frm_time);
                                sat_frm_time=final_frm_time;
                                sat_to_tv.setText(final_to_time);
                                sat_to_time=final_to_time;
                            }

                        }
                        day=day_str_bldr+"";
                        Log.e("day_str_bldr", day_str_bldr.toString() );

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(UpdateChurchDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(UpdateChurchDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(UpdateChurchDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(UpdateChurchDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(UpdateChurchDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(UpdateChurchDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }



    private void set_time(final TextView textview){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(UpdateChurchDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String time = selectedHour+":"+selectedMinute;
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                date = null;
                try {
                    date = sdf.parse(time);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm a");
                    lasttime = sdf2.format(date);
                }catch (Exception e){
                    e.printStackTrace();
                }


                int id=textview.getId();
                textview.setText(lasttime);
                switch (id){
                    case R.id.sun_frm_tv:
                        sun_frm_time=textview.getText().toString();
                        
                        break;
                    case R.id.sun_to_tv:
                        sun_to_time=textview.getText().toString();
                        
                        break;
                    case R.id.mon_frm_tv:
                        mon_frm_time=textview.getText().toString();
                        
                        break;
                    case R.id.mon_to_tv:
                        mon_to_time=textview.getText().toString();
                        
                        break;
                    case R.id.tues_frm_tv:
                        tues_frm_time=textview.getText().toString();
                        
                        break;
                    case R.id.tues_to_tv:
                        tues_to_time=textview.getText().toString();
                        
                        break;
                    case R.id.wed_frm_tv:
                        wed_frm_time=textview.getText().toString();
                        
                        break;
                    case R.id.wed_to_tv:
                        wed_to_time=textview.getText().toString();
                        
                        break;
                    case R.id.thu_frm_tv:
                        thu_frm_time=textview.getText().toString();
                        
                        break;
                    case R.id.thu_to_tv:
                        thu_to_time=textview.getText().toString();
                        
                        break;
                    case R.id.fri_frm_tv:
                        fri_frm_time=textview.getText().toString();
                        
                        break;
                    case R.id.fri_to_tv:
                        fri_to_time=textview.getText().toString();
                        
                        break;
                    case R.id.sat_frm_tv:
                        sat_frm_time=textview.getText().toString();
                        
                        break;
                    case R.id.sat_to_tv:
                        sat_to_time=textview.getText().toString();
                        
                        break;

                }

            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void set_time_at_textview(final TextView textview){
        textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set_time(textview);
            }
        });
    }



    public void back(View view) {
        onBackPressed();
        finish();
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }public void  openTime(View view){
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(UpdateChurchDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
    private void check_as_from_time(){
        if (sun_frm_time.equals("From Time")){
            sun_frm_time="";
        }
        if (mon_frm_time.equals("From Time")){
            mon_frm_time="";
        }
        if (tues_frm_time.equals("From Time")){
            tues_frm_time="";
        }
        if (wed_frm_time.equals("From Time")){
            wed_frm_time="";
        }
        if (thu_frm_time.equals("From Time")){
            thu_frm_time="";
        }
        if (fri_frm_time.equals("From Time")){
            fri_frm_time="";
        }
        if (sat_frm_time.equals("From Time")){
            sat_frm_time="";
        }
    }
    private void check_as_to_time(){
        if (sun_to_time.equals("To Time")){
            sun_to_time="";
        }
        if (mon_to_time.equals("To Time")){
            mon_to_time="";
        }
        if (tues_to_time.equals("To Time")){
            tues_to_time="";
        }
        if (wed_to_time.equals("To Time")){
            wed_to_time="";
        }
        if (thu_to_time.equals("To Time")){
            thu_to_time="";
        }
        if (fri_to_time.equals("To Time")){
            fri_to_time="";
        }
        if (sat_to_time.equals("To Time")){
            sat_to_time="";
        }
    }
}
