package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.CommentsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Adapter.SeedsAdapter;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_TITLE;

public class DestinyVideoDetails extends AppCompatActivity {
    int[] sampleImagesArr = {R.drawable.image60,R.drawable.image95,R.drawable.image96,R.drawable.image97, R.drawable.image98};
    private String[] title_array = {"Dr Becky Paul-Enenche - SEEDS OF DESTINY - THURSDAY, 12 MARCH, 2020", "Dr. Becky Paul Enenche - Seeds of Destiny - FRIDAY 11ST MARCH 2020", "Dr. Becky Paul Enenche - Seeds of Destiny - THURSHDAT 20TH MARCH 2020","Dr. Becky Paul Enenche - Seeds of Destiny - WEDNESDAY 19TH MARCH 2020","Dr. Becky Paul Enenche - Seeds of Destiny - MONDAY 17TH MARCH 2020"};
    private String[] Date_array = {"Mar 12,2020", "Mar 11,2020", "Mar 10,2020", "Mar 09,2020", "Mar 07,2020"};
    private ArrayList<HashMap<String,String>> map_list;
    private RecyclerView rv_Dash1;
    private RelativeLayout imglayout;
    private YouTubePlayerView youtube_player_view;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;


    private ImageView image_tv;
    private ImageView fav;
    private TextView title;
    private TextView desc;
    private TextView seddLike;
    private TextView seedComm;
    private TextView seedShare;

    private LinearLayout seddLikeCon;
    private LinearLayout seedCommCon;
    private LinearLayout seedShareCon;

    private TextView nav_name_tv;
    private CircleImageView imgPro;


    private String from = "";


    private NestedScrollView scrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destiny_video_details);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        scrollView = findViewById(R.id.scrollView);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        imglayout = (RelativeLayout) findViewById(R.id.imglayout);
        youtube_player_view = (YouTubePlayerView) findViewById(R.id.youtube_player_view);


        image_tv =  findViewById(R.id.image_tv);
        fav =  findViewById(R.id.fav);
        title =  findViewById(R.id.title);
        desc =  findViewById(R.id.desc);
        seddLike =  findViewById(R.id.seddLike);
        seedComm =  findViewById(R.id.seedComm);
        seedShare =  findViewById(R.id.seedShare);

        seddLikeCon =  findViewById(R.id.seddLikeCon);
        seedCommCon =  findViewById(R.id.seedCommCon);
        seedShareCon =  findViewById(R.id.seedShareCon);

        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);


        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);

        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = getString(R.string.SERVER_URL) + "favourite";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", getIntent().getStringExtra("id"));
                params.put("type", "S");
                params.put("language",PreferenceManager.getDefaultSharedPreferences(DestinyVideoDetails.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(DestinyVideoDetails.this, response);
                            if (result_Object != null) {
                                String favourite = result_Object.getString("favourite");
                                if(favourite.equals("Y")){
                                    fav.setImageResource(R.drawable.ic_heart);
                                }else{
                                    fav.setImageResource(R.drawable.ic_heart_blank);
                                }
                                String message = result_Object.getString("message");
                                String meaning = result_Object.getString("meaning");
                                new SweetAlertDialog(DestinyVideoDetails.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(message)
                                        .setContentText(meaning)
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(DestinyVideoDetails.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DestinyVideoDetails.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(DestinyVideoDetails.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(DestinyVideoDetails.this).addToRequestQueue(jsonObjectRequest);
            }
        });

        getDep();


        seedShareCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "http://phpwebdevelopmentservices.com/development/dunamis/api/share?content="+getIntent().getStringExtra("id")+"&language="+PreferenceManager.getDefaultSharedPreferences(DestinyVideoDetails.this).getString("lang","en")+"&type=S");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        seddLikeCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = getString(R.string.SERVER_URL) + "seedof-destiny/like";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("seedof_destiny_id", getIntent().getStringExtra("id"));
                params.put("language",PreferenceManager.getDefaultSharedPreferences(DestinyVideoDetails.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(DestinyVideoDetails.this, response);
                            if (result_Object != null) {
                                String total_likes = result_Object.getString("total_likes");
                                seddLike.setText(total_likes+" "+DestinyVideoDetails.this.getString(R.string.likes));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(DestinyVideoDetails.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DestinyVideoDetails.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(DestinyVideoDetails.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(DestinyVideoDetails.this).addToRequestQueue(jsonObjectRequest);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));

    }
    public void getDep(){
        MethodClass.showProgressDialog(DestinyVideoDetails.this);
        String server_url = DestinyVideoDetails.this.getString(R.string.SERVER_URL) + "seedof-destiny/details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(DestinyVideoDetails.this).getString("LANG","en"));
        params.put("seedof_destiny_id",getIntent().getStringExtra("id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(DestinyVideoDetails.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(DestinyVideoDetails.this, response);
                    if (result_Object != null) {
                        String favourites = result_Object.getString("favourite");
                        JSONObject sedddetails = result_Object.getJSONObject("details");
                        title.setText(sedddetails.getJSONObject("destinydetails").getString("title"));
                        desc.setText(sedddetails.getJSONObject("destinydetails").getString("description"));
                        final String codes = sedddetails.getString("code");
                        String url = "https://img.youtube.com/vi/"+codes+"/mqdefault.jpg";
                        Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).into(image_tv);

                        seedComm.setText(sedddetails.getString("total_comments")+" "+getString(R.string.comm));
                        seddLike.setText(sedddetails.getString("total_likes")+" "+getString(R.string.likes));
                        seedShare.setText(sedddetails.getString("total_share")+" "+getString(R.string.shar));

                        if(favourites.equals("Y")){
                            fav.setImageResource(R.drawable.ic_heart);
                        }else{
                            fav.setImageResource(R.drawable.ic_heart_blank);
                        }
                        seedCommCon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent I = new Intent(DestinyVideoDetails.this, CommentsActivity.class);
                                I.putExtra("from","seed");
                                I.putExtra("id",getIntent().getStringExtra("id"));
                                startActivity(I);
                            }
                        });

                        imglayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imglayout.setVisibility(View.GONE);
                                youtube_player_view.setVisibility(View.VISIBLE);
                                youtube_player_view.initialize(new YouTubePlayerInitListener() {
                                    @Override
                                    public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                                        initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                            @Override
                                            public void onReady() {
                                                String videoId = codes;
                                                initializedYouTubePlayer.loadVideo(videoId, 0);
                                            }
                                        });
                                    }
                                }, true);
                            }
                        });



                        JSONArray seedArray = result_Object.getJSONArray("recent");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            JSONObject SeedObj = seedArray.getJSONObject(i);
                            String title = SeedObj.getJSONObject("destinydetails").getString("title");
                            String thubmnail = SeedObj.getString("thumbnail");
                            String code = SeedObj.getString("code");
                            String date = SeedObj.getString("date");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String id = SeedObj.getString("id");


                            HashMap<String,String> map = new HashMap<>();
                            map.put(SEED_TITLE,title);
                            map.put(SEED_THUMB,thubmnail);
                            map.put(SEED_DATE,date);
                            map.put(SEED_CODE,code);
                            map.put(SEED_LIKE,total_likes);
                            map.put(SEED_COMMENT,total_comments);
                            map.put(SEED_SHARE,total_share);
                            map.put(SEED_ID,id);

                            map_list.add(map);
                        }
                        SeedsAdapter bibleDeuteronomyAdapter= new SeedsAdapter(DestinyVideoDetails.this,map_list);
                        rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(DestinyVideoDetails.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(DestinyVideoDetails.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(DestinyVideoDetails.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DestinyVideoDetails.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(DestinyVideoDetails.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(DestinyVideoDetails.this).addToRequestQueue(jsonObjectRequest);
//        map_list = new ArrayList<>();
//
//        for (int i = 0; i <title_array.length ; i++) {
//            HashMap<Object,Object> map = new HashMap<>();
//            map.put(TITLE,title_array[i]);
//            map.put(DATE,Date_array[i]);
//            map.put(IMAGE,sampleImagesArr[i]);
//            map_list.add(map);
//        }
//        SeedsAdapter bibleDeuteronomyAdapter= new SeedsAdapter(DestinyVideoDetails.this,map_list);
//        rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
//        rv_Dash1.setFocusable(false);
    }
    public void back(View view){
        if(isTaskRoot()){
            if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
                startActivity(new Intent(this,DashboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
                startActivity(new Intent(this,GuestDashboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
                startActivity(new Intent(this,HomeChurchDahboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
                startActivity(new Intent(this,ChurchDepartmentDashboardActivity.class));
                finish();
            }

        }else{
            super.onBackPressed();
        }

    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }public void  comments(View view){
        Intent I = new Intent(this, CommentsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
