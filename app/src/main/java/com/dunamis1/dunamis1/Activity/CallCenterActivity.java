package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.dunamis1.dunamis1.R;

public class CallCenterActivity extends AppCompatActivity {

    private ImageView call,call1,call2,msg,msg1,msg2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_center);
        call = findViewById(R.id.call);
        call1 = findViewById(R.id.call1);
        call2 = findViewById(R.id.call2);
        msg = findViewById(R.id.msg);
        msg1 = findViewById(R.id.msg1);
        msg2 = findViewById(R.id.msg2);

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:2348033144509"));
                startActivity(intent);
            }
        });
        call1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:2347032144609"));
                startActivity(intent);
            }
        });
        call2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:2349031144619"));
                startActivity(intent);
            }
        });
        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:2348033144509"));
                intentsms.putExtra("sms_body", "");
                startActivity(intentsms);
            }
        });
        msg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:2347032144609"));
                intentsms.putExtra("sms_body", "");
                startActivity(intentsms);
            }
        });
        msg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:2349031144619"));
                intentsms.putExtra("sms_body", "");
                startActivity(intentsms);
            }
        });


    }
    public void back(View view){
        super.onBackPressed();
    }
}
