package com.dunamis1.dunamis1.Activity.ChurchDepartment;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.DashboardActivity;
import com.dunamis1.dunamis1.Adapter.DashboardThreeAdapter;
import com.dunamis1.dunamis1.Adapter.HomeChurchTImeAdapter;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NoticeActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.HomeChurchDetailsPhotoAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.DAYS;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.FROM_TIME;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.TO_TIME;
import static com.dunamis1.dunamis1.Helper.Constant.VIEWS;

public class HomeChurchDetailsActivity extends AppCompatActivity {
    int[] pic_array = {R.drawable.fgh, R.drawable.fgdfh, R.drawable.image115};
    private ArrayList<HashMap<String, Object>> photo_map_list;
    private ArrayList<HashMap<String, Object>> time_map_list;
    private RecyclerView photo_rv;
    private RecyclerView rv_DashTime;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private ImageView editDet;
    private String edit = "F";
    private CircleImageView imgPro;
    private TextView nav_name_tv;
    private TextView hc_name,hc_loc,hc_member,hc_rules,hc_host;
    private ImageView hc_banner;
    private NestedScrollView scrollView;
    private LinearLayout photoLayout;
    private LinearLayout noticeLayout;
    private TextView noticeDesc;
    private TextView titleHead;
    private TextView mesgs;
    private String user_id = "";
    private Button register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_church_details);
        photo_rv = (RecyclerView) findViewById(R.id.photo_rv);
        rv_DashTime = (RecyclerView) findViewById(R.id.rv_DashTime);
        editDet = (ImageView) findViewById(R.id.editDet);
        noticeLayout = (LinearLayout) findViewById(R.id.noticeLayout);
        noticeDesc = (TextView) findViewById(R.id.noticeDesc);
        hc_name = findViewById(R.id.hc_name);
        hc_loc = findViewById(R.id.hc_loc);
        hc_member = findViewById(R.id.hc_member);
        hc_rules = findViewById(R.id.hc_rules);
        hc_host = findViewById(R.id.hc_host);
        hc_banner = findViewById(R.id.hc_banner);
        scrollView = findViewById(R.id.scrollView);
        photoLayout = findViewById(R.id.photoLayout);
        titleHead = findViewById(R.id.titleHead);
        mesgs = findViewById(R.id.mesgs);
        register = findViewById(R.id.register);

        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        user_id = PreferenceManager.getDefaultSharedPreferences(this).getString("user_id","");
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
            editDet.setVisibility(View.GONE);
            hc_member.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
            editDet.setVisibility(View.GONE);
            hc_member.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
            editDet.setVisibility(View.VISIBLE);
            hc_member.setVisibility(View.VISIBLE);
            editDet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent I = new Intent(HomeChurchDetailsActivity.this,UpdateChurchDetailsActivity.class);
                    I.putExtra("home_church_id", getIntent().getStringExtra("home_church_id"));
                    startActivity(I);
                }
            });
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
            editDet.setVisibility(View.GONE);
            hc_member.setVisibility(View.GONE);
        }
        get_photo_list();
    }
    @Override
    protected void onResume() {
        super.onResume();
        get_photo_list();
        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void back(View view){
        super.onBackPressed();
    }
    private void get_photo_list() {
        MethodClass.showProgressDialog(HomeChurchDetailsActivity.this);
        String server_url = HomeChurchDetailsActivity.this.getString(R.string.SERVER_URL) + "member/homechurch-details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(HomeChurchDetailsActivity.this).getString("LANG","en"));
        params.put("home_church_id", getIntent().getStringExtra("home_church_id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(HomeChurchDetailsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(HomeChurchDetailsActivity.this, response);
                    if (result_Object != null) {

                        final String admin_id = result_Object.getString("admin_id");
                        if(PreferenceManager.getDefaultSharedPreferences(HomeChurchDetailsActivity.this).getString("role","Member").equals("Home")){
                            mesgs.setVisibility(View.GONE);
                            register.setVisibility(View.GONE);
                        }
                        else if(user_id.equals(admin_id)){
                            mesgs.setVisibility(View.GONE);
                            register.setVisibility(View.GONE);
                        }else {
                            String my_home_church = result_Object.getString("my_home_church");
                            if(!my_home_church.equals("")) {
                                JSONObject myHome = new JSONObject(my_home_church);
                                String home_church_join_status = result_Object.getString("home_church_join_status");
                                String id = myHome.getString("id");
                                if(getIntent().getStringExtra("home_church_id").equals(id)){
                                    if(home_church_join_status.equals("R")){
                                        register.setVisibility(View.VISIBLE);
                                        register.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                registerCHurch();
                                            }
                                        });
                                    }else if(home_church_join_status.equals("I")){
                                        register.setText(getString(R.string.waiting));
                                        register.setVisibility(View.VISIBLE);
                                        register.setClickable(false);
                                        register.setOnClickListener(null);
                                    }else {
                                        register.setVisibility(View.GONE);
                                    }
                                }else {
                                    register.setVisibility(View.GONE);
                                }
                            }else {
                                register.setVisibility(View.VISIBLE);
                                register.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        registerCHurch();
                                    }
                                });
                            }
                            mesgs.setVisibility(View.VISIBLE);
                            mesgs.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(HomeChurchDetailsActivity.this,MessageChatActivity.class);
                                    I.putExtra("rec_id",admin_id);
                                    startActivity(I);
                                }
                            });
                        }
                        JSONObject home_church_details = result_Object.getJSONObject("home_church_details");
                        String banner_img = home_church_details.getString("banner_img");
                        String tot_member = home_church_details.getString("tot_member");
                        String google_address = home_church_details.getString("google_address");
                        JSONObject details = home_church_details.getJSONObject("details");
                        String home_church_name = details.getString("home_church_name");
                        String host_name = details.getString("host_name");
                        String rules = details.getString("rules");

                        Picasso.get().load(HOME_BANNER_IMG_URL+banner_img).placeholder(R.drawable.image60).error(R.drawable.image60).into(hc_banner);
                        hc_host.setText(host_name);
                        hc_member.setText(getString(R.string.member)+" : "+tot_member);
                        hc_name.setText(home_church_name);
                        titleHead.setText(home_church_name);
                        hc_loc.setText(google_address);
                        hc_rules.setText(rules);

                        time_map_list = new ArrayList<HashMap<String, Object>>();
                        JSONArray timing = home_church_details.getJSONArray("timing");
                        for (int i = 0; i <timing.length() ; i++) {
                            String day = timing.getJSONObject(i).getString("day");
                            String from_time = timing.getJSONObject(i).getString("from_time");
                            String to_time = timing.getJSONObject(i).getString("to_time");
                            HashMap<String, Object> maps = new HashMap<String, Object>();

                            maps.put(DAYS,day);
                            maps.put(FROM_TIME,from_time);
                            maps.put(TO_TIME,to_time);
                            time_map_list.add(maps);
                        }
                        HomeChurchTImeAdapter timeadapter = new HomeChurchTImeAdapter(HomeChurchDetailsActivity.this, time_map_list);
                        rv_DashTime.setAdapter(timeadapter);
                        rv_DashTime.setFocusable(false);

                        JSONArray photos = result_Object.getJSONArray("photos");
                        if(photos.length()>0){
                            photo_map_list = new ArrayList<HashMap<String, Object>>();
                            for (int i = 0; i <photos.length(); i++) {
                                HashMap<String, Object> map = new HashMap<String, Object>();
                                map.put(IMG, photos.getJSONObject(i).getString("image"));
                                photo_map_list.add(map);
                            }
                            HomeChurchDetailsPhotoAdapter adapter = new HomeChurchDetailsPhotoAdapter(HomeChurchDetailsActivity.this, photo_map_list);
                            photo_rv.setAdapter(adapter);
                            photo_rv.setFocusable(false);
                            photoLayout.setVisibility(View.VISIBLE);
                        }else {
                            photoLayout.setVisibility(View.GONE);
                        }

                        String notice = result_Object.getString("notice");
                        if(!notice.equals("null") && !notice.equals(null)){
                            noticeLayout.setVisibility(View.VISIBLE);
                            noticeDesc.setText(result_Object.getJSONObject("notice").getJSONObject("notice_details").getString("description"));
                        }else {
                            noticeDesc.setText("N/A");
                        }
                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(HomeChurchDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(HomeChurchDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(HomeChurchDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(HomeChurchDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(HomeChurchDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(HomeChurchDetailsActivity.this).addToRequestQueue(jsonObjectRequest);

    }

    public void registerCHurch(){
        MethodClass.showProgressDialog(HomeChurchDetailsActivity.this);
        String server_url = HomeChurchDetailsActivity.this.getString(R.string.SERVER_URL) + "member/homechurch/join";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(HomeChurchDetailsActivity.this).getString("LANG","en"));
        params.put("home_church_id", getIntent().getStringExtra("home_church_id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(HomeChurchDetailsActivity.this);
                    JSONObject result_Object = MethodClass.get_result_from_webservice(HomeChurchDetailsActivity.this, response);
                    if (result_Object != null) {
                        String message = result_Object.getString("message");
                        get_photo_list();
                        final Dialog dialog = new Dialog(HomeChurchDetailsActivity.this);
                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.join_popup);
                        ImageView close = (ImageView) dialog.findViewById(R.id.close);
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(HomeChurchDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(HomeChurchDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(HomeChurchDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(HomeChurchDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(HomeChurchDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(HomeChurchDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void member(View view){
        Intent I = new Intent(HomeChurchDetailsActivity.this,ManageHomeChurchMembersActivity.class);
        I.putExtra("home_church_id",getIntent().getStringExtra("home_church_id"));
        startActivity(I);
    }
    public void notice(View view){
        Intent I = new Intent(HomeChurchDetailsActivity.this,NoticeActivity.class);
        I.putExtra("from","H");
        I.putExtra("id",getIntent().getStringExtra("home_church_id"));

        startActivity(I);
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
    public void  mesgs(View view){
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.msg_popup);
        dialog.setCancelable(false);
        Button yes_btn = (Button) dialog.findViewById(R.id.yes_btn);
        Button no_btn = (Button) dialog.findViewById(R.id.no_btn);
        yes_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(HomeChurchDetailsActivity.this, MessageChatActivity.class));
            }
        });
        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
