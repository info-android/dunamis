package com.dunamis1.dunamis1.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.DepartmentListingActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.DepartmentUpdateProfileActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ManageHomeChurchMembersActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.RotaActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Adapter.DashboardThreeAdapter;
import com.dunamis1.dunamis1.Adapter.SliderAdapterExample;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.Helper.SliderItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chahinem.pageindicator.PageIndicator;
import com.dunamis1.dunamis1.Adapter.DashboardOneAdapter;
import com.dunamis1.dunamis1.Adapter.DashboardSlideAdapter;
import com.dunamis1.dunamis1.Adapter.DashboardTwoAdapter;
import com.dunamis1.dunamis1.R;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_ID;
import static com.dunamis1.dunamis1.Helper.Constant.DEPARTMENT_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SAVE;
import static com.dunamis1.dunamis1.Helper.Constant.FROMNOTI;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_ID;
import static com.dunamis1.dunamis1.Helper.Constant.ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SOCIAL;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.VIEWS;

public class ChurchDepartmentDashboardActivity extends AppCompatActivity implements OnMapReadyCallback {
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;

    RelativeLayout depChurch,depChurch1;

    int[] sampleImages = {R.drawable.dashcarousel, R.drawable.dashcarousel, R.drawable.dashcarousel};
    private String[] title_array = {"Dunamis International Gospel Centre", "Dunamis Gospel", "Dunamisgospel", "MID-DAY WORSHIP 20.02.2020"};
    private String[] title_array2 = {"National worship presence", "Event title show here", "Event title show here"};
    private String[] title_array3 = {"House of Favour", "House of Light", "Prosperity Centre"};
    private String[] date_array = {"20 Mar 2019", "24 Mar 2019", "29 Mar 2019"};
    private String desc_array[] = {"Beloved, who are you blessing? Who is feeding from your table other than you and your family? Make up...", "Welcome to the 2020 Nations Worship In His Presence Live at the Dunamis Glory Dome. Get ready to be....", "We are thankful to god for strength so far, for help available, for the abundunce of testimoies for the..",
            "Subscribe to our youtube channel for more videos: https://www.youtube.com/user/dunamistvng/live..."};
    int[] sampleImagesArr = {R.drawable.taaftr, R.drawable.tdftr, R.drawable.tfftr,R.drawable.tftr};
    int[] sampleImagesArr2 = {R.drawable.daddy, R.drawable.mummy, R.drawable.daddy};
    int[] sampleImagesArr3 = {R.drawable.dashcarousel, R.drawable.image60, R.drawable.dashchurchnear3};
    int[] sampleImagesArr4 = {R.drawable.imgvideo9,0, R.drawable.march_worship__word_and_wonders_night, R.drawable.drone_view};
    int[] socialArr = {R.drawable.image99, R.drawable.image100, R.drawable.image101,R.drawable.image102};

    private String[] name_array = {"Mrs. Josephine Egwuonwu", "Mr/Mrs. Ogun", "Bro. Emmanuel"};
    private String[] place_array = {"F13 Close House 17 Cities Mobora Estate", "A. avenue, No 27 Citec Estate", "B6, House No 5, Citec"};
    private String[] member_array = {"Members: 16", "Members: 20", "Members: 18"};
    private String[] vieworJoin = {"View Details", "+ join", "+ join"};


    private String[] depName = {"Councelling", "Audio Ministry", "Councelling"};


    private ArrayList<HashMap<Object,Object>> map_list;
    private ArrayList<HashMap<Object,Object>> map_list2;
    private ArrayList<HashMap<Object,Object>> map_list3;
    private ArrayList<HashMap<Object,Object>> map_list4;

    private RecyclerView rv_Dash1;
    private RecyclerView rv_Dash2;
    private RecyclerView rv_Dash3;

    private CircleImageView imgPro;
    private TextView nav_name_tv;
    private TextView prdct_title_tv;
    private ImageView prdct_iv;

    private NestedScrollView scrollView;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;

    private TextView totMember;
    private TextView newsFeedHead;
    private TextView churchDepTitle;
    private LinearLayout eventHead,departHead;
    private String branch_id="";
    private String idss="";
    private SliderView imageSlider;
    private SliderAdapterExample adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_church_department_dashboard);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        rv_Dash2 = (RecyclerView) findViewById(R.id.rv_Dash2);
        depChurch = (RelativeLayout) findViewById(R.id.depChurch);
        depChurch1 = (RelativeLayout) findViewById(R.id.depChurch1);
        totMember = (TextView) findViewById(R.id.totMember);
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        prdct_title_tv = (TextView) findViewById(R.id.prdct_title_tv);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        eventHead = findViewById(R.id.eventHead);
        departHead = findViewById(R.id.departHead);
        newsFeedHead = findViewById(R.id.newsFeedHead);
        churchDepTitle = findViewById(R.id.churchDepTitle);
        prdct_iv = findViewById(R.id.prdct_iv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(ChurchDepartmentDashboardActivity.this);
        imageSlider = findViewById(R.id.imageSlider);

        getDash1();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void getDash1(){
        MethodClass.showProgressDialog(ChurchDepartmentDashboardActivity.this);
        String server_url = ChurchDepartmentDashboardActivity.this.getString(R.string.SERVER_URL) + "church-department/dashboard";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language",PreferenceManager.getDefaultSharedPreferences(this).getString("lang","en"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ChurchDepartmentDashboardActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(ChurchDepartmentDashboardActivity.this, response);
                    if (result_Object != null) {
                        //for newsfeed
                        map_list = new ArrayList<>();
                        for (int i = 0; i <title_array.length ; i++) {
                            HashMap<Object,Object> map = new HashMap<>();
                            map.put(TITLE,title_array[i]);
                            map.put(DESC,desc_array[i]);
                            map.put(IMAGE,sampleImagesArr[i]);
                            map.put(SOCIAL,socialArr[i]);
                            map_list.add(map);
                        }
                        DashboardOneAdapter bibleDeuteronomyAdapter= new DashboardOneAdapter(ChurchDepartmentDashboardActivity.this,map_list);
                        rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                        rv_Dash1.setFocusable(false);
                        //newsfeed ends


                        //for events
                        JSONArray event = result_Object.getJSONArray("event");
                        if(event.length()>0){
                            map_list2 = new ArrayList<>();

                            for (int j = 0; j <event.length() ; j++) {

                                String thumbnail = event.getJSONObject(j).getString("thumbnail");
                                String from_date = event.getJSONObject(j).getString("from_date");
                                String id = event.getJSONObject(j).getString("id");
                                String title = event.getJSONObject(j).getJSONObject("eventdetails").getString("title");
                                HashMap<Object,Object> map2 = new HashMap<>();
                                map2.put(TITLE,title);
                                map2.put(DESC,from_date);
                                map2.put(IMAGE,thumbnail);
                                map2.put(EVENT_ID,id);
                                map2.put(EVENT_SAVE,"");

                                map_list2.add(map2);
                            }
                            DashboardTwoAdapter adapter2= new DashboardTwoAdapter(ChurchDepartmentDashboardActivity.this,map_list2);
                            rv_Dash2.setAdapter(adapter2);
                            rv_Dash2.setFocusable(false);
                        }else{
                            rv_Dash2.setVisibility(View.GONE);
                            eventHead.setVisibility(View.GONE);
                        }

                        //events end


                        //department starts
                        String my_church_department = result_Object.getString("my_church_department");
                        if(!my_church_department.equals(null) && !my_church_department.equals("null")){
                            JSONObject department = result_Object.getJSONObject("my_church_department");
                            if(department.length()>0){
                                depChurch.setVisibility(View.VISIBLE);
                                prdct_title_tv.setText(department.getJSONObject("department").getString("department_name"));
                                Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+department.getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(prdct_iv);
                                final String depId = department.getString("id");
                                idss = department.getString("id");
                                churchDepTitle.setText(department.getJSONObject("branchdetails").getString("name")+">"+getString(R.string.depart));
                                totMember.setText(getString(R.string.totMem)+" "+department.getString("total_member"));
                                branch_id = department.getString("branch_id");
                                depChurch.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent I = new Intent(ChurchDepartmentDashboardActivity.this, ChurchDetailActivity.class);
                                        I.putExtra("id",depId);
                                        startActivity(I);
                                    }
                                });
                            }else {
                                depChurch.setVisibility(View.GONE);
                                departHead.setVisibility(View.GONE);
                            }
                        }else{
                            depChurch.setVisibility(View.GONE);
                            departHead.setVisibility(View.GONE);
                        }

                        //department end

                        //dunamis1 near me
                        JSONArray dunamis_church_list =  result_Object.getJSONArray("dunamis_church_list");
                        if(dunamis_church_list.length()>0){
                            for (int S = 0; S <dunamis_church_list.length() ; S++) {
                                String latitude = dunamis_church_list.getJSONObject(S).getString("latitude");
                                String longitude = dunamis_church_list.getJSONObject(S).getString("longitude");
                                String name = dunamis_church_list.getJSONObject(S).getJSONObject("churchdetails").getString("name");
                                createMArker(Double.parseDouble(latitude),Double.parseDouble(longitude),name);
                            }
                        }

                        //end dunamis1 near me
                        List<SliderItem> sliderItemList = new ArrayList<>();
                        //dummy data
                        for (int i = 0; i < 4; i++) {
                            SliderItem sliderItem = new SliderItem();
                            if(i == 0){
                                sliderItem.setImageUrl(R.drawable.imgvideo9);
                            }else if(i == 1){
                                sliderItem.setDescription(getString(R.string.dunamis_meaning));
                            }else if(i == 2){
                                sliderItem.setImageUrl(R.drawable.march_worship__word_and_wonders_night);
                            }else if(i == 3){
                                sliderItem.setImageUrl(R.drawable.drone_view);
                            }
                            sliderItemList.add(sliderItem);
                        }
                        adapter = new SliderAdapterExample(ChurchDepartmentDashboardActivity.this,sliderItemList);
                        imageSlider.setSliderAdapter(adapter);

                        imageSlider.setIndicatorAnimation(IndicatorAnimations.THIN_WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
                        imageSlider.setIndicatorSelectedColor(Color.WHITE);
                        imageSlider.setIndicatorUnselectedColor(Color.GRAY);
                        imageSlider.startAutoCycle();
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ChurchDepartmentDashboardActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ChurchDepartmentDashboardActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ChurchDepartmentDashboardActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChurchDepartmentDashboardActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ChurchDepartmentDashboardActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ChurchDepartmentDashboardActivity.this).addToRequestQueue(jsonObjectRequest);
    };

    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this,TestimonyVideo.class);
        startActivity(I);
    }
    public void editDepa(View view){
        Intent I = new Intent(this, DepartmentUpdateProfileActivity.class);
        startActivity(I);
    }
    public void viewMem(View view){
        Intent I = new Intent(this, ManageHomeChurchMembersActivity.class);
        startActivity(I);
    }
    public void AssRoat(View view){
        Intent I = new Intent(this, RotaActivity.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this,SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this,ProfileActivity.class);
        I.putExtra("from","D");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this,DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this,DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this,DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this,NotificationActivity.class);
        FROMNOTI = "D";
        ID = idss;
        BRANCH_ID = branch_id;
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskIntroActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }

    public void  kingsKids(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://ubdavid.org/youth-world/youth.html"));
        startActivity(intent);
    }
    public void  Dashstore(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    } public void  upcoming(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }public void  churchDeps(View view){
        Intent I = new Intent(ChurchDepartmentDashboardActivity.this, DepartmentListingActivity.class);
        startActivity(I);
    }public void  callCen(View view){
        Intent I = new Intent(ChurchDepartmentDashboardActivity.this, CallCenterActivity.class);
        startActivity(I);
    }public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
    public void createMArker(double latitude, double longitude, String title) {

        if(mMap !=null){
            Log.e("Mark", title );
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .anchor(0.5f, 0.5f)
                    .title(title));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude),7));
        }
    }
}
