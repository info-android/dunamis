package com.dunamis1.dunamis1.Activity.ChurchDepartment;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.dunamis1.dunamis1.Fragment.RotaStepOneFragment;
import com.dunamis1.dunamis1.Fragment.RotaStepTwoFragment;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;

public class RotaActivity extends AppCompatActivity {
    private LinearLayout step_one_layout,step_two_layout;
    private TextView step_one_tv,step_two_tv;
    private ImageView step_one_iv,step_two_iv;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rota);
        step_one_layout=(LinearLayout)findViewById(R.id.step_one_layout);
        step_two_layout=(LinearLayout)findViewById(R.id.step_two_layout);
        step_one_tv=(TextView) findViewById(R.id.step_one_tv);
        step_two_tv=(TextView) findViewById(R.id.step_two_tv);
        step_one_iv=(ImageView) findViewById(R.id.step_one_iv);
        step_two_iv=(ImageView) findViewById(R.id.step_two_iv);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        set_fragment(new RotaStepOneFragment(),getResources().getString(R.string.step_one));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        step_one_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_selection(step_one_layout,step_one_tv,step_one_iv);
                set_fragment(new RotaStepOneFragment(),getResources().getString(R.string.step_one));
            }
        });

        step_two_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_selection(step_two_layout,step_two_tv,step_two_iv);
                set_fragment(new RotaStepTwoFragment(),getResources().getString(R.string.step_two));
            }
        });
    }
    public void back(View view) {
        onBackPressed();
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();

        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    private void btn_selection(LinearLayout linearLayout,TextView textView,ImageView imageView) {
        step_one_layout.setBackgroundResource(R.color.white);
        step_two_layout.setBackgroundResource(R.color.white);
        step_one_iv.setColorFilter(ContextCompat.getColor(RotaActivity.this, R.color.black), PorterDuff.Mode.SRC_IN);
        step_two_iv.setColorFilter(ContextCompat.getColor(RotaActivity.this, R.color.black), PorterDuff.Mode.SRC_IN);
        step_one_tv.setTextColor(getResources().getColor(R.color.black));
        step_two_tv.setTextColor(getResources().getColor(R.color.black));

        linearLayout.setBackgroundResource(R.color.transparent);
        textView.setTextColor(getResources().getColor(R.color.white));
        imageView.setColorFilter(ContextCompat.getColor(RotaActivity.this, R.color.white), PorterDuff.Mode.SRC_IN);
    }

    protected void addFragment(int containerViewId,
                               Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, tag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(int containerViewId,
                                   Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, tag)
                .commit();
    }

    private void set_fragment(Fragment fragment,String tag) {
        Fragment step_one_fragment = getSupportFragmentManager().findFragmentByTag(getResources().getString(R.string.step_one));
        Fragment step_two_fragment = getSupportFragmentManager().findFragmentByTag(getResources().getString(R.string.step_two));
        if (step_one_fragment == null && step_two_fragment == null) {
            addFragment(R.id.frame_layout, fragment, tag);
        } else {
            replaceFragment(R.id.frame_layout, fragment, tag);

        }
    }


    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
