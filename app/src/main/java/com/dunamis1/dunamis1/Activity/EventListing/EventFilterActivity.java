package com.dunamis1.dunamis1.Activity.EventListing;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.vanillaplacepicker.presentation.builder.VanillaPlacePicker;
import com.vanillaplacepicker.utils.KeyUtils;
import com.vanillaplacepicker.utils.MapType;
import com.vanillaplacepicker.utils.PickerLanguage;
import com.vanillaplacepicker.utils.PickerType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventFilterActivity extends AppCompatActivity {

    private Spinner country_spinner;
    private Spinner city_spinner;
    private EditText address;
    private CheckBox all_checkbox,today_checkbox,this_week_chbox,this_mounth,nextSixMonth_ChBox;

    private String country_id="",cityId="";
    private double lat,long_;
    private int REQUEST_PLACE_PICKER = 9;
    private Button applyBtn;

    private String dateRange="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_filter);
        city_spinner = findViewById(R.id.city_spinner);
        country_spinner = findViewById(R.id.country_spinner);
        address = findViewById(R.id.address_et);
        all_checkbox = findViewById(R.id.all_checkbox);
        today_checkbox = findViewById(R.id.today_checkbox);
        this_week_chbox = findViewById(R.id.this_week_chbox);
        this_mounth = findViewById(R.id.this_mounth);
        nextSixMonth_ChBox = findViewById(R.id.nextSixMonth_ChBox);
        applyBtn = findViewById(R.id.applyBtn);
        setDateRange();
        getCouontry();


        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent intent = new VanillaPlacePicker.Builder(EventFilterActivity.this)
                            .withLocation(lat, long_)
                            .setPickerLanguage(PickerLanguage.ENGLISH) // Apply language to picker
                            .setTintColor(getResources().getColor(R.color.colorPrimary)) // Apply Tint color to Back, Clear button of AutoComplete UI
                            /*
                             * Configuration for AutoComplete UI
                             */
                            .setLanguage(PreferenceManager.getDefaultSharedPreferences(EventFilterActivity.this).getString("LANG","en"))
                            .isOpenNow(true) // Returns only those places that are open for business at the time the query is sent.

                            /*
                             * Configuration for Map UI
                             */
                            .with(PickerType.AUTO_COMPLETE) // Select Picker type to enable autocompelte, map or both
                            .setMapType(MapType.SATELLITE) // Choose map type (Only applicable for map screen)
                            // containing the JSON style declaration for night-mode styling
                            .setMapPinDrawable(android.R.drawable.ic_menu_mylocation) // To give custom pin image for map marker
                            .build();
                    startActivityForResult(intent, REQUEST_PLACE_PICKER);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });



        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(EventFilterActivity.this,EventListingActivity.class);
                intent.putExtra("country_id",country_id);
                intent.putExtra("city_id",cityId);
                intent.putExtra("latitude",String.valueOf(lat));
                intent.putExtra("longitude",String.valueOf(long_));
                Log.e("latitude", String.valueOf(lat));
                intent.putExtra("address",address.getText().toString().trim());
                intent.putExtra("date_range",dateRange);
                startActivity(intent);
                finish();
            }
        });

    }

    private void setDateRange(){
        all_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dateRange="ALL";
                all_checkbox.setChecked(true);
                today_checkbox.setChecked(false);
                this_week_chbox.setChecked(false);
                this_mounth.setChecked(false);
                nextSixMonth_ChBox.setChecked(false);
            }
        });

        today_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateRange="TODAY";
                all_checkbox.setChecked(false);
                today_checkbox.setChecked(true);
                this_week_chbox.setChecked(false);
                this_mounth.setChecked(false);
                nextSixMonth_ChBox.setChecked(false);
            }
        });

        this_week_chbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateRange="WEEK";
                all_checkbox.setChecked(false);
                today_checkbox.setChecked(false);
                this_week_chbox.setChecked(true);
                this_mounth.setChecked(false);
                nextSixMonth_ChBox.setChecked(false);
            }
        });

        this_mounth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateRange="MONTH";
                all_checkbox.setChecked(false);
                today_checkbox.setChecked(false);
                this_week_chbox.setChecked(false);
                this_mounth.setChecked(true);
                nextSixMonth_ChBox.setChecked(false);
            }
        });

        nextSixMonth_ChBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateRange="SIXMONTH";
                all_checkbox.setChecked(false);
                today_checkbox.setChecked(false);
                this_week_chbox.setChecked(false);
                this_mounth.setChecked(false);
                nextSixMonth_ChBox.setChecked(true);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == REQUEST_PLACE_PICKER && data != null) {
                String vanillaAddress = String.valueOf(data.getSerializableExtra(KeyUtils.SELECTED_PLACE));
                //String vanillaAddress2 = String.valueOf(data.getSerializableExtra(KeyUtils.LOCATION));
                Log.e("vanillaAddress", vanillaAddress);

                String toSplit = "a+b-c*d/e=f";
                String[] splitted = vanillaAddress.split("[,()=]");
                ArrayList<String> address_list = new ArrayList<>();
                for (String split : splitted) {
                    System.out.println(split);
                    Log.e("split", split);
                    address_list.add(split);

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("latitude")){
                        try {
                            lat= Double.parseDouble(address_list.get(i+1));
                            Log.e("lat", String.valueOf(lat));

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("longitude")){
                        try {
                            long_= Double.parseDouble(address_list.get(i+1));
                            Log.e("long_", String.valueOf(long_));


                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }
                }
                String get_add = address_list.get(2) + ", " + address_list.get(3) + ", " + address_list.get(4);
                address.setError(null);
                Log.e("vanillaAddress", vanillaAddress);

                address.setText(get_add);

            }

        }
    }

    public void clear(View view){
        Intent intent=new Intent(EventFilterActivity.this,EventListingActivity.class);
        intent.putExtra("country_id","");
        intent.putExtra("city_id","");
        intent.putExtra("latitude","");
        intent.putExtra("longitude","");
        Log.e("latitude", "");
        intent.putExtra("address","");
        intent.putExtra("date_range","");
        startActivity(intent);
        finish();
    }
    public void getCouontry(){
        if (MethodClass.isNetworkConnected(EventFilterActivity.this)) {
            MethodClass.showProgressDialog(EventFilterActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "country";
            HashMap<String, String> params = new HashMap<String, String>();
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(EventFilterActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(EventFilterActivity.this, response);
                        if (result_Object != null) {

                            JSONArray countryArray = result_Object.getJSONArray("country");

                            List<StringWithTag> spinnerArraysCoun = new ArrayList<StringWithTag>();
                            spinnerArraysCoun.add(new StringWithTag(getString(R.string.selCountry),""));
                            for (int i = 0; i <countryArray.length() ; i++) {

                                String counid = countryArray.getJSONObject(i).getString("id");
                                String countryname = countryArray.getJSONObject(i).getString("countryname");
                                spinnerArraysCoun.add(new StringWithTag(countryname, counid));
                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(EventFilterActivity.this, R.layout.spinner_item, spinnerArraysCoun) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    country_id = String.valueOf(s.tag);
                                    getCity(country_id);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(EventFilterActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(EventFilterActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(EventFilterActivity.this);
                    } else {
                        MethodClass.error_alert(EventFilterActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(EventFilterActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventFilterActivity.this).getString("token", ""));
                    }
                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getCity(String cou_id){
        if (MethodClass.isNetworkConnected(EventFilterActivity.this)) {
            MethodClass.showProgressDialog(EventFilterActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "city";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("country_id", cou_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(EventFilterActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(EventFilterActivity.this, response);
                        if (result_Object != null) {

                            JSONArray cityArray = result_Object.getJSONArray("city");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selCity),""));
                            for (int i = 0; i <cityArray.length() ; i++) {

                                String city_id = cityArray.getJSONObject(i).getString("id");
                                String cityname = cityArray.getJSONObject(i).getString("city");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(EventFilterActivity.this, R.layout.spinner_item, spinnerArrays2) {
                            };
                            city_spinner.setAdapter(adapter4);
                            city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    cityId = String.valueOf(s.tag);
                                   /* if(country_id.equals("161")){
                                        getDistrict(cityId);
                                    }*/

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(EventFilterActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(EventFilterActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(EventFilterActivity.this);
                    } else {
                        MethodClass.error_alert(EventFilterActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(EventFilterActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventFilterActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
/*
    public void getDistrict(String cit_id){
        if (MethodClass.isNetworkConnected(EventFilterActivity.this)) {
            MethodClass.showProgressDialog(EventFilterActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "district";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("city_id", cit_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(EventFilterActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(EventFilterActivity.this, response);
                        if (result_Object != null) {

                            JSONArray districtArray = result_Object.getJSONArray("district");
                            List<StringWithTag> spinnerArrays3 = new ArrayList<StringWithTag>();
                            for (int i = 0; i <districtArray.length() ; i++) {

                                String city_id = districtArray.getJSONObject(i).getString("id");
                                String cityname = districtArray.getJSONObject(i).getString("district");
                                spinnerArrays3.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter5 = new ArrayAdapter(EventFilterActivity.this, R.layout.spinner_item, spinnerArrays3) {
                            };
                            dis_spinner.setAdapter(adapter5);
                            dis_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    distId = String.valueOf(s.tag);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(EventFilterActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(EventFilterActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(EventFilterActivity.this);
                    } else {
                        MethodClass.error_alert(EventFilterActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(EventFilterActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventFilterActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
*/

    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }





}