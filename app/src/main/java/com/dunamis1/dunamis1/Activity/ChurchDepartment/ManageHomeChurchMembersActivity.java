package com.dunamis1.dunamis1.Activity.ChurchDepartment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.ManageHomeChurchMemberAdapter;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.AGE;
import static com.dunamis1.dunamis1.Helper.Constant.APPROVED;
import static com.dunamis1.dunamis1.Helper.Constant.COUNTRY_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.EMAIL;
import static com.dunamis1.dunamis1.Helper.Constant.FNAME;
import static com.dunamis1.dunamis1.Helper.Constant.GENDER;
import static com.dunamis1.dunamis1.Helper.Constant.ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.LNAME;
import static com.dunamis1.dunamis1.Helper.Constant.MARITAL;
import static com.dunamis1.dunamis1.Helper.Constant.PHONE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class ManageHomeChurchMembersActivity extends AppCompatActivity {
    private RecyclerView members_rv;
    private ArrayList<HashMap<String, Object>> map_list;
    private String[] title_array = {"Anthony James", "Obi", "Paul Alfred", "Uju","Lola Babs", "Emeka", "Olivia", "Paul Alfred"};
    private Integer[] pic_array = {R.drawable.man1, R.drawable.man2, R.drawable.man3, R.drawable.man4,
            R.drawable.man5, R.drawable.man6, R.drawable.man7};
    private String[] about_array = {"Male,42 Years, Unmarried", "Female,36 Years, Married", "Male,42 Years, Unmarried", "Female,36 Years, Married",
            "Female,40 Years, Married", "Male,24 Years, Unmarried", "Male,36 Years, Unmarried"};
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private CircleImageView imgPro;
    private TextView nav_name_tv;

    private TextView newReq,member,all;
    private String filter = "A";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_home_church_members);
        members_rv=(RecyclerView)findViewById(R.id.members_rv);
        newReq=findViewById(R.id.newReq);
        member=findViewById(R.id.member);
        all=findViewById(R.id.all);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }

        member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter = "M";
                get_members_list();
            }
        });
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter = "A";
                get_members_list();
            }
        });
        newReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter = "N";
                get_members_list();
            }
        });
        get_members_list();
    }
    @Override
    protected void onResume() {
        super.onResume();

        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void back(View view) {
        onBackPressed();
        finish();
    }
    public void get_members_list() {
        MethodClass.showProgressDialog(ManageHomeChurchMembersActivity.this);
        String server_url = "";
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            server_url = ManageHomeChurchMembersActivity.this.getString(R.string.SERVER_URL) + "church-department/members";
        }else {
            server_url = ManageHomeChurchMembersActivity.this.getString(R.string.SERVER_URL) + "home-church/members";
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(ManageHomeChurchMembersActivity.this).getString("LANG","en"));
        params.put("type",filter);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ManageHomeChurchMembersActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(ManageHomeChurchMembersActivity.this, response);
                    if (result_Object != null) {
                        JSONArray seedArray = result_Object.getJSONArray("members");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            HashMap<String,Object> map = new HashMap<>();

                            if(!seedArray.getJSONObject(i).getString("user").equals("null")){
                                JSONObject SeedObj = seedArray.getJSONObject(i).getJSONObject("user");
                                String title = SeedObj.getString("title");
                                String fname = SeedObj.getString("fname");
                                String lname = SeedObj.getString("lname");
                                String email = SeedObj.getString("email");
                                String usrId = SeedObj.getString("id");
                                String country_code = SeedObj.getString("country_code");
                                String phone_number = SeedObj.getString("phone_number");
                                String profile_pic = SeedObj.getString("profile_pic");
                                String is_approved = SeedObj.getString("is_approved");
                                String age = SeedObj.getString("age");
                                String gender = SeedObj.getString("gender");
                                String marital_status = SeedObj.getString("marital_status");
                                String id = seedArray.getJSONObject(i).getString("id");

                                if(!PreferenceManager.getDefaultSharedPreferences(ManageHomeChurchMembersActivity.this).getString("role","Member").equals("Department")){
                                    String status = seedArray.getJSONObject(i).getString("status");
                                    map.put(APPROVED,status);
                                    map.put(SEED_ID,id);
                                }else{
                                    map.put(APPROVED,is_approved);
                                    map.put(SEED_ID,usrId);
                                }
                                map.put(TITLE,title);
                                map.put(FNAME,fname);
                                map.put(LNAME,lname);
                                map.put(EMAIL,email);
                                map.put(PHONE,phone_number);
                                map.put(AGE,age);
                                map.put(GENDER,gender);
                                map.put(MARITAL,marital_status);
                                map.put(IMGG,profile_pic);
                                map.put(COUNTRY_CODE,country_code);
                                map.put(ID,usrId);

                            }else {
                                String id = seedArray.getJSONObject(i).getString("id");
                                if(!PreferenceManager.getDefaultSharedPreferences(ManageHomeChurchMembersActivity.this).getString("role","Member").equals("Department")){
                                    String status = seedArray.getJSONObject(i).getString("status");
                                    map.put(APPROVED,status);
                                    map.put(SEED_ID,id);
                                }else{
                                    map.put(APPROVED,"N");
                                    map.put(SEED_ID,"");
                                }
                                map.put(TITLE,"");
                                map.put(FNAME,"");
                                map.put(LNAME,"");
                                map.put(EMAIL,"");
                                map.put(PHONE,"");
                                map.put(AGE,"");
                                map.put(GENDER,"");
                                map.put(MARITAL,"");
                                map.put(IMGG,"");
                                map.put(COUNTRY_CODE,"");
                                map.put(ID,"");
                            }
                            map_list.add(map);
                        }
                        ManageHomeChurchMemberAdapter adapter = new ManageHomeChurchMemberAdapter(ManageHomeChurchMembersActivity.this,map_list);
                        members_rv.setAdapter(adapter);
                        members_rv.setFocusable(false);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ManageHomeChurchMembersActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ManageHomeChurchMembersActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ManageHomeChurchMembersActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ManageHomeChurchMembersActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ManageHomeChurchMembersActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ManageHomeChurchMembersActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
