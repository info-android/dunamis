package com.dunamis1.dunamis1.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Fragment.MemberSetp1Fragment;
import com.dunamis1.dunamis1.Fragment.MemberStep2Fragment;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class MemberEditProfileActivity extends AppCompatActivity {
    Button step1,step2,step3;
    private FrameLayout viewpager;
    private TextView header;
    private CircleImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_edit_profile);

        viewpager = (FrameLayout) findViewById(R.id.viewpager);
        step1 = (Button) findViewById(R.id.step1);
        step2 = (Button) findViewById(R.id.step2);

        header = (TextView) findViewById(R.id.header);
        image = (CircleImageView) findViewById(R.id.image);

        step1.setTextColor(getResources().getColor(R.color.white));
        step1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a5, 0);
        step1.setBackground(getDrawable(R.drawable.btm_white));

        step2.setTextColor(getResources().getColor(R.color.black));
        step2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step2.setBackground(getDrawable(R.drawable.rght_blue));

        Fragment FragmentGuestStep1 = getSupportFragmentManager().findFragmentByTag("STEP1");
        Fragment FragmentFragmentGuestStep2 = getSupportFragmentManager().findFragmentByTag("STEP2");

        if (FragmentGuestStep1 == null && FragmentFragmentGuestStep2 == null) {
            addFragment(R.id.viewpager, new MemberSetp1Fragment(), "STEP1");
        }else{
            replaceFragment(R.id.viewpager, new MemberSetp1Fragment(), "STEP2");
        }


        step1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step1part();
            }
        });
        step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step2part();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+ PreferenceManager.getDefaultSharedPreferences(MemberEditProfileActivity.this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(image);
    }
    public void step1part(){
        viewpager.setVisibility(View.VISIBLE);
        header.setText(getString(R.string.edtGuesHead));
        step1.setTextColor(getResources().getColor(R.color.white));
        step1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a5, 0);
        step1.setBackground(getDrawable(R.drawable.btm_white));

        step2.setTextColor(getResources().getColor(R.color.black));
        step2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step2.setBackground(getDrawable(R.drawable.rght_blue));

        Fragment FragmentGuestStep1 = getSupportFragmentManager().findFragmentByTag("STEP1");
        Fragment FragmentFragmentGuestStep2 = getSupportFragmentManager().findFragmentByTag("STEP2");

        if (FragmentGuestStep1 == null && FragmentFragmentGuestStep2 == null) {
            addFragment(R.id.viewpager, new MemberSetp1Fragment(), "STEP1");
        }else{
            replaceFragment(R.id.viewpager, new MemberSetp1Fragment(), "STEP1");
        }

    }

    public void step2part(){
        viewpager.setVisibility(View.VISIBLE);
        header.setText(getString(R.string.edtGuesHead2));
        step1.setTextColor(getResources().getColor(R.color.black));
        step1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step1.setBackground(getDrawable(R.drawable.rght_blue));

        step2.setTextColor(getResources().getColor(R.color.white));
        step2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a5, 0);
        step2.setBackground(getDrawable(R.drawable.btm_white));

        Fragment FragmentGuestStep1 = getSupportFragmentManager().findFragmentByTag("STEP1");
        Fragment FragmentFragmentGuestStep2 = getSupportFragmentManager().findFragmentByTag("STEP2");

        if (FragmentGuestStep1 == null && FragmentFragmentGuestStep2 == null) {
            addFragment(R.id.viewpager, new MemberStep2Fragment(), "STEP2");
        }else{
            replaceFragment(R.id.viewpager, new MemberStep2Fragment(), "STEP2");
        }
    }

    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment, @NonNull String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, tag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment, @NonNull String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, tag)
                .commit();
    }
    public void back(View view){
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("CODE", String.valueOf(resultCode) );
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("STEP2");
        if(fragment !=null){
            fragment.onActivityResult(requestCode, resultCode, data);
        }else{
            Fragment fragment2 = getSupportFragmentManager().findFragmentByTag("STEP1");
            fragment2.onActivityResult(requestCode, resultCode, data);
        }

    }
}
