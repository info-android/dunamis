package com.dunamis1.dunamis1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReApplyMembershipActivity extends AppCompatActivity {
    EditText founda;
    Button cont;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_apply_membership);

        founda = (EditText) findViewById(R.id.founda);
        cont = (Button) findViewById(R.id.cont);
        founda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(ReApplyMembershipActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, (month));
                        calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                        String myFormat = "dd-MM-yyyy";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        founda.setText(sdf.format(calendar.getTime()));
                    }
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
                dialog.show();
            }
        });
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(founda.getText().toString().trim().length()==0){
                    founda.setError(ReApplyMembershipActivity.this.getString(R.string.compleDate));
                    founda.requestFocus();
                    return;
                }
                MethodClass.showProgressDialog(ReApplyMembershipActivity.this);
                String server_url = ReApplyMembershipActivity.this.getString(R.string.SERVER_URL) + "guest/completion-date";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", PreferenceManager.getDefaultSharedPreferences(ReApplyMembershipActivity.this).getString("LANG","en"));
                params.put("completion_date", founda.getText().toString().trim());
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            MethodClass.hideProgressDialog(ReApplyMembershipActivity.this);

                            JSONObject result_Object = MethodClass.get_result_from_webservice(ReApplyMembershipActivity.this, response);
                            if (result_Object != null) {
                                String message = result_Object.getString("message");
                                new SweetAlertDialog(ReApplyMembershipActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Request Sent")
                                        .setContentText("Your new membership request sent to the admin please wait for the approval")
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                finish();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(ReApplyMembershipActivity.this);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(ReApplyMembershipActivity.this);

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(ReApplyMembershipActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ReApplyMembershipActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ReApplyMembershipActivity.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(ReApplyMembershipActivity.this).addToRequestQueue(jsonObjectRequest);
            }
        });
    }
}
