package com.dunamis1.dunamis1.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.DepartmentUpdateProfileActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.HomeChurchDetailsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.RotaActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Adapter.HomeChurchDetailsPhotoAdapter;
import com.dunamis1.dunamis1.Adapter.HomeChurchTImeAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Adapter.ChurchDepOneAdapter;
import com.dunamis1.dunamis1.Adapter.ChurchDepTwoAdapter;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.DAYS;
import static com.dunamis1.dunamis1.Helper.Constant.DEPARTMENT_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.FROM_TIME;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.TO_TIME;

public class ChurchDetailActivity extends AppCompatActivity {
    private String[] title_array = {"Anthony James", "Obi", "Paul Alfred", "Uju","Lola Babs", "Emeka", "Olivia"};
    int[] sampleImagesArr = {R.drawable.user4, R.drawable.user2, R.drawable.user3, R.drawable.image60, R.drawable.untgnamed, R.drawable.user6, R.drawable.user5};
    int[] sampleImagesArr2 = {R.drawable.image60, R.drawable.dashchurchnear3, R.drawable.tfftr};

    private ArrayList<HashMap<Object,Object>> map_list;
    private ArrayList<HashMap<Object,Object>> map_list2;

    private RecyclerView rv_Dash1;
    private RecyclerView rv_Dash2;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private String edit = "F";
    private ImageView editDep;
    private TextView assRO;
    private TextView headTitle;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    private LinearLayout memLayout;
    private LinearLayout photoLayout;

    private NestedScrollView scrollView;
    private TextView depEmail,depPhn,depAssHead,dephead,depAbt,depName;
    private ImageView depBanner;
    private String branch_id="";
    private String user_id="";
    private String department_name_id="";
    private LinearLayout noticeLayout;
    private LinearLayout joinDepart;
    private TextView noticeDesc;
    private  TextView prdct_view,prdct_view1,prdct_view2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_church_detail);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        rv_Dash2 = (RecyclerView) findViewById(R.id.rv_Dash2);
        editDep = (ImageView) findViewById(R.id.editDep);
        assRO = (TextView) findViewById(R.id.assRO);
        noticeDesc = (TextView) findViewById(R.id.noticeDesc);
        memLayout = (LinearLayout) findViewById(R.id.memLayout);
        photoLayout = (LinearLayout) findViewById(R.id.photoLayout);
        noticeLayout = (LinearLayout) findViewById(R.id.noticeLayout);
        joinDepart = (LinearLayout) findViewById(R.id.joinDepart);

        depEmail = (TextView) findViewById(R.id.depEmail);
        depPhn = (TextView) findViewById(R.id.depPhn);
        depAssHead = (TextView) findViewById(R.id.depAssHead);
        dephead = (TextView) findViewById(R.id.dephead);
        depAbt = (TextView) findViewById(R.id.depAbt);
        prdct_view = (TextView) findViewById(R.id.prdct_view);
        prdct_view1 = (TextView) findViewById(R.id.prdct_view1);
        prdct_view2 = (TextView) findViewById(R.id.prdct_view2);
        headTitle = (TextView) findViewById(R.id.headTitle);
        depName = (TextView) findViewById(R.id.depName);
        depBanner = (ImageView) findViewById(R.id.depBanner);
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);

        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        user_id = PreferenceManager.getDefaultSharedPreferences(this).getString("user_id","");
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
            assRO.setVisibility(View.GONE);
            editDep.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
            assRO.setVisibility(View.GONE);
            editDep.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
            assRO.setVisibility(View.GONE);
            editDep.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
            assRO.setVisibility(View.VISIBLE);
            editDep.setVisibility(View.VISIBLE);
            joinDepart.setVisibility(View.GONE);
            editDep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent I = new Intent(ChurchDetailActivity.this, DepartmentUpdateProfileActivity.class);
                    startActivity(I);
                }
            });
        }

        getDep();
    }
    @Override
    protected void onResume() {
        super.onResume();

        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void getDep(){
        MethodClass.showProgressDialog(ChurchDetailActivity.this);
        String server_url = ChurchDetailActivity.this.getString(R.string.SERVER_URL) + "church-department/view/details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(ChurchDetailActivity.this).getString("LANG","en"));
        params.put("id",getIntent().getStringExtra("id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ChurchDetailActivity.this);
                    JSONObject result_Object = MethodClass.get_result_from_webservice(ChurchDetailActivity.this, response);
                    if (result_Object != null) {

                        final String admin_id = result_Object.getString("admin_id");

                        if(PreferenceManager.getDefaultSharedPreferences(ChurchDetailActivity.this).getString("role","Member").equals("Department")){
                            prdct_view.setVisibility(View.GONE);
                            prdct_view1.setVisibility(View.GONE);
                            prdct_view2.setVisibility(View.GONE);
                        }
                        else if(user_id.equals(admin_id)){
                            prdct_view.setVisibility(View.GONE);
                            prdct_view1.setVisibility(View.GONE);
                            prdct_view2.setVisibility(View.GONE);
                        }else{
                            prdct_view.setVisibility(View.VISIBLE);
                            prdct_view1.setVisibility(View.VISIBLE);
                            prdct_view2.setVisibility(View.VISIBLE);
                            prdct_view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(ChurchDetailActivity.this,MessageChatActivity.class);
                                    I.putExtra("rec_id",admin_id);
                                    startActivity(I);
                                }
                            });
                            prdct_view2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(ChurchDetailActivity.this,MessageChatActivity.class);
                                    I.putExtra("rec_id",admin_id);
                                    startActivity(I);
                                }
                            });
                            prdct_view1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(ChurchDetailActivity.this,MessageChatActivity.class);
                                    I.putExtra("rec_id",admin_id);
                                    startActivity(I);
                                }
                            });
                        }
                        JSONObject home_church_details = result_Object.getJSONObject("my_church_department");

                        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+home_church_details.getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(depBanner);
                        depName.setText(home_church_details.getJSONObject("department").getString("department_name"));
                        if(!home_church_details.getJSONObject("departmentdetails").getString("department_head_name").equals("null") && !home_church_details.getJSONObject("departmentdetails").getString("department_head_name").equals(null)){
                            dephead.setText(home_church_details.getJSONObject("departmentdetails").getString("department_head_name"));
                        }else {
                            dephead.setText("N/A");
                        }
                        if(!home_church_details.getJSONObject("departmentdetails").getString("assistance_head_name").equals("null") && !home_church_details.getJSONObject("departmentdetails").getString("assistance_head_name").equals(null)){
                            depAssHead.setText(home_church_details.getJSONObject("departmentdetails").getString("assistance_head_name"));
                        }else {
                            depAssHead.setText("N/A");
                        }
                        if(!home_church_details.getJSONObject("departmentdetails").getString("about_department").equals("null") && !home_church_details.getJSONObject("departmentdetails").getString("about_department").equals(null)){

                            depAbt.setText(home_church_details.getJSONObject("departmentdetails").getString("about_department"));

                        }else {
                            depAbt.setText("N/A");
                        }
                        branch_id = home_church_details.getString("branch_id");
                        department_name_id = home_church_details.getString("department_name_id");
                        final String department_count = result_Object.getString("department_count");
                        if(PreferenceManager.getDefaultSharedPreferences(ChurchDetailActivity.this).getString("role","Member").equals("Department")){
                            joinDepart.setVisibility(View.GONE);
                        }else if(department_count.equals("0") && result_Object.getString("join").equals("N")){
                            joinDepart.setVisibility(View.VISIBLE);
                        }else if(department_count.equals("1") && result_Object.getString("join").equals("N")){
                            joinDepart.setVisibility(View.VISIBLE);
                        }else if(department_count.equals("2")){
                            joinDepart.setVisibility(View.GONE);
                        }else {
                            joinDepart.setVisibility(View.GONE);
                        }
                        headTitle.setText(home_church_details.getJSONObject("branchdetails").getString("name"));

                        if(!home_church_details.getString("department_email").equals("null")){
                            depEmail.setText(home_church_details.getString("department_email"));
                        }else {
                            depEmail.setText("N/A");
                        }
                        if(!home_church_details.getString("department_phone").equals("null")){
                            depPhn.setText(home_church_details.getString("department_phone"));
                        }else {
                            depPhn.setText("N/A");
                        }

                        String notice = result_Object.getString("notice");
                        if(!notice.equals("null") && !notice.equals(null)){
                            noticeLayout.setVisibility(View.VISIBLE);
                            noticeDesc.setText(result_Object.getJSONObject("notice").getJSONObject("notice_details").getString("description"));
                        }else {
                            noticeDesc.setText("N/A");
                        }

                        map_list = new ArrayList<>();
                        map_list2 = new ArrayList<>();

                        JSONArray members  = result_Object.getJSONArray("members");
                        if(members.length()>0){
                            for (int i = 0; i <members.length() ; i++) {

                                if(!members.getJSONObject(i).getString("user").equals("null")){
                                    HashMap<Object,Object> map = new HashMap<>();
                                    map.put(TITLE,members.getJSONObject(i).getJSONObject("user").getString("title")+" "+members.getJSONObject(i).getJSONObject("user").getString("fname")+" "+members.getJSONObject(i).getJSONObject("user").getString("lname"));
                                    map.put(IMAGE,members.getJSONObject(i).getJSONObject("user").getString("profile_pic"));
                                    map.put(ID,members.getJSONObject(i).getJSONObject("user").getString("id"));
                                    map_list.add(map);
                                }

                            }
                            ChurchDepOneAdapter bibleDeuteronomyAdapter= new ChurchDepOneAdapter(ChurchDetailActivity.this,map_list);
                            rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                            rv_Dash1.setFocusable(false);
                            memLayout.setVisibility(View.VISIBLE);
                        }else{
                            memLayout.setVisibility(View.GONE);
                        }

                        JSONArray photos = result_Object.getJSONArray("photos");
                        if(photos.length()>0){
                            for (int l = 0; l <photos.length() ; l++) {
                                HashMap<Object,Object> map4 = new HashMap<>();
                                map4.put(IMAGE,photos.getJSONObject(l).getString("image"));
                                map_list2.add(map4);
                            }
                            ChurchDepTwoAdapter adapter4= new ChurchDepTwoAdapter(ChurchDetailActivity.this,map_list2);
                            rv_Dash2.setAdapter(adapter4);
                            rv_Dash2.setFocusable(false);
                            photoLayout.setVisibility(View.VISIBLE);
                        }else {
                            photoLayout.setVisibility(View.GONE);
                        }

                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ChurchDetailActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ChurchDetailActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ChurchDetailActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChurchDetailActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ChurchDetailActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ChurchDetailActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void back(View view){
        super.onBackPressed();
    }
    public void seeAll(View view){
        Intent I = new Intent(this, RotaActivity.class);
        startActivity(I);
    }
    public void join(View view){
        MethodClass.showProgressDialog(ChurchDetailActivity.this);
        String server_url = ChurchDetailActivity.this.getString(R.string.SERVER_URL) + "church-department/join";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(ChurchDetailActivity.this).getString("LANG","en"));
        params.put("id",department_name_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ChurchDetailActivity.this);
                    JSONObject result_Object = MethodClass.get_result_from_webservice(ChurchDetailActivity.this, response);
                    if (result_Object != null) {
                        String message = result_Object.getString("message");
                        String meaning = result_Object.getString("meaning");
                        getDep();
                        new SweetAlertDialog(ChurchDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message)
                                .setContentText(meaning)
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ChurchDetailActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ChurchDetailActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ChurchDetailActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChurchDetailActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ChurchDetailActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ChurchDetailActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void mesgs(View view){
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.msg_popup);
        dialog.setCancelable(false);
        Button yes_btn = (Button) dialog.findViewById(R.id.yes_btn);
        Button no_btn = (Button) dialog.findViewById(R.id.no_btn);
        yes_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(ChurchDetailActivity.this, MessageChatActivity.class));
            }
        });
        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void notice(View view){
        Intent I = new Intent(this, NoticeActivity.class);
        I.putExtra("from","D");
        I.putExtra("id",department_name_id);
        I.putExtra("branch_id",branch_id);
        startActivity(I);
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
