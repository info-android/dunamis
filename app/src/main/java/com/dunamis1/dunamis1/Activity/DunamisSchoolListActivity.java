package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Adapter.SeedsAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Adapter.DunamisSchoolAdapter;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.DATE;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_DESC;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_ID;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.SCH_TYPE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class DunamisSchoolListActivity extends AppCompatActivity {
    int[] sampleImagesArr = {R.drawable.image91,R.drawable.image110,R.drawable.image109,R.drawable.image108};
    private String[] title_array = {"Foundation Class", "Maturity Class", "Destiny Academy","Dunamis School of Ministry"};
    private String[] Date_array = {"10th Jan,2020", "25th Mar,2020", "10th Oct,2020", "10th Aug,2020"};
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private ArrayList<HashMap<Object,Object>> map_list;
    private RecyclerView rv_Dash2;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dunamis_school_list);
        rv_Dash2 = (RecyclerView) findViewById(R.id.rv_Dash2);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        getDep();
    }
    @Override
    protected void onResume() {
        super.onResume();

        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void getDep(){

        MethodClass.showProgressDialog(DunamisSchoolListActivity.this);
        String server_url = DunamisSchoolListActivity.this.getString(R.string.SERVER_URL) + "school/list";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(DunamisSchoolListActivity.this).getString("LANG","en"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(DunamisSchoolListActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(DunamisSchoolListActivity.this, response);
                    if (result_Object != null) {
                        JSONArray seedArray = result_Object.getJSONArray("list");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            JSONObject SeedObj = seedArray.getJSONObject(i);
                            String title = SeedObj.getJSONObject("school_details").getString("school_name");
                            String about_school = SeedObj.getJSONObject("school_details").getString("about_school");
                            String thubmnail = SeedObj.getString("banner_image");
                            String code = SeedObj.getString("web_site");
                            String date = SeedObj.getString("session_date");
                            String google_address = SeedObj.getString("google_address");
                            String id = SeedObj.getString("id");


                            HashMap<Object,Object> map = new HashMap<>();
                            map.put(SCH_TITLE,title);
                            map.put(DATE,date);
                            map.put(SCH_THUMB,thubmnail);
                            map.put(SCH_DESC,about_school);
                            map.put(SCH_CODE,code);
                            map.put(SCH_TYPE,google_address);
                            map.put(SCH_ID,id);
                            map_list.add(map);
                        }
                        DunamisSchoolAdapter bibleDeuteronomyAdapter= new DunamisSchoolAdapter(DunamisSchoolListActivity.this,map_list);
                        rv_Dash2.setAdapter(bibleDeuteronomyAdapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(DunamisSchoolListActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(DunamisSchoolListActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(DunamisSchoolListActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DunamisSchoolListActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(DunamisSchoolListActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(DunamisSchoolListActivity.this).addToRequestQueue(jsonObjectRequest);


    }

    public void back(View view){
        super.onBackPressed();
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
