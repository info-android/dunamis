package com.dunamis1.dunamis1.Activity.EventListing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.CommentsAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_USER;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.THUMB_IMG_URL;

public class CommentsActivity extends AppCompatActivity {
    private RecyclerView cmmnts_rv;
    private TextView cmmnts_count_tv;
    private ArrayList<HashMap<String, Object>> map_list;
    private String[] title_array = {"Anthony James", "Obi", "Paul Alfred", "Uju","Lola Babs"};
    private Integer[] pic_array = {R.drawable.man1, R.drawable.man3, R.drawable.man4, R.drawable.man5,
            R.drawable.man6};
    private String[] Date_array = {"Mar 12,2020", "Mar 21,2020", "Mar 10,2020", "Mar 09,2020", "Mar 07,2020"};

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    private String from = "";
    private String id = "";
    ImageView prdct_iv;
    TextView prdct_mem_tv3;
    TextView prdct_title_tv;
    TextView totLike;
    TextView totComment;
    TextView totShare;
    ImageView musicprdct_iv;
    TextView musicprdct_mem_tv3;
    TextView musicprdct_title_tv;
    TextView musictotLike;
    TextView musictotComment;
    TextView musictotShare;
    CircleImageView usrImg;
    CircleImageView eventImg;
    private TextView eventTitle,eventLoc,date_tv,like_tv,msg_tv,share_tv;
    private EditText comm_EDT;
    private ImageView send;
    private LinearLayout eventLayout,seedLayout,musicLayout,saveLayout;
    private NestedScrollView scrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        MethodClass.hide_keyboard(CommentsActivity.this);
        cmmnts_count_tv=(TextView)findViewById(R.id.cmmnts_count_tv);
        cmmnts_rv=(RecyclerView)findViewById(R.id.cmmnts_rv);
        eventLayout=(LinearLayout)findViewById(R.id.eventLayout);
        seedLayout=(LinearLayout)findViewById(R.id.seedLayout);
        musicLayout=(LinearLayout)findViewById(R.id.musicLayout);
        saveLayout=(LinearLayout)findViewById(R.id.saveLayout);
        usrImg=(CircleImageView)findViewById(R.id.usrImg);
        send=(ImageView) findViewById(R.id.send);
        comm_EDT=(EditText)findViewById(R.id.comm_EDT);

        eventImg = findViewById(R.id.eventImg);
        eventTitle = findViewById(R.id.eventTitle);
        eventLoc = findViewById(R.id.eventLoc);
        date_tv = findViewById(R.id.date_tv);
        like_tv = findViewById(R.id.like_tv);
        share_tv = findViewById(R.id.share_tv);
        msg_tv = findViewById(R.id.msg_tv);
        scrollView = findViewById(R.id.scrollView);

        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }

        if(getIntent().getExtras() != null){
            from = getIntent().getStringExtra("from");
            id = getIntent().getStringExtra("id");

        }

        if(from.equals("seed")){
            seedLayout.setVisibility(View.VISIBLE);
            eventLayout.setVisibility(View.GONE);
            musicLayout.setVisibility(View.GONE);
        }else if(from.equals("music")){
            seedLayout.setVisibility(View.GONE);
            eventLayout.setVisibility(View.GONE);
            musicLayout.setVisibility(View.VISIBLE);
        }else {
            seedLayout.setVisibility(View.GONE);
            eventLayout.setVisibility(View.VISIBLE);
            musicLayout.setVisibility(View.GONE);
        }
        prdct_iv =  findViewById(R.id.prdct_iv);
        prdct_title_tv =  findViewById(R.id.prdct_title_tv);
        prdct_mem_tv3 =  findViewById(R.id.prdct_mem_tv3);
        totLike =  findViewById(R.id.totLike);
        totComment =  findViewById(R.id.totCom);
        totShare =  findViewById(R.id.totShare);

        musicprdct_iv =  findViewById(R.id.musicprdct_iv);
        musicprdct_title_tv =  findViewById(R.id.musicprdct_title_tv);
        musicprdct_mem_tv3 =  findViewById(R.id.musicprdct_mem_tv3);
        musictotLike =  findViewById(R.id.musictotLike);
        musictotComment =  findViewById(R.id.musictotCom);
        musictotShare =  findViewById(R.id.musictotShare);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });

    }
    public void sendComment(){
        if(comm_EDT.getText().toString().trim().length() == 0){
            comm_EDT.setError(getString(R.string.commReq));
            comm_EDT.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(CommentsActivity.this);
        String server_url = "";
        HashMap<String, String> params;
        if(from.equals("music")){
            server_url = CommentsActivity.this.getString(R.string.SERVER_URL) + "music/add-comment";
            params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("LANG","en"));
            params.put("music_id", id);
            params.put("comment", comm_EDT.getText().toString().trim());
        }else if(from.equals("seed")){
            server_url = CommentsActivity.this.getString(R.string.SERVER_URL) + "seedof-destiny/add-comment";
            params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("LANG","en"));
            params.put("seedof_destiny_id", id);
            params.put("comment", comm_EDT.getText().toString().trim());
        }else {
            server_url = CommentsActivity.this.getString(R.string.SERVER_URL) + "event/add-comment";
            params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("LANG","en"));
            params.put("event_id", id);
            params.put("comment", comm_EDT.getText().toString().trim());
        }

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(CommentsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(CommentsActivity.this, response);
                    if (result_Object != null) {
                        JSONArray SeedObj = result_Object.getJSONArray("comments");
                        comm_EDT.setText("");
                        get_comments_list();
                    }
                } catch (JSONException e) {
                    MethodClass.hideProgressDialog(CommentsActivity.this);
                    e.printStackTrace();
                    Log.e("error", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(CommentsActivity.this);
                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(CommentsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CommentsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(CommentsActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    public void back(View view) {
        onBackPressed();
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();

        get_comments_list();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(imgPro);
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(usrImg);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));

    }
    private void get_comments_list() {
        MethodClass.showProgressDialog(CommentsActivity.this);
        String server_url = "";
        HashMap<String, String> params;
        if(from.equals("music")){
            server_url = CommentsActivity.this.getString(R.string.SERVER_URL) + "music/comment";;
            params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("LANG","en"));
            params.put("music_id", id);
            params.put("comment", comm_EDT.getText().toString().trim());
        }else if(from.equals("seed")){
            server_url = CommentsActivity.this.getString(R.string.SERVER_URL) + "seedof-destiny/comment";;
            params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("LANG","en"));
            params.put("seedof_destiny_id", id);
            params.put("comment", comm_EDT.getText().toString().trim());
        }else {
            server_url = CommentsActivity.this.getString(R.string.SERVER_URL) + "event/comment";;
            params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("LANG","en"));
            params.put("event_id", id);
            params.put("comment", comm_EDT.getText().toString().trim());
        }
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(CommentsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(CommentsActivity.this, response);
                    if (result_Object != null) {
                        if(from.equals("seed")){
                            JSONObject SeedObj = result_Object.getJSONObject("details");
                            String thubmnail = SeedObj.getString("thumbnail");
                            String code = SeedObj.getString("code");
                            String date = SeedObj.getString("date");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String title = SeedObj.getJSONObject("destinydetails").getString("title");

                            String url = "https://img.youtube.com/vi/"+code+"/mqdefault.jpg";
                            Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).into(prdct_iv);
                            prdct_title_tv.setText(title);
                            prdct_mem_tv3.setText(date);
                            totLike.setText(total_likes+" "+getResources().getString(R.string.likes));
                            totShare.setText(total_share+" "+getResources().getString(R.string.shar));
                            cmmnts_count_tv.setText(total_comments+" "+getResources().getString(R.string.comm));
                            totComment.setText(total_comments+" "+getResources().getString(R.string.comm));
                        }else  if(from.equals("music")){
                            JSONObject SeedObj = result_Object.getJSONObject("details");
                            String thubmnail = SeedObj.getString("thumbnail");
                            String code = SeedObj.getString("code");
                            String type = SeedObj.getString("type");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String title = SeedObj.getJSONObject("musicdetails").getString("title");
                            String description = SeedObj.getJSONObject("musicdetails").getString("description");

                            if(type.equals("F")){
                                Picasso.get().load(THUMB_IMG_URL+thubmnail).placeholder(R.drawable.image60).error(R.drawable.image60).into(musicprdct_iv);
                            }else{
                                String url = "https://img.youtube.com/vi/"+code+"/mqdefault.jpg";
                                Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).into(musicprdct_iv);
                            }

                            musicprdct_title_tv.setText(title);
                            musicprdct_mem_tv3.setText(description);
                            musictotLike.setText(total_likes+" "+getResources().getString(R.string.likes));
                            musictotShare.setText(total_share+" "+getResources().getString(R.string.shar));
                            cmmnts_count_tv.setText(total_comments+" "+getResources().getString(R.string.comm));
                            musictotComment.setText(total_comments+" "+getResources().getString(R.string.comm));
                        }else  if(from.equals("event")){
                            if(getIntent().getStringExtra("save").equals("YES")){
                                saveLayout.setVisibility(View.VISIBLE);
                            }else {
                                saveLayout.setVisibility(View.GONE);
                            }
                            JSONObject SeedObj = result_Object.getJSONObject("details");
                            String thumbnail = SeedObj.getString("thumbnail");
                            String from_date = SeedObj.getString("from_date");
                            String google_address = SeedObj.getString("google_address");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String title = SeedObj.getJSONObject("eventdetails").getString("title");
                            String description = SeedObj.getJSONObject("eventdetails").getString("description");

                            Picasso.get().load(EVENT_IMG_URL+thumbnail).placeholder(R.drawable.image60).error(R.drawable.image60).into(eventImg);

                            eventTitle.setText(title);
                            eventLoc.setText(google_address);
                            like_tv.setText(total_likes);
                            share_tv.setText(total_share);
                            cmmnts_count_tv.setText(total_comments+" "+getResources().getString(R.string.comm));
                            msg_tv.setText(total_comments);
                        }


                        JSONArray comments = result_Object.getJSONArray("comments");
                        if(comments.length()>0){
                            map_list = new ArrayList<HashMap<String, Object>>();
                            for (int i = 0; i < comments.length(); i++) {

                                JSONObject commObj = comments.getJSONObject(i);

                                String created_at = commObj.getString("created_at");
                                String comment = commObj.getString("comment");

                                HashMap<String, Object> map = new HashMap<String, Object>();
                                map.put(COMMENT, comment);
                                map.put(COMM_DATE,converDateFormate("yyyy-MM-dd HH:mm:ss" ,"MMM dd,yyyy",created_at));
                                if(!commObj.getString("user").equals("null")){
                                    String titles = commObj.getJSONObject("user").getString("title");
                                    String fnames = commObj.getJSONObject("user").getString("fname");
                                    String lname = commObj.getJSONObject("user").getString("lname");
                                    String profile_pic = commObj.getJSONObject("user").getString("profile_pic");
                                    map.put(COMM_PIC, profile_pic);
                                    map.put(COMM_USER,titles+" "+fnames+" "+lname);
                                }else{
                                    String titles = "";
                                    String fnames = "";
                                    String lname = "";
                                    String profile_pic = "";
                                    map.put(COMM_PIC, profile_pic);
                                    map.put(COMM_USER,titles+" "+fnames+" "+lname);
                                }
                                map_list.add(map);
                            }
                            CommentsAdapter adapter = new CommentsAdapter(CommentsActivity.this,map_list);
                            cmmnts_rv.setAdapter(adapter);
                            cmmnts_rv.setFocusable(false);
                        }
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(CommentsActivity.this);
                    Log.e("error", e.getMessage());
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.e("DATE", e.toString() );
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(CommentsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(CommentsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CommentsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CommentsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(CommentsActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public static String converDateFormate(String oldpattern,
                                           String newPattern, String dateString) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(oldpattern);
        Date testDate = null;
        testDate = sdf.parse(dateString);
        SimpleDateFormat formatter = new SimpleDateFormat(newPattern);
        String newFormat = formatter.format(testDate);
        System.out.println("" + newFormat);
        return newFormat;
    }


    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }

    public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
