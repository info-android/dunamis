package com.dunamis1.dunamis1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.vanillaplacepicker.presentation.builder.VanillaPlacePicker;
import com.vanillaplacepicker.utils.KeyUtils;
import com.vanillaplacepicker.utils.MapType;
import com.vanillaplacepicker.utils.PickerLanguage;
import com.vanillaplacepicker.utils.PickerType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RequestMemberActivity extends AppCompatActivity {
    private Spinner country_spinner;
    private EditText churchDep;
    private EditText address;
    private EditText founda;
    private Button continues;
    private String branch_id="";
    private String dept_id="";
    private double lat,long_;
    private int REQUEST_PLACE_PICKER = 9;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_member);

        country_spinner  = findViewById(R.id.country_spinner);
        churchDep  = findViewById(R.id.churchDep);
        continues  = findViewById(R.id.continues);
        address  = findViewById(R.id.address);
        founda  = findViewById(R.id.founda);

        getChurchBranch();
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent intent = new VanillaPlacePicker.Builder(RequestMemberActivity.this)
                            .withLocation(lat, long_)
                            .setPickerLanguage(PickerLanguage.ENGLISH) // Apply language to picker
                            .setTintColor(getResources().getColor(R.color.colorPrimary)) // Apply Tint color to Back, Clear button of AutoComplete UI
                            /*
                             * Configuration for AutoComplete UI
                             */
                            .setLanguage(PreferenceManager.getDefaultSharedPreferences(RequestMemberActivity.this).getString("LANG","en"))
                            .isOpenNow(true) // Returns only those places that are open for business at the time the query is sent.

                            /*
                             * Configuration for Map UI
                             */
                            .with(PickerType.AUTO_COMPLETE) // Select Picker type to enable autocompelte, map or both
                            .setMapType(MapType.SATELLITE) // Choose map type (Only applicable for map screen)
                            // containing the JSON style declaration for night-mode styling
                            .setMapPinDrawable(android.R.drawable.ic_menu_mylocation) // To give custom pin image for map marker
                            .build();
                    startActivityForResult(intent, REQUEST_PLACE_PICKER);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == REQUEST_PLACE_PICKER && data != null) {
                String vanillaAddress = String.valueOf(data.getSerializableExtra(KeyUtils.SELECTED_PLACE));
                //String vanillaAddress2 = String.valueOf(data.getSerializableExtra(KeyUtils.LOCATION));
                Log.e("vanillaAddress", vanillaAddress);

                String toSplit = "a+b-c*d/e=f";
                String[] splitted = vanillaAddress.split("[,()=]");
                ArrayList<String> address_list = new ArrayList<>();
                for (String split : splitted) {
                    System.out.println(split);
                    Log.e("split", split);
                    address_list.add(split);

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("latitude")){
                        try {
                            lat= Double.parseDouble(address_list.get(i+1));
                            Log.e("lat", String.valueOf(lat));

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("longitude")){
                        try {
                            long_= Double.parseDouble(address_list.get(i+1));
                            Log.e("long_", String.valueOf(long_));


                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }
                }
                String get_add = address_list.get(2) + ", " + address_list.get(3) + ", " + address_list.get(4);



                address.setError(null);
                Log.e("vanillaAddress", vanillaAddress);

                address.setText(get_add);
            }

        }
    }
    public void getChurchBranch(){
        if (MethodClass.isNetworkConnected(RequestMemberActivity.this)) {
            MethodClass.showProgressDialog(RequestMemberActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "church-location";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(RequestMemberActivity.this).getString("LANG","en"));
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(RequestMemberActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(RequestMemberActivity.this, response);
                        if (result_Object != null) {

                            JSONArray locationArray = result_Object.getJSONArray("location");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selctChurchLoc),""));
                            for (int i = 0; i <locationArray.length() ; i++) {

                                String city_id = locationArray.getJSONObject(i).getString("id");
                                JSONObject churchdetails = locationArray.getJSONObject(i).getJSONObject("churchdetails");
                                String cityname = churchdetails.getString("name");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(RequestMemberActivity.this, R.layout.spinner_item, spinnerArrays2) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    branch_id = String.valueOf(s.tag);

                                    churchDep.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getDepartment(branch_id);
                                        }
                                    });

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(RequestMemberActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(RequestMemberActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(RequestMemberActivity.this);
                    } else {
                        MethodClass.error_alert(RequestMemberActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(RequestMemberActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(RequestMemberActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getDepartment(String branc_id){
        if (MethodClass.isNetworkConnected(RequestMemberActivity.this)) {
            MethodClass.showProgressDialog(RequestMemberActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "church-department";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("location_id",branc_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(RequestMemberActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(RequestMemberActivity.this, response);
                        if (result_Object != null) {

                            JSONArray districtArray = result_Object.getJSONArray("department");
                            ArrayList<MultiSelectModel> listOfCountries= new ArrayList<>();
                            for (int i = 0; i <districtArray.length() ; i++) {
                                JSONObject department = districtArray.getJSONObject(i).getJSONObject("department");
                                listOfCountries.add(new MultiSelectModel(Integer.parseInt(districtArray.getJSONObject(i).getString("department_name_id")),department.getString("department_name")));
                            }
                            MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                                    .title(getResources().getString(R.string.chrchDep)) //setting title for dialog
                                    .titleSize(25)
                                    .positiveText("Done")
                                    .negativeText(getResources().getString(R.string.dialog_cancel))
                                    .setMinSelectionLimit(0)
                                    .setMaxSelectionLimit(2)
                                    .multiSelectList(listOfCountries) // the multi select model list with ids and name
                                    .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                        @Override
                                        public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                            //will return list of selected IDS
                                            for (int s = 0; s < selectedIds.size(); s++) {
                                                churchDep.setText(dataString);
                                                dept_id = TextUtils.join(",",selectedIds);
                                            }
                                        }
                                        @Override
                                        public void onCancel() {

                                        }
                                    });
                            multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(RequestMemberActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(RequestMemberActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(RequestMemberActivity.this);
                    } else {
                        MethodClass.error_alert(RequestMemberActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(RequestMemberActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(RequestMemberActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }
    public void founda(View view){
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(RequestMemberActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, (month));
                calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                String myFormat = "dd-MM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                founda.setText(sdf.format(calendar.getTime()));
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
        dialog.show();
    }

    public void back(View view){
        super.onBackPressed();
    }

    public void nextPage(View view){
        if(branch_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseChurchBanch), Snackbar.LENGTH_LONG);
            snackbar.show();
            country_spinner.requestFocus();
            return;
        }
        if(dept_id.equals("")){
            churchDep.setError(getString(R.string.chooseDept));
            churchDep.requestFocus();
            return;
        }
        if(founda.getText().toString().length() == 0){
            founda.setError(getString(R.string.compleDate));
            founda.requestFocus();
            return;
        }if(address.getText().toString().length() == 0){
            address.setError(getString(R.string.addressReq));
            address.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(RequestMemberActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "guest-to-member";


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("church_branch_id", branch_id);
        params.put("church_department_id", dept_id);
        params.put("completion_date", founda.getText().toString().trim());
        params.put("address", address.getText().toString().trim());
        params.put("latitude", String.valueOf(lat));
        params.put("longitude", String.valueOf(long_));


        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(RequestMemberActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(RequestMemberActivity.this, response);
                    if (jsonObject != null) {
                        final String meaning = jsonObject.getString("meaning");
                        new SweetAlertDialog(RequestMemberActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Request Sent")
                                .setContentText(meaning)
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        finish();
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(RequestMemberActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(RequestMemberActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(RequestMemberActivity.this);
                } else {
                    MethodClass.error_alert(RequestMemberActivity.this);
                }
            }
        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(RequestMemberActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(RequestMemberActivity.this).addToRequestQueue(jsonObjectRequest);
    }
}
