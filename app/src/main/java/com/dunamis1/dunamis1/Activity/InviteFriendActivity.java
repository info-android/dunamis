package com.dunamis1.dunamis1.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Adapter.AboutPhotoAdapter;
import com.dunamis1.dunamis1.Helper.ContactObject;
import com.dunamis1.dunamis1.Helper.ContactsListClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.ContactsContract;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Adapter.ContactsAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.ABT_BANNER_IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class InviteFriendActivity extends AppCompatActivity {
    private String[] title_array = {"Anthony James", "Obi", "Paul Alfred", "Uju","Lola Babs", "Emeka", "Olivia"};
    ArrayList<HashMap<String,String>> map_list;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView nav_name_tv;
    private CircleImageView imgPro;

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 2;
    private static final int MY_PERMISSIONS_READ_PHONE_STATE = 3;

    Cursor cursor;
    int counter;
    private ContactsAdapter cAdapter;
    Handler updateBarHandler;
    private String phonelist;
    private String emaillist;
    private List<String> numberList;
    private String message = "";
    private int mMessageSentParts;
    private int mMessageSentTotalParts;
    private int mMessageSentCount;
    private ListView listView;
    private AlertDialog ConfirmDialog;

    private EditText email_tv;
    private Button sendEmail;
    private String smsCOntent = "";

    private Button send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        listView = (ListView) findViewById(R.id.listContact);
        send = (Button) findViewById(R.id.send);
        sendEmail = (Button) findViewById(R.id.sendEmail);
        email_tv = (EditText) findViewById(R.id.email_tv);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        MethodClass.hide_keyboard(InviteFriendActivity.this);


        getCOntent();

        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmailToInvite();
            }
        });
    }
    public void getCOntent(){
        MethodClass.showProgressDialog(InviteFriendActivity.this);
        String server_url = InviteFriendActivity.this.getString(R.string.SERVER_URL) + "sms/content";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(InviteFriendActivity.this).getString("LANG","en"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(InviteFriendActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(InviteFriendActivity.this, response);
                    if (result_Object != null) {
                        smsCOntent = result_Object.getString("content");

                        if (ContextCompat.checkSelfPermission(InviteFriendActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(InviteFriendActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                        } else {
                            // Since reading contacts takes more time, let's run it on a separate thread.
                            shareText();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(InviteFriendActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(InviteFriendActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(InviteFriendActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(InviteFriendActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(InviteFriendActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(InviteFriendActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void sendEmailToInvite(){
        if(!MethodClass.emailValidator(email_tv.getText().toString().trim())){
            email_tv.setError(getString(R.string.emailReq));
            email_tv.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(InviteFriendActivity.this);
        String server_url = InviteFriendActivity.this.getString(R.string.SERVER_URL) + "invite-friend";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(InviteFriendActivity.this).getString("LANG","en"));
        params.put("friend_email", email_tv.getText().toString().trim());
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(InviteFriendActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(InviteFriendActivity.this, response);
                    if (result_Object != null) {
                        String message = result_Object.getString("message");

                        new SweetAlertDialog(InviteFriendActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.emailSents))
                                .setContentText(getString(R.string.emailSentFriend)+email_tv.getText().toString().toLowerCase())
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        email_tv.setText("");
                                    }
                                })
                                .show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(InviteFriendActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(InviteFriendActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(InviteFriendActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(InviteFriendActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(InviteFriendActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(InviteFriendActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    //share in text
    public void shareText() {
        updateBarHandler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                setSimpleList(listView, "TXT");
            }
        }).start();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void back(View view){
        super.onBackPressed();
        finish();
    }
    //get contact list
    public void setSimpleList(final ListView listview, String type) {

        String phoneNumber = null;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String SORT_ORDER = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC";
        ContentResolver contentResolver = getContentResolver();
        cursor = contentResolver.query(CONTENT_URI, null, null, null, SORT_ORDER);
        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {
            counter = 0;
            ContactsListClass.phoneList = new ArrayList<ContactObject>();
            while (cursor.moveToNext()) {

                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    counter++;
                    ContactObject cp = new ContactObject();
                    cp.setName(name);
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                    int i = 0;
                    while (phoneCursor.moveToNext()) {
                        if (i == 0) {
                            phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                            cp.setNumber(phoneNumber);
                        }
                        i++;

                    }
                    ContactsListClass.phoneList.add(cp);
                    phoneCursor.close();

                }
            }
            // ListView has to be updated using a ui thread
            InviteFriendActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cAdapter = new ContactsAdapter(InviteFriendActivity.this, ContactsListClass.phoneList, false);
                    listview.setAdapter(cAdapter);
                    cAdapter.notifyDataSetChanged();
                }
            });

        }
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer sb = new StringBuffer();
                for (ContactObject bean : ContactsListClass.phoneList) {
                    if (bean.isSelected()) {
                        sb.append(bean.getNumber());
                        sb.append(",");
                    }
                }
                String s = sb.toString().trim();
                if (TextUtils.isEmpty(s)) {
                    Toast.makeText(InviteFriendActivity.this, "Select atleast one contact", Toast.LENGTH_SHORT).show();
                } else {
                    phonelist = s.substring(0, s.length() - 1);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(InviteFriendActivity.this);
                    builder.setMessage("Are you sure to share Dunamis to the all selected contacts? \n\nThis will charge your account as per service provider sms rate.").setTitle("Share");
                    builder.setCancelable(false);

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            sendtext();
                        }
                    });
                    builder.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked cancel button
                            ConfirmDialog.hide();
                        }
                    });
                    ConfirmDialog = builder.create();

                    ConfirmDialog.show();

                }
            }
        });


    }

    //function to send message
    public void sendtext() {
        //check for send sms permission
        // User clicked OK button
        if (ActivityCompat.checkSelfPermission(InviteFriendActivity.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(AlertContactActivity.this,"has per",Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(InviteFriendActivity.this, new String[]{Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);

        } else {
            if (ActivityCompat.checkSelfPermission(InviteFriendActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(AlertContactActivity.this,"has per",Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(InviteFriendActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_READ_PHONE_STATE);

            } else {
                //snding text message
                try{
                    if(phonelist!=null){
                        numberList = Arrays.asList(phonelist.split(","));
                        startSendMessages();
                    }
                }catch (Exception e){
                    Log.e("Phone", phonelist.toString());
                    e.printStackTrace();
                }


            }

        }

    }

    //start message send
    private void startSendMessages() {
        mMessageSentCount = 0;
        sendSMS(TextUtils.join(",",numberList), message);
    }

    //sendSms
    private void sendSMS(final String phoneNumber, String message) {
        Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("smsto", phoneNumber, null));
        sendIntent.putExtra("sms_body", smsCOntent);
        startActivity(sendIntent);
    }

    //send next sms
    private void sendNextMessage() {
        if (thereAreSmsToSend()) {
            sendSMS(numberList.get(mMessageSentCount).toString(), message);
        } else {
           MethodClass.hideProgressDialog(this);
            Toast.makeText(getBaseContext(), "All Text Messages have been sent", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private boolean thereAreSmsToSend() {
        return mMessageSentCount < numberList.size();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Since reading contacts takes more time, let's run it on a separate thread.
                    shareText();


                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Since reading contacts takes more time, let's run it on a separate thread.
                    if (ActivityCompat.checkSelfPermission(InviteFriendActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        //Toast.makeText(AlertContactActivity.this,"has per",Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(InviteFriendActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_READ_PHONE_STATE);

                    } else {
                        //snding text message
                        sendtext();
                    }


                }
                return;
            }
            case MY_PERMISSIONS_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Since reading contacts takes more time, let's run it on a separate thread.
                    sendtext();


                }
                return;
            }

        }
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
