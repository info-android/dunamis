package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Adapter.AboutPhotoAdapter;
import com.dunamis1.dunamis1.Adapter.TestimonyAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.ABT_BANNER_IMG;
import static com.dunamis1.dunamis1.Helper.Constant.DATE;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TYPE;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.VID_IMG;

public class AboutUsActivity extends AppCompatActivity {
    int[] pic_array = {R.drawable.gallery1, R.drawable.gallery2, R.drawable.gallery4};
    private ArrayList<HashMap<String, Object>> photo_map_list;
    private RecyclerView photo_rv;

    private ImageView abtBnr;
    private TextView abtHead;
    private TextView abtDesc;
    private TextView abtHim;
    private TextView abtHimVisit;
    private TextView abtHer;
    private TextView abtHerVisit;
    private NestedScrollView scrollView;
    private LinearLayout fbLayout,twitterLayout,instaLayout,youtubelayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        photo_rv = (RecyclerView) findViewById(R.id.photo_rv);
        abtBnr = findViewById(R.id.abtBnr);
        abtHead = findViewById(R.id.abtHead);
        abtDesc = findViewById(R.id.abtDesc);
        abtHim = findViewById(R.id.abtHim);
        abtHimVisit = findViewById(R.id.abtHimVisit);
        abtHer = findViewById(R.id.abtHer);
        abtHerVisit = findViewById(R.id.abtHerVisit);
        fbLayout = findViewById(R.id.fbLayout);
        twitterLayout = findViewById(R.id.twitterLayout);
        instaLayout = findViewById(R.id.instaLayout);
        youtubelayout = findViewById(R.id.youtubelayout);
        scrollView = findViewById(R.id.scrollView);


        get_photo_list();
    }
    public void back(View view){
        super.onBackPressed();
        finish();
    }
    private void get_photo_list() {
        MethodClass.showProgressDialog(AboutUsActivity.this);
        String server_url = AboutUsActivity.this.getString(R.string.SERVER_URL) + "about/details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(AboutUsActivity.this).getString("LANG","en"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(AboutUsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(AboutUsActivity.this, response);
                    if (result_Object != null) {
                        final JSONObject SeedObj = result_Object.getJSONObject("details");
                        String lng = PreferenceManager.getDefaultSharedPreferences(AboutUsActivity.this).getString("LANG","en");
                        Picasso.get().load(ABT_BANNER_IMG+SeedObj.getString("banner_image")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(abtBnr);
                        if(lng.equals("en")){
                            abtHead.setText(Html.fromHtml(SeedObj.getString("heading_en")));
                            abtDesc.setText(Html.fromHtml(SeedObj.getString("description_en")));
                            abtHim.setText(Html.fromHtml(SeedObj.getString("about_him_en")));
                            abtHer.setText(Html.fromHtml(SeedObj.getString("about_her_en")));
                        }else if(lng.equals("fr")){
                            abtHead.setText(Html.fromHtml(SeedObj.getString("heading_fr")));
                            abtDesc.setText(Html.fromHtml(SeedObj.getString("description_fr")));
                            abtHim.setText(Html.fromHtml(SeedObj.getString("about_him_fr")));
                            abtHer.setText(Html.fromHtml(SeedObj.getString("about_her_fr")));
                        }else {
                            abtHead.setText(Html.fromHtml(SeedObj.getString("heading_es")));
                            abtDesc.setText(Html.fromHtml(SeedObj.getString("description_es")));
                            abtHim.setText(Html.fromHtml(SeedObj.getString("about_him_es")));
                            abtHer.setText(Html.fromHtml(SeedObj.getString("about_her_es")));
                        }

                        abtHimVisit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(SeedObj.getString("about_him_website"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        abtHerVisit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(SeedObj.getString("about_her_website"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        fbLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(SeedObj.getString("facebook_link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        twitterLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(SeedObj.getString("twitter_link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        instaLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(SeedObj.getString("instagram_link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        youtubelayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(SeedObj.getString("youtube_link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        photo_map_list = new ArrayList<HashMap<String, Object>>();
                        for (int i = 0; i <pic_array.length; i++) {
                            HashMap<String, Object> map = new HashMap<String, Object>();
                            map.put(IMG, pic_array[i]);
                            photo_map_list.add(map);
                        }
                        AboutPhotoAdapter adapter = new AboutPhotoAdapter(AboutUsActivity.this, photo_map_list);
                        photo_rv.setAdapter(adapter);
                        photo_rv.setFocusable(false);
                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(AboutUsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(AboutUsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(AboutUsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AboutUsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AboutUsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(AboutUsActivity.this).addToRequestQueue(jsonObjectRequest);

    }

}
