package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;

public class ChooseSignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(ChooseSignupActivity.this);
        setContentView(R.layout.activity_choose_signup);
    }
    public void memSign(View view){
        Intent I =new Intent(this,MemberSignupStep1Activity.class);
        startActivity(I);
    }
    public void gueSign(View view){
        Intent I =new Intent(this,GuestSignupStep1Activity.class);
        startActivity(I);
    }
    public void login(View view){
        Intent I =new Intent(this,LoginActivity.class);
        startActivity(I);
    }
    public void back(View view){
        super.onBackPressed();
    }
}
