package com.dunamis1.dunamis1.Activity.Message;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.MemberSignupStep2Activity;
import com.dunamis1.dunamis1.Activity.VerificationActivity;
import com.dunamis1.dunamis1.Adapter.MessageListAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.NotificationCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.MessageChatAdapter;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.COMM_USER;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MESSAGE_ID;
import static com.dunamis1.dunamis1.Helper.Constant.MSG;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_DAT;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_REC_ID;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_REC_NAME;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_REC_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_SND_ID;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_SND_NAME;
import static com.dunamis1.dunamis1.Helper.Constant.MSG_SND_PIC;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class MessageChatActivity extends AppCompatActivity {

    private ArrayList<HashMap<String,String>> arrayList;
    private RecyclerView recyView;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    private String user_id = "";
    private String msg_id = "";
    private EditText messageType;
    private ImageView send;
    private NotificationManager mNotificationManager;
    private   MessageChatAdapter messageChatAdapter;
    private String messageArray[]={"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus urna, dignissim ut ipsum tincidunt, rhoncus gravida nunc. Nullam elit eros congue ut ",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus urna",
            "Lorem ipsum dolor sit amet",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit Vestibulum",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus urna, dignissim ut ipsum tincidunt, rhoncus gravida nunc. Nullam elit eros",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit Vestibulum"};
    public int[] userPicArray={R.drawable.user_image1, R.drawable.man1, R.drawable.user_image1,
            R.drawable.man1, R.drawable.user_image1, R.drawable.man1, R.drawable.man1};
    private String[] nameArray={"Paul Alfred","Anthony James","Paul Alfred","Anthony James"
    ,"Paul Alfred","Anthony James","Anthony James"};
    private String[] timeArray={"10:15 AM","10:19 AM","12:15 PM","1:18 PM"
    ,"2:15 PM","5:40 PM","1:18 PM"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_chat);
        recyView=findViewById(R.id.recyView);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        messageType  = findViewById(R.id.messageType);
        send = findViewById(R.id.send);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        user_id = PreferenceManager.getDefaultSharedPreferences(this).getString("user_id","");
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(messageType.getText().toString().trim().length() == 0){
                    messageType.setError(getString(R.string.enterMsg));
                    messageType.requestFocus();
                    return;
                }
                sendMessage();
            }
        });
        arrayList = new ArrayList<>();
        chatList();
    }

    public void sendMessage(){
        MethodClass.showProgressDialog(MessageChatActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "message/create";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MessageChatActivity.this).getString("LANG","en"));
        params.put("receiver_id", getIntent().getStringExtra("rec_id"));
        params.put("message", messageType.getText().toString().trim());
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MessageChatActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MessageChatActivity.this, response);
                    if (result_Object != null) {
                        String messagess = result_Object.getString("message");
                        messageType.setText("");
                        JSONObject msgArray = result_Object.getJSONObject("message");

                        String message = msgArray.getJSONObject("message_details").getString("message");

                        String sender_title = msgArray.getJSONObject("sender_details").getString("title");
                        String sender_fname = msgArray.getJSONObject("sender_details").getString("fname");
                        String sender_lname = msgArray.getJSONObject("sender_details").getString("lname");
                        String sender_id = msgArray.getJSONObject("sender_details").getString("id");
                        String sender_profile_pic = msgArray.getJSONObject("sender_details").getString("profile_pic");

                        String rec_title = msgArray.getJSONObject("recever_details").getString("title");
                        String rec_fname = msgArray.getJSONObject("recever_details").getString("fname");
                        String rec_lname = msgArray.getJSONObject("recever_details").getString("lname");
                        String rec_id = msgArray.getJSONObject("recever_details").getString("id");
                        String rec_profile_pic = msgArray.getJSONObject("recever_details").getString("profile_pic");

                        String date = msgArray.getString("updated_at");

                        String id = msgArray.getString("id");
                        HashMap<String,String> map = new HashMap<>();

                        map.put(MSG_SND_PIC,sender_profile_pic);
                        map.put(MSG_SND_ID,sender_id);
                        map.put(MSG_REC_ID,rec_id);
                        if(sender_title.equals("null")){
                            map.put(MSG_SND_NAME,sender_fname+" "+sender_lname);
                        }else {
                            map.put(MSG_SND_NAME,sender_title+" "+sender_fname+" "+sender_lname);
                        }
                        map.put(MSG_REC_PIC,rec_profile_pic);
                        if(rec_title.equals("null")){
                            map.put(MSG_REC_NAME,rec_title+" "+rec_fname+" "+rec_lname);
                        }else {
                            map.put(MSG_REC_NAME,rec_title+" "+rec_fname+" "+rec_lname);
                        }

                        map.put(MSG,message);
                        map.put(MSG_DAT,date);
                        map.put(MESSAGE_ID,id);
                        arrayList.add(map);
                        if(arrayList.size()>0){
                            messageChatAdapter.notifyDataSetChanged();
                            recyView.post(new Runnable() {
                                @Override
                                public void run() {
                                    // Select the last row so it will scroll into view...
                                    recyView.smoothScrollToPosition(arrayList.size()-1);
                                }
                            });
                        }else{
                            messageChatAdapter = new MessageChatAdapter(MessageChatActivity.this,arrayList);
                            recyView.setAdapter(messageChatAdapter);
                            messageChatAdapter.notifyDataSetChanged();
                            recyView.post(new Runnable() {
                                @Override
                                public void run() {
                                    // Select the last row so it will scroll into view...
                                    recyView.smoothScrollToPosition(arrayList.size()-1);
                                }
                            });
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MessageChatActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MessageChatActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MessageChatActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MessageChatActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MessageChatActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MessageChatActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void chatList(){
        MethodClass.showProgressDialog(MessageChatActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "message/details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MessageChatActivity.this).getString("LANG","en"));
        params.put("receiver_id", getIntent().getStringExtra("rec_id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MessageChatActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MessageChatActivity.this, response);
                    if (result_Object != null) {

                        String status = result_Object.getString("status");
                        if(status.equals("A")){
                            JSONArray msgArray = result_Object.getJSONArray("list");
                            for (int i = 0; i <msgArray.length() ; i++) {
                                JSONObject SeedObj = msgArray.getJSONObject(i);
                                String message = SeedObj.getJSONObject("message_details").getString("message");

                                String sender_title = SeedObj.getJSONObject("sender_details").getString("title");
                                String sender_fname = SeedObj.getJSONObject("sender_details").getString("fname");
                                String sender_lname = SeedObj.getJSONObject("sender_details").getString("lname");
                                String sender_id = SeedObj.getJSONObject("sender_details").getString("id");
                                String sender_profile_pic = SeedObj.getJSONObject("sender_details").getString("profile_pic");

                                String rec_title = SeedObj.getJSONObject("recever_details").getString("title");
                                String rec_fname = SeedObj.getJSONObject("recever_details").getString("fname");
                                String rec_lname = SeedObj.getJSONObject("recever_details").getString("lname");
                                String rec_id = SeedObj.getJSONObject("recever_details").getString("id");
                                String rec_profile_pic = SeedObj.getJSONObject("recever_details").getString("profile_pic");

                                String date = SeedObj.getString("updated_at");

                                String id = SeedObj.getString("id");
                                msg_id = id;
                                HashMap<String,String> map = new HashMap<>();

                                map.put(MSG_SND_PIC,sender_profile_pic);
                                map.put(MSG_SND_ID,sender_id);
                                map.put(MSG_REC_ID,rec_id);
                                if(sender_title.equals("null")){
                                    map.put(MSG_SND_NAME,sender_fname+" "+sender_lname);
                                }else {
                                    map.put(MSG_SND_NAME,sender_title+" "+sender_fname+" "+sender_lname);
                                }
                                map.put(MSG_REC_PIC,rec_profile_pic);
                                if(rec_title.equals("null")){
                                    map.put(MSG_REC_NAME,rec_title+" "+rec_fname+" "+rec_lname);
                                }else {
                                    map.put(MSG_REC_NAME,rec_title+" "+rec_fname+" "+rec_lname);
                                }

                                map.put(MSG,message);
                                map.put(MSG_DAT,date);
                                map.put(MESSAGE_ID,id);
                                arrayList.add(map);
                            }
                            messageChatAdapter=new MessageChatAdapter(MessageChatActivity.this,arrayList);
                            recyView.setAdapter(messageChatAdapter);
                            recyView.setVisibility(View.VISIBLE);
                            recyView.post(new Runnable() {
                                @Override
                                public void run() {
                                    // Select the last row so it will scroll into view...
                                    recyView.smoothScrollToPosition(messageChatAdapter.getItemCount()-1);
                                }
                            });
                        }else if(status.equals("P") || status.equals("R")){
                            JSONArray msgArray = result_Object.getJSONArray("list");
                            String sender_title = msgArray.getJSONObject(0).getJSONObject("sender_details").getString("title");
                            String sender_fname = msgArray.getJSONObject(0).getJSONObject("sender_details").getString("fname");
                            String sender_lname = msgArray.getJSONObject(0).getJSONObject("sender_details").getString("lname");
                            String sender_id = msgArray.getJSONObject(0).getJSONObject("sender_details").getString("id");
                            String sender_profile_pic = msgArray.getJSONObject(0).getJSONObject("sender_details").getString("profile_pic");
                            final Dialog dialog = new Dialog(MessageChatActivity.this);
                            dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            dialog.setContentView(R.layout.msg_popup);
                            dialog.setCancelable(false);
                            Button yes_btn = (Button) dialog.findViewById(R.id.yes_btn);
                            Button no_btn = (Button) dialog.findViewById(R.id.no_btn);
                            CircleImageView ImgUser = (CircleImageView) dialog.findViewById(R.id.ImgUser);
                            TextView name = (TextView) dialog.findViewById(R.id.name);
                            TextView chatReq = (TextView) dialog.findViewById(R.id.chatReq);
                            Picasso.get().load(PROFILE_IMG_URL+sender_profile_pic).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(ImgUser);
                            if(!sender_title.equals("null") && !sender_title.equals(null)){
                                name.setText(sender_title+" "+sender_fname+" "+sender_lname);
                                chatReq.setText(sender_title+" "+sender_fname+" "+sender_lname+" "+getString(R.string.chat_req_msg));
                            }else{
                                name.setText(sender_fname+" "+sender_lname);
                                chatReq.setText(sender_fname+" "+sender_lname+" "+getString(R.string.chat_req_msg));
                            }

                            yes_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    approve(getIntent().getStringExtra("rec_id"),"A");
                                }
                            });
                            no_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    approve(getIntent().getStringExtra("rec_id"),"R");
                                }
                            });
                            dialog.show();
                        }else{
                            new SweetAlertDialog(MessageChatActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Approval Pending")
                                    .setContentText(getString(R.string.approvalReq))
                                    .setConfirmText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            Intent I =new Intent(MessageChatActivity.this, MessageListActivity.class);
                                            finish();
                                            startActivity(I);
                                        }
                                    })
                                    .show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MessageChatActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MessageChatActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MessageChatActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MessageChatActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MessageChatActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MessageChatActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    private void approve(String recId, final String type){
        String server_url = getString(R.string.SERVER_URL) + "message/status/update";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("reciever_id",recId);
        params.put("type",type);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MessageChatActivity.this, response);
                    if (result_Object != null) {
                        String message = result_Object.getString("message");
                        String meaning = result_Object.getString("meaning");
                        new SweetAlertDialog(MessageChatActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message)
                                .setContentText(meaning)
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        if(type.equals("A")){
                                            chatList();
                                        }else{
                                            Intent I =new Intent(MessageChatActivity.this, MessageListActivity.class);
                                            finish();
                                            startActivity(I);
                                        }

                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MessageChatActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MessageChatActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MessageChatActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MessageChatActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    private void chatList2(){
        String server_url = getString(R.string.SERVER_URL) + "message/single";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MessageChatActivity.this).getString("LANG","en"));
        params.put("id", msg_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MessageChatActivity.this, response);
                    if (result_Object != null) {
                        JSONObject msgArray = result_Object.getJSONObject("message");

                            String message = msgArray.getJSONObject("message_details").getString("message");

                            String sender_title = msgArray.getJSONObject("sender_details").getString("title");
                            String sender_fname = msgArray.getJSONObject("sender_details").getString("fname");
                            String sender_lname = msgArray.getJSONObject("sender_details").getString("lname");
                            String sender_id = msgArray.getJSONObject("sender_details").getString("id");
                            String sender_profile_pic = msgArray.getJSONObject("sender_details").getString("profile_pic");

                            String rec_title = msgArray.getJSONObject("recever_details").getString("title");
                            String rec_fname = msgArray.getJSONObject("recever_details").getString("fname");
                            String rec_lname = msgArray.getJSONObject("recever_details").getString("lname");
                            String rec_id = msgArray.getJSONObject("recever_details").getString("id");
                            String rec_profile_pic = msgArray.getJSONObject("recever_details").getString("profile_pic");

                            String date = msgArray.getString("updated_at");

                            String id = msgArray.getString("id");
                            HashMap<String,String> map = new HashMap<>();

                            map.put(MSG_SND_PIC,sender_profile_pic);
                            map.put(MSG_SND_ID,sender_id);
                            map.put(MSG_REC_ID,rec_id);
                            if(sender_title.equals("null")){
                                map.put(MSG_SND_NAME,sender_fname+" "+sender_lname);
                            }else {
                                map.put(MSG_SND_NAME,sender_title+" "+sender_fname+" "+sender_lname);
                            }
                            map.put(MSG_REC_PIC,rec_profile_pic);
                            if(rec_title.equals("null")){
                                map.put(MSG_REC_NAME,rec_title+" "+rec_fname+" "+rec_lname);
                            }else {
                                map.put(MSG_REC_NAME,rec_title+" "+rec_fname+" "+rec_lname);
                            }

                            map.put(MSG,message);
                            map.put(MSG_DAT,date);
                            map.put(MESSAGE_ID,id);
                            arrayList.add(map);
                            if(arrayList.size()>0){
                                messageChatAdapter.notifyDataSetChanged();
                                recyView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Select the last row so it will scroll into view...
                                        recyView.smoothScrollToPosition(arrayList.size()-1);
                                    }
                                });
                            }else{
                                messageChatAdapter = new MessageChatAdapter(MessageChatActivity.this,arrayList);
                                recyView.setAdapter(messageChatAdapter);
                                messageChatAdapter.notifyDataSetChanged();
                                recyView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Select the last row so it will scroll into view...
                                        recyView.smoothScrollToPosition(arrayList.size()-1);
                                    }
                                });
                            }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MessageChatActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MessageChatActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MessageChatActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MessageChatActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            final String from_id = intent.getStringExtra("id");
            msg_id = intent.getStringExtra("message_id");
            Log.e("TAG", from_id);
            final String titles = intent.getStringExtra("title");
            final String msg = intent.getStringExtra("message");

            if (getIntent().getStringExtra("rec_id").equals(from_id)) {
                chatList2();
            } else {
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(getApplicationContext(), "notify_001");
                Intent ii = null;
                ii = new Intent(getApplicationContext(), MessageChatActivity.class);
                ii.putExtra("rec_id",from_id);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);
                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                mBuilder.setContentTitle(titles);
                mBuilder.setContentText(msg);
                mBuilder.setSound(soundUri);
                mBuilder.setAutoCancel(true);
                mBuilder.setPriority(Notification.PRIORITY_MAX);

                mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    String channelId = "1003";
                    NotificationChannel channel = new NotificationChannel(channelId,
                            "DUNMAIS",
                            NotificationManager.IMPORTANCE_HIGH
                    );
                    mNotificationManager.createNotificationChannel(channel);
                    mBuilder.setChannelId(channelId);
                }

                mNotificationManager.notify(0, mBuilder.build());
            }
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("updateMessage"));
        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    public void back(View view) {
        super.onBackPressed();
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
