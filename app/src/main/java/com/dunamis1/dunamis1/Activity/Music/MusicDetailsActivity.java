package com.dunamis1.dunamis1.Activity.Music;

import android.content.Intent;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.CommentsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.MusicVideoAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_DESC;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_ID;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_TYPE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class MusicDetailsActivity extends AppCompatActivity {
    int[] sampleImagesArr = {R.drawable.image51,R.drawable.image52,R.drawable.image53,R.drawable.image54};
    private String[] title_array = {"AN INSTRUMENT OF WORSHIP", "I am all for you.mp3", "I HAVE COME BEFORE YOUR HOLY HILL","I remain your baby"};
    private ArrayList<HashMap<String,String>> map_list;
    private RecyclerView rv_Dash1;

    private TextView headtitle;
    private ImageView pauseButton;
    private ImageView playButton;
    private ImageView prevButton;
    private ImageView nextButton;
    private ImageView video_img_iv;
    private ImageView image_tv;
    private ImageView fav;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView nav_name_tv;
    private CircleImageView imgPro;


    private RelativeLayout imglayout;
    private RelativeLayout youtubelayout;
    private YouTubePlayerView youtube_player_view;

    private LinearLayout gallery_video_layout;

    private TextView title,desc,seedComm,seddLike,seedShare;
    private LinearLayout seddLikeCon;
    private LinearLayout seedCommCon;
    private LinearLayout seedShareCon;

    private SeekBar seekBar;
    private NestedScrollView scrollView;
    private TextView seekBarHint;
    private TextView textView1;
    private String music_id = "";
    private ProgressBar progree;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_details);
        rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        headtitle = (TextView) findViewById(R.id.headtitle);
        playButton = (ImageView) findViewById(R.id.playButton);
        pauseButton = (ImageView) findViewById(R.id.pauseButton);
        nextButton = (ImageView) findViewById(R.id.nextButton);
        prevButton = (ImageView) findViewById(R.id.prevButton);
        video_img_iv = (ImageView) findViewById(R.id.video_img_iv);
        imglayout = (RelativeLayout) findViewById(R.id.imglayout);
        youtubelayout = (RelativeLayout) findViewById(R.id.youtubelayout);
        scrollView = findViewById(R.id.scrollView);
        seekBarHint = findViewById(R.id.textView);
        textView1 = findViewById(R.id.textView1);
        seekBar = findViewById(R.id.seekbar);
        title =  findViewById(R.id.title);
        desc =  findViewById(R.id.desc);
        seedComm =  findViewById(R.id.seedComm);
        seddLike =  findViewById(R.id.seddLike);
        seedShare =  findViewById(R.id.seedShare);
        image_tv =  findViewById(R.id.image_tv);
        progree =  findViewById(R.id.progree);

        seddLikeCon =  findViewById(R.id.seddLikeCon);
        seedCommCon =  findViewById(R.id.seedCommCon);
        seedShareCon =  findViewById(R.id.seedShareCon);


        youtube_player_view = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        gallery_video_layout = (LinearLayout) findViewById(R.id.gallery_video_layout);
        fav = (ImageView) findViewById(R.id.fav);
        headtitle.setText(getIntent().getStringExtra("title"));

        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }

        MethodClass.seekInitate(seekBar,seekBarHint,MusicDetailsActivity.this,textView1);

        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = getString(R.string.SERVER_URL) + "favourite";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", getIntent().getStringExtra("id"));
                params.put("type", "M");
                params.put("language",PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(MusicDetailsActivity.this, response);
                            if (result_Object != null) {
                                String favourite = result_Object.getString("favourite");
                                if(favourite.equals("Y")){
                                    fav.setImageResource(R.drawable.ic_heart);
                                }else{
                                    fav.setImageResource(R.drawable.ic_heart_blank);
                                }
                                String message = result_Object.getString("message");
                                String meaning = result_Object.getString("meaning");
                                new SweetAlertDialog(MusicDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(message)
                                        .setContentText(meaning)
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(MusicDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MusicDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(MusicDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
            }
        });

        seedShareCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "http://phpwebdevelopmentservices.com/development/dunamis/api/share?content="+getIntent().getStringExtra("id")+"&language="+PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("lang","en")+"&type=M");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        seddLikeCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = getString(R.string.SERVER_URL) + "music/like";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("music_id", getIntent().getStringExtra("id"));
                params.put("language",PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(MusicDetailsActivity.this, response);
                            if (result_Object != null) {
                                String total_likes = result_Object.getString("total_likes");
                                seddLike.setText(total_likes+" "+MusicDetailsActivity.this.getString(R.string.likes));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(MusicDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MusicDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(MusicDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
            }
        });
        getDep();
    }
    public void getDep(){

        MethodClass.showProgressDialog(MusicDetailsActivity.this);
        String server_url = MusicDetailsActivity.this.getString(R.string.SERVER_URL) + "music/details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("LANG","en"));
        params.put("music_id",getIntent().getStringExtra("id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MusicDetailsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MusicDetailsActivity.this, response);
                    if (result_Object != null) {
                        MethodClass.hideProgressDialog(MusicDetailsActivity.this);
                        String favourites = result_Object.getString("favourite");
                        final JSONObject sedddetails = result_Object.getJSONObject("details");
                        title.setText(sedddetails.getJSONObject("musicdetails").getString("title"));

                        desc.setText(sedddetails.getJSONObject("musicdetails").getString("description"));
                        headtitle.setText(sedddetails.getJSONObject("musicdetails").getString("title"));
                        music_id = sedddetails.getJSONObject("musicdetails").getString("music_id");
                        final String codes = sedddetails.getString("code");
                        final String file_name = sedddetails.getString("file_name");


                        final String thumbnail = sedddetails.getString("thumbnail");
                        final String type = sedddetails.getString("type");

                        seedComm.setText(sedddetails.getString("total_comments")+" "+getString(R.string.comm));
                        seddLike.setText(sedddetails.getString("total_likes")+" "+getString(R.string.likes));
                        seedShare.setText(sedddetails.getString("total_share")+" "+getString(R.string.shar));

                        if(favourites.equals("Y")){
                            fav.setImageResource(R.drawable.ic_heart);
                        }else{
                            fav.setImageResource(R.drawable.ic_heart_blank);
                        }
                        seedCommCon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent I = new Intent(MusicDetailsActivity.this, CommentsActivity.class);
                                I.putExtra("from","music");
                                I.putExtra("id",getIntent().getStringExtra("id"));
                                startActivity(I);
                            }
                        });

                        if(type.equals("Y")){
                            youtubelayout.setVisibility(View.VISIBLE);
                            gallery_video_layout.setVisibility(View.GONE);
                            String url = "https://img.youtube.com/vi/"+codes+"/mqdefault.jpg";
                            Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(image_tv);
                            imglayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imglayout.setVisibility(View.GONE);
                                    youtube_player_view.setVisibility(View.VISIBLE);
                                    youtube_player_view.initialize(new YouTubePlayerInitListener() {
                                        @Override
                                        public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                                            initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                                @Override
                                                public void onReady() {
                                                    String videoId = codes;
                                                    initializedYouTubePlayer.loadVideo(videoId, 0);
                                                }
                                            });
                                        }
                                    }, true);
                                }
                            });

                        }else {
                            youtubelayout.setVisibility(View.GONE);
                            gallery_video_layout.setVisibility(View.VISIBLE);
                            Picasso.get().load(MUSIC_IMG_URL+thumbnail).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(video_img_iv);
                            playButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("file",file_name).commit();
                                    try{
                                        PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mname",sedddetails.getJSONObject("musicdetails").getString("title")).commit();
                                        PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mdesc",sedddetails.getJSONObject("musicdetails").getString("description")).commit();
                                        PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mimg",sedddetails.getString("thumbnail")).commit();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    playButton.setVisibility(View.GONE);
                                    pauseButton.setVisibility(View.VISIBLE);
                                    MethodClass.PlayAudio2(MusicDetailsActivity.this,PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("file",""),seekBar,textView1);
                                }
                            });
                            pauseButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    playButton.setVisibility(View.VISIBLE);
                                    pauseButton.setVisibility(View.GONE);
                                    MethodClass.PauseAudio(MusicDetailsActivity.this);
                                }
                            });
                            nextButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    playButton.setVisibility(View.VISIBLE);
                                    pauseButton.setVisibility(View.GONE);
                                    MethodClass.StopAudio(MusicDetailsActivity.this);
                                    textView1.setText("00:00");
                                    seekBarHint.setText("00:00");
                                    getDepnext();
                                }
                            });
                            prevButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    playButton.setVisibility(View.VISIBLE);
                                    pauseButton.setVisibility(View.GONE);
                                    MethodClass.StopAudio(MusicDetailsActivity.this);
                                    textView1.setText("00:00");
                                    seekBarHint.setText("00:00");
                                    getDepprev();
                                }
                            });
                        }




                        JSONArray seedArray = result_Object.getJSONArray("recent");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            JSONObject SeedObj = seedArray.getJSONObject(i);
                            String title = SeedObj.getJSONObject("musicdetails").getString("title");
                            String description = SeedObj.getJSONObject("musicdetails").getString("description");
                            String code = SeedObj.getString("code");
                            String types = SeedObj.getString("type");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String thumbnailss = SeedObj.getString("thumbnail");
                            String id = SeedObj.getString("id");


                            HashMap<String,String> map = new HashMap<>();
                            map.put(MUSIC_TITLE,title);
                            map.put(MUSIC_DESC,description);
                            map.put(MUSIC_TYPE,types);
                            map.put(MUSIC_CODE,code);
                            map.put(MUSIC_LIKE,total_likes);
                            map.put(MUSIC_COMMENT,total_comments);
                            map.put(MUSIC_SHARE,total_share);
                            map.put(MUSIC_ID,id);
                            map.put(MUSIC_THUMB,thumbnailss);

                            map_list.add(map);
                        }
                        MusicVideoAdapter bibleDeuteronomyAdapter= new MusicVideoAdapter(MusicDetailsActivity.this,map_list);
                        rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MusicDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MusicDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MusicDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MusicDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MusicDetailsActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void getDepnext(){
        MethodClass.showProgressDialog(MusicDetailsActivity.this);
        String server_url = MusicDetailsActivity.this.getString(R.string.SERVER_URL) + "music/control";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("LANG","en"));
        params.put("music_id",music_id);
        params.put("type","N");
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MusicDetailsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MusicDetailsActivity.this, response);
                    if (result_Object != null) {
                        String no_music = result_Object.getString("no_music");
                        if(no_music.equals("N")){
                            Toast.makeText(MusicDetailsActivity.this,getString(R.string.nomusic),Toast.LENGTH_LONG).show();
                        }else {
                            String favourites = result_Object.getString("favourite");
                            final JSONObject sedddetails = result_Object.getJSONObject("details");
                            title.setText(sedddetails.getJSONObject("musicdetails").getString("title"));
                            desc.setText(sedddetails.getJSONObject("musicdetails").getString("description"));
                            final String codes = sedddetails.getString("code");
                            final String file_name = sedddetails.getString("file_name");
                            final String thumbnail = sedddetails.getString("thumbnail");
                            final String type = sedddetails.getString("type");
                            headtitle.setText(sedddetails.getJSONObject("musicdetails").getString("title"));
                            music_id = sedddetails.getJSONObject("musicdetails").getString("music_id");
                            seedComm.setText(sedddetails.getString("total_comments")+" "+getString(R.string.comm));
                            seddLike.setText(sedddetails.getString("total_likes")+" "+getString(R.string.likes));
                            seedShare.setText(sedddetails.getString("total_share")+" "+getString(R.string.shar));

                            if(favourites.equals("Y")){
                                fav.setImageResource(R.drawable.ic_heart);
                            }else{
                                fav.setImageResource(R.drawable.ic_heart_blank);
                            }
                            seedCommCon.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(MusicDetailsActivity.this, CommentsActivity.class);
                                    I.putExtra("from","music");
                                    I.putExtra("id",getIntent().getStringExtra("id"));
                                    startActivity(I);
                                }
                            });

                            if(type.equals("Y")){
                                youtubelayout.setVisibility(View.VISIBLE);
                                gallery_video_layout.setVisibility(View.GONE);
                                String url = "https://img.youtube.com/vi/"+codes+"/mqdefault.jpg";
                                Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(image_tv);
                                imglayout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imglayout.setVisibility(View.GONE);
                                        youtube_player_view.setVisibility(View.VISIBLE);
                                        youtube_player_view.initialize(new YouTubePlayerInitListener() {
                                            @Override
                                            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                                                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                                    @Override
                                                    public void onReady() {
                                                        String videoId = codes;
                                                        initializedYouTubePlayer.loadVideo(videoId, 0);
                                                    }
                                                });
                                            }
                                        }, true);
                                    }
                                });

                            }else {
                                youtubelayout.setVisibility(View.GONE);
                                gallery_video_layout.setVisibility(View.VISIBLE);
                                Picasso.get().load(MUSIC_IMG_URL+thumbnail).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(video_img_iv);
                                playButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("file",file_name).commit();
                                        try{
                                            PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mname",sedddetails.getJSONObject("musicdetails").getString("title")).commit();
                                            PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mdesc",sedddetails.getJSONObject("musicdetails").getString("description")).commit();
                                            PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mimg",sedddetails.getString("thumbnail")).commit();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                        playButton.setVisibility(View.GONE);
                                        pauseButton.setVisibility(View.VISIBLE);
                                        MethodClass.PlayAudio2(MusicDetailsActivity.this,PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("file",""),seekBar,textView1);
                                    }
                                });
                                pauseButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        playButton.setVisibility(View.VISIBLE);
                                        pauseButton.setVisibility(View.GONE);
                                        MethodClass.PauseAudio(MusicDetailsActivity.this);
                                    }
                                });
                                nextButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        playButton.setVisibility(View.VISIBLE);
                                        pauseButton.setVisibility(View.GONE);
                                        MethodClass.StopAudio(MusicDetailsActivity.this);
                                        textView1.setText("00:00");
                                        seekBarHint.setText("00:00");
                                        getDepnext();
                                    }
                                });
                                prevButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        playButton.setVisibility(View.VISIBLE);
                                        pauseButton.setVisibility(View.GONE);
                                        MethodClass.StopAudio(MusicDetailsActivity.this);
                                        textView1.setText("00:00");
                                        seekBarHint.setText("00:00");
                                        getDepprev();
                                    }
                                });
                            }




                            JSONArray seedArray = result_Object.getJSONArray("recent");

                            map_list = new ArrayList<>();

                            for (int i = 0; i <seedArray.length() ; i++) {
                                JSONObject SeedObj = seedArray.getJSONObject(i);
                                String title = SeedObj.getJSONObject("musicdetails").getString("title");
                                String description = SeedObj.getJSONObject("musicdetails").getString("description");
                                String code = SeedObj.getString("code");
                                String types = SeedObj.getString("type");
                                String total_likes = SeedObj.getString("total_likes");
                                String total_comments = SeedObj.getString("total_comments");
                                String total_share = SeedObj.getString("total_share");
                                String thumbnailss = SeedObj.getString("thumbnail");
                                String id = SeedObj.getString("id");


                                HashMap<String,String> map = new HashMap<>();
                                map.put(MUSIC_TITLE,title);
                                map.put(MUSIC_DESC,description);
                                map.put(MUSIC_TYPE,types);
                                map.put(MUSIC_CODE,code);
                                map.put(MUSIC_LIKE,total_likes);
                                map.put(MUSIC_COMMENT,total_comments);
                                map.put(MUSIC_SHARE,total_share);
                                map.put(MUSIC_ID,id);
                                map.put(MUSIC_THUMB,thumbnailss);

                                map_list.add(map);
                            }
                            MusicVideoAdapter bibleDeuteronomyAdapter= new MusicVideoAdapter(MusicDetailsActivity.this,map_list);
                            rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                        }

                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MusicDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MusicDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MusicDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MusicDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MusicDetailsActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void getDepprev(){
        MethodClass.showProgressDialog(MusicDetailsActivity.this);
        String server_url = MusicDetailsActivity.this.getString(R.string.SERVER_URL) + "music/control";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("LANG","en"));
        params.put("music_id",music_id);
        params.put("type","P");
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MusicDetailsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MusicDetailsActivity.this, response);
                    if (result_Object != null) {
                        String no_music = result_Object.getString("no_music");
                        if(no_music.equals("N")){
                            Toast.makeText(MusicDetailsActivity.this,getString(R.string.nomusic),Toast.LENGTH_LONG).show();
                        }else {
                            String favourites = result_Object.getString("favourite");
                            final JSONObject sedddetails = result_Object.getJSONObject("details");
                            title.setText(sedddetails.getJSONObject("musicdetails").getString("title"));
                            desc.setText(sedddetails.getJSONObject("musicdetails").getString("description"));
                            final String codes = sedddetails.getString("code");
                            final String file_name = sedddetails.getString("file_name");
                            final String thumbnail = sedddetails.getString("thumbnail");
                            final String type = sedddetails.getString("type");
                            headtitle.setText(sedddetails.getJSONObject("musicdetails").getString("title"));
                            music_id = sedddetails.getJSONObject("musicdetails").getString("music_id");
                            seedComm.setText(sedddetails.getString("total_comments")+" "+getString(R.string.comm));
                            seddLike.setText(sedddetails.getString("total_likes")+" "+getString(R.string.likes));
                            seedShare.setText(sedddetails.getString("total_share")+" "+getString(R.string.shar));

                            if(favourites.equals("Y")){
                                fav.setImageResource(R.drawable.ic_heart);
                            }else{
                                fav.setImageResource(R.drawable.ic_heart_blank);
                            }
                            seedCommCon.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(MusicDetailsActivity.this, CommentsActivity.class);
                                    I.putExtra("from","music");
                                    I.putExtra("id",getIntent().getStringExtra("id"));
                                    startActivity(I);
                                }
                            });

                            if(type.equals("Y")){
                                youtubelayout.setVisibility(View.VISIBLE);
                                gallery_video_layout.setVisibility(View.GONE);
                                String url = "https://img.youtube.com/vi/"+codes+"/mqdefault.jpg";
                                Picasso.get().load(url).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(image_tv);
                                imglayout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imglayout.setVisibility(View.GONE);
                                        youtube_player_view.setVisibility(View.VISIBLE);
                                        youtube_player_view.initialize(new YouTubePlayerInitListener() {
                                            @Override
                                            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                                                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                                    @Override
                                                    public void onReady() {
                                                        String videoId = codes;
                                                        initializedYouTubePlayer.loadVideo(videoId, 0);
                                                    }
                                                });
                                            }
                                        }, true);
                                    }
                                });

                            }else {
                                youtubelayout.setVisibility(View.GONE);
                                gallery_video_layout.setVisibility(View.VISIBLE);
                                Picasso.get().load(MUSIC_IMG_URL+thumbnail).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(video_img_iv);
                                playButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("file",file_name).commit();
                                        try{
                                            PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mname",sedddetails.getJSONObject("musicdetails").getString("title")).commit();
                                            PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mdesc",sedddetails.getJSONObject("musicdetails").getString("description")).commit();
                                            PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).edit().putString("Mimg",sedddetails.getString("thumbnail")).commit();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                        playButton.setVisibility(View.GONE);
                                        pauseButton.setVisibility(View.VISIBLE);
                                        MethodClass.PlayAudio2(MusicDetailsActivity.this,PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("file",""),seekBar,textView1);
                                    }
                                });
                                pauseButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        playButton.setVisibility(View.VISIBLE);
                                        pauseButton.setVisibility(View.GONE);
                                        MethodClass.PauseAudio(MusicDetailsActivity.this);
                                    }
                                });
                                nextButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        playButton.setVisibility(View.VISIBLE);
                                        pauseButton.setVisibility(View.GONE);
                                        MethodClass.StopAudio(MusicDetailsActivity.this);
                                        textView1.setText("00:00");
                                        seekBarHint.setText("00:00");
                                        getDepnext();

                                    }
                                });
                                prevButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        playButton.setVisibility(View.VISIBLE);
                                        pauseButton.setVisibility(View.GONE);
                                        MethodClass.StopAudio(MusicDetailsActivity.this);
                                        textView1.setText("00:00");
                                        seekBarHint.setText("00:00");
                                        getDepprev();
                                    }
                                });
                            }




                            JSONArray seedArray = result_Object.getJSONArray("recent");

                            map_list = new ArrayList<>();

                            for (int i = 0; i <seedArray.length() ; i++) {
                                JSONObject SeedObj = seedArray.getJSONObject(i);
                                String title = SeedObj.getJSONObject("musicdetails").getString("title");
                                String description = SeedObj.getJSONObject("musicdetails").getString("description");
                                String code = SeedObj.getString("code");
                                String types = SeedObj.getString("type");
                                String total_likes = SeedObj.getString("total_likes");
                                String total_comments = SeedObj.getString("total_comments");
                                String total_share = SeedObj.getString("total_share");
                                String thumbnailss = SeedObj.getString("thumbnail");
                                String id = SeedObj.getString("id");


                                HashMap<String,String> map = new HashMap<>();
                                map.put(MUSIC_TITLE,title);
                                map.put(MUSIC_DESC,description);
                                map.put(MUSIC_TYPE,types);
                                map.put(MUSIC_CODE,code);
                                map.put(MUSIC_LIKE,total_likes);
                                map.put(MUSIC_COMMENT,total_comments);
                                map.put(MUSIC_SHARE,total_share);
                                map.put(MUSIC_ID,id);
                                map.put(MUSIC_THUMB,thumbnailss);

                                map_list.add(map);
                            }
                            MusicVideoAdapter bibleDeuteronomyAdapter= new MusicVideoAdapter(MusicDetailsActivity.this,map_list);
                            rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                        }

                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MusicDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MusicDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MusicDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MusicDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MusicDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MusicDetailsActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void back(View view){
        super.onBackPressed();
    }
    public void fav(View view){
        MethodClass.StopAudio(this);
        MethodClass.go_to_next_activity(this, MyFavouriteActivity.class);
    }public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
    public void  logout(View view){
            Intent I = new Intent(this, SplashActivity.class);
            PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
            startActivity(I);
    }
    public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
    public void showProg(){
        Log.e("Inser", "showProg: " );
        progree.setVisibility(View.VISIBLE);
        progree.bringToFront();
    }
    public void hideProg(){

    }

}
