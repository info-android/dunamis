package com.dunamis1.dunamis1.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import de.hdodenhof.circleimageview.CircleImageView;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.dunamis1.dunamis1.Fragment.MemberSetp1Fragment;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.Helper.RequestManager;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;
import com.vanillaplacepicker.presentation.builder.VanillaPlacePicker;
import com.vanillaplacepicker.utils.KeyUtils;
import com.vanillaplacepicker.utils.MapType;
import com.vanillaplacepicker.utils.PickerLanguage;
import com.vanillaplacepicker.utils.PickerType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.PICK_FROM_GALLERY;
import static com.dunamis1.dunamis1.Helper.Constant.PICK_FROM_GALLERY_OREO;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.TAKE_PHOTO_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.TAKE_PHOTO_CODE2;

public class MemberNewEditProfileActivity extends AppCompatActivity {
    private CircleImageView image;
    private Spinner mar_spinner;
    private Spinner title_spinner;
    private Button cont;
    private String user_id;
    private String martial="",titles="",emails="";
    private EditText fname,lname,emailaddress,phone,address;
    private Integer selected_mar = 0,selected_title = 0;
    private String code = "";
    private double lat,long_;
    private int REQUEST_PLACE_PICKER = 9;
    CountryCodePicker ccp;
    private CircleImageView upload_imgs;
    private EditText aboutme;
    private LinearLayout uploac_img;
    private LinearLayout uploac_img2;
    private LinearLayout upload_img;
    private LinearLayout upload_img3;
    private LinearLayout dstlay;
    private Uri outputFileUri;
    private String imageFilePath;
    private String product_image ="";
    private ArrayList<String> ListImage = new ArrayList<String>();
    private ArrayList<String> ListImage2 = new ArrayList<String>();
    private JSONArray photoArr;
    private EditText oldPass,newPass,confPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_new_edit_profile);

        title_spinner = findViewById(R.id.title_spinner);
        mar_spinner = findViewById(R.id.mar_spinner);
        cont = findViewById(R.id.cont);
        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        emailaddress = findViewById(R.id.emailaddress);
        phone = findViewById(R.id.phone);
        address = findViewById(R.id.address);
        image = findViewById(R.id.image);
        upload_imgs = findViewById(R.id.upload_imgs);
        aboutme = findViewById(R.id.aboutme);
        uploac_img = findViewById(R.id.uploac_img);
        uploac_img2 = findViewById(R.id.uploac_img2);
        upload_img = findViewById(R.id.upload_img);
        upload_img3 = findViewById(R.id.upload_img3);
        dstlay = findViewById(R.id.dstlay);

        oldPass = findViewById(R.id.oldPass);
        newPass = findViewById(R.id.newPass);
        confPass = findViewById(R.id.confPass);

        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.registerCarrierNumberEditText(phone);
        ccp.setNumberAutoFormattingEnabled(false);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                //Toast.makeText(MemberNewEditProfileActivity.this, "Updated " + ccp.getSelectedCountryCodeWithPlus(), Toast.LENGTH_SHORT).show();
                code = ccp.getSelectedCountryCodeWithPlus();
            }
        });


        uploac_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu= new PopupMenu(MemberNewEditProfileActivity.this,uploac_img);
                popupMenu.getMenuInflater().inflate(R.menu.camera_popup_menu,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id=item.getItemId();
                        if (id== R.id.camera){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (MemberNewEditProfileActivity.this.checkSelfPermission(Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                                }
                                else{
                                    CameraIntent2();
                                }
                            }
                        }
                        else if (id==R.id.gallery){
                            try {
                                if (ActivityCompat.checkSelfPermission(MemberNewEditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(MemberNewEditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                                } else {
                                    galleryIntent2();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        uploac_img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu= new PopupMenu(MemberNewEditProfileActivity.this,uploac_img2);
                popupMenu.getMenuInflater().inflate(R.menu.camera_popup_menu,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id=item.getItemId();
                        if (id== R.id.camera){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (MemberNewEditProfileActivity.this.checkSelfPermission(Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                                }
                                else{
                                    CameraIntent();
                                }
                            }
                        }
                        else if (id==R.id.gallery){
                            try {
                                if (ActivityCompat.checkSelfPermission(MemberNewEditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(MemberNewEditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                                } else {
                                    galleryIntent();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent intent = new VanillaPlacePicker.Builder(MemberNewEditProfileActivity.this)
                            .withLocation(lat, long_)
                            .setPickerLanguage(PickerLanguage.ENGLISH) // Apply language to picker
                            .setTintColor(getResources().getColor(R.color.colorPrimary)) // Apply Tint color to Back, Clear button of AutoComplete UI
                            /*
                             * Configuration for AutoComplete UI
                             */
                            .setLanguage(PreferenceManager.getDefaultSharedPreferences(MemberNewEditProfileActivity.this).getString("LANG","en"))
                            .isOpenNow(true) // Returns only those places that are open for business at the time the query is sent.

                            /*
                             * Configuration for Map UI
                             */
                            .with(PickerType.AUTO_COMPLETE) // Select Picker type to enable autocompelte, map or both
                            .setMapType(MapType.SATELLITE) // Choose map type (Only applicable for map screen)
                            // containing the JSON style declaration for night-mode styling
                            .setMapPinDrawable(android.R.drawable.ic_menu_mylocation) // To give custom pin image for map marker
                            .build();
                    startActivityForResult(intent, REQUEST_PLACE_PICKER);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });
        getUserDet();
    }
    public void updateProfile(){
        if(titles.equals("")){
            View parentLayout = MemberNewEditProfileActivity.this.findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseTitle), Snackbar.LENGTH_LONG);
            snackbar.show();
            title_spinner.requestFocus();
            return;
        }
        if(fname.getText().toString().trim().length() == 0){
            fname.setError(MemberNewEditProfileActivity.this.getString(R.string.fnameReq));
            fname.requestFocus();
            return;
        }
        if(lname.getText().toString().trim().length() == 0){
            lname.setError(MemberNewEditProfileActivity.this.getString(R.string.lnameReq));
            lname.requestFocus();
            return;
        }
        if(!MethodClass.emailValidator(emailaddress.getText().toString().trim())){
            emailaddress.setError(MemberNewEditProfileActivity.this.getString(R.string.emailReq));
            lname.requestFocus();
            return;
        }
        if(code.equals("")){
            View parentLayout = MemberNewEditProfileActivity.this.findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCode), Snackbar.LENGTH_LONG);
            snackbar.show();
            mar_spinner.requestFocus();
            return;
        }
        if(phone.getText().toString().trim().length() == 0){
            phone.setError(getString(R.string.lnameReq));
            phone.requestFocus();
            return;
        }
        if(martial.equals("")){
            View parentLayout = MemberNewEditProfileActivity.this.findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseMartial), Snackbar.LENGTH_LONG);
            snackbar.show();
            mar_spinner.requestFocus();
            return;
        }
        if(dstlay.getVisibility() == View.VISIBLE){
            if(address.getText().toString().trim().length() == 0){
                address.setError(MemberNewEditProfileActivity.this.getString(R.string.addressReq));
                address.requestFocus();
                return;
            }
        }

        if(aboutme.getText().toString().trim().length() == 0){
            aboutme.setError(MemberNewEditProfileActivity.this.getString(R.string.abtReq));
            aboutme.requestFocus();
            return;
        }
        if(oldPass.getText().toString().trim().length()>0){
            if(newPass.getText().toString().trim().length() == 0){
                newPass.setError(getString(R.string.passReq));
                newPass.requestFocus();
                return;
            }
            if(confPass.getText().toString().trim().length() == 0){
                confPass.setError(getString(R.string.passReq));
                confPass.requestFocus();
                return;
            }
            if(!newPass.getText().toString().trim().equals(confPass.getText().toString().trim())){
                confPass.setError(getString(R.string.confirmpassMismatch));
                confPass.requestFocus();
                return;
            }
        }

        if (MethodClass.isNetworkConnected(MemberNewEditProfileActivity.this)) {
            MethodClass.showProgressDialog(MemberNewEditProfileActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "member-update";
            SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);
                    try {
                        JSONObject jsonObject = MethodClass.get_result_from_webservice(MemberNewEditProfileActivity.this, new JSONObject(response));
                        Log.e("RESULT", jsonObject.toString() );
                        if (jsonObject != null) {

                            if(!emails.equals(emailaddress.getText().toString().trim())){
                                final String phoness = jsonObject.getString("phone");
                                final String otp = jsonObject.getString("otp");
                                new SweetAlertDialog(MemberNewEditProfileActivity.this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(getString(R.string.verfiNeeded))
                                        .setContentText(getString(R.string.verfiNeededMsg))
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                Intent I = new Intent(MemberNewEditProfileActivity.this,EmailVerificationActivity.class);
                                                I.putExtra("vcode",otp);
                                                I.putExtra("phone",phoness);
                                                I.putExtra("email",emails);
                                                startActivity(I);
                                            }
                                        })
                                        .show();
                            }else{
                                new SweetAlertDialog(MemberNewEditProfileActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(jsonObject.getString("message"))
                                        .setContentText(jsonObject.getString("meaning"))
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                finish();
                                            }
                                        })
                                        .show();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);
                    Log.e("error", error.toString());
                    MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(MemberNewEditProfileActivity.this);
                    } else {
                        MethodClass.error_alert(MemberNewEditProfileActivity.this);
                    }
                }
            }){
                // Passing some request headers
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberNewEditProfileActivity.this).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            RequestQueue requestQueue = RequestManager.getnstance(MemberNewEditProfileActivity.this);


            simpleMultiPartRequest.addMultipartParam("about_me", "text", aboutme.getText().toString().trim());
            simpleMultiPartRequest.addMultipartParam("title", "text", titles);
            simpleMultiPartRequest.addMultipartParam("fname", "text", fname.getText().toString().trim());
            simpleMultiPartRequest.addMultipartParam("lname", "text", lname.getText().toString().trim());
            simpleMultiPartRequest.addMultipartParam("marital_status", "text",martial);
            if(dstlay.getVisibility() == View.VISIBLE){
                simpleMultiPartRequest.addMultipartParam("address", "text",address.getText().toString().trim());
                simpleMultiPartRequest.addMultipartParam("latitude", "text",String.valueOf(lat));
                simpleMultiPartRequest.addMultipartParam("longitude", "text",String.valueOf(long_));
            }else{
                simpleMultiPartRequest.addMultipartParam("address", "text","");
                simpleMultiPartRequest.addMultipartParam("latitude", "text","");
                simpleMultiPartRequest.addMultipartParam("longitude", "text","");
            }

            simpleMultiPartRequest.addMultipartParam("phone_number", "text",phone.getText().toString().trim());
            simpleMultiPartRequest.addMultipartParam("email", "text",emailaddress.getText().toString().trim());
            simpleMultiPartRequest.addMultipartParam("country_code", "text",code);

            if(oldPass.getText().toString().trim().length()>0){
                simpleMultiPartRequest.addMultipartParam("old_password", "text",oldPass.getText().toString().trim());
                simpleMultiPartRequest.addMultipartParam("new_password", "text",newPass.getText().toString().trim());
            }


            //simpleMultiPartRequest.addMultipartParam("email", "text", get_email);
            simpleMultiPartRequest.addMultipartParam("count","text", String.valueOf(ListImage.size()));
            if(ListImage.size()>0){
                for (int i = 0; i <ListImage.size() ; i++) {
                    simpleMultiPartRequest.addFile("profile_images"+(i+1), ListImage.get(i).toString());
                }
            }simpleMultiPartRequest.addMultipartParam("profile_count","text", String.valueOf(ListImage2.size()));
            if(ListImage2.size()>0){
                for (int i = 0; i <ListImage2.size() ; i++) {
                    simpleMultiPartRequest.addFile("profile_image"+(i+1), ListImage2.get(i).toString());
                }
            }

            simpleMultiPartRequest.setFixedStreamingMode(true);
            int socketTimeout = 100000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
            simpleMultiPartRequest.setRetryPolicy(policy);
            requestQueue.getCache().clear();
            requestQueue.add(simpleMultiPartRequest);

        } else {
            Snackbar snackbar = Snackbar.make(MemberNewEditProfileActivity.this.findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == REQUEST_PLACE_PICKER && data != null) {
                String vanillaAddress = String.valueOf(data.getSerializableExtra(KeyUtils.SELECTED_PLACE));
                //String vanillaAddress2 = String.valueOf(data.getSerializableExtra(KeyUtils.LOCATION));
                Log.e("vanillaAddress", vanillaAddress);

                String toSplit = "a+b-c*d/e=f";
                String[] splitted = vanillaAddress.split("[,()=]");
                ArrayList<String> address_list = new ArrayList<>();
                for (String split : splitted) {
                    System.out.println(split);
                    Log.e("split", split);
                    address_list.add(split);

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("latitude")){
                        try {
                            lat= Double.parseDouble(address_list.get(i+1));
                            Log.e("lat", String.valueOf(lat));

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("longitude")){
                        try {
                            long_= Double.parseDouble(address_list.get(i+1));
                            Log.e("long_", String.valueOf(long_));


                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }
                }
                String get_add = address_list.get(2) + ", " + address_list.get(3) + ", " + address_list.get(4);



                address.setError(null);
                Log.e("vanillaAddress", vanillaAddress);

                address.setText(get_add);
            }
            if (requestCode == PICK_FROM_GALLERY) {
                Uri selectedUri;
                if (null != data) { // checking empty selection

                        if (null != data.getClipData()) { // checking multiple selection or not
                            if(data.getClipData().getItemCount()+ListImage.size()+photoArr.length() >= 3){
                                ListImage = new ArrayList<String>();
                                Snackbar snackbar = Snackbar.make(MemberNewEditProfileActivity.this.findViewById(android.R.id.content),"You can have maximum three photos", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                            for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                                selectedUri = data.getClipData().getItemAt(i).getUri();
                                try {
                                    product_image = getPath(MemberNewEditProfileActivity.this, selectedUri);
                                    ListImage.add(product_image);
                                } catch (URISyntaxException e) {
                                    e.printStackTrace();
                                }
                            }

                        } else {
                            if(photoArr.length()+ListImage.size()>=3){
                                Snackbar snackbar = Snackbar.make(MemberNewEditProfileActivity.this.findViewById(android.R.id.content),"You can have maximum three photos", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }else{
                                if(!data.getData().getAuthority().equals("com.android.providers.downloads.documents")){
                                    selectedUri = data.getData();
                                    try {
                                        product_image = getPath(MemberNewEditProfileActivity.this, selectedUri);
                                        ListImage.add(product_image);
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    Log.e("SIZE", String.valueOf(ListImage.size()));
                                    Snackbar snackbar = Snackbar.make(MemberNewEditProfileActivity.this.findViewById(android.R.id.content),"Please choose image from other folder", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }
                            }
                        }
                        Log.e("SIZE", String.valueOf(ListImage.size()));
                        showImage();

                }
            } else if (requestCode == TAKE_PHOTO_CODE2) {

                product_image = imageFilePath;
                ListImage2.add(product_image);
                showImage2();
            }
            if (requestCode == PICK_FROM_GALLERY_OREO) {
                Uri selectedUri;
                if (null != data) { // checking empty selection
                    if (null != data.getClipData()) { // checking multiple selection or not
                        if(data.getClipData().getItemCount()+ListImage2.size() > 1){
                            ListImage2 = new ArrayList<String>();
                        }
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            selectedUri = data.getClipData().getItemAt(i).getUri();
                            try {
                                product_image = getPath(MemberNewEditProfileActivity.this, selectedUri);
                                ListImage2.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        if(!data.getData().getAuthority().equals("com.android.providers.downloads.documents")){
                            selectedUri = data.getData();
                            try {
                                product_image = getPath(MemberNewEditProfileActivity.this, selectedUri);
                                ListImage2.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.e("SIZE", String.valueOf(ListImage.size()));
                            Snackbar snackbar = Snackbar.make(MemberNewEditProfileActivity.this.findViewById(android.R.id.content),"Please choose image from other folder", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    }
                    Log.e("SIZE", String.valueOf(ListImage.size()));
                    showImage2();
                }
            } else if (requestCode == TAKE_PHOTO_CODE) {
                if(ListImage.size()+photoArr.length()>=3){
                    Snackbar snackbar = Snackbar.make(MemberNewEditProfileActivity.this.findViewById(android.R.id.content),"You can have maximum three photos", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else {
                    product_image = imageFilePath;
                    ListImage.add(product_image);
                    showImage();
                }

            }

        }
    }

    public void getUserDet(){
        MethodClass.showProgressDialog(MemberNewEditProfileActivity.this);
        String server_url = MemberNewEditProfileActivity.this.getString(R.string.SERVER_URL) + "member-details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MemberNewEditProfileActivity.this, response);
                    if (result_Object != null) {
                        JSONObject user_details = result_Object.getJSONObject("user");
                        code = user_details.getString("country_code");

                        ccp.setFullNumber(user_details.getString("country_code")+user_details.getString("phone_number"));

                        List<StringWithTag> spinnerArraysTitle = new ArrayList<StringWithTag>();
                        spinnerArraysTitle.add(new StringWithTag(getString(R.string.selTitle),""));
                        for (int l = 0; l <11 ; l++) {
                            if(l ==0){
                                spinnerArraysTitle.add(new StringWithTag(getString(R.string.dr), "M"));
                            }
                            if(l ==1){
                                spinnerArraysTitle.add(new StringWithTag("Mr.", "S"));
                            }if(l ==2){
                                spinnerArraysTitle.add(new StringWithTag("Ms.", "S"));
                            }if(l ==3){
                                spinnerArraysTitle.add(new StringWithTag("Mrs.", "S"));
                            }if(l ==4){
                                spinnerArraysTitle.add(new StringWithTag("Eng.", "S"));
                            }if(l ==5){
                                spinnerArraysTitle.add(new StringWithTag("Prof.", "S"));
                            }if(l ==6){
                                spinnerArraysTitle.add(new StringWithTag("Rev.", "S"));
                            }if(l ==7){
                                spinnerArraysTitle.add(new StringWithTag("Rt. Hon.", "S"));
                            }if(l ==8){
                                spinnerArraysTitle.add(new StringWithTag("Sr.", "S"));
                            }if(l ==9){
                                spinnerArraysTitle.add(new StringWithTag("Esq.", "S"));
                            }if(l ==10){
                                spinnerArraysTitle.add(new StringWithTag("Hon.", "S"));
                            }if(user_details.getString("title").equals("Dr.")){
                                selected_title = 1;
                            }
                            else if(user_details.getString("title").equals("Mr.")){
                                selected_title = 2;
                            }else if(user_details.getString("title").equals("Ms.")){
                                selected_title = 3;
                            }else if(user_details.getString("title").equals("Eng.")){
                                selected_title = 5;
                            }else if(user_details.getString("title").equals("Prof.")){
                                selected_title = 6;
                            }else if(user_details.getString("title").equals("Rev.")){
                                selected_title = 7;
                            }else if(user_details.getString("title").equals("Rt. Hon.")){
                                selected_title = 8;
                            }else if(user_details.getString("title").equals("Sr.")){
                                selected_title = 9;
                            }else if(user_details.getString("title").equals("Esq.")){
                                selected_title = 10;
                            }else if(user_details.getString("title").equals("Hon.")){
                                selected_title = 11;
                            }
                            else if(user_details.getString("title").equals("Mrs.")){
                                selected_title = 4;
                            }

                        }
                        ArrayAdapter title_spinneradapter3 = new ArrayAdapter(MemberNewEditProfileActivity.this, R.layout.spinner_item, spinnerArraysTitle) {
                        };
                        title_spinner.setAdapter(title_spinneradapter3);
                        title_spinner.setSelection(selected_title);
                        title_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                if(pos>0){
                                    titles = String.valueOf(s.string);
                                }


                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });
                        List<StringWithTag> spinnerArraysMart = new ArrayList<StringWithTag>();
                        spinnerArraysMart.add(new StringWithTag(getString(R.string.selMartial),""));
                        for (int K = 0; K <2 ; K++) {

                            if(K ==0){
                                spinnerArraysMart.add(new StringWithTag(getString(R.string.Married), "M"));
                            }
                            if(K ==1){
                                spinnerArraysMart.add(new StringWithTag(getString(R.string.Single), "S"));
                            }
                            if(user_details.getString("marital_status").equals("M")){
                                selected_mar = 1;
                            }else if(user_details.getString("marital_status").equals("S")){
                                selected_mar = 2;
                            }

                        }
                        ArrayAdapter adapter3 = new ArrayAdapter(MemberNewEditProfileActivity.this, R.layout.spinner_item, spinnerArraysMart) {
                        };
                        mar_spinner.setAdapter(adapter3);
                        mar_spinner.setSelection(selected_mar);
                        mar_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                martial = String.valueOf(s.tag);

                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });

                        fname.setText(user_details.getString("fname"));
                        lname.setText(user_details.getString("lname"));
                        emailaddress.setText(user_details.getString("email"));
                        emails = user_details.getString("email");
                        phone.setText(user_details.getString("phone_number"));
                        if(!user_details.getString("address").equals("null")){
                            address.setText(user_details.getString("address"));
                        }else {
                            address.setText("");
                        }
                        if(!user_details.getString("about_me").equals("null")){
                            aboutme.setText(user_details.getString("about_me"));
                        }else {
                            aboutme.setText("");
                        }
                        if(!user_details.getString("latitude").equals("null") && !user_details.getString("longitude").equals("null")){
                            lat = Double.parseDouble(user_details.getString("latitude"));
                            long_ = Double.parseDouble(user_details.getString("longitude"));
                        }

                        if(user_details.getString("country_id").equals("161")){
                            dstlay.setVisibility(View.GONE);
                        }else {
                            dstlay.setVisibility(View.VISIBLE);
                        }

                        Picasso.get().load(PROFILE_IMG_URL+user_details.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(upload_imgs);
                        String phot = result_Object.getString("photos");
                        upload_img3.removeAllViews();
                        try{
                            photoArr = new JSONArray(phot);
                            for (int j = 0; j < photoArr.length(); j++) {
                                View child = LayoutInflater.from(MemberNewEditProfileActivity.this).inflate(R.layout.upload_img_gv, null);
                                final ImageView image = (ImageView) child.findViewById(R.id.upload_imgs);
                                final ImageButton dlt_btn = (ImageButton) child.findViewById(R.id.dlt_btn);
                                dlt_btn.setVisibility(View.VISIBLE);
                                Picasso.get().load(PROFILE_IMG_URL+photoArr.getJSONObject(j).getString("profile_image")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(image);
                                final int finalJ = j;
                                dlt_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            removeImage(photoArr.getJSONObject(finalJ).getString("id"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                upload_img3.addView(child);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MemberNewEditProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MemberNewEditProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberNewEditProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MemberNewEditProfileActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void removeImage(String pho_ID){
        MethodClass.showProgressDialog(MemberNewEditProfileActivity.this);
        String server_url = MemberNewEditProfileActivity.this.getString(R.string.SERVER_URL) + "user/photo-delete";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        params.put("photo_id",pho_ID);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MemberNewEditProfileActivity.this, response);
                    if (result_Object != null) {
                        String message = result_Object.getString("message");
                        String phot = result_Object.getString("photos");
                        upload_img3.removeAllViews();
                        try{
                            photoArr = new JSONArray(phot);
                            for (int j = 0; j < photoArr.length(); j++) {
                                View child = LayoutInflater.from(MemberNewEditProfileActivity.this).inflate(R.layout.upload_img_gv, null);
                                final ImageView image = (ImageView) child.findViewById(R.id.upload_imgs);
                                final ImageButton dlt_btn = (ImageButton) child.findViewById(R.id.dlt_btn);
                                dlt_btn.setVisibility(View.VISIBLE);
                                Picasso.get().load(PROFILE_IMG_URL+photoArr.getJSONObject(j).getString("profile_image")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(image);
                                final int finalJ = j;
                                dlt_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            removeImage(photoArr.getJSONObject(finalJ).getString("id"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                upload_img3.addView(child);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MemberNewEditProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MemberNewEditProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberNewEditProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MemberNewEditProfileActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+ PreferenceManager.getDefaultSharedPreferences(MemberNewEditProfileActivity.this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(image);
    }
    public void back(View view){
        super.onBackPressed();
    }
    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }
//for multipicture

    private void CameraIntent(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();

        }catch (Exception e){
            e.printStackTrace();
        }
        outputFileUri = FileProvider.getUriForFile(MemberNewEditProfileActivity.this, "com.dunamis1.dunamis1.fileprovider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,outputFileUri);
        Log.e("Intentdata", intent.toString() );
        startActivityForResult(intent, TAKE_PHOTO_CODE);

    }

    private File createImageFile() throws IOException {
        File image = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = MemberNewEditProfileActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }
    private void CameraIntent2(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();

        }catch (Exception e){
            e.printStackTrace();
        }
        outputFileUri = FileProvider.getUriForFile(MemberNewEditProfileActivity.this, "com.dunamis1.dunamis1.fileprovider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,outputFileUri);
        Log.e("Intentdata", intent.toString() );
        startActivityForResult(intent, TAKE_PHOTO_CODE2);

    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_FROM_GALLERY);
    }
    private void galleryIntent2()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_FROM_GALLERY_OREO);
    }


    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        Log.e("LOGURI", uri.getScheme() );
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = MemberNewEditProfileActivity.this.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private void showImage(){
        MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);
        upload_img.removeAllViews();
        if(ListImage.size()>0){
            for (int i = 0; i < ListImage.size(); i++) {

                View child = LayoutInflater.from(this).inflate(R.layout.upload_img_gv, null);
                final ImageView image = (ImageView) child.findViewById(R.id.upload_imgs);
                final ImageButton dlt_btn = (ImageButton) child.findViewById(R.id.dlt_btn);
                dlt_btn.setVisibility(View.VISIBLE);
                final int finalI1 = i;
                dlt_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ListImage.remove(finalI1);
                        showImage();
                    }
                });
                File imgFile = new  File(ListImage.get(i));
                if(imgFile.exists()){
                    Bitmap b = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    image.setImageBitmap(Bitmap.createScaledBitmap(b, 120, 120, false));
                    //image.setImageBitmap(myBitmap);
                }
                upload_img.addView(child);
            }

        }
    }
    private void showImage2(){
        MethodClass.hideProgressDialog(MemberNewEditProfileActivity.this);
        if(ListImage2.size()>0){
            for (int i = 0; i < ListImage2.size(); i++) {
                File imgFile = new  File(ListImage2.get(i));
                if(imgFile.exists()){
                    Bitmap b = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    upload_imgs.setImageBitmap(Bitmap.createScaledBitmap(b, 120, 120, false));
                    //image.setImageBitmap(myBitmap);
                }
                final int finalI = i;
            }

        }
    }
}
