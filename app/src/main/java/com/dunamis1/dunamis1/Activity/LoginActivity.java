package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MySingleton;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.dunamis1.dunamis1.Helper.Constant.SHARED_PREF;

public class LoginActivity extends AppCompatActivity {

    private EditText email_tv,pass_tv;
    private  String regId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(LoginActivity.this);
        setContentView(R.layout.activity_login);
        MethodClass.hide_keyboard(LoginActivity.this);
        email_tv = findViewById(R.id.email_tv);
        pass_tv = findViewById(R.id.pass_tv);

    }

    public void signUp(View view){
        Intent I =new Intent(this,ChooseSignupActivity.class);
        startActivity(I);
    }
    public void Login(View view){
        if(!MethodClass.emailValidator(email_tv.getText().toString().trim())){
            email_tv.setError(getString(R.string.emailReq));
            email_tv.requestFocus();
            return;
        }
        if(pass_tv.getText().toString().trim().length() == 0){
            pass_tv.setError(getString(R.string.passReq));
            pass_tv.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(LoginActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "login";


        SharedPreferences pref = LoginActivity.this.getSharedPreferences(SHARED_PREF, 0);
        regId = pref.getString("regId", null);

//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                regId = instanceIdResult.getToken();
//                Log.e("TOKEN", regId);
//            }
//        });

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email_tv.getText().toString().trim());
        params.put("password", pass_tv.getText().toString().trim());
        params.put("reg_id", regId);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(LoginActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(LoginActivity.this, response);
                    if (jsonObject != null) {
                        JSONObject jsonObject1 = new JSONObject(jsonObject.getString("userdata"));
                        String id = jsonObject1.getString("id");
                        String fname = jsonObject1.getString("fname");
                        String lname = jsonObject1.getString("lname");
                        String emails = jsonObject1.getString("email");
                        String image = jsonObject1.getString("profile_pic");
                        String user_type = jsonObject1.getString("user_type");
                        String phone_number = jsonObject1.getString("phone_number");
                        String is_phone_verified = jsonObject1.getString("is_phone_verified");
                        String vcode="";
                        if (jsonObject1.has("phone_vcode")){
                            vcode = jsonObject1.getString("phone_vcode");
                        }
                        String token="";
                        if (jsonObject.has("token")){
                            token = jsonObject.getString("token");
                        }
                        String status = jsonObject1.getString("status");
                        if (status.equals("A")) {

                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("token", token).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_name", fname+" "+lname).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_id", id).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("email", emails).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putBoolean("is_logged_in", true).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("profile_pic", image).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("phone", phone_number).commit();
                            if(user_type.equals("M")){
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("role","Member").commit();
                                Intent I =new Intent(LoginActivity.this,DashboardActivity.class);
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }else if(user_type.equals("G")){
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("role","Guest").commit();
                                Intent I =new Intent(LoginActivity.this,GuestDashboardActivity.class);
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }else if(user_type.equals("HCA")){
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("role","Home").commit();
                                Intent I =new Intent(LoginActivity.this,HomeChurchDahboardActivity.class);
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }else if(user_type.equals("CDA")){
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("role","Department").commit();
                                Intent I =new Intent(LoginActivity.this,ChurchDepartmentDashboardActivity.class);
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }else{
                                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Inactive User")
                                        .setContentText("Your account is inactive.Please contact with admin")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }

                        } else if (status.equals("U") || is_phone_verified.equals("N")) {
                            Intent intent = new Intent(LoginActivity.this, VerificationActivity.class);
                            intent.putExtra("phone", phone_number);
                            intent.putExtra("vcode", vcode);
                            intent.putExtra("type", "S");
                            startActivity(intent);
                        } else if (status.equals("I")) {
                            if (!LoginActivity.this.isFinishing()) {
                                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Inactive User")
                                        .setContentText("Your account is inactive.Please contact with admin")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();

                            }
                        }
                        else if (status.equals("D")) {
                            if (!LoginActivity.this.isFinishing()) {
                                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText(getString(R.string.accDelet))
                                        .setContentText(getString(R.string.accDelmSG))
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();

                            }
                        }
                    }


                } catch (JSONException e) {
                    Log.e("ERROR", e.toString());
                    MethodClass.error_alert(LoginActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("ERROR", error.toString());
                MethodClass.hideProgressDialog(LoginActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(LoginActivity.this);
                } else {
                    MethodClass.error_alert(LoginActivity.this);
                }
            }
        });

        MySingleton.getInstance(LoginActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void forgot(View view){
        Intent I =new Intent(this,ForgotPassword.class);
        startActivity(I);
    }
    public void back(View view){
        super.onBackPressed();
    }

}
