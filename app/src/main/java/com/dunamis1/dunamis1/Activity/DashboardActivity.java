package com.dunamis1.dunamis1.Activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.DepartmentListingActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Adapter.SliderAdapterExample;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.Helper.SliderItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chahinem.pageindicator.PageIndicator;
import com.dunamis1.dunamis1.Adapter.DashboardOneAdapter;
import com.dunamis1.dunamis1.Adapter.DashboardSlideAdapter;
import com.dunamis1.dunamis1.Adapter.DashboardThreeAdapter;
import com.dunamis1.dunamis1.Adapter.DashboardTwoAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.DEPARTMENT_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SAVE;
import static com.dunamis1.dunamis1.Helper.Constant.HOME_ID;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SOCIAL;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.USER_LANG;
import static com.dunamis1.dunamis1.Helper.Constant.USER_LAT;
import static com.dunamis1.dunamis1.Helper.Constant.VIEWS;

public class DashboardActivity extends AppCompatActivity implements OnMapReadyCallback {
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;

    RelativeLayout depChurch,depChurch1,depChurch2;

    int[] sampleImages = {R.drawable.dashcarousel, R.drawable.dashcarousel, R.drawable.dashcarousel};
    private String[] title_array = {"Dunamis International Gospel Centre", "Dunamis Gospel", "Dunamisgospel", "MID-DAY WORSHIP 20.02.2020"};

    private String desc_array[] = {"Beloved, who are you blessing? Who is feeding from your table other than you and your family? Make up...", "Welcome to the 2020 Nations Worship In His Presence Live at the Dunamis Glory Dome. Get ready to be....", "We are thankful to god for strength so far, for help available, for the abundunce of testimoies for the..",
            "Subscribe to our youtube channel for more videos: https://www.youtube.com/user/dunamistvng/live..."};
    int[] sampleImagesArr = {R.drawable.taaftr, R.drawable.tdftr, R.drawable.tfftr,R.drawable.tftr};

    int[] sampleImagesArr4 = {R.drawable.imgvideo9,0, R.drawable.march_worship__word_and_wonders_night, R.drawable.drone_view};
    int[] socialArr = {R.drawable.image99, R.drawable.image100, R.drawable.image101,R.drawable.image102};


    private ArrayList<HashMap<Object,Object>> map_list;
    private ArrayList<HashMap<Object,Object>> map_list2;
    private ArrayList<HashMap<Object,Object>> map_list3;
    private ArrayList<HashMap<Object,Object>> map_list4;

    private PageIndicator pageIndicator;

    private RecyclerView rv_Dash1;
    private RecyclerView rv_Dash2;
    private RecyclerView rv_Dash3;
    private RecyclerView rv_Dashslide;
    //private RecyclerView rv_Dash4;

    private CircleImageView imgPro;
    private TextView nav_name_tv;
    private TextView prdct_title_tv;
    private TextView prdct_title_tv2;

    private ImageView prdct_iv;
    private ImageView prdct_iv1;
    private NestedScrollView scrollView;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;

    private TextView newsFeedHead,homeCHead;
    private TextView showMore;
    private LinearLayout eventHead,departHead;
    private DashboardThreeAdapter adapter3;

    private Integer offset  = 0;
    private TextView prdct_location_tv2;
    private TextView prdct_title_tvs;
    private LinearLayout seALL;
    private CircleImageView prdct_ivs;
    private LinearLayout containers;
    private LinearLayout container;
    private SliderView imageSlider;
    private SliderAdapterExample adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(DashboardActivity.this);
        MethodClass.setStatusBarGradiant(DashboardActivity.this);
        setContentView(R.layout.activity_dashboard);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        rv_Dash2 = (RecyclerView) findViewById(R.id.rv_Dash2);
        rv_Dash3 = (RecyclerView) findViewById(R.id.rv_Dash3);
        //rv_Dashslide = (RecyclerView) findViewById(R.id.rv_Dashslide);
        depChurch = (RelativeLayout) findViewById(R.id.depChurch);
        depChurch1 = (RelativeLayout) findViewById(R.id.depChurch1);
        depChurch2 = (RelativeLayout) findViewById(R.id.depChurch2);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        //pageIndicator=(PageIndicator) findViewById(R.id.pageIndicator);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        prdct_title_tv = findViewById(R.id.prdct_title_tv);
        prdct_title_tv2 = findViewById(R.id.prdct_title_tv2);
        scrollView = findViewById(R.id.scrollView);
        departHead = findViewById(R.id.departHead);
        eventHead = findViewById(R.id.eventHead);
        homeCHead = findViewById(R.id.homeCHead);
        newsFeedHead = findViewById(R.id.newsFeedHead);
        prdct_iv1 = findViewById(R.id.prdct_iv1);
        prdct_iv = findViewById(R.id.prdct_iv);
        showMore = findViewById(R.id.showMore);
        imageSlider = findViewById(R.id.imageSlider);

        prdct_ivs = findViewById(R.id.prdct_ivs);
        prdct_title_tvs = findViewById(R.id.prdct_title_tvs);
        prdct_location_tv2 = findViewById(R.id.prdct_location_tv2);
        containers = findViewById(R.id.containers);
        container = findViewById(R.id.container);
        seALL = findViewById(R.id.seALL);

        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }


        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(DashboardActivity.this);
        getDash1();
    }
    @Override
    protected void onResume() {
        super.onResume();
        getDash1();
        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void getDash1(){
        MethodClass.showProgressDialog(DashboardActivity.this);
        String server_url = DashboardActivity.this.getString(R.string.SERVER_URL) + "member/dashboard";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language",PreferenceManager.getDefaultSharedPreferences(this).getString("lang","en"));
        params.put("latitude",String.valueOf(USER_LAT));
        params.put("longitude",String.valueOf(USER_LANG));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try{
                    MethodClass.hideProgressDialog(DashboardActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(DashboardActivity.this, response);
                    if (result_Object != null) {
                        String showmore = result_Object.getString("showmore");
                        if(showmore.equals("Y")){
                            showMore.setVisibility(View.VISIBLE);
                        }else {
                            showMore.setVisibility(View.GONE);
                        }
                        String my_home_church = result_Object.getString("my_home_church");
                        if(!my_home_church.equals("")){
                            JSONObject myHome = new JSONObject(my_home_church);
                            String home_church_join_status = result_Object.getString("home_church_join_status");
                            if(!home_church_join_status.equals("R")){
                                map_list3 = new ArrayList<>();
                                showMore.setVisibility(View.GONE);
                                for (int k = 0; k <1 ; k++) {
                                    String id = myHome.getString("id");
                                    String google_address = myHome.getString("google_address");
                                    String tot_member = myHome.getString("tot_member");
                                    String banner_img = myHome.getString("banner_img");
                                    String home_church_name = myHome.getJSONObject("details").getString("home_church_name");
                                    String host_name = myHome.getJSONObject("details").getString("host_name");
                                    HashMap<Object,Object> map3 = new HashMap<>();
                                    map3.put(TITLE,home_church_name);
                                    map3.put(NAME,host_name);
                                    map3.put(MEMBER,tot_member);
                                    map3.put(DESC,google_address);
                                    if(home_church_join_status.equals("I")){
                                        map3.put(VIEWS,"Waiting");
                                    }else{
                                        map3.put(VIEWS,"Details");
                                    }

                                    map3.put(IMAGE,banner_img);
                                    map3.put(HOME_ID,id);

                                    map_list3.add(map3);
                                }
                                adapter3= new DashboardThreeAdapter(DashboardActivity.this,map_list3);
                                rv_Dash3.setAdapter(adapter3);
                                rv_Dash3.setFocusable(false);
                            }else{
                                JSONArray home_church_list = result_Object.getJSONArray("home_church_list");
                                if(home_church_list.length()>0){
                                    map_list3 = new ArrayList<>();

                                    for (int k = 0; k <home_church_list.length() ; k++) {
                                        String id = home_church_list.getJSONObject(k).getString("id");
                                        String google_address = home_church_list.getJSONObject(k).getString("google_address");
                                        String tot_member = home_church_list.getJSONObject(k).getString("tot_member");
                                        String banner_img = home_church_list.getJSONObject(k).getString("banner_img");
                                        String home_church_name = home_church_list.getJSONObject(k).getJSONObject("details").getString("home_church_name");
                                        String host_name = home_church_list.getJSONObject(k).getJSONObject("details").getString("host_name");
                                        HashMap<Object,Object> map3 = new HashMap<>();
                                        map3.put(TITLE,home_church_name);
                                        map3.put(NAME,host_name);
                                        map3.put(MEMBER,tot_member);
                                        map3.put(DESC,google_address);
                                        if(!tot_member.equals("null") && !tot_member.equals(null)){
                                            if(Integer.parseInt(tot_member)>25){
                                                map3.put(VIEWS,"joinss");
                                            }else{
                                                map3.put(VIEWS,"join");
                                            }
                                        }else{
                                            map3.put(VIEWS,"join");
                                        }
                                        map3.put(IMAGE,banner_img);
                                        map3.put(HOME_ID,id);

                                        map_list3.add(map3);
                                    }
                                    adapter3= new DashboardThreeAdapter(DashboardActivity.this,map_list3);
                                    rv_Dash3.setAdapter(adapter3);
                                    rv_Dash3.setFocusable(false);
                                }else{
                                    rv_Dash3.setVisibility(View.GONE);
                                }
                            }
                        }else{
                            JSONArray home_church_list = result_Object.getJSONArray("home_church_list");
                            if(home_church_list.length()>0){
                                map_list3 = new ArrayList<>();

                                for (int k = 0; k <home_church_list.length() ; k++) {
                                    String id = home_church_list.getJSONObject(k).getString("id");
                                    String google_address = home_church_list.getJSONObject(k).getString("google_address");
                                    String tot_member = home_church_list.getJSONObject(k).getString("tot_member");
                                    String banner_img = home_church_list.getJSONObject(k).getString("banner_img");
                                    String home_church_name = home_church_list.getJSONObject(k).getJSONObject("details").getString("home_church_name");
                                    String host_name = home_church_list.getJSONObject(k).getJSONObject("details").getString("host_name");
                                    HashMap<Object,Object> map3 = new HashMap<>();
                                    map3.put(TITLE,home_church_name);
                                    map3.put(NAME,host_name);
                                    map3.put(MEMBER,tot_member);
                                    map3.put(DESC,google_address);
                                    if(!tot_member.equals("null") && !tot_member.equals(null)){
                                        if(Integer.parseInt(tot_member)>25){
                                            map3.put(VIEWS,"joinss");
                                        }else{
                                            map3.put(VIEWS,"join");
                                        }
                                    }else{
                                        map3.put(VIEWS,"join");
                                    }

                                    map3.put(IMAGE,banner_img);
                                    map3.put(HOME_ID,id);

                                    map_list3.add(map3);
                                }
                                adapter3= new DashboardThreeAdapter(DashboardActivity.this,map_list3);
                                rv_Dash3.setAdapter(adapter3);
                                rv_Dash3.setFocusable(false);
                                homeCHead.setVisibility(View.VISIBLE);
                            }else{
                                rv_Dash3.setVisibility(View.GONE);
                                homeCHead.setVisibility(View.GONE);
                            }
                        }




                        //for newsfeed
                        map_list = new ArrayList<>();
                        for (int i = 0; i <title_array.length ; i++) {
                            HashMap<Object,Object> map = new HashMap<>();
                            map.put(TITLE,title_array[i]);
                            map.put(DESC,desc_array[i]);
                            map.put(IMAGE,sampleImagesArr[i]);
                            map.put(SOCIAL,socialArr[i]);
                            map_list.add(map);
                        }
                        DashboardOneAdapter bibleDeuteronomyAdapter= new DashboardOneAdapter(DashboardActivity.this,map_list);
                        rv_Dash1.setAdapter(bibleDeuteronomyAdapter);
                        rv_Dash1.setFocusable(false);
                        //newsfeed ends


                        //for events

                        JSONArray event = result_Object.getJSONArray("event");
                        Log.e("resp", event.toString());
                        if(event.length()>0){
                            map_list2 = new ArrayList<>();

                            for (int j = 0; j <event.length() ; j++) {

                                String thumbnail = event.getJSONObject(j).getString("thumbnail");
                                String from_date = event.getJSONObject(j).getString("from_date");
                                String id = event.getJSONObject(j).getString("id");
                                String title = event.getJSONObject(j).getJSONObject("eventdetails").getString("title");
                                HashMap<Object,Object> map2 = new HashMap<>();
                                map2.put(TITLE,title);
                                map2.put(DESC,from_date);
                                map2.put(IMAGE,thumbnail);
                                map2.put(EVENT_ID,id);
                                map2.put(EVENT_SAVE,"");

                                map_list2.add(map2);
                            }
                            DashboardTwoAdapter adapter2= new DashboardTwoAdapter(DashboardActivity.this,map_list2);
                            rv_Dash2.setAdapter(adapter2);
                            rv_Dash2.setFocusable(false);
                        }else{
                            rv_Dash2.setVisibility(View.GONE);
                            eventHead.setVisibility(View.GONE);
                        }

                        //events end


                        //department starts
                        final JSONArray department = result_Object.getJSONArray("department");
                        Log.e("resp", department.toString());

                        if(department.length()>0){
                            seALL.setVisibility(View.GONE);
                            container.setVisibility(View.VISIBLE);
                            for (int i = 0; i <department.length() ; i++) {
                                if(i == 0){
                                    depChurch.setVisibility(View.VISIBLE);
                                    if(!department.getJSONObject(0).getString("churchdepartment").equals(null) && !department.getJSONObject(0).getString("churchdepartment").equals("null")){
                                        prdct_title_tv.setText(department.getJSONObject(0).getJSONObject("churchdepartment").getJSONObject("department").getString("department_name"));
                                        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+department.getJSONObject(0).getJSONObject("churchdepartment").getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).into(prdct_iv);
                                    }else{
                                        prdct_title_tv.setText("N/A");
                                    }
                                    final String depId = department.getJSONObject(0).getJSONObject("churchdepartment").getString("id");
                                    depChurch.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent I = new Intent(DashboardActivity.this, ChurchDetailActivity.class);
                                            I.putExtra("id",depId);
                                            startActivity(I);
                                        }
                                    });
                                }
                                if(department.length()>1){
                                    if(i == 1){
                                        depChurch1.setVisibility(View.VISIBLE);
                                        depChurch2.setVisibility(View.GONE);
                                        if(!department.getJSONObject(1).getString("churchdepartment").equals(null) && !department.getJSONObject(1).getString("churchdepartment").equals("null")){
                                            prdct_title_tv2.setText(department.getJSONObject(1).getJSONObject("churchdepartment").getJSONObject("department").getString("department_name"));
                                            Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+department.getJSONObject(1).getJSONObject("churchdepartment").getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).into(prdct_iv1);
                                        }else{
                                            prdct_title_tv2.setText("N/A");
                                        }

                                        final String depId = department.getJSONObject(1).getJSONObject("churchdepartment").getString("id");
                                        depChurch1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent I = new Intent(DashboardActivity.this, ChurchDetailActivity.class);
                                                I.putExtra("id",depId);
                                                startActivity(I);
                                            }
                                        });
                                    }
                                }else{
                                    depChurch1.setVisibility(View.GONE);
                                    depChurch2.setVisibility(View.VISIBLE);
                                }

                            }

                        }else {
                            container.setVisibility(View.GONE);
                            departHead.setVisibility(View.VISIBLE);
                            seALL.setVisibility(View.VISIBLE);
                        }
                        //department end

                        //for chhurch branch
                        final JSONObject chrchBranch = result_Object.getJSONObject("branch_details");
                        Picasso.get().load(BRANCH_IMG_URL+chrchBranch.getString("banner_img")).resize(1024,1024).placeholder(R.drawable.image60).error(R.drawable.image60).into(prdct_ivs);
                        prdct_title_tvs.setText(chrchBranch.getJSONObject("churchdetails").getString("name"));
                        prdct_location_tv2.setText(chrchBranch.getString("address"));
                        containers.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent I= new Intent(DashboardActivity.this,MainChurchDetailsActivity.class);
                                try {
                                    I.putExtra("id",chrchBranch.getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                startActivity(I);
                            }
                        });
                        //for church branch

                        //dunamis1 near me
                        JSONArray dunamis_church_list =  result_Object.getJSONArray("dunamis_church_list");

                        if(dunamis_church_list.length()>0){
                            for (int S = 0; S <dunamis_church_list.length() ; S++) {
                                String latitude = dunamis_church_list.getJSONObject(S).getString("latitude");
                                String longitude = dunamis_church_list.getJSONObject(S).getString("longitude");
                                String name = dunamis_church_list.getJSONObject(S).getJSONObject("churchdetails").getString("name");
                                createMArker(Double.parseDouble(latitude),Double.parseDouble(longitude),name);
                            }
                        }

                        //

                        List<SliderItem> sliderItemList = new ArrayList<>();
                        //dummy data
                        for (int i = 0; i < 4; i++) {
                            SliderItem sliderItem = new SliderItem();
                            if(i == 0){
                                sliderItem.setImageUrl(R.drawable.imgvideo9);
                            }else if(i == 1){
                                sliderItem.setDescription(getString(R.string.dunamis_meaning));
                            }else if(i == 2){
                                sliderItem.setImageUrl(R.drawable.march_worship__word_and_wonders_night);
                            }else if(i == 3){
                                sliderItem.setImageUrl(R.drawable.drone_view);
                            }
                            sliderItemList.add(sliderItem);
                        }
                        adapter = new SliderAdapterExample(DashboardActivity.this,sliderItemList);
                        imageSlider.setSliderAdapter(adapter);
                        imageSlider.setIndicatorAnimation(IndicatorAnimations.COLOR); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
                        imageSlider.setIndicatorSelectedColor(Color.WHITE);
                        imageSlider.setIndicatorUnselectedColor(Color.GRAY);
                        imageSlider.setScrollTimeInSec(3);
                        imageSlider.setAutoCycle(true);
                        imageSlider.startAutoCycle();
                        //end dunamis1 near me
//
//                        map_list4 = new ArrayList<>();
//                        for (int s = 0; s <sampleImagesArr4.length ; s++) {
//                            HashMap<Object,Object> map = new HashMap<>();
//                            map.put(IMAGE,sampleImagesArr4[s]);
//                            map_list4.add(map);
//                        }
//
//                        final DashboardSlideAdapter slideAdapter= new DashboardSlideAdapter(DashboardActivity.this,map_list4);
//                        rv_Dashslide.setAdapter(slideAdapter);
//                        pageIndicator.attachTo(rv_Dashslide);
//                        rv_Dashslide.setFocusable(false);
//                        final int speedScroll = 5000;
//                        final Handler handler = new Handler();
//                        final Runnable runnable = new Runnable() {
//                            int count = 0;
//                            boolean flag = true;
//                            @Override
//                            public void run() {
//
//                                if(count < slideAdapter.getItemCount()){
//                                    Log.e("COUNT", String.valueOf(count));
//                                    if(count== slideAdapter.getItemCount()-1){
//                                        flag = false;
//                                    }else if(count == 0){
//                                        flag = true;
//                                    }
//                                    if(flag) count++;
//                                    else count = 0;
//                                    rv_Dashslide.smoothScrollToPosition(count);
//                                    handler.postDelayed(this,speedScroll);
//                                }
//                            }
//                        };
//                        handler.postDelayed(runnable,speedScroll);
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(DashboardActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(DashboardActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(DashboardActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DashboardActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(DashboardActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(DashboardActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void renewItems(View view) {
        List<SliderItem> sliderItemList = new ArrayList<>();
        //dummy data
        for (int i = 0; i < 4; i++) {
            SliderItem sliderItem = new SliderItem();
            if(i == 0){
                sliderItem.setImageUrl(R.drawable.imgvideo9);
            }else if(i == 1){
                sliderItem.setDescription(getString(R.string.dunamis_meaning));
            }else if(i == 2){
                sliderItem.setImageUrl(R.drawable.march_worship__word_and_wonders_night);
            }else if(i == 3){
                sliderItem.setImageUrl(R.drawable.drone_view);
            }
            sliderItemList.add(sliderItem);
        }
        adapter.renewItems(sliderItemList);
    }

    public void removeLastItem(View view) {
        if (adapter.getCount() - 1 >= 0)
            adapter.deleteItem(adapter.getCount() - 1);
    }
    public void showMores(View view){
        offset = map_list3.size();
        MethodClass.showProgressDialog(DashboardActivity.this);
        String server_url = DashboardActivity.this.getString(R.string.SERVER_URL) + "homechurch/showmore";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language",PreferenceManager.getDefaultSharedPreferences(this).getString("lang","en"));
        params.put("offset", String.valueOf(offset));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try{
                    MethodClass.hideProgressDialog(DashboardActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(DashboardActivity.this, response);
                    if (result_Object != null) {
                        String showmore = result_Object.getString("showmore");
                        if(showmore.equals("Y")){
                            showMore.setVisibility(View.VISIBLE);
                        }else {
                            showMore.setVisibility(View.GONE);
                        }
                        JSONArray home_church_list = result_Object.getJSONArray("home_church_list");
                        if(home_church_list.length()>0){
                            for (int k = 0; k <home_church_list.length() ; k++) {
                                String id = home_church_list.getJSONObject(k).getString("id");
                                String google_address = home_church_list.getJSONObject(k).getString("google_address");
                                String tot_member = home_church_list.getJSONObject(k).getString("tot_member");
                                String banner_img = home_church_list.getJSONObject(k).getString("banner_img");
                                String home_church_name = home_church_list.getJSONObject(k).getJSONObject("details").getString("home_church_name");
                                String host_name = home_church_list.getJSONObject(k).getJSONObject("details").getString("host_name");
                                HashMap<Object,Object> map3 = new HashMap<>();
                                map3.put(TITLE,home_church_name);
                                map3.put(NAME,host_name);
                                map3.put(MEMBER,tot_member);
                                map3.put(DESC,google_address);
                                if(!tot_member.equals("null") && !tot_member.equals(null)){
                                    if(Integer.parseInt(tot_member)>25){
                                        map3.put(VIEWS,"joinss");
                                    }else{
                                        map3.put(VIEWS,"join");
                                    }
                                }else{
                                    map3.put(VIEWS,"join");
                                }

                                map3.put(IMAGE,banner_img);
                                map3.put(HOME_ID,id);

                                map_list3.add(map3);
                            }
                            adapter3.notifyDataSetChanged();
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    MethodClass.hideProgressDialog(DashboardActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(DashboardActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(DashboardActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DashboardActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(DashboardActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(DashboardActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(final int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog =new Dialog(DashboardActivity.this);
                    dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.setContentView(R.layout.photo_popup_view2);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    ImageView close_iv =(ImageView)dialog.findViewById(R.id.close_iv);
                    final ImageView photo_iv =(ImageView)dialog.findViewById(R.id.photo_iv);
                    final RelativeLayout photoLayout =(RelativeLayout)dialog.findViewById(R.id.photoLayout);
                    final YouTubePlayerView youtube_player_view =(YouTubePlayerView)dialog.findViewById(R.id.youtube_player_view);

                    if(position == 0){
                        photoLayout.setVisibility(View.GONE);
                        youtube_player_view.setVisibility(View.VISIBLE);
                        youtube_player_view.initialize(new YouTubePlayerInitListener() {
                            @Override
                            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                    @Override
                                    public void onReady() {
                                        String videoId = "u_GBKzxfzjQ";
                                        initializedYouTubePlayer.loadVideo(videoId, 0);
                                    }
                                });
                            }
                        }, true);
                    }else {
                        photo_iv.setImageResource(sampleImages[position]);
                        photoLayout.setVisibility(View.VISIBLE);
                        youtube_player_view.setVisibility(View.GONE);
                    }


                    close_iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            youtube_player_view.release();
                        }
                    });
                    dialog.show();
                }
            });
        }
    };

    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this,TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this,SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this,ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this,DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this,DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this,DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this,NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskIntroActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }

    public void  kingsKids(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://ubdavid.org/youth-world/youth.html"));
        startActivity(intent);
    }
    public void  Dashstore(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    } public void  upcoming(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }public void  churchDeps(View view){
        Intent I = new Intent(DashboardActivity.this, DepartmentListingActivity.class);
        startActivity(I);
    }public void  callCen(View view){
        Intent I = new Intent(DashboardActivity.this, CallCenterActivity.class);
        startActivity(I);
    }public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
    public void createMArker(double latitude, double longitude, String title) {

        if(mMap !=null){
            Log.e("Mark", title );
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .anchor(0.5f, 0.5f)
                    .title(title));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude),7));
        }
    }
}
