package com.dunamis1.dunamis1.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageChatActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Adapter.ChurchBranchPhotoAdapter;
import com.dunamis1.dunamis1.Adapter.ChurchDepOneAdapter;
import com.dunamis1.dunamis1.Adapter.ChurchDepTwoAdapter;
import com.dunamis1.dunamis1.Adapter.ChurchNearAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Adapter.ChurchOneAdapter;
import com.dunamis1.dunamis1.Adapter.ChurchTwoAdapter;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.ADDRESS;
import static com.dunamis1.dunamis1.Helper.Constant.BRANCH_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.MEMBER;
import static com.dunamis1.dunamis1.Helper.Constant.NAME;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.TITLE;

public class MainChurchDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private String[] title_array = {"DIGC ABA CENTRAL", "DIGC UMUAHIA CENTRAL", "DIGC NUMAN", "DIGC YOLA CENTRAL", "DIGC KADUNA CENTRAL"};
    private String[] loc_array = {"158 ABA OWERRI ROAD ABA| 10 Miles", "2/4 FINBARRS ROAD, BY UMUOBASI, UMUAHIA | 12 Miles", "DUNAMIS ARENA 60 DOWAYA, YOLA ROAD, NUMAN | 10 Miles", "BACHURE ROAD, JIMETA, YOLA | 16 Miles", "DUNAMIS ARENA, BEHIND CICMA HOUSE  | 6 Miles"};
    int[] sampleImagesArr = {R.drawable.image60, R.drawable.dashchurchnear3, R.drawable.image60,R.drawable.dashcarousel,R.drawable.church4};
    int[] sampleImagesArr2 = {R.drawable.image60, R.drawable.dashchurchnear3, R.drawable.tfftr,R.drawable.image60};

    private ArrayList<HashMap<String,Object>> map_list;
    private ArrayList<HashMap<Object,Object>> map_list2;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private RecyclerView rv_Dash1;
    private RecyclerView rv_Dash2;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    private ImageView dep1;
    private TextView branchTitle;
    private TextView branchLoc;
    private LinearLayout fbLayout,twitterLayout,instaLayout,photoLayout;
    private TextView eventTv,photoTv;
    private TextView brnchAbt;
    private TextView headTitle;
    private TextView branchPastor;
    private NestedScrollView scrollView;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_church_details);
        rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        rv_Dash2 = (RecyclerView) findViewById(R.id.rv_Dash2);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MainChurchDetailsActivity.this);
        dep1 = findViewById(R.id.dep1);
        branchTitle = findViewById(R.id.branchTitle);
        branchLoc = findViewById(R.id.branchLoc);
        fbLayout = findViewById(R.id.fbLayout);
        twitterLayout = findViewById(R.id.twitterLayout);
        instaLayout = findViewById(R.id.instaLayout);
        photoLayout = findViewById(R.id.photoLayout);
        eventTv = findViewById(R.id.eventTv);
        photoTv = findViewById(R.id.photoTv);
        brnchAbt = findViewById(R.id.brnchAbt);
        branchPastor = findViewById(R.id.branchPastor);
        scrollView = findViewById(R.id.scrollView);
        headTitle = findViewById(R.id.headTitle);




        getDep();
    }
    @Override
    protected void onResume() {
        super.onResume();

        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void getDep(){

        MethodClass.showProgressDialog(MainChurchDetailsActivity.this);
        String server_url = MainChurchDetailsActivity.this.getString(R.string.SERVER_URL) + "branch/details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(MainChurchDetailsActivity.this).getString("LANG","en"));
        params.put("id",getIntent().getStringExtra("id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(MainChurchDetailsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(MainChurchDetailsActivity.this, response);
                    if (result_Object != null) {
                        final JSONObject branch_details = result_Object.getJSONObject("branch_details");
                        branchLoc.setText(branch_details.getString("address"));

                        branchTitle.setText(branch_details.getJSONObject("churchdetails").getString("name"));
                        headTitle.setText(branch_details.getJSONObject("churchdetails").getString("name"));
                        brnchAbt.setText(branch_details.getJSONObject("churchdetails").getString("about_branch"));
                        branchPastor.setText(branch_details.getJSONObject("churchdetails").getString("pastor_name"));
                        Picasso.get().load(BRANCH_IMG_URL+branch_details.getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(dep1);

                        photoTv.setText("Photo: "+branch_details.getString("total_photos"));
                        eventTv.setText("Events: "+result_Object.getString("total_event"));
                        createMArker(Double.parseDouble(branch_details.getString("latitude")),Double.parseDouble(branch_details.getString("longitude")),branch_details.getJSONObject("churchdetails").getString("name"));
                        fbLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(branch_details.getString("fb_link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });

                        instaLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(branch_details.getString("ins_link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        twitterLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = null; // missing 'http://' will cause crashed
                                try {
                                    uri = Uri.parse(branch_details.getString("twi_link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });
                        map_list = new ArrayList<>();
                        map_list2 = new ArrayList<>();

                        JSONArray dunamis_church_list = result_Object.getJSONArray("similar");

                        map_list = new ArrayList<>();
                        for (int i = 0; i < dunamis_church_list.length(); i++) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put(TITLE, dunamis_church_list.getJSONObject(i).getJSONObject("churchdetails").getString("name"));
                            map.put(IMGG, dunamis_church_list.getJSONObject(i).getString("banner_img"));
                            map.put(NAME, dunamis_church_list.getJSONObject(i).getString("address"));
                            map.put(MEMBER, dunamis_church_list.getJSONObject(i).getString("total_members"));
                            map.put(SEED_ID, dunamis_church_list.getJSONObject(i).getString("id"));
                            map_list.add(map);
                        }
                        ChurchNearAdapter adapter = new ChurchNearAdapter(MainChurchDetailsActivity.this, map_list);
                        rv_Dash1.setAdapter(adapter);
                        rv_Dash1.setFocusable(false);


                        JSONArray photos = result_Object.getJSONArray("branch_photos");
                        if(photos.length()>0){
                            for (int l = 0; l <photos.length() ; l++) {
                                HashMap<Object,Object> map4 = new HashMap<>();
                                map4.put(IMAGE,photos.getJSONObject(l).getString("image"));
                                map_list2.add(map4);
                            }
                            ChurchBranchPhotoAdapter adapter4= new ChurchBranchPhotoAdapter(MainChurchDetailsActivity.this,map_list2);
                            rv_Dash2.setAdapter(adapter4);
                            photoLayout.setVisibility(View.VISIBLE);
                        }else {
                            photoLayout.setVisibility(View.GONE);
                        }

                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(MainChurchDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(MainChurchDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(MainChurchDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainChurchDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MainChurchDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(MainChurchDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void back(View view){
        super.onBackPressed();
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }public void  mesgs(View view){
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.msg_popup);
        dialog.setCancelable(false);
        Button yes_btn = (Button) dialog.findViewById(R.id.yes_btn);
        Button no_btn = (Button) dialog.findViewById(R.id.no_btn);
        yes_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(MainChurchDetailsActivity.this, MessageChatActivity.class));
            }
        });
        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
    public void createMArker(double latitude, double longitude, String title) {

        if(mMap !=null){
            Log.e("Mark", title );
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .anchor(0.5f, 0.5f)
                    .title(title));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude),7));
        }
    }
}
