package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.hbb20.CountryCodePicker;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.vanillaplacepicker.presentation.builder.VanillaPlacePicker;
import com.vanillaplacepicker.utils.KeyUtils;
import com.vanillaplacepicker.utils.MapType;
import com.vanillaplacepicker.utils.PickerLanguage;
import com.vanillaplacepicker.utils.PickerType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.SHARED_PREF;

public class MemberSignupStep1Activity extends AppCompatActivity {
    Spinner title_spinner;
    Spinner age_spinner;
    Spinner gender_spinner;
    Spinner mar_spinner;
    Spinner country_spinner;
    Spinner code_spinner;
    Spinner city_spinner;
    Spinner dis_spinner;
    RelativeLayout ctlay,dstlay;
    private EditText fname,lname,phone,emailaddress,confirmEmail,address,pass,confPass;
    private double lat,long_;
    private int REQUEST_PLACE_PICKER = 9;
    private String[] codeArr = {"+376","+971","+93","+1268","+1264","+355","+374","+599","+244","+672","+54","+1684","+43","+61","+297","+994","+387","+1246","+880","+32","+226","+359","+973","+257","+229","+590","+1441","+673","+591","+55","+1242","+975","+267","+375","+501","+1","+61","+243","+236","+242","+41","+225","+682","+56","+237","+86","+57","+506","+53","+238","+61","+357","+420","+49","+253","+45","+1767","+1809","+213","+593","+372","+20","+291","+34","+251","+358","+679","+500","+691","+298","+33","+241","+44","+1473","+995","+233","+350","+299","+220","+224","+240","+30","+502","+1671","+245","+592","+852","+504","+385","+509","+36","+62","+353","+972","+44","+91","+964","+98","+354","+39","+1876","+962","+81","+254","+996","+855","+686","+269","+1869","+850","+82","+965","+1345","+7","+856","+961","+1758","+423","+94","+231","+266","+370","+352","+371","+218","+212","+377","+373","+382","+1599","+261","+692","+389","+223","+95","+976","+853","+1670","+222","+1664","+356","+230","+960","+265","+52","+60","+258","+264","+687","+227","+234","+505","+31","+47","+977","+674","+683","+64","+968","+507","+51","+689","+675","+63","+92","+48","+508","+870","+1","+351","+680","+595","+974","+40","+381","+7","+250","+966","+677","+248","+249","+46","+65","+290","+386","+421","+232","+378","+221","+252","+597","+239","+503","+963","+268","+1649","+235","+228","+66","+992","+690","+670","+993","+216","+676","+90","+1868","+688","+886","+255","+380","+256","+1","+598","+998","+39","+1784","+58","+1284","+1340","+84","+678","+681","+685","+381","+967","+262","+27","+260","+263"};
    private String country_id = "";
    private String cityId = "";
    private String distId = "";
    private String title = "";
    private String age = "";
    private String gender = "";
    private String martial = "";
    private String code = "";
    private LinearLayout addLayout;
    CountryCodePicker ccp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(MemberSignupStep1Activity.this);
        setContentView(R.layout.activity_member_signup_step1);
        MethodClass.hide_keyboard(MemberSignupStep1Activity.this);
        title_spinner  = findViewById(R.id.title_spinner);
        age_spinner  = findViewById(R.id.age_spinner);
        gender_spinner  = findViewById(R.id.gender_spinner);
        mar_spinner  = findViewById(R.id.mar_spinner);
        country_spinner  = findViewById(R.id.country_spinner);
        city_spinner  = findViewById(R.id.city_spinner);
        dis_spinner  = findViewById(R.id.dis_spinner);
        code_spinner  = findViewById(R.id.code_spinner);
        ctlay  = findViewById(R.id.ctlay);
        dstlay  = findViewById(R.id.dstlay);

        fname  = findViewById(R.id.fname);
        lname  = findViewById(R.id.lname);
        phone  = findViewById(R.id.phone);
        emailaddress  = findViewById(R.id.emailaddress);
        confirmEmail  = findViewById(R.id.confirmEmail);
        pass  = findViewById(R.id.pass);
        confPass  = findViewById(R.id.confPass);
        address  = findViewById(R.id.address);
        addLayout  = findViewById(R.id.addLayout);

        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.registerCarrierNumberEditText(phone);
        ccp.setNumberAutoFormattingEnabled(false);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                //Toast.makeText(MemberSignupStep1Activity.this, "Updated " + ccp.getSelectedCountryCode(), Toast.LENGTH_SHORT).show();
                code = ccp.getSelectedCountryCodeWithPlus();
            }
        });
        ccp.setCountryForPhoneCode(294);
        getCouontry();

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.title, R.layout.spinner_item);
        ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this, R.array.age, R.layout.spinner_item);

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent intent = new VanillaPlacePicker.Builder(MemberSignupStep1Activity.this)
                            .withLocation(lat, long_)
                            .setPickerLanguage(PickerLanguage.ENGLISH) // Apply language to picker
                            .setTintColor(getResources().getColor(R.color.colorPrimary)) // Apply Tint color to Back, Clear button of AutoComplete UI
                            /*
                             * Configuration for AutoComplete UI
                             */
                            .setLanguage(PreferenceManager.getDefaultSharedPreferences(MemberSignupStep1Activity.this).getString("LANG","en"))
                            .isOpenNow(true) // Returns only those places that are open for business at the time the query is sent.

                            /*
                             * Configuration for Map UI
                             */
                            .with(PickerType.AUTO_COMPLETE) // Select Picker type to enable autocompelte, map or both
                            .setMapType(MapType.SATELLITE) // Choose map type (Only applicable for map screen)
                            // containing the JSON style declaration for night-mode styling
                            .setMapPinDrawable(android.R.drawable.ic_menu_mylocation) // To give custom pin image for map marker
                            .build();
                    startActivityForResult(intent, REQUEST_PLACE_PICKER);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        title_spinner.setAdapter(adapter);
        age_spinner.setAdapter(adapter2);


        title_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(
                    AdapterView<?> adapterView, View view,
                    int i, long l) {
                title = title_spinner.getItemAtPosition(i).toString();
            }

            public void onNothingSelected(
                    AdapterView<?> adapterView) {

            }
        });
        age_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(
                    AdapterView<?> adapterView, View view,
                    int i, long l) {
                age = age_spinner.getItemAtPosition(i).toString();
            }

            public void onNothingSelected(
                    AdapterView<?> adapterView) {

            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == REQUEST_PLACE_PICKER && data != null) {
                String vanillaAddress = String.valueOf(data.getSerializableExtra(KeyUtils.SELECTED_PLACE));
                //String vanillaAddress2 = String.valueOf(data.getSerializableExtra(KeyUtils.LOCATION));
                Log.e("vanillaAddress", vanillaAddress);

                String toSplit = "a+b-c*d/e=f";
                String[] splitted = vanillaAddress.split("[,()=]");
                ArrayList<String> address_list = new ArrayList<>();
                for (String split : splitted) {
                    System.out.println(split);
                    Log.e("split", split);
                    address_list.add(split);

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("latitude")){
                        try {
                            lat= Double.parseDouble(address_list.get(i+1));
                            Log.e("lat", String.valueOf(lat));

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }

                }

                for (int i = 0; i <address_list.size() ; i++) {
                    if (address_list.get(i).trim().equals("longitude")){
                        try {
                            long_= Double.parseDouble(address_list.get(i+1));
                            Log.e("long_", String.valueOf(long_));


                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;

                    }
                }
                String get_add = address_list.get(2) + ", " + address_list.get(3) + ", " + address_list.get(4);



                address.setError(null);
                Log.e("vanillaAddress", vanillaAddress);

                address.setText(get_add);
                pass.requestFocus();
            }

        }
    }

    public void getCouontry(){
        if (MethodClass.isNetworkConnected(MemberSignupStep1Activity.this)) {
            MethodClass.showProgressDialog(MemberSignupStep1Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "country";
            HashMap<String, String> params = new HashMap<String, String>();
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(MemberSignupStep1Activity.this, response);
                        if (result_Object != null) {

                            JSONArray countryArray = result_Object.getJSONArray("country");

                            List<StringWithTag> spinnerArraysCode = new ArrayList<StringWithTag>();
                            for (int j = 0; j <codeArr.length ; j++) {
                                spinnerArraysCode.add(new StringWithTag(codeArr[j], "M"));

                            }
                            ArrayAdapter adapterCode = new ArrayAdapter(MemberSignupStep1Activity.this, R.layout.spinner_item, spinnerArraysCode) {
                            };
                            code_spinner.setAdapter(adapterCode);
                            code_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    code = s.string;
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                            List<StringWithTag> spinnerArraysGen = new ArrayList<StringWithTag>();
                            spinnerArraysGen.add(new StringWithTag(getString(R.string.selgender),""));
                            for (int j = 0; j <3 ; j++) {
                                if(j ==0){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.Male), "M"));
                                }
                                if(j ==1){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.Female), "F"));
                                }
                                if(j ==2){
                                    spinnerArraysGen.add(new StringWithTag(getString(R.string.Other), "O"));
                                }

                            }
                            ArrayAdapter adapterGen = new ArrayAdapter(MemberSignupStep1Activity.this, R.layout.spinner_item, spinnerArraysGen) {
                            };
                            gender_spinner.setAdapter(adapterGen);
                            gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    gender = String.valueOf(s.tag);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                            List<StringWithTag> spinnerArraysMart = new ArrayList<StringWithTag>();
                            spinnerArraysMart.add(new StringWithTag(getString(R.string.selMartial),""));
                            for (int K = 0; K <2 ; K++) {

                                if(K ==0){
                                    spinnerArraysMart.add(new StringWithTag(getString(R.string.Married), "M"));
                                }
                                if(K ==1){
                                    spinnerArraysMart.add(new StringWithTag(getString(R.string.Single), "S"));
                                }

                            }
                            ArrayAdapter adapter3 = new ArrayAdapter(MemberSignupStep1Activity.this, R.layout.spinner_item, spinnerArraysMart) {
                            };
                            mar_spinner.setAdapter(adapter3);
                            mar_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    martial = String.valueOf(s.tag);

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                            List<StringWithTag> spinnerArraysCoun = new ArrayList<StringWithTag>();
                            spinnerArraysCoun.add(new StringWithTag(getString(R.string.selCountry),""));
                            for (int i = 0; i <countryArray.length() ; i++) {

                                String counid = countryArray.getJSONObject(i).getString("id");
                                String countryname = countryArray.getJSONObject(i).getString("countryname");
                                spinnerArraysCoun.add(new StringWithTag(countryname, counid));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(MemberSignupStep1Activity.this, R.layout.spinner_item, spinnerArraysCoun) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    country_id = String.valueOf(s.tag);
                                    if(country_id.equals("161")){
                                        dstlay.setVisibility(View.VISIBLE);
                                        addLayout.setVisibility(View.GONE);

                                    }else {
                                        dstlay.setVisibility(View.GONE);
                                        addLayout.setVisibility(View.VISIBLE);
                                    }
                                    getCity(country_id);

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(MemberSignupStep1Activity.this);
                    } else {
                        MethodClass.error_alert(MemberSignupStep1Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(MemberSignupStep1Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberSignupStep1Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getCity(String cou_id){
        if (MethodClass.isNetworkConnected(MemberSignupStep1Activity.this)) {
            MethodClass.showProgressDialog(MemberSignupStep1Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "city";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("country_id", cou_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(MemberSignupStep1Activity.this, response);
                        if (result_Object != null) {

                            JSONArray cityArray = result_Object.getJSONArray("city");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selCity),""));
                            for (int i = 0; i <cityArray.length() ; i++) {

                                String city_id = cityArray.getJSONObject(i).getString("id");
                                String cityname = cityArray.getJSONObject(i).getString("city");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(MemberSignupStep1Activity.this, R.layout.spinner_item, spinnerArrays2) {
                            };
                            city_spinner.setAdapter(adapter4);
                            city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    cityId = String.valueOf(s.tag);
                                    if(country_id.equals("161")){
                                        getDistrict(cityId);
                                    }

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(MemberSignupStep1Activity.this);
                    } else {
                        MethodClass.error_alert(MemberSignupStep1Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(MemberSignupStep1Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberSignupStep1Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getDistrict(String cit_id){
        if (MethodClass.isNetworkConnected(MemberSignupStep1Activity.this)) {
            MethodClass.showProgressDialog(MemberSignupStep1Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "district";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("city_id", cit_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(MemberSignupStep1Activity.this, response);
                        if (result_Object != null) {

                            JSONArray districtArray = result_Object.getJSONArray("district");
                            List<StringWithTag> spinnerArrays3 = new ArrayList<StringWithTag>();
                            spinnerArrays3.add(new StringWithTag(getString(R.string.Dist), ""));
                            for (int i = 0; i <districtArray.length() ; i++) {

                                String city_id = districtArray.getJSONObject(i).getString("id");
                                String cityname = districtArray.getJSONObject(i).getString("district");
                                spinnerArrays3.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter5 = new ArrayAdapter(MemberSignupStep1Activity.this, R.layout.spinner_item, spinnerArrays3) {
                            };
                            dis_spinner.setAdapter(adapter5);
                            dis_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    distId = String.valueOf(s.tag);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(MemberSignupStep1Activity.this);
                    } else {
                        MethodClass.error_alert(MemberSignupStep1Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(MemberSignupStep1Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberSignupStep1Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }



    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }
    public void nextPage(final View view){

        if(title.equals(getString(R.string.title))){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseTitle), Snackbar.LENGTH_LONG);
            snackbar.show();
            title_spinner.requestFocus();
            return;
        }
        if(fname.getText().toString().trim().length() == 0){
            fname.setError(getString(R.string.fnameReq));
            fname.requestFocus();
            return;
        }

        if(lname.getText().toString().trim().length() == 0){
            lname.setError(getString(R.string.lnameReq));
            lname.requestFocus();
            return;
        }if(code.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCode), Snackbar.LENGTH_LONG);
            snackbar.show();
            ccp.requestFocus();
            return;
        }
        if(phone.getText().toString().trim().length() == 0){
            phone.setError(getString(R.string.phoneReq));
            phone.requestFocus();
            return;
        }if(age.equals(getString(R.string.ageHint))){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseAge), Snackbar.LENGTH_LONG);
            snackbar.show();
            age_spinner.requestFocus();
            return;
        }if(gender.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseGender), Snackbar.LENGTH_LONG);
            snackbar.show();
            gender_spinner.requestFocus();
            return;
        }if(martial.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseMartial), Snackbar.LENGTH_LONG);
            snackbar.show();
            mar_spinner.requestFocus();
            return;
        }if(!MethodClass.emailValidator(emailaddress.getText().toString().trim())){
            emailaddress.setError(getString(R.string.emailReq));
            emailaddress.requestFocus();
            return;
        }if(!MethodClass.emailValidator(confirmEmail.getText().toString().trim())){
            confirmEmail.setError(getString(R.string.emailReq));
            confirmEmail.requestFocus();
            return;
        }
        if(!emailaddress.getText().toString().trim().equals(confirmEmail.getText().toString().trim())){
            confirmEmail.setError(getString(R.string.confirmMismatch));
            confirmEmail.requestFocus();
            return;
        }
        if(country_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCountry), Snackbar.LENGTH_LONG);
            snackbar.show();
            country_spinner.requestFocus();
            return;
        }
        if(cityId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseCity), Snackbar.LENGTH_LONG);
            snackbar.show();
            city_spinner.requestFocus();
            return;
        }
        if(dstlay.getVisibility() == view.VISIBLE){
            if(distId.equals("")){
                View parentLayout = findViewById(android.R.id.content);
                Snackbar snackbar = Snackbar
                        .make(parentLayout, getString(R.string.chooseDist), Snackbar.LENGTH_LONG);
                snackbar.show();
                dis_spinner.requestFocus();
                return;
            }
        }
        if(addLayout.getVisibility()==view.VISIBLE){
            if(address.getText().toString().trim().length() == 0){
                address.setError(getString(R.string.addressReq));
                address.requestFocus();
                return;
            }
        }

        if(pass.getText().toString().trim().length() == 0){
            pass.setError(getString(R.string.passReq));
            pass.requestFocus();
            return;
        }
        if(confPass.getText().toString().trim().length() == 0){
            confPass.setError(getString(R.string.passReq));
            confPass.requestFocus();
            return;
        }
        if(!pass.getText().toString().trim().equals(confPass.getText().toString().trim())){
            confPass.setError(getString(R.string.confirmpassMismatch));
            confPass.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(MemberSignupStep1Activity.this);
        String server_url = getString(R.string.SERVER_URL) + "checkemail";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", emailaddress.getText().toString().trim());
        params.put("phone", phone.getText().toString().trim());
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                Log.e("resp", response.toString());
                    JSONObject jsonObject = MethodClass.get_result_from_webservice(MemberSignupStep1Activity.this, response);
                    if (jsonObject != null) {
                        Intent I = new Intent(MemberSignupStep1Activity.this,MemberSignupStep2Activity.class);
                        I.putExtra("title",title);
                        I.putExtra("fname",fname.getText().toString().trim());
                        I.putExtra("lname",lname.getText().toString().trim());
                        I.putExtra("phone",phone.getText().toString().trim());
                        I.putExtra("age",age);
                        I.putExtra("code",code);
                        I.putExtra("gender",gender);
                        I.putExtra("martial",martial);
                        I.putExtra("email",emailaddress.getText().toString().trim());
                        I.putExtra("country",country_id);
                        I.putExtra("city",cityId);

                        Log.e("latitude", String.valueOf(lat));
                        if(addLayout.getVisibility() == view.VISIBLE){
                            I.putExtra("address",address.getText().toString().trim());
                            I.putExtra("latitude",String.valueOf(lat));
                            I.putExtra("longitude",String.valueOf(long_));
                        }else{
                            I.putExtra("address","");
                            I.putExtra("latitude","");
                            I.putExtra("longitude","");
                        }

                        I.putExtra("password",pass.getText().toString().trim());
                        if(dstlay.getVisibility() == view.VISIBLE){
                            I.putExtra("district",distId);
                        }
                        startActivity(I);
                    }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("ERROR", error.toString());
                MethodClass.hideProgressDialog(MemberSignupStep1Activity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(MemberSignupStep1Activity.this);
                } else {
                    MethodClass.error_alert(MemberSignupStep1Activity.this);
                }
            }
        });

        MySingleton.getInstance(MemberSignupStep1Activity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void Login(View view){
        Intent I = new Intent(this,LoginActivity.class);
        startActivity(I);
    }
    public void back(View view){
        super.onBackPressed();
    }

}
