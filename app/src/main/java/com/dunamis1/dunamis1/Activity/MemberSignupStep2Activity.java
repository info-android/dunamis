package com.dunamis1.dunamis1.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.SHARED_PREF;

public class MemberSignupStep2Activity extends AppCompatActivity {
    private Spinner country_spinner;
    private EditText churchDep;
    private EditText address;
    private Button continues;
    private CheckBox chk;
    private Spinner branch_spinner;

    private Boolean agreed = false;
    private String branch_id="";
    private String dept_id="";
    private  String regId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(MemberSignupStep2Activity.this);
        setContentView(R.layout.activity_member_signup_step2);

        country_spinner  = findViewById(R.id.country_spinner);
        churchDep  = findViewById(R.id.churchDep);
        continues  = findViewById(R.id.continues);
        address  = findViewById(R.id.address);
        chk  = findViewById(R.id.chk);
        String text = "<font color=#ffffff>"+getString(R.string.byCre)+"</font><font color=#f3a600>"+" <u>"+getString(R.string.privacy)+"</u></font><font color=#ffffff>"+" "+getString(R.string.and)+"</font><font color=#f3a600>"+" <u>"+getString(R.string.terms)+"</u></font>";
        chk.setText(Html.fromHtml(text));

        chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(MemberSignupStep2Activity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.50);
                dialog.setContentView(R.layout.terms);
                dialog.getWindow().setLayout(width,height);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                Button conti = (Button) dialog.findViewById(R.id.conti);

                conti.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        agreed = true;
                        chk.setChecked(true);
                        signUp();
                    }
                });close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        getChurchBranch();

    }
    public void getChurchBranch(){
        if (MethodClass.isNetworkConnected(MemberSignupStep2Activity.this)) {
            MethodClass.showProgressDialog(MemberSignupStep2Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "church-location";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("language",PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("LANG","en"));
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(MemberSignupStep2Activity.this, response);
                        if (result_Object != null) {

                            JSONArray locationArray = result_Object.getJSONArray("location");
                            List<StringWithTag> spinnerArrays2 = new ArrayList<StringWithTag>();
                            spinnerArrays2.add(new StringWithTag(getString(R.string.selctChurchLoc),""));
                            for (int i = 0; i <locationArray.length() ; i++) {

                                String city_id = locationArray.getJSONObject(i).getString("id");
                                JSONObject churchdetails = locationArray.getJSONObject(i).getJSONObject("churchdetails");
                                String cityname = churchdetails.getString("name");
                                spinnerArrays2.add(new StringWithTag(cityname, city_id));

                            }
                            ArrayAdapter adapter4 = new ArrayAdapter(MemberSignupStep2Activity.this, R.layout.spinner_item, spinnerArrays2) {
                            };
                            country_spinner.setAdapter(adapter4);
                            country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                    branch_id = String.valueOf(s.tag);

                                    churchDep.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getDepartment(branch_id);
                                        }
                                    });

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(MemberSignupStep2Activity.this);
                    } else {
                        MethodClass.error_alert(MemberSignupStep2Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getDepartment(String branc_id){
        if (MethodClass.isNetworkConnected(MemberSignupStep2Activity.this)) {
            MethodClass.showProgressDialog(MemberSignupStep2Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "church-department";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("location_id",branc_id);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(MemberSignupStep2Activity.this, response);
                        if (result_Object != null) {

                            JSONArray districtArray = result_Object.getJSONArray("department");
                            ArrayList<MultiSelectModel> listOfCountries= new ArrayList<>();
                            for (int i = 0; i <districtArray.length() ; i++) {
                                JSONObject department = districtArray.getJSONObject(i).getJSONObject("department");
                                listOfCountries.add(new MultiSelectModel(Integer.parseInt(districtArray.getJSONObject(i).getString("department_name_id")),department.getString("department_name")));
                            }
                            MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                                    .title(getResources().getString(R.string.chrchDep)) //setting title for dialog
                                    .titleSize(25)
                                    .positiveText("Done")
                                    .negativeText(getResources().getString(R.string.dialog_cancel))
                                    .setMinSelectionLimit(0)
                                    .setMaxSelectionLimit(2)
                                    .multiSelectList(listOfCountries) // the multi select model list with ids and name
                                    .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                        @Override
                                        public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                            //will return list of selected IDS
                                            for (int s = 0; s < selectedIds.size(); s++) {
                                                churchDep.setText(dataString);
                                                dept_id = TextUtils.join(",",selectedIds);
                                            }
                                        }
                                        @Override
                                        public void onCancel() {

                                        }
                                    });
                            multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(MemberSignupStep2Activity.this);
                    } else {
                        MethodClass.error_alert(MemberSignupStep2Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public class StringWithTag {
        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public void signUp() {
        if(branch_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseChurchBanch), Snackbar.LENGTH_LONG);
            snackbar.show();
            country_spinner.requestFocus();
            return;
        }
        if(address.getText().toString().length() == 0){
            address.setError(getString(R.string.compleDate));
            address.requestFocus();
            return;
        }
        if(agreed){
            if (MethodClass.isNetworkConnected(MemberSignupStep2Activity.this)) {
                MethodClass.showProgressDialog(MemberSignupStep2Activity.this);
                SharedPreferences pref = getSharedPreferences(SHARED_PREF, 0);
                regId = pref.getString("regId", null);
                String server_url = getString(R.string.SERVER_URL) + "member-signup";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("title",getIntent().getStringExtra("title"));
                params.put("fname",getIntent().getStringExtra("fname"));
                params.put("lname",getIntent().getStringExtra("lname"));
                params.put("email",getIntent().getStringExtra("email"));
                params.put("phone",getIntent().getStringExtra("phone"));
                params.put("age",getIntent().getStringExtra("age"));
                params.put("gender",getIntent().getStringExtra("gender"));
                params.put("marital_status",getIntent().getStringExtra("martial"));
                params.put("country_id",getIntent().getStringExtra("country"));
                params.put("city_id",getIntent().getStringExtra("city"));
                params.put("district_id",getIntent().getStringExtra("district"));
                params.put("password",getIntent().getStringExtra("password"));
                params.put("church_location_id",branch_id);
                params.put("church_department_id",dept_id);
                params.put("address",getIntent().getStringExtra("address"));
                params.put("latitude",getIntent().getStringExtra("latitude"));
                params.put("longitude",getIntent().getStringExtra("longitude"));
                params.put("completion_date",address.getText().toString().trim());
                params.put("language",PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("LANG","en"));
                params.put("country_code",getIntent().getStringExtra("code"));
                params.put("reg_id", regId);
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("HomeRes", response.toString());

                        try {
                            MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);

                            JSONObject result_Object = MethodClass.get_result_from_webservice(MemberSignupStep2Activity.this, response);
                            if (result_Object != null) {
                                final String vcode = result_Object.getString("otp");
                                new SweetAlertDialog(MemberSignupStep2Activity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("REGISTRATION SUCCESSFUL")
                                        .setContentText("An OTP has been sent to your registered phone number. Please enter that in the next page")
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                Intent I =new Intent(MemberSignupStep2Activity.this, VerificationActivity.class);
                                                I.putExtra("vcode",vcode);
                                                I.putExtra("phone",getIntent().getStringExtra("phone"));
                                                I.putExtra("email",getIntent().getStringExtra("email"));
                                                I.putExtra("password",getIntent().getStringExtra("password"));
                                                I.putExtra("from","M");
                                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(I);
                                            }
                                        })
                                        .show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR", error.toString());
                        MethodClass.hideProgressDialog(MemberSignupStep2Activity.this);
                        if (error.toString().contains("ConnectException")) {
                            MethodClass.network_error_alert(MemberSignupStep2Activity.this);
                        } else {
                            MethodClass.error_alert(MemberSignupStep2Activity.this);
                        }

                    }
                }){
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        if(!PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("token", "").equals("")){
                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MemberSignupStep2Activity.this).getString("token", ""));
                        }


                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Access the RequestQueue through your singleton class.
                MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
            } else {
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                snackbar.show();
                return;
            }
        }else{
            final Dialog dialog = new Dialog(MemberSignupStep2Activity.this);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.terms);
            ImageView close = (ImageView) dialog.findViewById(R.id.close);
            Button conti = (Button) dialog.findViewById(R.id.conti);

            conti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    agreed  = true;
                    chk.setChecked(true);
                    signUp();
                }
            });close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    public void nextPage(View view){
        signUp();
    }
    public void Login(View view){
        Intent I = new Intent(this,LoginActivity.class);
        startActivity(I);
    }
    public void founda(View view){
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(MemberSignupStep2Activity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, (month));
                calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                String myFormat = "dd-MM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                address.setText(sdf.format(calendar.getTime()));
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
        dialog.show();
    }
    public void back(View view){
        super.onBackPressed();
    }

}
