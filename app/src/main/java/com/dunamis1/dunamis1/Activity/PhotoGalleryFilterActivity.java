package com.dunamis1.dunamis1.Activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.dunamis1.dunamis1.Helper.Constant.EVENTNAME;
import static com.dunamis1.dunamis1.Helper.Constant.FROMDATE;
import static com.dunamis1.dunamis1.Helper.Constant.TODATE;

public class PhotoGalleryFilterActivity extends AppCompatActivity {

    private EditText eventName;
    private EditText fromDate;
    private EditText toDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_gallery_filter);
        eventName = findViewById(R.id.eventName);
        fromDate = findViewById(R.id.fromDate);
        toDate = findViewById(R.id.toDate);

        if(!EVENTNAME.equals("")){
            eventName.setText(EVENTNAME);
        }
        if(!FROMDATE.equals("")){
            fromDate.setText(FROMDATE);
        }
        if(!TODATE.equals("")){
            toDate.setText(TODATE);
        }

        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(PhotoGalleryFilterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, (month));
                        calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                        String myFormat = "dd-MM-yyyy";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        fromDate.setText(sdf.format(calendar.getTime()));
                    }
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
                dialog.show();
            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(PhotoGalleryFilterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, (month));
                        calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                        String myFormat = "dd-MM-yyyy";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        toDate.setText(sdf.format(calendar.getTime()));
                    }
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
                dialog.show();
            }
        });



    }
    public void back(View view) {
        FROMDATE ="";
        TODATE = "";
        EVENTNAME = "";
        onBackPressed();
        finish();
    }
    public void close(View view) {
        FROMDATE ="";
        TODATE = "";
        EVENTNAME = "";
        onBackPressed();
        finish();
    }
    public void openDate(View view){
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd MMMM yyyy"; // your format
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
            }

        };
        new DatePickerDialog(this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    public void applyFilter(View view){
        EVENTNAME = eventName.getText().toString().trim();
        FROMDATE = fromDate.getText().toString();
        TODATE = toDate.getText().toString();
        onBackPressed();
        finish();
    }
}
