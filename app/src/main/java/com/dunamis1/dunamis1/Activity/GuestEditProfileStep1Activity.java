package com.dunamis1.dunamis1.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.dunamis1.dunamis1.Fragment.GuestStep1Fragment;
import com.dunamis1.dunamis1.Fragment.GuestStep2Fragment;
import com.dunamis1.dunamis1.Fragment.GuestStep3fragment;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class GuestEditProfileStep1Activity extends AppCompatActivity {

    Button step1,step2,step3;
    private FrameLayout viewpager;
    private TextView header;
    private CircleImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_edit_profile_step1);

        viewpager = (FrameLayout) findViewById(R.id.viewpager);
        step1 = (Button) findViewById(R.id.step1);
        step2 = (Button) findViewById(R.id.step2);
        step3 = (Button) findViewById(R.id.step3);
        header = (TextView) findViewById(R.id.header);
        image = findViewById(R.id.image);

        step1.setTextColor(getResources().getColor(R.color.white));
        step1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a5, 0);
        step1.setBackground(getDrawable(R.drawable.btm_white));

        step2.setTextColor(getResources().getColor(R.color.black));
        step2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step2.setBackground(getDrawable(R.drawable.rght_blue));

        step3.setTextColor(getResources().getColor(R.color.black));
        step3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step3.setBackground(getDrawable(R.drawable.rght_blue));

        Fragment FragmentGuestStep1 = getSupportFragmentManager().findFragmentByTag("STEP1");
        Fragment FragmentFragmentGuestStep2 = getSupportFragmentManager().findFragmentByTag("STEP2");
        Fragment FragmentFragmentGuestStep3 = getSupportFragmentManager().findFragmentByTag("STEP3");

        if (FragmentGuestStep1 == null && FragmentFragmentGuestStep2 == null && FragmentFragmentGuestStep3 == null) {
            addFragment(R.id.viewpager, new GuestStep1Fragment(), "STEP1");
        }else{
            replaceFragment(R.id.viewpager, new GuestStep2Fragment(), "STEP2");
        }

        step1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step1part();
            }
        });
        step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step2part();
            }
        });
        step3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step3part();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+ PreferenceManager.getDefaultSharedPreferences(GuestEditProfileStep1Activity.this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image);
    }
    public void step1part(){
        viewpager.setVisibility(View.VISIBLE);
        header.setText(getString(R.string.edtGuesHead));
        step1.setTextColor(getResources().getColor(R.color.white));
        step1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a5, 0);
        step1.setBackground(getDrawable(R.drawable.btm_white));

        step2.setTextColor(getResources().getColor(R.color.black));
        step2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step2.setBackground(getDrawable(R.drawable.rght_blue));

        step3.setTextColor(getResources().getColor(R.color.black));
        step3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step3.setBackground(getDrawable(R.drawable.rght_blue));

        Fragment FragmentGuestStep1 = getSupportFragmentManager().findFragmentByTag("STEP1");
        Fragment FragmentFragmentGuestStep2 = getSupportFragmentManager().findFragmentByTag("STEP2");
        Fragment FragmentFragmentGuestStep3 = getSupportFragmentManager().findFragmentByTag("STEP3");

        if (FragmentGuestStep1 == null && FragmentFragmentGuestStep2 == null && FragmentFragmentGuestStep3 == null) {
            addFragment(R.id.viewpager, new GuestStep1Fragment(), "STEP1");
        }else{
            replaceFragment(R.id.viewpager, new GuestStep1Fragment(), "STEP1");
        }

    }
    public void step2part(){
        viewpager.setVisibility(View.VISIBLE);
        header.setText(getString(R.string.edtGuesHead2));
        step1.setTextColor(getResources().getColor(R.color.black));
        step1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step1.setBackground(getDrawable(R.drawable.rght_blue));

        step2.setTextColor(getResources().getColor(R.color.white));
        step2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a5, 0);
        step2.setBackground(getDrawable(R.drawable.btm_white));

        step3.setTextColor(getResources().getColor(R.color.black));
        step3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step3.setBackground(getDrawable(R.drawable.rght_blue));

        Fragment FragmentGuestStep1 = getSupportFragmentManager().findFragmentByTag("STEP1");
        Fragment FragmentFragmentGuestStep2 = getSupportFragmentManager().findFragmentByTag("STEP2");
        Fragment FragmentFragmentGuestStep3 = getSupportFragmentManager().findFragmentByTag("STEP3");

        if (FragmentGuestStep1 == null && FragmentFragmentGuestStep2 == null && FragmentFragmentGuestStep3 == null) {
            addFragment(R.id.viewpager, new GuestStep2Fragment(), "STEP2");
        }else{
            replaceFragment(R.id.viewpager, new GuestStep2Fragment(), "STEP2");
        }
    }
    public void step3part(){
        viewpager.setVisibility(View.VISIBLE);
        header.setText(getString(R.string.edtGuesHead2));
        step1.setTextColor(getResources().getColor(R.color.black));
        step1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step1.setBackground(getDrawable(R.drawable.rght_blue));

        step2.setTextColor(getResources().getColor(R.color.black));
        step2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a4, 0);
        step2.setBackground(getDrawable(R.drawable.rght_blue));

        step3.setTextColor(getResources().getColor(R.color.white));
        step3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.a5, 0);
        step3.setBackground(getDrawable(R.drawable.btm_white));

        Fragment FragmentGuestStep1 = getSupportFragmentManager().findFragmentByTag("STEP1");
        Fragment FragmentFragmentGuestStep2 = getSupportFragmentManager().findFragmentByTag("STEP2");
        Fragment FragmentFragmentGuestStep3 = getSupportFragmentManager().findFragmentByTag("STEP3");

        if (FragmentGuestStep1 == null && FragmentFragmentGuestStep2 == null && FragmentFragmentGuestStep3 == null) {
            addFragment(R.id.viewpager, new GuestStep3fragment(), "STEP3");
        }else{
            replaceFragment(R.id.viewpager, new GuestStep3fragment(), "STEP3");
        }
    }
    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment, @NonNull String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, tag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment, @NonNull String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, tag)
                .commit();
    }
    public void back(View view){
        super.onBackPressed();
    }
}
