package com.dunamis1.dunamis1.Activity.ChurchDepartment;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.dunamis1.dunamis1.Adapter.SeedsAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.Helper.RequestManager;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.ChurchPhotoGalleryAdapter;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.IMGG;
import static com.dunamis1.dunamis1.Helper.Constant.PICK_FROM_GALLERY;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_CODE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_ID;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.SEED_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.TAKE_PHOTO_CODE;

public class ChurchPhotoGalleryActivity extends AppCompatActivity {
    private RecyclerView gallery_rv;
    private ArrayList<HashMap<String, Object>> map_list;
    private Integer[] pic_array = {R.drawable.gallery1, R.drawable.gallery2, R.drawable.gallery3, R.drawable.gallery4,
            R.drawable.gallery5, R.drawable.gallery6, R.drawable.gallery7, R.drawable.gallery8,
            R.drawable.gallery9, R.drawable.gallery10, R.drawable.gallery11, R.drawable.gallery12,
            R.drawable.gallery13, R.drawable.gallery14, R.drawable.gallery15};
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView uploadTxt;

    private CircleImageView imgPro;
    private TextView nav_name_tv;

    private LinearLayout uploadImg;
    private Uri outputFileUri;
    private String imageFilePath;
    private String product_image ="";
    View rootView;
    private ArrayList<String> ListImage = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_church_photo_gallery);
        gallery_rv=(RecyclerView)findViewById(R.id.gallery_rv);
        uploadTxt=(TextView) findViewById(R.id.uploadTxt);
        uploadImg=(LinearLayout) findViewById(R.id.uploadImg);

        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);

        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
            uploadTxt.setText("Upload Home Church Photo");
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
            uploadTxt.setText("Upload Department Photo");
        }

        uploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu= new PopupMenu(ChurchPhotoGalleryActivity.this,uploadImg);
                popupMenu.getMenuInflater().inflate(R.menu.camera_popup_menu,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id=item.getItemId();
                        if (id== R.id.camera){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (ChurchPhotoGalleryActivity.this.checkSelfPermission(Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                                }
                                else{
                                    CameraIntent();
                                }
                            }
                        }
                        else if (id==R.id.gallery){
                            try {
                                if (ActivityCompat.checkSelfPermission(ChurchPhotoGalleryActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(ChurchPhotoGalleryActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                                } else {
                                    galleryIntent();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        get_gallery_list();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));
    }
    public void back(View view) {
        onBackPressed();
        finish();
    }
    public void get_gallery_list() {
        MethodClass.showProgressDialog(ChurchPhotoGalleryActivity.this);
        String server_url = "";
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            server_url = ChurchPhotoGalleryActivity.this.getString(R.string.SERVER_URL) + "home-church/photos";
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            server_url = ChurchPhotoGalleryActivity.this.getString(R.string.SERVER_URL) + "church-department/photos";
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(ChurchPhotoGalleryActivity.this).getString("LANG","en"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ChurchPhotoGalleryActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(ChurchPhotoGalleryActivity.this, response);
                    if (result_Object != null) {
                        JSONArray seedArray = result_Object.getJSONArray("photos");

                        if(seedArray.length()>0){
                            map_list = new ArrayList<>();

                            for (int i = 0; i <seedArray.length() ; i++) {
                                JSONObject SeedObj = seedArray.getJSONObject(i);

                                String image = SeedObj.getString("image");
                                String id = SeedObj.getString("id");


                                HashMap<String,Object> map = new HashMap<>();
                                map.put(IMGG,image);
                                map.put(SEED_ID,id);

                                map_list.add(map);
                            }
                            ChurchPhotoGalleryAdapter adapter = new ChurchPhotoGalleryAdapter(ChurchPhotoGalleryActivity.this, map_list);
                            gallery_rv.setAdapter(adapter);
                            gallery_rv.setFocusable(false);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ChurchPhotoGalleryActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ChurchPhotoGalleryActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ChurchPhotoGalleryActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChurchPhotoGalleryActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ChurchPhotoGalleryActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ChurchPhotoGalleryActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void upladIMGG(){
        if (MethodClass.isNetworkConnected(ChurchPhotoGalleryActivity.this)) {
            MethodClass.showProgressDialog(ChurchPhotoGalleryActivity.this);
            String server_url = "";
            if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
                server_url = ChurchPhotoGalleryActivity.this.getString(R.string.SERVER_URL) + "home-church/photos/upload";
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
                server_url = ChurchPhotoGalleryActivity.this.getString(R.string.SERVER_URL) + "church-department/photos/upload";
            }
            SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    MethodClass.hideProgressDialog(ChurchPhotoGalleryActivity.this);
                    try {
                        JSONObject jsonObject = MethodClass.get_result_from_webservice(ChurchPhotoGalleryActivity.this, new JSONObject(response));
                        if (jsonObject != null) {
                           get_gallery_list();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    MethodClass.hideProgressDialog(ChurchPhotoGalleryActivity.this);
                    Log.e("error", error.toString());
                    MethodClass.hideProgressDialog(ChurchPhotoGalleryActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(ChurchPhotoGalleryActivity.this);
                    } else {
                        MethodClass.error_alert(ChurchPhotoGalleryActivity.this);
                    }
                }
            }){
                // Passing some request headers
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ChurchPhotoGalleryActivity.this).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            RequestQueue requestQueue = RequestManager.getnstance(ChurchPhotoGalleryActivity.this);
            simpleMultiPartRequest.addMultipartParam("count","text", String.valueOf(ListImage.size()));
            if(ListImage.size()>0){
                for (int i = 0; i <ListImage.size() ; i++) {
                    simpleMultiPartRequest.addFile("church_images"+(i+1), ListImage.get(i).toString());
                }
            }

            simpleMultiPartRequest.setFixedStreamingMode(true);
            int socketTimeout = 100000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
            simpleMultiPartRequest.setRetryPolicy(policy);
            requestQueue.getCache().clear();
            requestQueue.add(simpleMultiPartRequest);

        } else {
            Snackbar snackbar = Snackbar.make(ChurchPhotoGalleryActivity.this.findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }

    private void CameraIntent(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();

        }catch (Exception e){
            e.printStackTrace();
        }
        outputFileUri = FileProvider.getUriForFile(ChurchPhotoGalleryActivity.this, "com.dunamis1.dunamis1.fileprovider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,outputFileUri);
        Log.e("Intentdata", intent.toString() );
        startActivityForResult(intent, TAKE_PHOTO_CODE);

    }

    private File createImageFile() throws IOException {
        File image = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // upload_map_list= new ArrayList<HashMap<String,String>>();
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY) {
                Uri selectedUri;
                if (null != data) { // checking empty selection
                    if (null != data.getClipData()) { // checking multiple selection or not
                        if(data.getClipData().getItemCount()+ListImage.size() > 1){
                            ListImage = new ArrayList<String>();
                        }
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            selectedUri = data.getClipData().getItemAt(i).getUri();
                            try {
                                product_image = getPath(ChurchPhotoGalleryActivity.this, selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        if(!data.getData().getAuthority().equals("com.android.providers.downloads.documents")){
                            selectedUri = data.getData();
                            try {
                                product_image = getPath(ChurchPhotoGalleryActivity.this, selectedUri);
                                ListImage.add(product_image);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.e("SIZE", String.valueOf(ListImage.size()));
                            Snackbar snackbar = Snackbar.make(ChurchPhotoGalleryActivity.this.findViewById(android.R.id.content),"Please choose image from other folder", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    }
                    Log.e("SIZE", String.valueOf(ListImage.size()));
                    upladIMGG();
                }
            } else if (requestCode == TAKE_PHOTO_CODE) {

                product_image = imageFilePath;
                ListImage.add(product_image);
                upladIMGG();
            }

        }
    }
    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        Log.e("LOGURI", uri.getScheme() );
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = ChurchPhotoGalleryActivity.this.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
