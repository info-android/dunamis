package com.dunamis1.dunamis1.Activity;

import android.app.DatePickerDialog;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FiregnersDeskStep2Activity extends AppCompatActivity {

    private Spinner year_spiner,month_spinner;

    private RadioGroup budRad;
    private RadioGroup transReq;
    private RadioGroup programAttend;
    private RadioGroup confAtten;
    private RadioGroup urgAss;
    private RadioGroup dunaDisiple;
    private LinearLayout budLay;
    private EditText dateOfAri;

    private RadioButton dunaDisipleYes,dunaDisipleNo,urgAssOne,urgAssTwo,urgAssThree,confAttenOne,confAttenTwo,confAttenThree,progAttenOne,progAttenTwo,progAttenThree,progAttenFour,transYes,transNo,yes,no;
    private EditText address,specialReq,speciReq;

    private String hotelReq_id = "",dunadisipleId = "",urgAss_id="",conf_id ="",program_id="",trans_id = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firegners_desk_step2);
        MethodClass.hide_keyboard(FiregnersDeskStep2Activity.this);
        year_spiner  = findViewById(R.id.year_spinner);
        month_spinner  = findViewById(R.id.month_spinner);
        budRad  = findViewById(R.id.budRad);
        budLay  = findViewById(R.id.budLay);
        dateOfAri  = findViewById(R.id.dateOfAri);
        transReq  = findViewById(R.id.transReq);
        programAttend  = findViewById(R.id.programAttend);
        confAtten  = findViewById(R.id.confAtten);
        urgAss  = findViewById(R.id.urgAss);
        dunaDisiple  = findViewById(R.id.dunaDisiple);
        address  = findViewById(R.id.address);
        specialReq  = findViewById(R.id.specialReq);
        speciReq  = findViewById(R.id.speciReq);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.years, R.layout.spinner_item);
        ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this, R.array.months, R.layout.spinner_item);

        year_spiner.setAdapter(adapter);
        month_spinner.setAdapter(adapter2);
        budRad.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.yes:
                        // TODO Something
                        hotelReq_id = "Y";
                        budLay.setVisibility(View.VISIBLE);
                        break;
                    case R.id.no:
                        // TODO Something
                        hotelReq_id = "N";
                        budLay.setVisibility(View.GONE);
                        break;
                }
            }
        });
        transReq.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.transYes:
                        // TODO Something
                        trans_id = "Y";

                        break;
                    case R.id.transNo:
                        // TODO Something
                        trans_id = "N";

                        break;
                }
            }
        });
        programAttend.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.progAttenOne:
                        // TODO Something
                        program_id = "1";

                        break;
                    case R.id.progAttenTwo:
                        // TODO Something
                        program_id = "2";

                        break;
                    case R.id.progAttenThree:
                        // TODO Something
                        program_id = "3";

                        break;
                    case R.id.progAttenFour:
                        // TODO Something
                        program_id = "4";

                        break;
                }
            }
        });
        confAtten.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.confAttenOne:
                        // TODO Something
                        conf_id = "1";

                        break;
                    case R.id.confAttenTwo:
                        // TODO Something
                        conf_id = "2";

                        break;
                    case R.id.confAttenThree:
                        // TODO Something
                        conf_id = "3";

                        break;
                    case R.id.confAttenFour:
                        // TODO Something
                        conf_id = "4";

                        break;
                }
            }
        });

        urgAss.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.urgAssOne:
                        // TODO Something
                        urgAss_id = "1";

                        break;
                    case R.id.urgAssTwo:
                        // TODO Something
                        urgAss_id = "2";

                        break;
                    case R.id.urgAssThree:
                        // TODO Something
                        urgAss_id = "3";

                        break;

                }
            }
        });
        dunaDisiple.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.dunaDisipleYes:
                        // TODO Something
                        dunadisipleId = "Y";

                        break;
                    case R.id.dunaDisipleNo:
                        // TODO Something
                        dunadisipleId = "N";

                        break;


                }
            }
        });

        dateOfAri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd MMMM yyyy"; // your format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        dateOfAri.setText(sdf.format(myCalendar.getTime()));
                    }

                };
                new DatePickerDialog(FiregnersDeskStep2Activity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public void back(View view){
        super.onBackPressed();
    }
    public void nextPage(View view){
        if(hotelReq_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseHotelReq), Snackbar.LENGTH_LONG);
            snackbar.show();
            budRad.requestFocus();
            return;
        }
        if(budLay.getVisibility() == view.VISIBLE){
            if(address.getText().toString().trim().length() == 0){
                address.setError(getString(R.string.budgReq));
                address.requestFocus();
                return;
            }
        }
        if(trans_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseTranslator), Snackbar.LENGTH_LONG);
            snackbar.show();
            transReq.requestFocus();
            return;
        }
        if(speciReq.getText().toString().trim().length() == 0){
            speciReq.setError(getString(R.string.speciReq));
            speciReq.requestFocus();
            return;
        }
        if(specialReq.getText().toString().trim().length() == 0){
            specialReq.setError(getString(R.string.anyspecReq));
            specialReq.requestFocus();
            return;
        }
        if(program_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseAttprog), Snackbar.LENGTH_LONG);
            snackbar.show();
            programAttend.requestFocus();
            return;
        }
        if(conf_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseConf), Snackbar.LENGTH_LONG);
            snackbar.show();
            confAtten.requestFocus();
            return;
        }
        if(urgAss_id.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseUrgAss), Snackbar.LENGTH_LONG);
            snackbar.show();
            urgAss.requestFocus();
            return;
        }
        if(dateOfAri.getText().toString().trim().length() == 0){
            dateOfAri.setError(getString(R.string.dateOfarrive));
            dateOfAri.requestFocus();
            return;
        }
        if(dunadisipleId.equals("")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseDisiple), Snackbar.LENGTH_LONG);
            snackbar.show();
            dunaDisiple.requestFocus();
            return;
        }
        if(month_spinner.getSelectedItem().toString().equals("How many in the group?")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseSpiner), Snackbar.LENGTH_LONG);
            snackbar.show();
            month_spinner.requestFocus();
            return;
        }
        if(year_spiner.getSelectedItem().toString().equals("How many in the group?")){
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, getString(R.string.chooseSpiner), Snackbar.LENGTH_LONG);
            snackbar.show();
            year_spiner.requestFocus();
            return;
        }
        if (MethodClass.isNetworkConnected(FiregnersDeskStep2Activity.this)) {
            MethodClass.showProgressDialog(FiregnersDeskStep2Activity.this);
            String server_url = getString(R.string.SERVER_URL) + "foreigners-desk/add";


            HashMap<String, String> params = new HashMap<String, String>();
            params.put("title",getIntent().getStringExtra("title"));
            params.put("fname",getIntent().getStringExtra("fname"));
            params.put("lname",getIntent().getStringExtra("lname"));
            params.put("gender",getIntent().getStringExtra("gender"));
            params.put("marital_status",getIntent().getStringExtra("martial"));
            params.put("email",getIntent().getStringExtra("email"));
            params.put("country_id",getIntent().getStringExtra("country"));
            params.put("hear_about_us",getIntent().getStringExtra("hear"));
            params.put("visiting_dunamis_international",getIntent().getStringExtra("dunaInter"));
            params.put("coming_as_group",getIntent().getStringExtra("grp"));
            params.put("assistance_required",urgAss_id);
            if(getIntent().getStringExtra("grp").equals("Y")){
                params.put("total_member",getIntent().getStringExtra("member"));
            }

            params.put("travelling_alone",getIntent().getStringExtra("traAlone"));
            params.put("transport_arrangements_required",getIntent().getStringExtra("transReq"));
            params.put("hotel_arrangement_required",hotelReq_id);
            if(budLay.getVisibility() == view.VISIBLE){
                params.put("hotel_requirements",address.getText().toString().trim());
            }
            params.put("translator_required",trans_id);
            params.put("requirements",speciReq.getText().toString().trim());
            params.put("special_requirements",specialReq.getText().toString().trim());
            params.put("conference_attending",conf_id);
            params.put("program_attending",program_id);
            params.put("arrival_date",dateOfAri.getText().toString().trim());
            params.put("disciple_class",dunadisipleId);
            params.put("month",month_spinner.getSelectedItem().toString());
            params.put("year",year_spiner.getSelectedItem().toString());
            params.put("language",PreferenceManager.getDefaultSharedPreferences(FiregnersDeskStep2Activity.this).getString("LANG","en"));

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(FiregnersDeskStep2Activity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(FiregnersDeskStep2Activity.this, response);
                        if (result_Object != null) {
                            String message = result_Object.getString("message");
                            new SweetAlertDialog(FiregnersDeskStep2Activity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUCCESSFUL")
                                    .setContentText(message)
                                    .setConfirmText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            if(PreferenceManager.getDefaultSharedPreferences(FiregnersDeskStep2Activity.this).getString("role","Member").equals("Member")){
                                                MethodClass.go_to_next_activity(FiregnersDeskStep2Activity.this,DashboardActivity.class);
                                            }else if(PreferenceManager.getDefaultSharedPreferences(FiregnersDeskStep2Activity.this).getString("role","Member").equals("Guest")){
                                                MethodClass.go_to_next_activity(FiregnersDeskStep2Activity.this,GuestDashboardActivity.class);
                                            }else if(PreferenceManager.getDefaultSharedPreferences(FiregnersDeskStep2Activity.this).getString("role","Member").equals("Home")){
                                                MethodClass.go_to_next_activity(FiregnersDeskStep2Activity.this,HomeChurchDahboardActivity.class);
                                            }else if(PreferenceManager.getDefaultSharedPreferences(FiregnersDeskStep2Activity.this).getString("role","Member").equals("Department")){
                                                MethodClass.go_to_next_activity(FiregnersDeskStep2Activity.this,ChurchDepartmentDashboardActivity.class);
                                            }
                                        }
                                    })
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(FiregnersDeskStep2Activity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(FiregnersDeskStep2Activity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(FiregnersDeskStep2Activity.this);
                    } else {
                        MethodClass.error_alert(FiregnersDeskStep2Activity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(FiregnersDeskStep2Activity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(FiregnersDeskStep2Activity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(FiregnersDeskStep2Activity.this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(FiregnersDeskStep2Activity.this.findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

    }
}
