package com.dunamis1.dunamis1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EmailVerificationActivity extends AppCompatActivity {
    private Button verify;
    private TextView number;
    private TextView otp;
    private EditText editText1,editText2,editText3,editText4;
    private String edtxt11,edtxt12,edtxt13,edtxt14;
    private TextView resend_tv;
    private String code="",email = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(EmailVerificationActivity.this);
        setContentView(R.layout.activity_email_verification);
        verify = findViewById(R.id.verify);
        number = findViewById(R.id.number);
        otp = findViewById(R.id.otp);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        resend_tv = (TextView) findViewById(R.id.resend_tv);

        if (getIntent().getExtras() != null) {
            code = getIntent().getStringExtra("vcode");
            email = getIntent().getStringExtra("phone");
            number.setText(getIntent().getStringExtra("email"));
            otp.setText(getString(R.string.enterDigit)+" "+code);
        }


        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText2.requestFocus();
                } else {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText3.requestFocus();
                } else {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText4.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    if (!editText1.getText().toString().equals("") && !editText2.getText().toString().equals("") && !editText3.getText().toString().equals("") && !editText4.getText().toString().equals("")) {
                        Log.e("editText1", editText1.getText().toString());
                        Log.e("editText1", editText2.getText().toString());
                        Log.e("editText1", editText3.getText().toString());
                        Log.e("editText1", editText4.getText().toString());
                        verification();
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getString(R.string.enterOtp), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } else {
                    //editText5.setBackground(getDrawable(R.drawable.verify_grey_edit_back));
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });


        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verification();
            }
        });
        resend_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resend_otp();
            }
        });
    }

    private void verification() {
        if(editText1.getText().toString().trim().length() == 0 || editText2.getText().toString().trim().length() == 0 || editText3.getText().toString().trim().length() == 0 || editText4.getText().toString().trim().length()==0){
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getString(R.string.enterInvalid), Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        edtxt11 = editText1.getText().toString();
        edtxt12 = editText2.getText().toString();
        edtxt13 = editText3.getText().toString();
        edtxt14 = editText4.getText().toString();
        final String Vcode = (edtxt11 +""+ edtxt12 +""+ edtxt13 +""+ edtxt14);
        MethodClass.showProgressDialog(EmailVerificationActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "phoneverify";


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("vcode", Vcode);
        params.put("phone", email);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(EmailVerificationActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(EmailVerificationActivity.this, response);
                    if (jsonObject != null) {
                        final String message = jsonObject.getString("userdata");
                        new SweetAlertDialog(EmailVerificationActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("VERIFICATION SUCCESSFUL")
                                .setContentText(getString(R.string.profileUpdate))
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent I =new Intent(EmailVerificationActivity.this, ProfileActivity.class);
                                        startActivity(I);
                                        finish();
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(EmailVerificationActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(EmailVerificationActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(EmailVerificationActivity.this);
                } else {
                    MethodClass.error_alert(EmailVerificationActivity.this);
                }
            }
        });

        MySingleton.getInstance(EmailVerificationActivity.this).addToRequestQueue(jsonObjectRequest);
    }



    private void resend_otp(){
        MethodClass.showProgressDialog(EmailVerificationActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "resendvcode";


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("phone", email);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(EmailVerificationActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(EmailVerificationActivity.this, response);
                    if (jsonObject != null) {
                        final String vcode = jsonObject.getString("vcode");

                        otp.setText(getString(R.string.enterDigit)+" "+vcode);
                        new SweetAlertDialog(EmailVerificationActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("OTP SENT")
                                .setContentText("Otp sent to your registered email address")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(EmailVerificationActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(EmailVerificationActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(EmailVerificationActivity.this);
                } else {
                    MethodClass.error_alert(EmailVerificationActivity.this);
                }
            }
        });

        MySingleton.getInstance(EmailVerificationActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        String et1="",et2="",et3="",et4="";

        et1=editText1.getText().toString().trim();
        et2=editText2.getText().toString().trim();
        et3=editText3.getText().toString().trim();
        et4=editText4.getText().toString().trim();

        if (!et4.equals("")){
            editText4.setText("");
            editText3.requestFocus();
            return;
        }
        if (!et3.equals("")){
            editText3.setText("");
            editText2.requestFocus();
            return;
        }
        if (!et2.equals("")){
            editText2.setText("");
            editText1.requestFocus();
            return;
        }
        if (!et1.equals("")){
            editText1.setText("");
            editText1.requestFocus();
            return;
        }
        super.onBackPressed();
    }
}
