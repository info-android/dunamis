package com.dunamis1.dunamis1.Activity.EventListing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.EventsAdapter;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ADD;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SAVE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class EventListingActivity extends AppCompatActivity {
    private RecyclerView events_rv;
    private ArrayList<HashMap<String, String>> map_list;
    private String[] title_array = {"Anthony James", "Obi", "Paul Alfred", "Uju","Lola Babs"};
    private String[] date_Array = {"14 Oct, 2020", "17 Oct, 2020", "24 Oct, 2020", "29 Oct, 2020","03 Dec, 2020"};
    private Integer[] pic_array = {R.drawable.man1, R.drawable.man3, R.drawable.man4, R.drawable.man5,
            R.drawable.man6};
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    private  String country_id="",city_id="",latitude="",longitude="",address="",date_range="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_listing);
        events_rv=(RecyclerView)findViewById(R.id.events_rv);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        if (getIntent().getExtras() !=null){
            country_id= getIntent().getStringExtra("country_id");
            city_id= getIntent().getStringExtra("city_id");
            address= getIntent().getStringExtra("address");
            if (!address.equals("")){
                latitude= getIntent().getStringExtra("latitude");
                longitude= getIntent().getStringExtra("longitude");
            }
            date_range= getIntent().getStringExtra("date_range");
        }

    }
    @Override
    protected void onResume() {
        super.onResume();

        get_events_list();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));

    }
    public void back(View view) {
        onBackPressed();
        finish();
    }

    public void filter(View view) {
        startActivity(new Intent(EventListingActivity.this, EventFilterActivity.class));
    }

    private void get_events_list() {
        MethodClass.showProgressDialog(EventListingActivity.this);
        String server_url = EventListingActivity.this.getString(R.string.SERVER_URL) + "event/list";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(EventListingActivity.this).getString("LANG","en"));
        params.put("country_id", country_id);
        params.put("city_id",city_id );
        params.put("address",address );
        params.put("latitude", latitude);
        params.put("longitude", longitude);
        params.put("date_range", date_range);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(EventListingActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(EventListingActivity.this, response);
                    if (result_Object != null) {
                        JSONArray seedArray = result_Object.getJSONArray("list");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            JSONObject SeedObj = seedArray.getJSONObject(i);
                            String title = SeedObj.getJSONObject("eventdetails").getString("title");
                            String google_address = SeedObj.getString("google_address");
                            String thumbnail = SeedObj.getString("thumbnail");
                            String date = SeedObj.getString("from_date");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String id = SeedObj.getString("id");

                            String favourite = SeedObj.getString("favourite");


                            HashMap<String,String> map = new HashMap<>();
                            map.put(EVENT_TITLE,title);
                            map.put(EVENT_THUMB,thumbnail);
                            map.put(EVENT_DATE,date);
                            map.put(EVENT_ADD,google_address);
                            map.put(EVENT_LIKE,total_likes);
                            map.put(EVENT_COMMENT,total_comments);
                            map.put(EVENT_SHARE,total_share);
                            map.put(EVENT_ID,id);
                            Log.e("FAV", favourite);
                            if(!favourite.equals(null) && !favourite.equals("null"))
                            {
                                map.put(EVENT_SAVE,"YES");
                            }else{
                                map.put(EVENT_SAVE,"NO");
                            }

                            map_list.add(map);
                        }
                        EventsAdapter adapter = new EventsAdapter(EventListingActivity.this,map_list);
                        events_rv.setAdapter(adapter);
                        events_rv.setFocusable(false);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(EventListingActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(EventListingActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(EventListingActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EventListingActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventListingActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(EventListingActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
