package com.dunamis1.dunamis1.Activity;

import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.DepartmentUpdateProfileActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.UpdateChurchDetailsActivity;
import com.dunamis1.dunamis1.Activity.EventListing.EventListingActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Adapter.ChurchTwoAdapter;
import com.dunamis1.dunamis1.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dunamis1.dunamis1.Helper.Constant.DEPARTMENT_BANNER_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.IMAGE;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;

public class ProfileActivity extends AppCompatActivity {
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    int[] sampleImagesArr = {R.drawable.image60, R.drawable.image103, R.drawable.image60};
    int[] sampleImagesArr2 = {R.drawable.image60, R.drawable.dashchurchnear3, R.drawable.image60,R.drawable.church4};
    private String[] depName = {"Councelling", "Audio Ministry", "Councelling"};

    private ArrayList<HashMap<Object,Object>> map_list;
    private ArrayList<HashMap<Object,Object>> map_list2;

    //private RecyclerView rv_Dash1;
    private RecyclerView rv_Dash2;
    RelativeLayout depChurch,depChurch1;
    private String from = "";
    private LinearLayout departmentLayout;
    private TextView classCom;

    private TextView name_tv,email_tv,phone_tv;
    private CircleImageView image,image2;
    private TextView nav_name_tv;
    private CircleImageView imgPro;
    private TextView chucrhLocTitle;
    private TextView chucrhLoc;
    private TextView churchTv;
    private TextView marTv;
    private TextView genTv;
    private TextView add_tv;
    private TextView aboutTv;
    private TextView prdct_title_tv;
    private TextView prdct_title_tv2;
    private TextView role_tv;
    private LinearLayout photoLay;
    private LinearLayout photoLayout;
    private NestedScrollView scrollView;
    private String home_id = "";
    private ImageView prdct_iv,prdct_iv2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        //rv_Dash1 = (RecyclerView) findViewById(R.id.rv_Dash1);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        rv_Dash2 = (RecyclerView) findViewById(R.id.rv_Dash2);
        depChurch = (RelativeLayout) findViewById(R.id.depChurch);
        depChurch1 = (RelativeLayout) findViewById(R.id.depChurch1);
        departmentLayout = (LinearLayout) findViewById(R.id.departmentLayout);
        classCom = (TextView) findViewById(R.id.classCom);
        chucrhLocTitle = (TextView) findViewById(R.id.chucrhLocTitle);
        chucrhLoc = (TextView) findViewById(R.id.chucrhLoc);
        name_tv = (TextView) findViewById(R.id.name_tv);
        email_tv = (TextView) findViewById(R.id.email_tv);
        phone_tv = (TextView) findViewById(R.id.phone_tv);
        image = (CircleImageView) findViewById(R.id.image);
        image2 = (CircleImageView) findViewById(R.id.image2);
        scrollView = findViewById(R.id.scrollView);
        prdct_iv = findViewById(R.id.prdct_iv);
        prdct_iv2 = findViewById(R.id.prdct_iv2);
        photoLayout = findViewById(R.id.photoLayout);

        churchTv =  findViewById(R.id.churchTv);
        marTv =  findViewById(R.id.marTv);
        genTv =  findViewById(R.id.genTv);
        add_tv =  findViewById(R.id.add_tv);
        aboutTv =  findViewById(R.id.aboutTv);
        prdct_title_tv =  findViewById(R.id.prdct_title_tv);
        prdct_title_tv2 =  findViewById(R.id.prdct_title_tv2);


        if(getIntent().getExtras()!=null){
            from = getIntent().getStringExtra("from");
        }
        role_tv = headerView.findViewById(R.id.role_tv);
        photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
            getMember();
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
            departmentLayout.setVisibility(View.GONE);
            classCom.setVisibility(View.GONE);
            getGuest();
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
            departmentLayout.setVisibility(View.GONE);
            classCom.setVisibility(View.GONE);
            chucrhLoc.setText("Your Home Church Location");
            getHomeCurch();
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
            departmentLayout.setVisibility(View.VISIBLE);
            classCom.setVisibility(View.GONE);
            chucrhLoc.setText("Your Church Location");
            getDep();
        }
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image);
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        Picasso.get().load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image2);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));

    }

    public void getMember(){

        MethodClass.showProgressDialog(ProfileActivity.this);
        String server_url = ProfileActivity.this.getString(R.string.SERVER_URL) + "member-profile";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(ProfileActivity.this, response);
                    if (result_Object != null) {
                        JSONObject user = result_Object.getJSONObject("user");

                        email_tv.setText(user.getString("email"));
                        if(!user.getString("country_code").equals(null) && !user.getString("country_code").equals("null")){
                            phone_tv.setText(user.getString("country_code")+" "+user.getString("phone_number"));
                        }else{
                            phone_tv.setText(user.getString("phone_number"));
                        }

                        Picasso.get().load(PROFILE_IMG_URL_THUMB+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image);
                        Picasso.get().load(PROFILE_IMG_URL_THUMB+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
                        Picasso.get().load(PROFILE_IMG_URL+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image2);
                        if(!user.getString("title").equals(null) && !user.getString("title").equals("null")){
                            nav_name_tv.setText(getString(R.string.hello)+" "+user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname"));
                            PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_name", user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname")).commit();
                            name_tv.setText(user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname"));
                        }else{
                            nav_name_tv.setText(getString(R.string.hello)+" "+user.getString("fname")+" "+user.getString("lname"));
                            PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_name", user.getString("fname")+" "+user.getString("lname")).commit();
                            name_tv.setText(user.getString("fname")+" "+user.getString("lname"));
                        }

                        String completion_date = user.getString("completion_date");
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = null;
                        try {
                            date = dateFormatter.parse(completion_date);
                            // Get time from date

                            SimpleDateFormat format = new SimpleDateFormat("d");
                            String datessa = format.format(date);

                            if(datessa.endsWith("1") && !datessa.endsWith("11"))
                                format = new SimpleDateFormat("d'st' MMM, yyyy");
                            else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                                format = new SimpleDateFormat("d'nd' MMM, yyyy");
                            else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                                format = new SimpleDateFormat("d'rd' MMM, yyyy");
                            else
                                format = new SimpleDateFormat("d'th' MMM, yyyy");

                            String yourDate = format.format(date);

                            classCom.setText(getString(R.string.classcom)+": "+yourDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if(!user.getString("church_branch").equals(null) && !user.getString("church_branch").equals("null")){
                            churchTv.setText(user.getJSONObject("church_branch").getString("address"));
                        }else{
                            churchTv.setText("N/A");
                        }

                        chucrhLocTitle.setText(result_Object.getJSONObject("branch_details").getJSONObject("churchdetails").getString("name"));



                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("profile_pic", user.getString("profile_pic")).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("phone", user.getString("phone_number")).commit();

                        if(user.getString("gender").equals("M")){
                            genTv.setText(getString(R.string.Male));
                        }else if(user.getString("gender").equals("F")){
                            genTv.setText(getString(R.string.Female));
                        }else if(user.getString("gender").equals("O")){
                            genTv.setText(getString(R.string.Other));
                        }

                        if(user.getString("marital_status").equals("M")){
                            marTv.setText(getString(R.string.Married));
                        }else if(user.getString("marital_status").equals("S")){
                            marTv.setText(getString(R.string.Single));
                        }

                        if(!user.getString("about_me").equals(null) && !user.getString("about_me").equals("null")){
                            aboutTv.setText(user.getString("about_me"));
                        }else {
                            aboutTv.setText("N/A");
                        }

                        if(!user.getString("address").equals(null) && !user.getString("address").equals("null")){
                            add_tv.setText(user.getString("address"));
                        }else {
                            add_tv.setText(user.getJSONObject("cityname").getString("city")+", "+user.getJSONObject("districtname").getString("district"));
                        }


                        JSONArray department  = result_Object.getJSONArray("department");
                        JSONArray images  = result_Object.getJSONArray("images");

                        if(department.length()>0){
                            for (int i = 0; i <department.length() ; i++) {
                                JSONObject depObj = department.getJSONObject(i);

                                if(i == 0){
                                    final String id = depObj.getJSONObject("churchdepartment").getString("id");
                                    prdct_title_tv.setText(depObj.getJSONObject("churchdepartment").getJSONObject("department").getString("department_name"));
                                    Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+depObj.getJSONObject("churchdepartment").getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(prdct_iv);
                                    depChurch.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent I = new Intent(ProfileActivity.this, ChurchDetailActivity.class);
                                            I.putExtra("id",id);
                                            startActivity(I);
                                        }
                                    });


                                }
                                if(department.length()>1){
                                    if(i == 1){
                                        final String id1 = depObj.getJSONObject("churchdepartment").getString("id");
                                        prdct_title_tv2.setText(depObj.getJSONObject("churchdepartment").getJSONObject("department").getString("department_name"));
                                        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+depObj.getJSONObject("churchdepartment").getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(prdct_iv2);
                                        depChurch1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent I = new Intent(ProfileActivity.this, ChurchDetailActivity.class);
                                                I.putExtra("id",id1);
                                                startActivity(I);
                                            }
                                        });
                                    }
                                }else{
                                    depChurch1.setVisibility(View.GONE);
                                }

                                departmentLayout.setVisibility(View.VISIBLE);
                            }
                        }else {
                            departmentLayout.setVisibility(View.GONE);
                        }


                        map_list2 = new ArrayList<>();
                        if(images.length()>0){
                            for (int i = 0; i <images.length() ; i++) {
                                JSONObject imgObj = images.getJSONObject(i);
                                HashMap<Object,Object> map4 = new HashMap<>();
                                map4.put(IMAGE,imgObj.getString("profile_image"));
                                map_list2.add(map4);
                            }
                            ChurchTwoAdapter adapter4= new ChurchTwoAdapter(ProfileActivity.this,map_list2);
                            rv_Dash2.setAdapter(adapter4);
                            photoLayout.setVisibility(View.VISIBLE);
                        }else {
                            photoLayout.setVisibility(View.GONE);
                        }

                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ProfileActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void getDep(){
        MethodClass.showProgressDialog(ProfileActivity.this);
        String server_url = ProfileActivity.this.getString(R.string.SERVER_URL) + "department-profile";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(ProfileActivity.this, response);
                    if (result_Object != null) {
                        JSONObject user = result_Object.getJSONObject("user");

                        email_tv.setText(user.getString("email"));
                        if(!user.getString("country_code").equals(null) && !user.getString("country_code").equals("null")){
                            phone_tv.setText(user.getString("country_code")+" "+user.getString("phone_number"));
                        }else{
                            phone_tv.setText(user.getString("phone_number"));
                        }


                        if(!user.getString("title").equals(null) && !user.getString("title").equals("null")){
                            nav_name_tv.setText(getString(R.string.hello)+" "+user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname"));
                            PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_name", user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname")).commit();
                            name_tv.setText(user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname"));
                        }else{
                            nav_name_tv.setText(getString(R.string.hello)+" "+user.getString("fname")+" "+user.getString("lname"));
                            PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_name", user.getString("fname")+" "+user.getString("lname")).commit();
                            name_tv.setText(user.getString("fname")+" "+user.getString("lname"));
                        }

                        classCom.setText("Class Completed -"+user.getString("total_class_complete"));
                        if(!user.getString("church_branch").equals(null) && !user.getString("church_branch").equals("null")){
                            churchTv.setText(user.getJSONObject("church_branch").getString("address"));
                        }else{
                            churchTv.setText("N/A");
                        }


                        chucrhLocTitle.setText(result_Object.getJSONObject("branch_details").getJSONObject("churchdetails").getString("name"));


                        if(user.getString("gender").equals("M")){
                            genTv.setText(getString(R.string.Male));
                        }else if(user.getString("gender").equals("F")){
                            genTv.setText(getString(R.string.Female));
                        }else if(user.getString("gender").equals("O")){
                            genTv.setText(getString(R.string.Other));
                        }

                        if(user.getString("marital_status").equals("M")){
                            marTv.setText(getString(R.string.Married));
                        }else if(user.getString("marital_status").equals("S")){
                            marTv.setText(getString(R.string.Single));
                        }

                        if(!user.getString("about_me").equals(null) && !user.getString("about_me").equals("null")){
                            aboutTv.setText(user.getString("about_me"));
                        }else {
                            aboutTv.setText("N/A");
                        }

                        if(!user.getString("address").equals(null) && !user.getString("address").equals("null")){
                            add_tv.setText(user.getString("address"));
                        }else {
                            if(!user.getString("cityname").equals(null) && !user.getString("cityname").equals("null") && !user.getString("districtname").equals(null) && !user.getString("districtname").equals("null")){
                                add_tv.setText(user.getJSONObject("cityname").getString("city")+", "+user.getJSONObject("districtname").getString("district"));
                            }else {
                                add_tv.setText("N/A");
                            }

                        }


                        JSONObject department  = result_Object.getJSONObject("department");
                        JSONArray images  = result_Object.getJSONArray("images");

                        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+department.getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(image);
                        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+department.getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(imgPro);
                        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+department.getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(image2);
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("profile_pic", department.getString("banner_img")).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("phone", user.getString("phone_number")).commit();

                        final String id = department.getString("id");
                        prdct_title_tv.setText(department.getJSONObject("department").getString("department_name"));
                        Picasso.get().load(DEPARTMENT_BANNER_IMG_URL+department.getString("banner_img")).placeholder(R.drawable.image60).error(R.drawable.image60).resize(1024,1024).onlyScaleDown().into(prdct_iv);
                        depChurch.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent I = new Intent(ProfileActivity.this, ChurchDetailActivity.class);
                                I.putExtra("id",id);
                                startActivity(I);
                            }
                        });
                        depChurch1.setVisibility(View.GONE);

                        map_list2 = new ArrayList<>();
                        for (int i = 0; i <images.length() ; i++) {
                            JSONObject imgObj = images.getJSONObject(i);
                            HashMap<Object,Object> map4 = new HashMap<>();
                            map4.put(IMAGE,imgObj.getString("profile_image"));
                            map_list2.add(map4);
                        }
                        ChurchTwoAdapter adapter4= new ChurchTwoAdapter(ProfileActivity.this,map_list2);
                        rv_Dash2.setAdapter(adapter4);
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ProfileActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void getGuest(){
        MethodClass.showProgressDialog(ProfileActivity.this);
        String server_url = ProfileActivity.this.getString(R.string.SERVER_URL) + "member-profile";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(ProfileActivity.this, response);
                    if (result_Object != null) {
                        JSONObject user = result_Object.getJSONObject("user");
                        name_tv.setText(user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname"));
                        email_tv.setText(user.getString("email"));
                        if(!user.getString("country_code").equals(null) && !user.getString("country_code").equals("null")){
                            phone_tv.setText(user.getString("country_code")+" "+user.getString("phone_number"));
                        }else{
                            phone_tv.setText(user.getString("phone_number"));
                        }

                        Picasso.get().load(PROFILE_IMG_URL_THUMB+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image);
                        Picasso.get().load(PROFILE_IMG_URL_THUMB+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
                        Picasso.get().load(PROFILE_IMG_URL+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image2);
                        nav_name_tv.setText(getString(R.string.hello)+" "+user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname"));

                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_name", user.getString("title")+" "+user.getString("fname")+" "+user.getString("lname")).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("profile_pic", user.getString("profile_pic")).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("phone", user.getString("phone_number")).commit();

                        if(!user.getString("church_branch").equals(null) && !user.getString("church_branch").equals("null")){
                            churchTv.setText(user.getJSONObject("church_branch").getString("address"));
                        }else{
                            churchTv.setText("N/A");
                        }

                        chucrhLocTitle.setText(result_Object.getJSONObject("branch_details").getJSONObject("churchdetails").getString("name"));

                        if(user.getString("gender").equals("M")){
                            genTv.setText(getString(R.string.Male));
                        }else if(user.getString("gender").equals("F")){
                            genTv.setText(getString(R.string.Female));
                        }else if(user.getString("gender").equals("O")){
                            genTv.setText(getString(R.string.Other));
                        }else{
                            genTv.setText("N/A");
                        }

                        if(user.getString("marital_status").equals("M")){
                            marTv.setText(getString(R.string.Married));
                        }else if(user.getString("marital_status").equals("S")){
                            marTv.setText(getString(R.string.Single));
                        }else{
                            marTv.setText("N/A");
                        }

                        if(!user.getString("address").equals(null) && !user.getString("address").equals("null")){
                            add_tv.setText(user.getString("address"));
                        }else {
                            if(!user.getString("cityname").equals(null) && !user.getString("cityname").equals("null") && !user.getString("districtname").equals(null) && !user.getString("districtname").equals("null")){
                                add_tv.setText(user.getJSONObject("cityname").getString("city")+", "+user.getJSONObject("districtname").getString("district"));
                            }else {
                                add_tv.setText("N/A");
                            }
                        }
                        if(!user.getString("about_me").equals(null) && !user.getString("about_me").equals("null")){
                            aboutTv.setText(user.getString("about_me"));
                        }else {
                            aboutTv.setText("N/A");
                        }
                        JSONArray images  = result_Object.getJSONArray("images");

                        map_list2 = new ArrayList<>();
                        for (int i = 0; i <images.length() ; i++) {
                            JSONObject imgObj = images.getJSONObject(i);
                            HashMap<Object,Object> map4 = new HashMap<>();
                            map4.put(IMAGE,imgObj.getString("profile_image"));
                            map_list2.add(map4);
                        }
                        ChurchTwoAdapter adapter4= new ChurchTwoAdapter(ProfileActivity.this,map_list2);
                        rv_Dash2.setAdapter(adapter4);
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ProfileActivity.this).addToRequestQueue(jsonObjectRequest);

    }
    public void getHomeCurch(){
        MethodClass.showProgressDialog(ProfileActivity.this);
        String server_url = ProfileActivity.this.getString(R.string.SERVER_URL) + "home-church/profile";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(ProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(ProfileActivity.this, response);
                    if (result_Object != null) {
                        JSONObject user = result_Object.getJSONObject("user");
                        name_tv.setText(user.getString("fname")+" "+user.getString("lname"));
                        email_tv.setText(user.getString("email"));
                        if(!user.getString("country_code").equals(null) && !user.getString("country_code").equals("null")){
                            phone_tv.setText(user.getString("country_code")+" "+user.getString("phone_number"));
                        }else if(!user.getString("phone_number").equals(null) && !user.getString("phone_number").equals("null")){
                            phone_tv.setText(user.getString("phone_number"));
                        }else{
                            phone_tv.setText("N/A");
                        }

                        Picasso.get().load(PROFILE_IMG_URL_THUMB+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image);
                        Picasso.get().load(PROFILE_IMG_URL_THUMB+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
                        Picasso.get().load(PROFILE_IMG_URL+user.getString("profile_pic")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(image2);
                        nav_name_tv.setText(getString(R.string.hello)+" "+user.getString("fname")+" "+user.getString("lname"));

                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_name", user.getString("fname")+" "+user.getString("lname")).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("profile_pic", user.getString("profile_pic")).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("phone", user.getString("phone_number")).commit();


                        if(user.getString("gender").equals("M")){
                            genTv.setText(getString(R.string.Male));
                        }else if(user.getString("gender").equals("F")){
                            genTv.setText(getString(R.string.Female));
                        }else if(user.getString("gender").equals("O")){
                            genTv.setText(getString(R.string.Other));
                        }else{
                            genTv.setText("N/A");
                        }

                        if(user.getString("marital_status").equals("M")){
                            marTv.setText(getString(R.string.Married));
                        }else if(user.getString("marital_status").equals("S")){
                            marTv.setText(getString(R.string.Single));
                        }else{
                            marTv.setText("N/A");
                        }

                        if(!result_Object.getJSONObject("branch_data").getString("google_address").equals(null) && !result_Object.getJSONObject("branch_data").getString("google_address").equals("null")){
                            churchTv.setText(result_Object.getJSONObject("branch_data").getString("google_address"));
                            home_id = result_Object.getJSONObject("branch_data").getString("id");
                            chucrhLocTitle.setText(result_Object.getJSONObject("branch_data").getJSONObject("details").getString("home_church_name"));
                        }else {
                            churchTv.setText("N/A");
                        }
                        if(!user.getString("about_me").equals(null) && !user.getString("about_me").equals("null")){
                            aboutTv.setText(user.getString("about_me"));
                        }else {
                            aboutTv.setText("N/A");
                        }if(!user.getString("address").equals(null) && !user.getString("address").equals("null")){
                            add_tv.setText(user.getString("address"));
                        }else {
                            if(!user.getString("cityname").equals(null) && !user.getString("cityname").equals("null") && !user.getString("districtname").equals(null) && !user.getString("districtname").equals("null")){
                                add_tv.setText(user.getJSONObject("cityname").getString("city")+", "+user.getJSONObject("districtname").getString("district"));
                            }else {
                                add_tv.setText("N/A");
                            }
                        }
                        JSONArray images  = result_Object.getJSONArray("images");

                        map_list2 = new ArrayList<>();
                        for (int i = 0; i <images.length() ; i++) {
                            JSONObject imgObj = images.getJSONObject(i);
                            HashMap<Object,Object> map4 = new HashMap<>();
                            map4.put(IMAGE,imgObj.getString("profile_image"));
                            map_list2.add(map4);
                        }
                        ChurchTwoAdapter adapter4= new ChurchTwoAdapter(ProfileActivity.this,map_list2);
                        rv_Dash2.setAdapter(adapter4);
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(ProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(ProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(ProfileActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void back(View view){
        if(isTaskRoot()){
            if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
                startActivity(new Intent(this,DashboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
                startActivity(new Intent(this,GuestDashboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
                startActivity(new Intent(this,HomeChurchDahboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
                startActivity(new Intent(this,ChurchDepartmentDashboardActivity.class));
                finish();
            }

        }else{
            super.onBackPressed();
        }
    }
    public void edit(View view){
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            Intent I = new Intent(this,MemberNewEditProfileActivity.class);
            startActivity(I);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            Intent I = new Intent(this,MemberNewEditProfileActivity.class);
            startActivity(I);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            Intent I = new Intent(this, UpdateChurchDetailsActivity.class);
            I.putExtra("home_church_id",home_id);
            startActivity(I);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            Intent I = new Intent(this, DepartmentUpdateProfileActivity.class);
            startActivity(I);
        }
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
}
