package com.dunamis1.dunamis1.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import pl.droidsonroids.gif.GifImageView;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.dunamis1.dunamis1.Activity.EventListing.EventDetailsActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicDetailsActivity;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.dunamis1.dunamis1.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.USER_LANG;
import static com.dunamis1.dunamis1.Helper.Constant.USER_LAT;

public class SplashActivity extends AppCompatActivity implements  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private String id = "",type = "";
    private Boolean is_logged_in = false;
    private GifImageView gifView;
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    private AlertDialog ExitApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(SplashActivity.this);
        setContentView(R.layout.activity_splash);

        gifView = findViewById(R.id.gifView);
        Glide.with(SplashActivity.this).asGif()
                .load(R.drawable.splash)
                .into(gifView);
        final Handler handler = new Handler();

        Uri data = this.getIntent().getData();


        if (data != null && data.isHierarchical()) {
            Uri uri = Uri.parse(data.toString());
            id = uri.getQueryParameter("content");
            type = uri.getQueryParameter("type");
        }

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getBoolean("is_logged_in",false);

        setUpGClient();

    }
    public void startActivity(){
        if(type.equals("S")){
            MethodClass.showProgressDialog(SplashActivity.this);
            String server_url = SplashActivity.this.getString(R.string.SERVER_URL) + "share/update";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("LANG","en"));
            params.put("id", id);
            params.put("type",type);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("resp", response.toString());
                    try{
                        MethodClass.hideProgressDialog(SplashActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(SplashActivity.this, response);
                        if (result_Object != null) {
                            String seedArray = result_Object.getString("total_share");

                            if(is_logged_in){
                                Intent I =new Intent(SplashActivity.this,DestinyVideoDetails.class);
                                I.putExtra("id",id);
                                I.putExtra("from","Splash");
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }else{
                                Intent I =new Intent(SplashActivity.this,LanguageActivity.class);
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(SplashActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(SplashActivity.this);

                    Log.e("error", error.toString());
                    if (error.toString().contains("AuthFailureError")) {
                        Toast.makeText(SplashActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SplashActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            MySingleton.getInstance(SplashActivity.this).addToRequestQueue(jsonObjectRequest);


        }else if(type.equals("M")){
            MethodClass.showProgressDialog(SplashActivity.this);
            String server_url = SplashActivity.this.getString(R.string.SERVER_URL) + "share/update";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("LANG","en"));
            params.put("id", id);
            params.put("type",type);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("resp", response.toString());
                    try{
                        MethodClass.hideProgressDialog(SplashActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(SplashActivity.this, response);
                        if (result_Object != null) {
                            String seedArray = result_Object.getString("total_share");

                            if(is_logged_in){
                                Intent I =new Intent(SplashActivity.this, MusicDetailsActivity.class);
                                I.putExtra("id",id);
                                I.putExtra("from","Splash");
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }else{
                                Intent I =new Intent(SplashActivity.this,LanguageActivity.class);
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(SplashActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(SplashActivity.this);

                    Log.e("error", error.toString());
                    if (error.toString().contains("AuthFailureError")) {
                        Toast.makeText(SplashActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SplashActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            MySingleton.getInstance(SplashActivity.this).addToRequestQueue(jsonObjectRequest);


        }else if(type.equals("E")){
            MethodClass.showProgressDialog(SplashActivity.this);
            String server_url = SplashActivity.this.getString(R.string.SERVER_URL) + "share/update";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("language", PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("LANG","en"));
            params.put("id", id);
            params.put("type",type);
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("resp", response.toString());
                    try{
                        MethodClass.hideProgressDialog(SplashActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(SplashActivity.this, response);
                        if (result_Object != null) {
                            String seedArray = result_Object.getString("total_share");

                            if(is_logged_in){
                                Intent I =new Intent(SplashActivity.this, EventDetailsActivity.class);
                                I.putExtra("id",id);
                                I.putExtra("save","NO");
                                I.putExtra("from","Splash");
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }else{
                                Intent I =new Intent(SplashActivity.this,LanguageActivity.class);
                                I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(I);
                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(SplashActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    MethodClass.hideProgressDialog(SplashActivity.this);

                    Log.e("error", error.toString());
                    if (error.toString().contains("AuthFailureError")) {
                        Toast.makeText(SplashActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SplashActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            MySingleton.getInstance(SplashActivity.this).addToRequestQueue(jsonObjectRequest);


        }
        else if(PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("role","").equals("Member")){
            Intent I =new Intent(SplashActivity.this,DashboardActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
        }else if(PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("role","").equals("Guest")){
            Intent I =new Intent(SplashActivity.this,GuestDashboardActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
        }else if(PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("role","").equals("Home")){
            Intent I =new Intent(SplashActivity.this,HomeChurchDahboardActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
        }else if(PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("role","").equals("Department")){
            Intent I =new Intent(SplashActivity.this,ChurchDepartmentDashboardActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
        }else{
            Intent I =new Intent(SplashActivity.this,LanguageActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(I);
        }
    }
    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude=mylocation.getLatitude();
            Double longitude=mylocation.getLongitude();
            USER_LAT = latitude;
            USER_LANG = longitude;
            //startMyActivity();
            //Toast.makeText(SplashActivity.this, "CHANGED "+USER_LAT.toString()+" // "+USER_LANG.toString(), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        checkPermissions();
    }
    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }
    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(300000);
                    locationRequest.setFastestInterval(300000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, (com.google.android.gms.location.LocationListener) this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(SplashActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //Do something after 100ms
                                                //Toast.makeText(SplashActivity.this, "FINE2 "+mylocation.getLatitude(), Toast.LENGTH_SHORT).show();
                                                Double latitude=mylocation.getLatitude();
                                                Double longitude=mylocation.getLongitude();
                                                USER_LAT = latitude;
                                                USER_LANG = longitude;
                                                if(USER_LAT!=null && USER_LANG!=null){
                                                   startActivity();
                                                }
                                            }
                                        }, 5000);
                                        //Toast.makeText(SplashActivity.this, "FINE2 "+USER_LAT.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.

                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(SplashActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });


                }
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        shoExitDialog();
                        break;
                }
                break;
        }
    }
    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
        }

    }
    private void shoExitDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setMessage("Without the location service you will not be able to use this application." +
                " Please give permission to this application to access your location and turn on your location service" +
                " to enjoy the exclusive features of this application.")
                .setTitle("Exit Application");
        builder.setCancelable(false);

        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                finish();
            }
        });
        ExitApplication = builder.create();
        ExitApplication.show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }else{

            shoExitDialog();
        }
    }
}
