package com.dunamis1.dunamis1.Activity.EventListing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.Activity.ChurchDepartment.ChurchPhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ChurchDepartmentDashboardActivity;
import com.dunamis1.dunamis1.Activity.DashboardActivity;
import com.dunamis1.dunamis1.Activity.GuestDashboardActivity;
import com.dunamis1.dunamis1.Activity.HomeChurchDahboardActivity;
import com.dunamis1.dunamis1.Adapter.EventsAdapter;
import com.dunamis1.dunamis1.Helper.MySingleton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dunamis1.dunamis1.Activity.AboutUsActivity;
import com.dunamis1.dunamis1.Activity.MyFavouriteActivity;
import com.dunamis1.dunamis1.Activity.Bible.BibleVersionsActivity;
import com.dunamis1.dunamis1.Activity.ContactUsActivity;
import com.dunamis1.dunamis1.Activity.DunamisChurch;
import com.dunamis1.dunamis1.Activity.DunamisSchoolListActivity;
import com.dunamis1.dunamis1.Activity.DunamisTvActivity;
import com.dunamis1.dunamis1.Activity.ForeignerDeskOneActivity;
import com.dunamis1.dunamis1.Activity.GivingActivity;
import com.dunamis1.dunamis1.Activity.InviteFriendActivity;
import com.dunamis1.dunamis1.Activity.Message.MessageListActivity;
import com.dunamis1.dunamis1.Activity.Music.MusicListActivity;
import com.dunamis1.dunamis1.Activity.NotificationActivity;
import com.dunamis1.dunamis1.Activity.PhotoGalleryActivity;
import com.dunamis1.dunamis1.Activity.ProfileActivity;
import com.dunamis1.dunamis1.Activity.SeedsOfDestinyActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.dunamis1.dunamis1.Activity.TestimonyVideo;
import com.dunamis1.dunamis1.Adapter.EventPhotoAdapter;
import com.dunamis1.dunamis1.Helper.MethodClass;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.dunamis1.dunamis1.Helper.Constant.DESC;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ADD;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_COMMENT;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_DATE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_ID;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_IMG_URL;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_LIKE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SAVE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_SHARE;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.EVENT_TITLE;
import static com.dunamis1.dunamis1.Helper.Constant.FROM_TIME;
import static com.dunamis1.dunamis1.Helper.Constant.IMG;
import static com.dunamis1.dunamis1.Helper.Constant.PROFILE_IMG_URL_THUMB;
import static com.dunamis1.dunamis1.Helper.Constant.TO_TIME;

public class EventDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    int[] pic_array = {R.drawable.mummy, R.drawable.daddy, R.drawable.mummy};
    private RecyclerView events_rv,photo_rv;
    private ArrayList<HashMap<String, String>> map_list;
    private ArrayList<HashMap<String, String>> photo_map_list;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerView ;
    private TextView nav_name_tv;
    private CircleImageView imgPro;

    private ImageView eventImg;
    private ImageView fav;
    private TextView date;
    private TextView title;
    private Button register;
    private TextView like_tv;
    private TextView desc;
    private TextView msg_tv;
    private TextView share_tv;
    private TextView eventHead;
    private TextView location;
    private TextView locations;
    private TextView time;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;

    private double latitude,longitude;
    private  String map_title;
    private LinearLayout getDirectionBtn;

    private NestedScrollView scrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        events_rv = (RecyclerView) findViewById(R.id.events_rv);
        photo_rv = (RecyclerView) findViewById(R.id.photo_rv);
        //rv_Dash4 = (RecyclerView) findViewById(R.id.rv_Dash4);
        scrollView = findViewById(R.id.scrollView);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(R.color.transYellow));
        TextView role_tv = headerView.findViewById(R.id.role_tv);
        LinearLayout photoLay = headerView.findViewById(R.id.photoLay);
        imgPro = headerView.findViewById(R.id.imgPro);
        nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
            role_tv.setText("Member");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
            role_tv.setText("Guest");
            photoLay.setVisibility(View.GONE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
            role_tv.setText("Home Church Admin");
            photoLay.setVisibility(View.VISIBLE);
        }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
            role_tv.setText("Department Admin");
            photoLay.setVisibility(View.VISIBLE);
        }
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        getDirectionBtn = (LinearLayout) findViewById(R.id.getDirectionBtn);
        eventImg = findViewById(R.id.eventImg);
        fav = findViewById(R.id.fav);
        title = findViewById(R.id.title);
        date = findViewById(R.id.date);
        desc = findViewById(R.id.desc);
        register = findViewById(R.id.register);
        like_tv = findViewById(R.id.like_tv);
        msg_tv = findViewById(R.id.msg_tv);
        share_tv = findViewById(R.id.share_tv);
        eventHead = findViewById(R.id.eventHead);
        location = findViewById(R.id.location);
        locations = findViewById(R.id.locations);
        time = findViewById(R.id.time);

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = getString(R.string.SERVER_URL) + "favourite";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", getIntent().getStringExtra("id"));
                params.put("type", "E");
                params.put("language",PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(EventDetailsActivity.this, response);
                            if (result_Object != null) {
                                String favourite = result_Object.getString("favourite");
                                if(favourite.equals("Y")){
                                    fav.setImageResource(R.drawable.ic_heart);
                                }else{
                                    fav.setImageResource(R.drawable.ic_heart_blank);
                                }
                                String message = result_Object.getString("message");
                                String meaning = result_Object.getString("meaning");
                                new SweetAlertDialog(EventDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(message)
                                        .setContentText(meaning)
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(EventDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EventDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(EventDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
            }
        });
        share_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "http://phpwebdevelopmentservices.com/development/dunamis/api/share?content="+getIntent().getStringExtra("id")+"&language="+ PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("lang","en")+"&type=E");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        msg_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(EventDetailsActivity.this, CommentsActivity.class);
                I.putExtra("from","event");
                I.putExtra("save",getIntent().getStringExtra("save"));
                I.putExtra("id",getIntent().getStringExtra("id"));
                startActivity(I);
            }
        });
        like_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = getString(R.string.SERVER_URL) + "event/like";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("event_id", getIntent().getStringExtra("id"));
                params.put("language",PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(EventDetailsActivity.this, response);
                            if (result_Object != null) {
                                String total_likes = result_Object.getString("total_likes");
                                like_tv.setText(total_likes);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(EventDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EventDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(EventDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String server_url = getString(R.string.SERVER_URL) + "event/add-interest";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("event_id", getIntent().getStringExtra("id"));
                params.put("language",PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("LANG","en"));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            JSONObject result_Object = MethodClass.get_result_from_webservice(EventDetailsActivity.this, response);
                            if (result_Object != null) {
                                register.setVisibility(View.VISIBLE);
                                register.setClickable(false);
                                register.setOnClickListener(null);
                                register.setText("Interest registered already");
                                String message = result_Object.getString("message");
                                String meaning = result_Object.getString("meaning");
                                new SweetAlertDialog(EventDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(message)
                                        .setContentText(meaning)
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(EventDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EventDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(EventDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
            }
        });
        getDirectionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mapss = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + map_title + ")";
                Toast.makeText(EventDetailsActivity.this, mapss, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(mapss));
                startActivity(i);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();

        get_events_list();
        Picasso.get().load(PROFILE_IMG_URL_THUMB+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_pic","")).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).resize(1024,1024).onlyScaleDown().into(imgPro);
        nav_name_tv.setText(getString(R.string.hello)+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("user_name",""));

    }
    public void back(View view) {
        if(isTaskRoot()){
            if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Member")){
                startActivity(new Intent(this, DashboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Guest")){
                startActivity(new Intent(this, GuestDashboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Home")){
                startActivity(new Intent(this, HomeChurchDahboardActivity.class));
                finish();
            }else if(PreferenceManager.getDefaultSharedPreferences(this).getString("role","Member").equals("Department")){
                startActivity(new Intent(this, ChurchDepartmentDashboardActivity.class));
                finish();
            }

        }else{
            super.onBackPressed();
        }
    }

    private void get_events_list() {

        MethodClass.showProgressDialog(EventDetailsActivity.this);
        String server_url = EventDetailsActivity.this.getString(R.string.SERVER_URL) + "event/details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("language", PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("LANG","en"));
        params.put("event_id",getIntent().getStringExtra("id"));
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(EventDetailsActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(EventDetailsActivity.this, response);
                    if (result_Object != null) {

                        String favourites = result_Object.getString("favourite");
                        JSONObject sedddetails = result_Object.getJSONObject("details");

                        if(!sedddetails.getString("favourite").equals("null")){
                            register.setVisibility(View.VISIBLE);
                            register.setClickable(false);
                            register.setOnClickListener(null);
                            register.setText("Interest registered already");
                            msg_tv.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(EventDetailsActivity.this, CommentsActivity.class);
                                    I.putExtra("from","event");
                                    I.putExtra("save","YES");
                                    I.putExtra("id",getIntent().getStringExtra("id"));
                                    startActivity(I);
                                }
                            });
                        }else {
                            register.setVisibility(View.VISIBLE);
                            msg_tv.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(EventDetailsActivity.this, CommentsActivity.class);
                                    I.putExtra("from","event");
                                    I.putExtra("save","NO");
                                    I.putExtra("id",getIntent().getStringExtra("id"));
                                    startActivity(I);
                                }
                            });
                        }

                        title.setText(sedddetails.getJSONObject("eventdetails").getString("title"));
                        eventHead.setText(sedddetails.getJSONObject("eventdetails").getString("title"));
                        desc.setText(sedddetails.getJSONObject("eventdetails").getString("description"));
                        final String thumbnail = sedddetails.getString("thumbnail");
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date datess = null;
                        try {
                            datess = dateFormatter.parse(sedddetails.getString("from_date"));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        location.setText(getString(R.string.venue)+": "+sedddetails.getString("venue"));
                        locations.setText(sedddetails.getString("google_address"));

                        SimpleDateFormat dateFormatterss = new SimpleDateFormat("hh:mm:ss");
                        Date datesa = null;
                        Date datesa2 = null;
                        try {
                            datesa = dateFormatterss.parse(sedddetails.getString("from_time"));
                            datesa2 = dateFormatterss.parse(sedddetails.getString("to_time"));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        // Get time from date
                        SimpleDateFormat timeFormatterss = new SimpleDateFormat("hh:mm a");
                        String frmTime = timeFormatterss.format(datesa);
                        String toTime = timeFormatterss.format(datesa2);

                        time.setText(getString(R.string.timing)+": "+frmTime+" to "+toTime);

                        // Get time from date
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("dd MMM yyyy");
                        String displayValue = timeFormatter.format(datess);
                        SimpleDateFormat format = new SimpleDateFormat("d");
                        String datessa = format.format(datess);

                        if(datessa.endsWith("1") && !datessa.endsWith("11"))
                            format = new SimpleDateFormat("d'st' MMM, yyyy");
                        else if(datessa.endsWith("2") && !datessa.endsWith("12"))
                            format = new SimpleDateFormat("d'nd' MMM, yyyy");
                        else if(datessa.endsWith("3") && !datessa.endsWith("13"))
                            format = new SimpleDateFormat("d'rd' MMM, yyyy");
                        else
                            format = new SimpleDateFormat("d'th' MMM, yyyy");

                        String yourDate = format.format(datess);
                        date.setText(yourDate);
                        latitude=sedddetails.getDouble("latitude");
                        longitude=sedddetails.getDouble("longitude");
                        map_title=sedddetails.getJSONObject("eventdetails").getString("title");

                        mapFragment.getMapAsync(EventDetailsActivity.this);
                        Picasso.get().load(EVENT_IMG_URL+thumbnail).placeholder(R.drawable.february_worship__word_and_wonders_night).error(R.drawable.february_worship__word_and_wonders_night).resize(1024,1024).onlyScaleDown().into(eventImg);

                        msg_tv.setText(sedddetails.getString("total_comments")+" "+getString(R.string.comm));
                        like_tv.setText(sedddetails.getString("total_likes")+" "+getString(R.string.likes));
                        share_tv.setText(sedddetails.getString("total_share")+" "+getString(R.string.shar));

                        if(favourites.equals("Y")){
                            fav.setImageResource(R.drawable.ic_heart);
                        }else{
                            fav.setImageResource(R.drawable.ic_heart_blank);
                        }


                        JSONArray seedArray = result_Object.getJSONArray("recent");

                        map_list = new ArrayList<>();

                        for (int i = 0; i <seedArray.length() ; i++) {
                            JSONObject SeedObj = seedArray.getJSONObject(i);
                            String title = SeedObj.getJSONObject("eventdetails").getString("title");
                            String google_address = SeedObj.getString("google_address");
                            String thumbnailsss = SeedObj.getString("thumbnail");
                            String date = SeedObj.getString("from_date");
                            String total_likes = SeedObj.getString("total_likes");
                            String total_comments = SeedObj.getString("total_comments");
                            String total_share = SeedObj.getString("total_share");
                            String id = SeedObj.getString("id");
                            String favourite = SeedObj.getString("favourite");

                            HashMap<String,String> map = new HashMap<>();
                            map.put(EVENT_TITLE,title);
                            map.put(EVENT_THUMB,thumbnailsss);
                            map.put(EVENT_DATE,date);
                            map.put(EVENT_ADD,google_address);
                            map.put(EVENT_LIKE,total_likes);
                            map.put(EVENT_COMMENT,total_comments);
                            map.put(EVENT_SHARE,total_share);
                            map.put(EVENT_ID,id);
                            if(favourite.equals(null) || favourite.equals("null"))
                            {
                                map.put(EVENT_SAVE,"NO");
                            }else{
                                map.put(EVENT_SAVE,"YES");
                            }

                            map_list.add(map);
                        }
                        EventsAdapter bibleDeuteronomyAdapter= new EventsAdapter(EventDetailsActivity.this,map_list);
                        events_rv.setAdapter(bibleDeuteronomyAdapter);
                        events_rv.setFocusable(false);

                        JSONArray photo = result_Object.getJSONArray("photos");
                        photo_map_list = new ArrayList<HashMap<String, String>>();
                        for (int i = 0; i <photo.length(); i++) {
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put(IMG, photo.getJSONObject(i).getString("image"));
                            photo_map_list.add(map);
                        }
                        EventPhotoAdapter eventPhotoAdapter = new EventPhotoAdapter(EventDetailsActivity.this, photo_map_list);
                        photo_rv.setAdapter(eventPhotoAdapter);
                        photo_rv.setFocusable(false);
                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(EventDetailsActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(EventDetailsActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(EventDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EventDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EventDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(EventDetailsActivity.this).addToRequestQueue(jsonObjectRequest);


    }

    public void comment(View view) {
        MethodClass.go_to_next_activity(this,CommentsActivity.class);
    }
    public void openMenu(View view){
        drawer.openDrawer(GravityCompat.START);
    }
    public void testi(View view){
        Intent I = new Intent(this, TestimonyVideo.class);
        startActivity(I);
    }
    public void seeds(View view){
        Intent I = new Intent(this, SeedsOfDestinyActivity.class);
        startActivity(I);
    }
    public void Profile(View view){
        Intent I = new Intent(this, ProfileActivity.class);
        I.putExtra("from","M");
        startActivity(I);
    }
    public void dunamisSchool(View view){
        Intent I = new Intent(this, DunamisSchoolListActivity.class);
        startActivity(I);
    }
    public void dunamisTv(View view){
        Intent I = new Intent(this, DunamisTvActivity.class);
        startActivity(I);
    }
    public void nearMe(View view){
        Intent I = new Intent(this, DunamisChurch.class);
        startActivity(I);
    }
    public void noti(View view){
        Intent I = new Intent(this, NotificationActivity.class);
        startActivity(I);
    }public void  event(View view){
        Intent I = new Intent(this, EventListingActivity.class);
        startActivity(I);
    }
    public void  bible(View view){
        Intent I = new Intent(this, BibleVersionsActivity.class);
        startActivity(I);
    } public void  mesg(View view){
        Intent I = new Intent(this, MessageListActivity.class);
        startActivity(I);
    }public void  store(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://drpaulenenche.org/store/"));
        startActivity(intent);
    }
    public void  music(View view){
        Intent I = new Intent(this, MusicListActivity.class);
        startActivity(I);
    }
    public void  foreigner(View view){
        Intent I = new Intent(this, ForeignerDeskOneActivity.class);
        startActivity(I);
    }
    public void  about(View view){
        Intent I = new Intent(this, AboutUsActivity.class);
        startActivity(I);
    }
    public void  invite(View view){
        Intent I = new Intent(this, InviteFriendActivity.class);
        startActivity(I);
    }
    public void  giving(View view){
        Intent I = new Intent(this, GivingActivity.class);
        startActivity(I);
    }public void  photo(View view){
        Intent I = new Intent(this, PhotoGalleryActivity.class);
        startActivity(I);
    }public void  contact(View view){
        Intent I = new Intent(this, ContactUsActivity.class);
        startActivity(I);
    }
    public void  churchphoto(View view){
        Intent I = new Intent(this, ChurchPhotoGalleryActivity.class);
        startActivity(I);
    }
public void  logout(View view){
        Intent I = new Intent(this, SplashActivity.class);
PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        startActivity(I);
    }
public void  favour(View view){
        Intent I = new Intent(this, MyFavouriteActivity.class);
        startActivity(I);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(latitude, longitude);
      /*  String url = getDirectionsUrl(latLng,another_latLng);
        DownloadTask downloadTask = new DownloadTask();
// Start downloading json data from Google Directions API
        downloadTask.execute(url);*/
        mMap.addMarker(new MarkerOptions().position(latLng).title(map_title));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
    }
}
