package com.dunamis1.dunamis1.Helper;

import java.util.ArrayList;

public class Constant {

    public static final String SHARED_PREF = "ah_firebase";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static Double USER_LAT = null;
    public static Double USER_LANG = null;

    public static final  Integer IMAGE=0;
    public static final  String TITLE="TITLE";
    public static String NOTI_TITLE="";
    public static final  String IMGG="IMGG";
    public static final  String HOME_ID="HOME_ID";
    public static final  String PHOTO_ARRAY="PHOTO_ARRAY";
    public static final  String DESC="DESC";
    public static final  String SOCIAL="SOCIAL";
    public static final  String NAME="NAME";
    public static final  String MEMBER="MEMBER";
    public static final  String VIEWS="VIEWS";
    public static final  String DATE="DATE";
    public static final  String VID_IMG="VID_IMG";
    public static String ABOUT ="ABOUT";
    public static String ADDRESS ="ADDRESS";
    public static String RADIUS ="";
    public static String FROMDATE ="";
    public static String TODATE ="";
    public static String EVENTNAME ="";
    public static String IMG ="IMG";
    public static String DAYS ="DAYS";
    public static String FROM_TIME ="FROM_TIME";
    public static String TO_TIME ="TO_TIME";

    public static String ANS_DATE ="ANS_DATE";
    public static String QUES_DATE ="QUES_DATE";
    public static final int MY_PERMISSIONS_REQUEST_CAMERA=100;
    public static final int TAKE_PHOTO_CODE = 200;
    public static final int TAKE_PHOTO_CODE2 = 500;
    public static final int PICK_FROM_GALLERY = 300;
    public static final int PICK_FROM_GALLERY_OREO = 400;

    public static ArrayList<Integer> USER_SELECTED_MEMBER;

    public static final  String PROFILE_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/user/profile/";
    public static final  String PROFILE_IMG_URL_THUMB="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/user/profile/thumb/";
    public static final  String MUSIC_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/music/thumb/";
    public static final  String MUSIC_FILE_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/music/";
    public static final  String EVENT_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/event/thumb/";
    public static final  String EVENT_IMG_URL2="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/event/";
    public static final  String HOME_BANNER_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/banner/home_church/thumb/";
    public static final  String THUMB_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/music/thumb";
    public static final  String HOME_CHURCH_THUMB_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/home_church/thumb/";
    public static final  String DEPARTMENT_THUMB_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/church_department/thumb/";
    public static final  String DEPARTMENT_BANNER_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/banner/church_department/thumb/";
    public static final  String BRANCH_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/banner/church_branch/";
    public static final  String BRANCH_PHOTO_IMG_URL="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/church_branch/thumb/";
    public static final  String SCHOOL_BANNER_IMG="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/banner/school/thumb/";
    public static final  String TESTIMONY_USER_IMG="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/testimonial/thumb/";
    public static final  String ABT_BANNER_IMG="http://phpwebdevelopmentservices.com/development/dunamis/storage/uploads/banner/about/thumb/";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    //seed constant
    public static final  String SEED_TITLE="SEED_TITLE";
    public static final  String SEED_DESC="SEED_DESC";
    public static final  String SEED_THUMB="SEED_THUMB";
    public static final  String SEED_CODE="SEED_CODE";
    public static final  String SEED_DATE="SEED_DATE";
    public static final  String SEED_LIKE="SEED_LIKE";
    public static final  String SEED_COMMENT="SEED_COMMENT";
    public static final  String SEED_SHARE="SEED_SHARE";
    public static final  String SEED_ID="SEED_ID";


    public static final  String BIBLE_ID="BIBLE_ID";
    public static final  String BIBLE_ABBR="BIBLE_ABBR";
    public static final  String BIBLE_NAME="BIBLE_NAME";

    //music constant
    public static final  String MUSIC_TITLE="MUSIC_TITLE";
    public static final  String MUSIC_DESC="MUSIC_DESC";
    public static final  String MUSIC_THUMB="MUSIC_THUMB";
    public static final  String MUSIC_CODE="MUSIC_CODE";
    public static final  String MUSIC_TYPE="MUSIC_TYPE";
    public static final  String MUSIC_LIKE="MUSIC_LIKE";
    public static final  String MUSIC_COMMENT="MUSIC_COMMENT";
    public static final  String MUSIC_SHARE="MUSIC_SHARE";
    public static final  String MUSIC_ID="MUSIC_ID";

    //school constant
    public static final  String SCH_TITLE="SCH_TITLE";
    public static final  String SCH_DESC="SCH_DESC";
    public static final  String SCH_THUMB="SCH_THUMB";
    public static final  String SCH_CODE="SCH_CODE";
    public static final  String SCH_TYPE="SCH_TYPE";
    public static final  String SCH_LIKE="SCH_LIKE";
    public static final  String SCH_COMMENT="SCH_COMMENT";
    public static final  String SCH_SHARE="SCH_SHARE";
    public static final  String SCH_ID="SCH_ID";

    //event constant
    public static final  String EVENT_TITLE="EVENT_TITLE";
    public static final  String EVENT_DESC="EVENT_DESC";
    public static final  String EVENT_THUMB="EVENT_THUMB";
    public static final  String EVENT_ADD="EVENT_ADD";
    public static final  String EVENT_DATE="EVENT_DATE";
    public static final  String EVENT_LIKE="EVENT_LIKE";
    public static final  String EVENT_COMMENT="EVENT_COMMENT";
    public static final  String EVENT_SHARE="EVENT_SHARE";
    public static final  String EVENT_ID="EVENT_ID";
    public static final  String EVENT_SAVE="EVENT_SAVE";

    public static final String COMMENT = "COMMENT";
    public static final String COMM_DATE = "COMM_DATE";
    public static final String COMM_USER = "COMM_USER";
    public static final String COMM_PIC = "COMM_PIC";

    public static final String MESSAGE_ID = "MESSAGE_ID";
    public static final String MSG_SND_ID = "MSG_SND_ID";
    public static final String MSG_SND_PIC = "MSG_SND_PIC";
    public static final String MSG_SND_NAME = "MSG_SND_NAME";
    public static final String MSG_REC_ID = "MSG_REC_ID";
    public static final String MSG_REC_PIC = "MSG_REC_PIC";
    public static final String MSG_REC_NAME = "MSG_REC_NAME";
    public static final String MSG = "MSG";
    public static final String MSG_DAT = "MSG_DAT";


    public static final String FNAME = "FNAME";
    public static final String LNAME = "LNAME";
    public static final String EMAIL = "EMAIL";
    public static final String PHONE = "PHONE";
    public static final String GENDER = "GENDER";
    public static final String AGE = "AGE";
    public static final String MARITAL = "MARITAL";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String APPROVED = "APPROVED";


    public static String MAP_LAT = "MAP_LAT";
    public static String MAP_LANG = "MAP_LANG";
    public static String MAP_TITLE = "MAP_TITLE";
    public static String FROMNOTI = "";
    public static String ID = "";
    public static String MESSAGE = "";
    public static String BRANCH_ID = "";

    public static String ABBR = "";
    public static String BOOK = "";
    public static String CHAPTER = "";
    public static String VERSE = "";

}
