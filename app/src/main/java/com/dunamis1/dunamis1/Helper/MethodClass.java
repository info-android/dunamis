package com.dunamis1.dunamis1.Helper;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;

import com.dunamis1.dunamis1.Activity.Music.MusicDetailsActivity;
import com.dunamis1.dunamis1.Activity.SplashActivity;
import com.google.android.material.navigation.NavigationView;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.dunamis1.dunamis1.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.dunamis1.dunamis1.Helper.Constant.MUSIC_FILE_URL;


public class MethodClass {

    public static NavigationView nav_view;
    public static LinearLayout profile_layout,login_layout, book_mark_layout, sign_up_layout, noti_layout, payment_layout, food_order_layout,
            table_book_layout, about_layout, logout_layout;
    public static LinearLayout login_line, sign_up_line;
    public static ImageView login_iv, sign_up_iv, book_mark_iv, noti_iv, payment_iv, food_order_iv, table_book_iv, about_iv, logout_iv;
    public static TextView login_tv, sign_up_tv, book_mark_tv, noti_tv, payment_tv, food_order_tv, table_book_tv, about_tv, logout_tv;
    public static boolean is_logged_in = false;

    public static View header_layout;

    private boolean isStarted;
    static boolean pauses = false;
    static int resumePosition;

    public static long elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds;
    public static void set_locale(Activity activity){
        String languageToLoad = PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","en"); // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        activity.getBaseContext().getResources().updateConfiguration(config,
                activity.getBaseContext().getResources().getDisplayMetrics());
    }
    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public static void set_status_bar_transparent(Activity activity) {
        //make translucent statusBar on kitkat devices
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(activity, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(activity, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

    }

    public static void go_to_next_activity(Activity activity, Class next_activity) {
        activity.startActivity(new Intent(activity, next_activity));
        //Animatoo.animateZoom(activity);

    }

    public static void back_animation(Activity activity) {
        //Animatoo.animateSlideLeft(activity);
        activity.finish();

    }

    public static void hide_keyboard(Activity activity) {
        (activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public static String getMimeType(Uri uri, Context context) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }







    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static String getRightAngleImage(String photoPath) {

        try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int degree = 0;

            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    degree = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    degree = 0;
                    break;
                default:
                    degree = 90;
            }

            return rotateImage(degree, photoPath);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return photoPath;
    }

    public static String rotateImage(int degree, String imagePath) {

        if (degree <= 0) {
            return imagePath;
        }
        try {
            Bitmap b = BitmapFactory.decodeFile(imagePath);

            Matrix matrix = new Matrix();
            if (b.getWidth() > b.getHeight()) {
                matrix.setRotate(degree);
                b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(),
                        matrix, true);
            }

            FileOutputStream fOut = new FileOutputStream(imagePath);
            String imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
            String imageType = imageName.substring(imageName.lastIndexOf(".") + 1);

            FileOutputStream out = new FileOutputStream(imagePath);
            if (imageType.equalsIgnoreCase("png")) {
                b.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else if (imageType.equalsIgnoreCase("jpeg") || imageType.equalsIgnoreCase("jpg")) {
                b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
            fOut.flush();
            fOut.close();

            b.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagePath;
    }
/*
    public static String get_reg_id(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(SHARED_PREF, 0);
        String reg_id=pref.getString("regId","");
        return reg_id;
    }
*/

    public static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
        }
        return vId;
    }


    public static void diff_date(long different) {
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;


        elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        elapsedSeconds = different / secondsInMilli;
    }


    public static String get_time_duration(String compare_date) {
        String duration = "";

        String inputPattern_date = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat inputFormat_date = new SimpleDateFormat(inputPattern_date);
        Date date1 = null;
        try {
            date1 = inputFormat_date.parse(compare_date);
            Log.e("date1", String.valueOf(date1));
            Calendar calendar = Calendar.getInstance();
            Date date2 = Calendar.getInstance().getTime();
            Log.e("date2", String.valueOf(date2));

            if (date2.equals(date1)) {
                long different = System.currentTimeMillis() - date1.getTime();
                Log.e("diff", String.valueOf(different));//result in millis


                diff_date(different);
                if (elapsedDays == 0) {

                    if (elapsedHours == 0) {

                        if (elapsedMinutes == 0) {

                            if (elapsedSeconds == 0) {
                                duration = "Just Now";

                            } else if (elapsedSeconds == 1) {
                                duration = elapsedSeconds + " second ago";
                            } else {
                                duration = elapsedSeconds + " seconds ago";
                            }

                        } else if (elapsedMinutes == 1) {
                            duration = elapsedMinutes + " minute ago";
                        } else {
                            duration = elapsedMinutes + " minutes ago";
                        }

                    } else if (elapsedHours == 1) {
                        duration = elapsedHours + " hour ago";
                    } else {
                        duration = elapsedHours + " hours ago";
                    }

                } else {
                    if (elapsedDays == 1) {
                        duration = elapsedDays + " day ago";
                    } else if (elapsedDays > 2) {
                        String inputPattern = "yyyy-MM-dd HH:mm:ss";
                        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);

                        Date date = null;
                        try {
                            date = inputFormat.parse(compare_date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
                        String day = (String) DateFormat.format("dd", date); // 20
                        String monthString = (String) DateFormat.format("MMM", date); // Jun
                        String monthNumber = (String) DateFormat.format("MM", date); // 06
                        String year = (String) DateFormat.format("yyyy", date); // 2013
                        String mint = (String) DateFormat.format("mm", date); // 2013
                        String h = (String) DateFormat.format("h", date); // 2013
                        String am = (String) DateFormat.format("a", date); // 2013


                        duration = day + " " + monthString + "," + year + " | " + h + ":" + mint + " " + am;
                    } else {
                        duration = elapsedDays + " days ago";
                    }
                }

            } else if (date1.before(date2)) {
                long different = System.currentTimeMillis() - date1.getTime();
                Log.e("diff", String.valueOf(different));//result in millis
                diff_date(different);
                if (elapsedDays == 0) {

                    if (elapsedHours == 0) {

                        if (elapsedMinutes == 0) {

                            if (elapsedSeconds == 0) {
                                duration = "A moment ago";

                            } else if (elapsedSeconds == 1) {
                                duration = elapsedSeconds + " second ago";
                            } else {
                                duration = elapsedSeconds + " seconds ago";
                            }

                        } else if (elapsedMinutes == 1) {
                            duration = elapsedMinutes + " minute ago";
                        } else {
                            duration = elapsedMinutes + " minutes ago";
                        }

                    } else if (elapsedHours == 1) {
                        duration = elapsedHours + " hour ago";
                    } else {
                        duration = elapsedHours + " hours ago";
                    }

                } else {
                    if (elapsedDays == 1) {
                        duration = elapsedDays + " day ago";
                    } else if (elapsedDays > 2) {
                        String inputPattern = "yyyy-MM-dd HH:mm:ss";
                        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);

                        Date date = null;
                        try {
                            date = inputFormat.parse(compare_date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
                        String day = (String) DateFormat.format("dd", date); // 20
                        String monthString = (String) DateFormat.format("MMM", date); // Jun
                        String monthNumber = (String) DateFormat.format("MM", date); // 06
                        String year = (String) DateFormat.format("yyyy", date); // 2013
                        String mint = (String) DateFormat.format("mm", date); // 2013
                        String h = (String) DateFormat.format("h", date); // 2013
                        String am = (String) DateFormat.format("a", date); // 2013


                        duration = day + " " + monthString + "," + year + " | " + h + ":" + mint + " " + am;
                    } else {
                        duration = elapsedDays + " days ago";
                    }
                }

                // Log.e( "onResponse: ", elapsedDays+"-" +elapsedHours+"-" +elapsedMinutes+"-"+ elapsedSeconds);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return duration;
    }


    public static int convert_into_sp_unit(Activity activity, int value) {
        Resources r = activity.getResources();
        int sp_value = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP, value
                ,
                r.getDisplayMetrics()
        );
        return sp_value;
    }


    public static void Log_request(String title, HashMap<String, String> params, String server_url) {
        Log.e(title, new JSONObject(params).toString());
        Log.e("server_url", server_url);
    }

    public static void Log_response(JSONObject response) {
        Log.e("response", response.toString());

    }

    public static JSONObject Json_rpc_format(HashMap<String, String> params) {

        HashMap<String, Object> main_param = new HashMap<String, Object>();

        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        Log.e("req", new JSONObject(main_param).toString());
        return new JSONObject(main_param);

    }
    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
    public static JSONObject get_result_from_webservice(Activity activity, JSONObject response) {
        JSONObject result = null;
        if (response.has("error")) {

            try {
                String error = response.getString("error");
                JSONObject jsonObject = new JSONObject(error);

                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(jsonObject.getString("message"))
                        .setContentText(jsonObject.getString("meaning"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (response.has("result")) {
            try {
                result = response.getJSONObject("result");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            error_alert(activity);

        }
        return result;

    }
    public static void error_alert(Activity activity) {
        try {
            if (!activity.isFinishing()) {
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText("Something went wrong. Please try after some time.")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void network_error_alert(final Activity activity) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Network Error")
                .setContentText("Please check your your internet connection.")
                .setConfirmText("Settings")
                .setCancelText("Okay")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }
    static Dialog mDialog;

    public static void showProgressDialog(Activity activity) {
        if(!activity.isFinishing()){
            if (mDialog != null) {
                mDialog.dismiss();
            }
            mDialog = new Dialog(activity);
            mDialog.setCancelable(false);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mDialog.setContentView(R.layout.custom_progress_dialog);
            mDialog.show();
        }

    }

    public static void hideProgressDialog(Activity activity) {
        if (mDialog != null) {
            mDialog.dismiss();
        }

    }
    public static void log_out(final Activity activity) {

        showProgressDialog(activity);

        String server_url = activity.getString(R.string.SERVER_URL) + "logout";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                hideProgressDialog(activity);
                PreferenceManager.getDefaultSharedPreferences(activity).edit().clear().commit();

                Intent intent = new Intent(activity, SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                hideProgressDialog(activity);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(activity, "Authentication Failure.Please login again to use the app", Toast.LENGTH_SHORT).show();
                    PreferenceManager.getDefaultSharedPreferences(activity).edit().clear().commit();

                    Intent intent = new Intent(activity, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activity.startActivity(intent);
                } else {
                    Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);


    }

    public static void set_linear_layout_height(Activity activity, LinearLayout linearLayout, int height) {
        int final_height = convert_into_sp_unit(activity, height);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        layoutParams.height = final_height;
        linearLayout.setLayoutParams(layoutParams);
    }


    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    static MediaPlayer mediaPlayer;

    public static void PlayAudio(final Activity activity,String file){
        if(mediaPlayer != null){
            if(pauses){ //initially, pause is set to false
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
                mediaPlayer.start();
                pauses = false;
                //playing audio when in paused state
            }else{
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                String url = MUSIC_FILE_URL+file;
                mediaPlayer = MediaPlayer.create(activity,Uri.parse(url));
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        StopAudio(activity);
                    }
                });
                mediaPlayer.start();
                //playing audio when in prepared state
            }
        }else{
            String url = MUSIC_FILE_URL+file;
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer = MediaPlayer.create(activity,Uri.parse(url));
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    StopAudio(activity);
                }
            });
            mediaPlayer.start();
        }


    }
    public static void PlayAudio2(final Activity activity, String file, final SeekBar seekBar, final TextView textView1){

        if(mediaPlayer != null){
            if(pauses){ //initially, pause is set to false
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
                mediaPlayer.setLooping(false);
                seekBar.setMax(mediaPlayer.getDuration());
                int dura = mediaPlayer.getDuration();
                long minutes = (dura / 1000)  / 60;
                int seconds = (int)((dura / 1000) % 60);
                if(seconds<10 && minutes<10){
                    textView1.setText("0"+String.valueOf(minutes)+":0"+String.valueOf(seconds));
                }else if(seconds<10){
                    textView1.setText(String.valueOf(minutes)+":0"+String.valueOf(seconds));
                }else {
                    textView1.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));
                }
                mediaPlayer.start();
                Runnable r = new MyRunnable(seekBar);
                new Thread(r).start();
                pauses = false;
                //playing audio when in paused state
            }else{
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                String url = MUSIC_FILE_URL+file;
                mediaPlayer = MediaPlayer.create(activity,Uri.parse(url));
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                        ((MusicDetailsActivity)activity).hideProg();
                        // Do something. For example: playButton.setEnabled(true);
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                StopAudio(activity);
                            }
                        });
                        mp.setLooping(false);
                        seekBar.setMax(mp.getDuration());
                        int dura = mp.getDuration();
                        long minutes = (dura / 1000)  / 60;
                        int seconds = (int)((dura / 1000) % 60);
                        if(seconds<10 && minutes<10){
                            textView1.setText("0"+String.valueOf(minutes)+":0"+String.valueOf(seconds));
                        }else if(seconds<10){
                            textView1.setText(String.valueOf(minutes)+":0"+String.valueOf(seconds));
                        }else {
                            textView1.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));
                        }
                        Runnable r = new MyRunnable(seekBar);
                        new Thread(r).start();
                    }
                });

                //playing audio when in prepared state
            }
        }else{
            mediaPlayer = new MediaPlayer();
            if(mediaPlayer !=null){
                String url = MUSIC_FILE_URL+file;
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer = MediaPlayer.create(activity,Uri.parse(url));
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        ((MusicDetailsActivity)activity).hideProg();
                        mp.start();
                        // Do something. For example: playButton.setEnabled(true);
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                StopAudio(activity);
                            }
                        });
                        mp.setLooping(false);
                        seekBar.setMax(mp.getDuration());
                        int dura = mp.getDuration();
                        long minutes = (dura / 1000)  / 60;
                        int seconds = (int)((dura / 1000) % 60);
                        if(seconds<10 && minutes<10){
                            textView1.setText("0"+String.valueOf(minutes)+":0"+String.valueOf(seconds));
                        }else if(seconds<10){
                            textView1.setText(String.valueOf(minutes)+":0"+String.valueOf(seconds));
                        }else {
                            textView1.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));
                        }
                        Runnable r = new MyRunnable(seekBar);
                        new Thread(r).start();
                    }
                });

            }

        }


    }
    public static class MyRunnable implements Runnable {
        private SeekBar seek;
        public MyRunnable(SeekBar parameter) {
            // store parameter for later user
            this.seek =  parameter;
        }

        public void run() {
            int currentPosition = mediaPlayer.getCurrentPosition();
            int total = mediaPlayer.getDuration();


            while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
                try {
                    Thread.sleep(1000);
                    currentPosition = mediaPlayer.getCurrentPosition();
                } catch (InterruptedException e) {
                    return;
                } catch (Exception e) {
                    return;
                }

                seek.setProgress(currentPosition);

            }
        }
    }

    public static void PauseAudio(Activity activity){
        if(mediaPlayer != null){
            mediaPlayer.pause();
            pauses = true;
        }

    }
    public static void StopAudio(Activity activity){
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            pauses = false;
        }
    }
    public static void StopAudio2(Activity activity){
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
    public static boolean AudioPlaying() {
        if(mediaPlayer != null){
            if(mediaPlayer.isPlaying()){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    public static void seekInitate(SeekBar seekBar, final TextView seekBarHint, final Activity activity,final TextView seekBarHintTotal) {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarHint.setVisibility(View.VISIBLE);
                seekBarHintTotal.setVisibility(View.VISIBLE);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
                seekBarHint.setVisibility(View.VISIBLE);
                long minutes = (progress / 1000)  / 60;
                int seconds = (int)((progress / 1000) % 60);
                if(seconds<10 && minutes<10){
                    seekBarHint.setText("0"+String.valueOf(minutes)+":0"+String.valueOf(seconds));
                }else if(seconds<10){
                    seekBarHint.setText(String.valueOf(minutes)+":0"+String.valueOf(seconds));
                }else {
                    seekBarHint.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));
                }


                double percent = progress / (double) seekBar.getMax();
                int offset = seekBar.getThumbOffset();
                int seekWidth = seekBar.getWidth();
                int val = (int) Math.round(percent * (seekWidth - 2 * offset));
                int labelWidth = seekBarHint.getWidth();
//                seekBarHint.setX(offset + seekBar.getX() + val
//                        - Math.round(percent * offset)
//                        - Math.round(percent * labelWidth / 2));

                if (progress > 0 && mediaPlayer != null && !mediaPlayer.isPlaying() && !pauses) {
                    StopAudio(activity);
                    seekBar.setProgress(0);
                }

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                }
            }
        });
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.newgradient);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant2(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.newgradient3);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
}
