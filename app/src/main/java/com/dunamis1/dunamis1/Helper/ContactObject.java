package com.dunamis1.dunamis1.Helper;

public class ContactObject {
    private String contactName;
    private String contactNo;
    private String contactEmail;
    private boolean selected;

    public String getName() {
        return contactName;
    }
    public void setName(String contactName) {
        this.contactName = contactName;
    }
    public String getEmail() {
        return contactEmail;
    }
    public void setEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
    public String getNumber() {
        return contactNo;
    }
    public void setNumber(String contactNo) {
        this.contactNo = contactNo;
    }
    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
